//
//  ClaimsDetails.swift
//  Talentoz
//
//  Created by forziamac on 27/04/16.
//  Copyright © 2016 Forzia Tech Private Ltd. All rights reserved.
//

import UIKit

class ClaimsDetails: BaseTabBarVC  {
        
        //# MARK: Local Variables
        var objLoadingIndicaor : UIActivityIndicatorView!
        var objCallBack = AjaxCallBack()
        let UserInfo = NSUserDefaults.standardUserDefaults()
        var Activities : Talentoz.JSON!
        var CurrentActionCode : String = "0"
        var ApprovalViewRef: UIView!
        var RowTotalCalculatedHeight : CGFloat = 0.0
        var objActivityIndicaor : UIActivityIndicatorView!
    
        var FilterView : UIView!
        var FilterBGLayer : UIView!
        var isFilterOpen = false
        var MultiAttachment : Talentoz.JSON!
    
        //# MARK: Public Variables
        internal var ClaimDetailsObj = ClaimRequest()
        internal var Mode : Int!
        internal var RowIndex: Int!
        var OnCompletion: ((RowID:Int , ActionCode:String) -> Void )!
        
        //# MARK: Outlets
        
        @IBOutlet var ScrollViewUI: UIScrollView!
        @IBOutlet var MainView: UIView!
        @IBOutlet var imgEmployeePhoto: UIImageView!
        @IBOutlet var lblEmpName: UILabel!
        @IBOutlet var lblStatus: UILabel!
        @IBOutlet var lblPosition: UILabel!
        @IBOutlet var lblWorkLocation: UILabel!
    
        @IBOutlet var lblTotalClaimTitle: UILabel!
        @IBOutlet var lblPurposeTitle: UILabel!
        @IBOutlet var lblPurpose: UILabel!
        @IBOutlet var lblFromDatetitle: UILabel!
        @IBOutlet var lblToDatetitle: UILabel!
        @IBOutlet var lblFromDate: UILabel!
        @IBOutlet var lblToDate: UILabel!
        @IBOutlet var vwTotalClaimAmount: UIView!
        @IBOutlet var lblCurrencySymbol: UILabel!
        @IBOutlet var lblTotalAmount: UILabel!
        @IBOutlet var lblClaimsCount: UILabel!
        @IBOutlet var vwClaimItemView: UIView!
    
    
    
        //# MARK: Default Methods
        override func viewDidLoad() {
            
            
            for subview in   self.view.subviews
            {
                subview.hidden = true
                
            }
            
            super.viewDidLoad()
            // self.ScrollViewUI.contentSize = CGSize(0, 60, width: Viewframe.width, height: 1100)
            // self.ScrollViewUI.frame = CGRectMake(0, 0 ,Viewframe.width, 1100)
            
           
            
            if(Viewframe.width==320)
            {
                self.ScrollViewUI.contentSize = CGSize(  width: Viewframe.width, height: self.RowTotalCalculatedHeight + 380 )
            }
            else
            {
                self.ScrollViewUI.frame = CGRectMake(0, 0 ,Viewframe.width, self.RowTotalCalculatedHeight + 380 )
            }
            self.ScrollViewUI.backgroundColor = UIColor.whiteColor()
            self.view.backgroundColor = UIColor.whiteColor()
            super.UINavigationBarTitle = "Claims Details"
            
        }
        
        override func viewDidAppear(animated: Bool) {
            
            
            super.viewDidAppear(animated)
            
            for subview in   self.view.subviews
            {
                subview.hidden = false
                
            }
           
            PositionControls()
            BindAdjustInfo()
        }
        
        //# MARK: Button Action
        
         func btnApprove_Click(sender: AnyObject) {
            
            self.ProcessLeaveRequest("1")
        }
        
         func btnReject_Click(sender: AnyObject) {
            
            self.ProcessLeaveRequest("2")
        }
        
        
        //# MARK: Design UI Controls
        func PositionControls()
        {
            self.MainView.frame=CGRectMake(0, 0 ,Viewframe.width, 250)
            
            self.imgEmployeePhoto.frame = CGRectMake(self.MainView.frame.size.width / 100 * 5, 80  , 60, 60)
            
            self.lblEmpName.frame = CGRectMake((Viewframe.width  / 100 * 10) + 60, 80  ,(Viewframe.width - 60) / 100 * 70, 20)
            self.lblEmpName.textColor = UIColor(hexString: "#666666")
            self.lblEmpName.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
            
         
            
            self.lblPosition.frame = CGRectMake((Viewframe.width   / 100 * 10) + 60 , 100  ,(Viewframe.width - 50) / 100 * 70, 20)
            self.lblPosition.textColor = UIColor(hexString: "#666666")
            self.lblPosition.font = UIFont(name:"HelveticaNeue", size: 14.0)
            
            self.lblWorkLocation.frame = CGRectMake((Viewframe.width  / 100 * 10) + 60, 120  ,(Viewframe.width - 50) / 100 * 70, 20)
            self.lblWorkLocation.textColor = UIColor(hexString: "#666666")
            self.lblWorkLocation.font = UIFont(name:"HelveticaNeue", size: 14.0)
            
            
            
            self.lblStatus.frame = CGRectMake((Viewframe.width  / 100 * 10) + 60  , 140  ,(Viewframe.width - 50) / 100 * 75, 20)
            self.lblStatus.font =  UIFont(name:"HelveticaNeue-Bold", size: 14.0)
            self.lblStatus.textAlignment = .Right
            
            
            
            self.lblPurposeTitle.frame = CGRectMake(Viewframe.width / 100 * 3, 160  ,Viewframe.width, 20)
            self.lblPurposeTitle.textColor = UIColor(hexString: "#666666")
            self.lblPurposeTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
            
            self.lblPurpose.frame = CGRectMake(Viewframe.width / 100 * 3, 180  ,Viewframe.width / 100 * 94, 40)
            self.lblPurpose.textColor = UIColor(hexString: "#666666")
            self.lblPurpose.font = UIFont(name:"HelveticaNeue", size: 14.0)
            self.lblPurpose.lineBreakMode = .ByWordWrapping
            self.lblPurpose.numberOfLines = 0
            
            self.lblFromDatetitle.frame = CGRectMake(Viewframe.width / 100 * 3, 230  ,Viewframe.width / 100 * 50, 20)
            self.lblFromDatetitle.textColor = UIColor(hexString: "#666666")
            self.lblFromDatetitle.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
            
            
            self.lblToDatetitle.frame = CGRectMake(Viewframe.width / 100 * 50, 230  ,Viewframe.width / 100 * 50, 20)
            self.lblToDatetitle.textColor = UIColor(hexString: "#666666")
            self.lblToDatetitle.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
            
            
            self.lblFromDate.frame = CGRectMake(Viewframe.width / 100 * 3, 260  ,Viewframe.width / 100 * 50, 20)
            self.lblFromDate.textColor = UIColor(hexString: "#666666")
            self.lblFromDate.font = UIFont(name:"HelveticaNeue", size: 14.0)
       
            
            self.lblToDate.frame = CGRectMake(Viewframe.width / 100 * 50, 260  ,Viewframe.width / 100 * 50, 20)
            self.lblToDate.textColor = UIColor(hexString: "#666666")
            self.lblToDate.font = UIFont(name:"HelveticaNeue", size: 14.0)
            
            self.lblTotalClaimTitle.frame = CGRectMake(Viewframe.width / 100 * 3, 310  ,Viewframe.width, 20)
            self.lblTotalClaimTitle.textColor = UIColor(hexString: "#555555")
            self.lblTotalClaimTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
            
            self.vwTotalClaimAmount.frame=CGRectMake(10, 290 ,Viewframe.width / 100 * 95, 40)
            vwTotalClaimAmount.backgroundColor = UIColor(hexString: "#ededed")
            
            
            
            self.lblCurrencySymbol.frame = CGRectMake(Viewframe.width/100 * 32, 10  ,Viewframe.width/100 * 20, 20)
            self.lblCurrencySymbol.textColor = UIColor(hexString: "#666666")
            self.lblCurrencySymbol.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
            self.lblCurrencySymbol.textAlignment = .Right
            
            self.lblTotalAmount.frame = CGRectMake(Viewframe.width/100 * 46, 10  ,Viewframe.width/100 * 30, 20)
            self.lblTotalAmount.textColor = UIColor(hexString: "#666666")
            self.lblTotalAmount.font = UIFont(name:"HelveticaNeue-Bold", size: 20.0)
            self.lblTotalAmount.textAlignment = .Left
            
            self.lblClaimsCount.frame = CGRectMake(Viewframe.width / 100 * 3, 340  ,Viewframe.width, 20)
            self.lblClaimsCount.textColor = UIColor(hexString: "#666666")
            self.lblClaimsCount.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
            
            
            self.vwClaimItemView.frame = CGRectMake(0, 370 ,Viewframe.width, self.RowTotalCalculatedHeight)
        
          
        }
        
        override func goBack() {
            if self.Mode != 0
            {
                if(self.RowIndex != nil)
                {
                    self.OnCompletion(RowID: self.RowIndex,ActionCode: self.CurrentActionCode )
                }
                
            }
            dispatch_async(dispatch_get_main_queue(),{
                self.dismissViewControllerAnimated(true, completion: nil)
            })
        }
        
        //Bind Adjustment Details
        func BindAdjustInfo()
        {
            
            if let tmpDomainURL = self.UserInfo.stringForKey("DomainURL")
            {
                
                if  (self.ClaimDetailsObj.EmployeePhoto == nil )
                {
                    
                    let img_EmpPic:  UIButton = UIButton(frame: CGRectMake(self.MainView.frame.size.width / 100 * 5, 80  , 60, 60))
                    img_EmpPic.layer.masksToBounds = false
                    //img_EmpPic.layer.borderWidth = 1
                    
                    img_EmpPic.backgroundColor = UIColor(hexString: "#f5f5f5")
                    img_EmpPic.clipsToBounds = true
                    img_EmpPic.layer.borderWidth = 1
                    img_EmpPic.layer.borderColor = UIColor(hexString: "#cccccc")?.CGColor
                    // img_EmpPic.titleLabel!.font = UIFont(name: "helvetica-bold", size: 25)
                    
                    img_EmpPic.setTitleColor(UIColor(hexString: "#666666"), forState: UIControlState.Normal)
                    img_EmpPic.setFAIcon(FAType.FAUser, iconSize: 35, forState: .Normal)
                    self.MainView.addSubview(img_EmpPic)
                    
                }
                else
                {
                    let employeePic = "http://" + tmpDomainURL + "/Uploadedfiles/Client" + String(self.UserInfo.stringForKey("ClientID")!) + "/Image/" + String(self.ClaimDetailsObj.EmployeePhoto)
                    imgEmployeePhoto.setImageWithUrl(NSURL(string: employeePic)!)
                }
                
            }
            
            lblEmpName.text = String(self.ClaimDetailsObj.EmployeeName)
            lblPosition.text = String(self.ClaimDetailsObj.PositionName)
            lblWorkLocation.text = String(self.ClaimDetailsObj.WorkLocationName)
            lblPurpose.text = String(self.ClaimDetailsObj.ProgramTitle)
            lblFromDate.setFAText(prefixText: "", icon: FAType.FACalendar, postfixText: "  " + String(self.ClaimDetailsObj.StartDateString), size: 14, iconSize: 14)
            
             lblToDate.setFAText(prefixText: "", icon: FAType.FACalendar, postfixText: "  " + String(self.ClaimDetailsObj.EndDateString), size: 14, iconSize: 14)
          
            lblCurrencySymbol.text = String(self.ClaimDetailsObj.CurrencyShortName)
            lblTotalAmount.text =  ConvertValueWithDecimal(String(self.ClaimDetailsObj.TotalAmount),NoofDecimal: 2)
            
            let CurrencySymbolWidth = CalculateWidthPercentage(String(lblTotalAmount.text!), pHoleWidth: self.vwTotalClaimAmount.frame.width  , Type: 0, Gap: 8.0)
            
            self.lblCurrencySymbol.frame = CGRectMake(0, 10  , CurrencySymbolWidth , 20)
            
            let CurrencyWidth = CalculateWidthPercentage(String(lblTotalAmount.text!), pHoleWidth: self.vwTotalClaimAmount.frame.width  , Type: 1, Gap: 8.0)
            
            self.lblTotalAmount.frame = CGRectMake(CurrencySymbolWidth + 8.0 , 10  , CurrencyWidth, 20)
            
            
            self.lblStatus.textAlignment = .Right
            if (self.ClaimDetailsObj.Status == 1)
            {
                self.lblStatus.textColor = UIColor(hexString:"#ffbb5c")
                
                if(self.Mode == 1 )
                {
                    
                    if self.ClaimDetailsObj.Access == 1 {
                        if self.ClaimDetailsObj.Type == 2 {
                            let ApprovalView : UIView = UIView(frame : CGRectMake(0, self.view.frame.height - 85  , self.view.frame.size.width , 30))
                            self.view.addSubview(ApprovalView)
                            self.ApprovalViewRef = ApprovalView
                            
                            let btnApprove : UIButton = UIButton(frame : CGRectMake((ApprovalView.frame.size.width / 100 * 5 ) , 0 , ApprovalView.frame.size.width / 100 * 40, 30))
                            btnApprove.setTitle("Approve", forState: .Normal)
                            btnApprove.setFAText(prefixText: "", icon: FAType.FACheck, postfixText: "   Approve", size: CGFloat(14), forState: .Normal)
                            btnApprove.backgroundColor = UIColor(hexString: "#48C9B0")
                            btnApprove.layer.cornerRadius=5
                            btnApprove.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                            btnApprove.addTarget(self, action: "btnApprove_Click:", forControlEvents: .TouchUpInside)
                            ApprovalView.addSubview(btnApprove)
                            
                            let btnReject : UIButton = UIButton(frame : CGRectMake((ApprovalView.frame.size.width / 100 * 55 ) , 0 , ApprovalView.frame.size.width / 100 * 40, 30))
                            btnReject.setTitle("Reject", forState: .Normal)
                            btnReject.setFAText(prefixText: "", icon: FAType.FAClose, postfixText: "   Reject", size: CGFloat(14), forState: .Normal)
                            btnReject.backgroundColor = UIColor(hexString: "#F46868")
                            btnReject.layer.cornerRadius=5
                            btnReject.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                            btnReject.addTarget(self, action: "btnReject_Click:", forControlEvents: .TouchUpInside)
                            ApprovalView.addSubview(btnReject)
                        }
                    
                    }                  
                }
                
            } else if (self.ClaimDetailsObj.Status == 2) {
               // self.btnApprove.hidden = true
               // self.btnReject.hidden = true
                self.lblStatus.textColor = UIColor(hexString: "#48c9b0")
            }
            else {
                //self.btnApprove.hidden = true
                //self.btnReject.hidden = true
                self.lblStatus.textColor = UIColor(hexString: "#ef6565")
            }
            
            
           
            self.lblStatus.text = String(self.ClaimDetailsObj.StatusText)
            
            
            BindAdjustmentList()
        }
        
        
        
        //Load and Bind Adjustment Timings
        func BindAdjustmentList()
        {
            objLoadingIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, Viewframe.width, Viewframe.height))
            objLoadingIndicaor.activityIndicatorViewStyle = .WhiteLarge
            let objPosition =  self.view.center
            objLoadingIndicaor.center = objPosition
            objLoadingIndicaor.hidesWhenStopped = true
            objLoadingIndicaor.color = UIColor.grayColor()
            self.view.addSubview(objLoadingIndicaor)
            self.objLoadingIndicaor.startAnimating()
            
            objCallBack = AjaxCallBack()
            objCallBack.MethodName = "GetClaimTypeRequestItems"
            
            if let UserInfo = UserInfo.stringForKey("UserInfo")
            {
                let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
                
                var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
                objCallBack.ParameterList.append(objparam)
                
                objparam = Parameter(Name: "pRequestID",value: String(self.ClaimDetailsObj.RequestID))
                objCallBack.ParameterList.append(objparam)
                
                
                objCallBack.post(BindAdjustmentListOnComplete, OnError:BindAdjustmentListOnError)
                
            }
            
        }
        
        func BindAdjustmentListOnError(Response: NSError ){
            
        }
        
        
        func BindAdjustmentListOnComplete (ResultString :NSString? ) {
            dispatch_async(dispatch_get_main_queue(),{
                self.objLoadingIndicaor.stopAnimating()
                self.Activities = JSON(data: self.objCallBack.GetJSONData(ResultString)!)
                
                if self.Activities["ClaimRequestItems"].count > 0
                {
                   self.lblClaimsCount.text = "Claims(" + String(self.Activities["ClaimRequestItems"].count) + ")"
                }
                else
                {
                    self.lblClaimsCount.text = "Claims(0)"
                }
                
                 self.RowTotalCalculatedHeight = CGFloat(self.Activities["ClaimRequestItems"].count) * 120.0
                
                if(self.Viewframe.width==320)
                {
                    self.ScrollViewUI.contentSize = CGSize(  width: self.Viewframe.width, height: self.RowTotalCalculatedHeight + 550)
                }
                else
                {
                    self.ScrollViewUI.frame = CGRectMake(0, 0 ,self.Viewframe.width, self.RowTotalCalculatedHeight + 550 )
                }
                
                let ActivityList : UIView = UIView(frame : CGRectMake(self.view.frame.size.width  / 100 * 3, 0 ,self.vwClaimItemView.frame.size.width / 100 * 94, self.RowTotalCalculatedHeight))
                ActivityList.backgroundColor = UIColor.clearColor()
                self.vwClaimItemView.addSubview(ActivityList)
                
                self.BindClaimsItemList(ActivityList)
                
            })
        }
        
        
        
        //# MARK: Approval
        func ProcessLeaveRequest(ActionCode: String  )
        {
            
            self.CurrentActionCode = ActionCode
            objLoadingIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, Viewframe.width, Viewframe.height))
            
            objLoadingIndicaor.activityIndicatorViewStyle = .WhiteLarge
            let objPosition =  self.view.center
            objLoadingIndicaor.center = objPosition
            objLoadingIndicaor.hidesWhenStopped = true
            objLoadingIndicaor.color = UIColor.grayColor()
            self.view.addSubview(objLoadingIndicaor)
            objLoadingIndicaor.startAnimating()
            
            if let UserInfo = UserInfo.stringForKey("UserInfo")
            {
                objCallBack = AjaxCallBack()
                let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
                
                objCallBack.MethodName = "ProcessAttendanceNote"
                
                
                var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
                objCallBack.ParameterList.append(objparam)
                
                
                objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
                objCallBack.ParameterList.append(objparam)
                
                objparam = Parameter(Name: "pRequestID",value: String(self.ClaimDetailsObj.RequestID))
                objCallBack.ParameterList.append(objparam)
                
                objparam = Parameter(Name: "pAction",value: ActionCode)
                objCallBack.ParameterList.append(objparam)
                
                objCallBack.post(ProcessLeaveRequestOnComplete, OnError: ProcessLeaveRequestOnError)
                
            }
            
        }
        
        
        func ProcessLeaveRequestOnError(Response: NSError ){
            
        }
        
        func ProcessLeaveRequestOnComplete (ResultString :NSString? ) {
            dispatch_async(dispatch_get_main_queue(),{
                
                dispatch_async(dispatch_get_main_queue(),{
                    self.objLoadingIndicaor.stopAnimating()
                    if(self.CurrentActionCode == "1")
                    {
                        self.lblStatus.textColor = UIColor(hexString: "#e67f22")
                        self.lblStatus.text = String("Approved")
                    }
                    else
                    {
                        self.lblStatus.textColor = UIColor(hexString: "#ff8877")
                        self.lblStatus.text = String("Rejected")
                    }
                     self.ApprovalViewRef.hidden = true
                    
                    DoneHUD.showInView(self.view, message: "Done")
                })
                
            })
            
            
        }
        
        //# MARK: Table List Bind Method
    
        
    func BindClaimsItemList(pView:UIView)
     {
        var IncHeight : CGFloat = 0
    
            for i in 0 ..< self.Activities["ClaimRequestItems"].count  {
    
    
    
    let ContentVw: UIView = UIView(frame: CGRectMake(0, IncHeight, pView.frame.size.width, 115))
    pView.addSubview(ContentVw)
    
    ContentVw.backgroundColor = UIColor(hexString: "#f5f5f5")
    ContentVw.layer.borderColor = UIColor(hexString: "#f5f5f5")?.CGColor
    ContentVw.layer.cornerRadius = 5
    ContentVw.layer.borderWidth = 1
    
    let lblClaimType : UILabel = UILabel(frame: CGRectMake(10  , 5, Viewframe.width / 100 * 95,20))
    lblClaimType.textColor = UIColor(hexString: "#666666")
    lblClaimType.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
    lblClaimType.text = self.Activities["ClaimRequestItems"][i]["ClaimTypeName"].string
    ContentVw.addSubview(lblClaimType)
    
    if( self.Activities["ClaimRequestItems"][i]["ProjectID"].int > 0 )
    {
    let lblProjectName : UILabel = UILabel(frame: CGRectMake(10  , 25, Viewframe.width / 100 * 95  ,20))
    lblProjectName.textColor = UIColor(hexString: "#666666")
    lblProjectName.font = UIFont(name:"HelveticaNeue", size: 14.0)
    lblProjectName.setFAText(prefixText: "", icon: FAType.FASuitcase, postfixText: "  " + self.Activities["ClaimRequestItems"][i]["ProjectName"].string!, size: 14.0, iconSize: 14.0)
    ContentVw.addSubview(lblProjectName)
    
    let lblClaimDescription : UILabel = UILabel(frame: CGRectMake(10  , 45, Viewframe.width / 100 * 95 ,20))
    lblClaimDescription.textColor = UIColor(hexString: "#666666")
    lblClaimDescription.font = UIFont(name:"HelveticaNeue", size: 14.0)
    lblClaimDescription.text = self.Activities["ClaimRequestItems"][i]["Description"].string
    ContentVw.addSubview(lblClaimDescription)
    }
    else
    {
    let lblClaimDescription : UILabel = UILabel(frame: CGRectMake(10  , 25, Viewframe.width / 100 * 95 , 40))
    lblClaimDescription.textColor = UIColor(hexString: "#666666")
    lblClaimDescription.font = UIFont(name:"HelveticaNeue", size: 14.0)
    lblClaimDescription.text = self.Activities["ClaimRequestItems"][i]["Description"].string
    ContentVw.addSubview(lblClaimDescription)
    }
    
    if (self.Activities["ClaimRequestItems"][i]["ClaimRequestItemsAttachments"].count > 0 )
    {
    let lblClaimAttachFile : UILabel = UILabel(frame: CGRectMake(10 , 65, Viewframe.width / 100 * 10  ,20))
    lblClaimAttachFile.textColor = UIColor(hexString: "#666666")
    lblClaimAttachFile.font = UIFont(name:"HelveticaNeue", size: 14.0)
    lblClaimAttachFile.setFAIcon(FAType.FAPaperclip, iconSize: 14.0)
    ContentVw.addSubview(lblClaimAttachFile)
    
    let btnDownload : UIButton = UIButton(frame: CGRectMake(Viewframe.width / 100 * 10   , 65, pView.frame.size.width / 100 * 90  ,20))
    btnDownload.setTitle(String(self.Activities["ClaimRequestItems"][i]["ClaimRequestItemsAttachments"].count) + " Attachment(s)", forState: .Normal)
    btnDownload.tag = i
    btnDownload.setTitleColor(UIColor(hexString: "#2190ea"), forState: .Normal)
    btnDownload.titleLabel!.font =  UIFont(name:"HelveticaNeue", size: 14.0)
    btnDownload.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
    btnDownload.contentVerticalAlignment = UIControlContentVerticalAlignment.Center
    btnDownload.addTarget(self, action: "DownLoadModal:", forControlEvents: .TouchUpInside)
    ContentVw.addSubview(btnDownload)
        
        
        
    }
    
    let vwline3 : UIView = UIView(frame: CGRectMake(10 ,  87 , Viewframe.width / 100 * 87,1))
    vwline3.layer.borderColor = UIColor(hexString: "#cccccc")?.CGColor
    vwline3.layer.borderWidth = 1
    ContentVw.addSubview(vwline3)
    
    
    let lblClaimItemDate : UILabel = UILabel(frame: CGRectMake(10  , 92, Viewframe.width / 100 * 40  ,20))
    lblClaimItemDate.textColor = UIColor(hexString: "#666666")
    lblClaimItemDate.font = UIFont(name:"HelveticaNeue", size: 14.0)
    
    ContentVw.addSubview(lblClaimItemDate)
    lblClaimItemDate.setFAText(prefixText: "", icon: FAType.FACalendar, postfixText: "  " + self.Activities["ClaimRequestItems"][i]["ClaimDateString"].string!, size: 14.0, iconSize: 14.0)
    
    
    let lblClaimCurrSymbol : UILabel = UILabel(frame: CGRectMake( ContentVw.frame.width / 100 * 60 ,  93, ContentVw.frame.width / 100 * 13,18))
    lblClaimCurrSymbol.textColor = UIColor(hexString: "#2190ea")
    lblClaimCurrSymbol.textAlignment = .Right
    lblClaimCurrSymbol.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
    lblClaimCurrSymbol.text = self.ClaimDetailsObj.CurrencyShortName
    ContentVw.addSubview(lblClaimCurrSymbol)
    
    
    let lblClaimItemAmount : UILabel = UILabel(frame: CGRectMake(ContentVw.frame.width / 100 * 75 , 92, ContentVw.frame.width / 100 * 28,20))
    lblClaimItemAmount.textColor = UIColor(hexString: "#2190ea")
    lblClaimItemAmount.textAlignment = .Left
    lblClaimItemAmount.font = UIFont(name:"HelveticaNeue-Bold", size: 16.0)
    lblClaimItemAmount.text = ConvertValueWithDecimal( String(self.Activities["ClaimRequestItems"][i]["Amount"]),NoofDecimal: 2)
    ContentVw.addSubview(lblClaimItemAmount)
                
    IncHeight = IncHeight + 120

    
    }
    
    
    }
    
    // Multiple Attachment Download
    
    func DownLoadModal(sender:UIButton) {
        
        self.MultiAttachment = self.Activities["ClaimRequestItems"][sender.tag]["ClaimItemsAttachments"]
        if( self.MultiAttachment.count > 1 )
        {
            FilterBGLayer = UIView(frame: CGRectMake(0, 0, Viewframe.width  ,  Viewframe.height))
            FilterBGLayer.backgroundColor = UIColor(colorLiteralRed: 0 , green: 0, blue: 0, alpha: 0.6)
            let GestureLeaveDetail1 = UITapGestureRecognizer(target: self, action: #selector(ClaimsDetails.DismissFilter(_:)))
            FilterBGLayer.addGestureRecognizer(GestureLeaveDetail1)
            FilterView = UIView(frame: CGRectMake(FilterBGLayer.frame.width / 100 * 10 , FilterBGLayer.frame.height  / 100 * 25 , FilterBGLayer.frame.width  / 100 * 80, FilterBGLayer.frame.height  / 100 * 60))
            FilterView.backgroundColor = UIColor.whiteColor()
            FilterView.layer.cornerRadius = 8
            
            let AttachScroll : UIScrollView = UIScrollView(frame: CGRectMake(0,  10, FilterView.frame.width, 300))
            FilterView.addSubview(AttachScroll )
            
            AttachScroll.contentSize = CGSize(  width: FilterView.frame.width, height: 350)
            
            
            let AttachmentListView : UIView = UIView(frame: CGRectMake(AttachScroll.frame.width / 100 * 2, 5 , AttachScroll.frame.width / 100 * 96 , CGFloat(self.MultiAttachment.count * 33) ))
            AttachScroll.addSubview(AttachmentListView )
            
            var HeightCal = CGFloat(5)
            
            for i in 0..<self.MultiAttachment.count {
                
                let AttachmentListViewInn : UIView = UIView(frame: CGRectMake(AttachScroll.frame.width / 100 * 2,  HeightCal, AttachScroll.frame.width / 100 * 96 , 30 ))
                AttachmentListView.addSubview(AttachmentListViewInn )
                
                let lbl_Approval : UILabel = UILabel(frame: CGRectMake(AttachmentListView.frame.size.width / 100 * 2 , 2 , AttachmentListView.frame.size.width / 100 * 60 ,18))
                lbl_Approval.backgroundColor = UIColor.clearColor()
                lbl_Approval.textAlignment = .Left
                lbl_Approval.font = UIFont(name:"HelveticaNeue-Bold", size: 16.0)
                lbl_Approval.textColor = UIColor(hexString: "#333333")
                lbl_Approval.text = "Attached File" + String((i+1))
                AttachmentListViewInn.addSubview(lbl_Approval)
                
                let btn_ShowDetail : UIButton = UIButton(frame: CGRectMake(AttachmentListView.frame.size.width / 100 * 60 , 2, AttachmentListView.frame.size.width / 100 * 30, 18))
                btn_ShowDetail.backgroundColor = UIColor.blueColor()
                btn_ShowDetail.setTitleColor(UIColor(hexString: "#2190ea"), forState: UIControlState.Normal)
                btn_ShowDetail.setTitle("Download" , forState: .Normal)
                btn_ShowDetail.titleLabel?.font = UIFont(name: "HelveticaNeue-bold", size: 12)
                btn_ShowDetail.tag = i
                btn_ShowDetail.addTarget(self, action: #selector(ClaimsDetails.DownloadFromList(_:)), forControlEvents: .TouchUpInside)
                AttachmentListViewInn.addSubview(btn_ShowDetail)
                
                
                HeightCal = HeightCal + 32
            }
            
            
            
            
            
            
            
            let btn_ApplyFilter : UIButton = UIButton(frame: CGRectMake(FilterBGLayer.frame.width / 100 * 24, FilterView.frame.size.height - 60 , 120 ,24))
            btn_ApplyFilter.setTitle("Dismiss", forState: .Normal)
            btn_ApplyFilter.layer.cornerRadius = 5
            btn_ApplyFilter.tag = sender.tag
            btn_ApplyFilter.backgroundColor = UIColor(hexString: "#2190ea")
            btn_ApplyFilter.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            btn_ApplyFilter.titleLabel!.font =  UIFont(name:"helvetica-Bold", size: 12)
            btn_ApplyFilter.addTarget(self, action:#selector(ClaimsDetails.DismissFilter(_:)), forControlEvents: .TouchUpInside)
            FilterView.addSubview(btn_ApplyFilter)
            
            
            FilterBGLayer.addSubview(FilterView)
            self.view.addSubview(FilterBGLayer)
        }
        else
        {
            
            let FileName = String(self.Activities["ClaimRequestItems"][sender.tag]["ClaimItemsAttachments"][0]["EncodeFileName"])
            self.DownLoadFile(FileName)
            
            
        }
        
    }
    
    func DismissFilter(sender: UIButton)
    {
        isFilterOpen = false
        FilterBGLayer.removeFromSuperview()
        
    }
    
    func DownloadFromList(sender:UIButton)
    {
        let FileName = String(self.MultiAttachment[sender.tag]["EncodeFileName"])
        self.DownLoadFile(FileName)
    }
    
    func DownLoadFile(FilePathURL:String)
    {
        
        if let tmpDomainURL = self.UserInfo.stringForKey("DomainURL")
        {
            objActivityIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, Viewframe.width, self.view.frame.size.height))
            objActivityIndicaor.activityIndicatorViewStyle = .WhiteLarge
            objActivityIndicaor.center = self.view.center
            objActivityIndicaor.transform = CGAffineTransformMakeScale(1.5, 1.5);
            objActivityIndicaor.hidesWhenStopped = true
            objActivityIndicaor.color = UIColor.grayColor()
            self.view.addSubview(objActivityIndicaor)
            objActivityIndicaor.startAnimating()
            
            let URLStr = "http://" + tmpDomainURL + "/WorkFlowAttachment/Client" + String(self.UserInfo.stringForKey("ClientID")!) + "/" + FilePathURL
            
            var image: UIImage?
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                image =  UIImage(data: NSData(contentsOfURL: NSURL(string:URLStr)!)!)
                dispatch_async(dispatch_get_main_queue()) {
                    
                    UIImageWriteToSavedPhotosAlbum(image! , self,nil, nil)
                    self.objActivityIndicaor.stopAnimating()
                    
                    let alert: UIAlertView = UIAlertView(title: "", message: "Download completed.", delegate: nil, cancelButtonTitle: nil);
                    alert.show()
                    
                    // Delay the dismissal by 5 seconds
                    let delay = 1.0 * Double(NSEC_PER_SEC)
                    let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
                    dispatch_after(time, dispatch_get_main_queue(), {
                        alert.dismissWithClickedButtonIndex(-1, animated: true)
                    })
                }
            }
        }
        
    }
    
    
    
    
}