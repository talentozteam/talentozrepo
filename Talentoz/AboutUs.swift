//
//  AboutUs.swift
//  Talentoz
//
//  Created by forziamac on 04/01/16.
//  Copyright © 2016 Forzia Tech Private Ltd. All rights reserved.
//

import UIKit

class AboutUs: BaseTabBarVC {
    
    
    @IBOutlet var TalentozIcon: UIImageView!

    @IBOutlet var lblVer: UILabel!
    
    @IBOutlet var lblCpyRights: UILabel!
    
    @IBOutlet var lblFollow: UILabel!
    
 
    @IBOutlet var contactus: UIButton!
    
    @IBOutlet var btnlinkedin: UIButton!
    @IBOutlet var btntwitter: UIButton!
    @IBOutlet var btnfacebook: UIButton!
    override func viewDidLoad() {
        
         let Viewframe = UIScreen.mainScreen().bounds
        
        super.UINavigationBarTitle = "About Us"
        
        self.TalentozIcon.frame = CGRectMake(0 , 100  ,Viewframe.width , 45)
        
        self.lblVer.frame = CGRectMake(0, 200 ,Viewframe.width , 30)
        
        self.lblVer.textAlignment = .Center
        
        self.lblCpyRights.frame = CGRectMake(0, 230 ,Viewframe.width , 30)
        
         self.lblCpyRights.textAlignment = .Center
        
        self.lblFollow.frame = CGRectMake(0, 260 ,Viewframe.width , 30)
        
        self.lblFollow.textAlignment = .Center
        
        
        self.btnfacebook.frame = CGRectMake(Viewframe.width / 100 * 17.5, 320 , Viewframe.width / 100 * 15 , 30)
        self.btnfacebook.backgroundColor = UIColor(hexString: "#3b5998")
        self.btnfacebook.setTitleColor(UIColor.whiteColor(), forState: .Normal)
       
        
        
          self.btntwitter.frame = CGRectMake(Viewframe.width / 100 * 42.5, 320 ,Viewframe.width / 100 * 15 , 30)
        
        self.btntwitter.backgroundColor = UIColor(hexString: "#00aced")
       
        self.btntwitter.setTitleColor(UIColor.whiteColor(), forState: .Normal)
       
        
        self.btnlinkedin.frame = CGRectMake(Viewframe.width / 100 * 67.5, 320 ,Viewframe.width / 100 * 15 , 30)
        
        self.btnlinkedin.backgroundColor = UIColor(hexString: "#007bb6")
        self.btnlinkedin.setTitleColor(UIColor.whiteColor(), forState: .Normal)
      
        
        self.btnfacebook.setFAIcon(FAType.FAFacebook, iconSize: 16, forState: .Normal)
        self.btntwitter.setFAIcon(FAType.FATwitter, iconSize: 16, forState: .Normal)
        self.btnlinkedin.setFAIcon(FAType.FALinkedin, iconSize: 16, forState: .Normal)
      
        self.btnfacebook.addTarget(self, action: "GotoFaceBook:", forControlEvents: .TouchUpInside)
         self.btntwitter.addTarget(self, action: "GotoTwitter:", forControlEvents: .TouchUpInside)
         self.btnlinkedin.addTarget(self, action: "GotoLinkedin:", forControlEvents: .TouchUpInside)
        
        self.contactus.frame = CGRectMake(0, 380 ,Viewframe.width , 30)
        
      
         self.contactus.setTitleColor(UIColor(colorLiteralRed: 28/255, green: 138/255, blue: 255/255, alpha: 1), forState: UIControlState.Normal)
        
        
        
             self.contactus.addTarget(self, action: "GotoContactUs:", forControlEvents: .TouchUpInside)
              
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func goBack() {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("HomeInitialView")
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    func GotoFaceBook(sender: UITapGestureRecognizer)
    {
        UIApplication.sharedApplication().openURL(NSURL(string: "https://www.facebook.com/TalentOz-443328422502499/")!)
    }
    
    func GotoTwitter(sender: UITapGestureRecognizer)
    {
        UIApplication.sharedApplication().openURL(NSURL(string: "https://twitter.com/TalentOZCloud")!)
    }
    
    func GotoLinkedin(sender: UITapGestureRecognizer)
    {
        UIApplication.sharedApplication().openURL(NSURL(string: "https://www.linkedin.com/company/talentoz?trk=top_nav_home")!)
    }
    
    func GotoContactUs(sender: UITapGestureRecognizer)
    {
        UIApplication.sharedApplication().openURL(NSURL(string: "http://www.talentoz.com/contact-us/")!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
