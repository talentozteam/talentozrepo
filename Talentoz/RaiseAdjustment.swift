//
//  RaiseAdjustment.swift
//  Talentoz
//
//  Created by forziamac on 08/02/16.
//  Copyright © 2016 Forzia Tech Private Ltd. All rights reserved.
//

import UIKit

class RaiseAdjustment: BaseTabBarVC , UITableViewDelegate , UITableViewDataSource {
    var objLoadingIndicaor : UIActivityIndicatorView!
    internal var AdjustmentDate: String!
    var objCallBack1 = AjaxCallBack()
    let defaults = NSUserDefaults.standardUserDefaults()
    var AdjustmentJSON: Talentoz.JSON!
    let vscrollView = UIScrollView(frame: UIScreen.mainScreen().bounds)
    internal var FilterView : UIView!
    internal var FilterBGLayer : UIView!
    var isFilterOpen = false
    var ActivityListView = UITableView()
    var SelectedTimeBtn: UIButton!
    var TotalHours: UILabel!
    var TotalHoursInSecond: Int! = 0
    var SelectedDate: String!
    var arrSelfActivityList = [ActivitiesList]()
    var EnterActivityType : Int!
    var Reason : UITextField!
    internal var Mode : Int = 0
    
    override func viewDidLoad() {
        super.UINavigationBarTitle = "Raise Adjustment"
        self.view.addSubview(vscrollView)
        
        self.vscrollView.contentSize = CGSize(width: self.view.frame.width, height: 700)
        self.vscrollView.backgroundColor = UIColor.whiteColor()
        super.viewDidLoad()
        LoadAdjustmentInfo()
    }
   
 
    func LoadAdjustmentInfo()
    {
        
        
        if self.Mode == 1
        {
            objLoadingIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, Viewframe.width, Viewframe.height))
            
            objLoadingIndicaor.activityIndicatorViewStyle = .WhiteLarge
            let objPosition =  self.view.center
            objLoadingIndicaor.center = objPosition
            objLoadingIndicaor.hidesWhenStopped = true
            objLoadingIndicaor.color = UIColor.grayColor()
            self.view.addSubview(objLoadingIndicaor)
            objLoadingIndicaor.startAnimating()
            
            objCallBack1 = AjaxCallBack()
            objCallBack1.MethodName = "GetBiometricDataonDate"
            
            if let UserInfo = defaults.stringForKey("UserInfo")
            {
                let ResultJSON = objCallBack1.DeserializeJSONString(UserInfo)
                
                var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
                objCallBack1.ParameterList.append(objparam)
                
                objparam = Parameter(Name: "pMode",value: "0")
                objCallBack1.ParameterList.append(objparam)
                
                objparam = Parameter(Name: "pDate",value: String(self.AdjustmentDate))
                objCallBack1.ParameterList.append(objparam)
                
                objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
                objCallBack1.ParameterList.append(objparam)
                
                
                objCallBack1.post(LoadAdjustmentInfoOnComplete, OnError: LoadAdjustmentInfoOnErrorOnError)
                
            }
        }
        else
        {
             self.DisplayControls()
        }
        
      
    }
    
    //Response on error callback
    func LoadAdjustmentInfoOnErrorOnError(Response: NSError ){
        
    }
    
    func LoadAdjustmentInfoOnComplete (ResultString :NSString? ) {
        
        dispatch_async(dispatch_get_main_queue(),{
            self.objLoadingIndicaor.stopAnimating()
            self.AdjustmentJSON = JSON(data: self.objCallBack1.GetJSONData(ResultString)!)
            self.arrSelfActivityList = self.GetAdjRequestArray(self.AdjustmentJSON)
            self.ActivityListView.removeFromSuperview()
            self.DisplayControls()
            
            
        })
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        animateViewMoving(true, moveValue: 100)
    }
    func textFieldDidEndEditing(textField: UITextField) {
        animateViewMoving(false, moveValue: 100)
    }
    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:NSTimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = CGRectOffset(self.view.frame, 0,  movement)
        UIView.commitAnimations()
    }
    
    func GetAdjRequestArray(ACtivitiesJSON: Talentoz.JSON!) -> [ActivitiesList]
    {
        
        var arrTempActivityList = [ActivitiesList]()
        for var i = 0; i < ACtivitiesJSON.count ; ++i {
            
            let madj = ActivitiesList()
            madj.TotalTime = String(ACtivitiesJSON[i]["TotalTime"].string)
            madj.OutTime  = ACtivitiesJSON[i]["OutTime"].string
            madj.RwNo  = ACtivitiesJSON[i]["RwNo"].string
            madj.InTime  = ACtivitiesJSON[i]["InTime"].string
            madj.ShirtOutTime  = ACtivitiesJSON[i]["ShirtOutTime"].string
            madj.ShirtInTime  = ACtivitiesJSON[i]["ShirtInTime"].string
            madj.Sum_of_TotalTime  = ACtivitiesJSON[i]["Sum_of_TotalTime"].string
            madj.IsInTime  = ACtivitiesJSON[i]["IsInTime"].int
            madj.RowID  = ACtivitiesJSON[i]["RowID"].int
            madj.IsOutTime  = ACtivitiesJSON[i]["IsOutTime"].int
            madj.IsNewlyCreated = false
            
            arrTempActivityList.append(madj)
            
        }
        
        return arrTempActivityList
    }
    
    func DisplayControls()
    {
        
        let lbl_DateTitle : UILabel = UILabel(frame: CGRectMake(self.vscrollView.frame.size.width / 100 * 5  , 80, self.vscrollView.frame.size.width / 100 * 10  ,24))
        lbl_DateTitle.backgroundColor = UIColor.clearColor()
        lbl_DateTitle.textAlignment = .Center
        lbl_DateTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        lbl_DateTitle.textColor = UIColor(hexString: "#666666")
        lbl_DateTitle.text = "Date"
        self.vscrollView.addSubview(lbl_DateTitle)
        
        
        let btn_Date : UIButton = UIButton(frame: CGRectMake(self.vscrollView.frame.size.width / 100 * 15  , 80, self.vscrollView.frame.size.width / 100 * 25 ,24))
        btn_Date.backgroundColor = UIColor.clearColor()
        
        
        
        btn_Date.setTitleColor(UIColor(hexString: "#2190ea"), forState: .Normal)
        btn_Date.titleLabel!.font =  UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        btn_Date.setTitle(self.AdjustmentDate, forState: .Normal)
        btn_Date.addTarget(self, action: "ShowDatePicker:", forControlEvents: .TouchUpInside)
        btn_Date.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Center
        self.vscrollView.addSubview(btn_Date)
        
        
        
        let lbl_InTimeTitle : UILabel = UILabel(frame: CGRectMake(self.vscrollView.frame.size.width / 100 * 5  , 110, self.vscrollView.frame.size.width / 100 * 30 ,20))
        lbl_InTimeTitle.backgroundColor = UIColor(hexString: "#2190ea")
        lbl_InTimeTitle.textAlignment = .Center
        lbl_InTimeTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        lbl_InTimeTitle.textColor = UIColor.whiteColor()
        lbl_InTimeTitle.text = "In Time"
        self.vscrollView.addSubview(lbl_InTimeTitle)
        
        let lbl_OutTimeTitle : UILabel = UILabel(frame: CGRectMake(self.vscrollView.frame.size.width / 100 * 35, 110, self.vscrollView.frame.size.width / 100 * 30 ,20))
        lbl_OutTimeTitle.backgroundColor = UIColor(hexString: "#2190ea")
        lbl_OutTimeTitle.textAlignment = .Center
        lbl_OutTimeTitle.textColor = UIColor.whiteColor()
        lbl_OutTimeTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        lbl_OutTimeTitle.text = "Out Time"
        self.vscrollView.addSubview(lbl_OutTimeTitle)
        
        
        let lbl_HoursTitle : UILabel = UILabel(frame: CGRectMake(self.vscrollView.frame.size.width / 100 * 65, 110, self.vscrollView.frame.size.width / 100 * 30 ,20))
        lbl_HoursTitle.backgroundColor = UIColor(hexString: "#2190ea")
        lbl_HoursTitle.textAlignment = .Center
        lbl_HoursTitle.textColor = UIColor.whiteColor()
        lbl_HoursTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        lbl_HoursTitle.text = "Hours"
        self.vscrollView.addSubview(lbl_HoursTitle)
        
        self.ActivityListView = UITableView(frame: CGRectMake(self.vscrollView.frame.width / 100 * 5 , 130 , self.vscrollView.frame.width / 100 * 90 , 150  ))
        
        self.ActivityListView.showsVerticalScrollIndicator = true
        self.ActivityListView.delegate = self
        self.ActivityListView .dataSource = self
        self.ActivityListView.rowHeight = 30
        self.ActivityListView.separatorColor = UIColor.whiteColor()
        self.ActivityListView.backgroundColor = UIColor.clearColor()
        
        self.vscrollView.addSubview(self.ActivityListView)
        
        
        let btn_Add : UIButton = UIButton(frame: CGRectMake(self.vscrollView.frame.size.width / 100 * 5  , 290 , self.vscrollView.frame.size.width / 100 * 40  ,20))
         btn_Add.setTitleColor(UIColor(colorLiteralRed: 28/255, green: 138/255, blue: 255/255, alpha: 1), forState: .Normal)
            btn_Add.setTitle("Add", forState: .Normal)
        btn_Add.setFAText(prefixText: "  " , icon: FAType.FAPlus, postfixText:"   Add Time Entry"  , size:  12, forState: .Normal)
        
        btn_Add.addTarget(self, action: "AddRow:", forControlEvents: .TouchUpInside)
       
        
        self.vscrollView.addSubview(btn_Add)
        
        let lbl_TotalHourstle : UILabel = UILabel(frame: CGRectMake(self.vscrollView.frame.size.width / 100 * 45 , 290, self.vscrollView.frame.size.width / 100 * 30  ,20))
        lbl_TotalHourstle.backgroundColor = UIColor.clearColor()
        lbl_TotalHourstle.textAlignment = .Center
        lbl_TotalHourstle.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        lbl_TotalHourstle.textColor = UIColor(hexString: "#666666")
        lbl_TotalHourstle.text = "Total Hours"
        self.vscrollView.addSubview(lbl_TotalHourstle)
        
      
         TotalHours  = UILabel(frame: CGRectMake(self.vscrollView.frame.size.width / 100 * 65  , 290, self.vscrollView.frame.size.width / 100 * 30  ,20))
        TotalHours.backgroundColor = UIColor.clearColor()
        TotalHours.textAlignment = .Center
        TotalHours.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        TotalHours.textColor = UIColor(hexString: "#333333")
        if self.arrSelfActivityList.count > 0
        {
             TotalHours.text = self.arrSelfActivityList[0].Sum_of_TotalTime
        }
       
        self.vscrollView.addSubview(TotalHours)
        
        
        let lbl_ReasonTitle : UILabel = UILabel(frame: CGRectMake(self.vscrollView.frame.size.width / 100 * 5  , 330, self.vscrollView.frame.size.width   ,20))
        lbl_ReasonTitle.backgroundColor = UIColor.clearColor()
        lbl_ReasonTitle.textAlignment = .Left
        lbl_ReasonTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        lbl_ReasonTitle.textColor = UIColor(hexString: "#666666")
        lbl_ReasonTitle.text = "Reason"
        self.vscrollView.addSubview(lbl_ReasonTitle)
        
        
        let txt_Reason : UITextField = UITextField(frame: CGRectMake(self.vscrollView.frame.size.width / 100 * 5  , 360, self.vscrollView.frame.size.width / 100 * 90 ,40))
        txt_Reason.backgroundColor = UIColor.whiteColor()
        txt_Reason.layer.borderWidth = 1
         txt_Reason.layer.borderColor = UIColor(hexString: "#cccccc")?.CGColor
        txt_Reason.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        txt_Reason.layer.cornerRadius = 5
         txt_Reason.textColor = UIColor(hexString: "#666666")
        txt_Reason.textAlignment = .Left
        txt_Reason.delegate = self
         self.vscrollView.addSubview(txt_Reason)
        
        Reason = txt_Reason
        
        let btn_Submit : UIButton = UIButton(frame: CGRectMake(self.vscrollView.frame.size.width / 100 * 5  , 440 , self.vscrollView.frame.size.width / 100 * 90  ,25))
        btn_Submit.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        btn_Submit.setTitle("Submit", forState: .Normal)
        btn_Submit.enabled = true
        btn_Submit.userInteractionEnabled = true
        btn_Submit.clearsContextBeforeDrawing = true
        btn_Submit.autoresizesSubviews = true
        btn_Submit.backgroundColor = UIColor(hexString: "#48c9b0")
        btn_Submit.setFAText(prefixText: "  " , icon: FAType.FACheck, postfixText:"   Submit"  , size:  12, forState: .Normal)
        
        btn_Submit.layer.cornerRadius = 5
 
        btn_Submit.addTarget(self, action: "SaveAttendanceAdjustment", forControlEvents: .TouchUpInside)
        
        
        
        self.vscrollView.addSubview(btn_Submit)
        
    }
    
    func SaveAttendanceAdjustment()
    {
        
        objLoadingIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, Viewframe.width, Viewframe.height))
        
        objLoadingIndicaor.activityIndicatorViewStyle = .WhiteLarge
        let objPosition =  self.view.center
        objLoadingIndicaor.center = objPosition
        objLoadingIndicaor.hidesWhenStopped = true
        objLoadingIndicaor.color = UIColor.grayColor()
        self.view.addSubview(objLoadingIndicaor)
        objLoadingIndicaor.startAnimating()
        
        let TempMsg =  String(self.ValidateAdjustmentEntries())
        
       
        
            if TempMsg != "0"
        {
            self.objLoadingIndicaor.stopAnimating()
            let alert = UIAlertController(title: "Validation", message: TempMsg , preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.presentViewController(alert, animated: true, completion: nil)
                
            }
        }
        else
            {
        
        let colAdjustmentList = AdjustmentListCollection()
        
        var ClientID: Int! = 0
        var UserID: Int! = 0
                
        var DateLineArr = self.AdjustmentDate.componentsSeparatedByString("/")
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            let ResultJSON = objCallBack1.DeserializeJSONString(UserInfo)
            
            ClientID = Int(ResultJSON!["ClientID"]as! NSNumber)
            UserID = Int(ResultJSON!["UserID"]as! NSNumber)
        }
        
        for var i = 0; i < self.arrSelfActivityList.count ; i++
        {
            let objAdjustmentListIn = AdjustmentList()
            objAdjustmentListIn.ClientID = ClientID
            objAdjustmentListIn.UserID = UserID
            objAdjustmentListIn.WebType = 1
            objAdjustmentListIn.IsAdjusted = self.arrSelfActivityList[i].IsInTime
            objAdjustmentListIn.DateLine = String(DateLineArr[2]) + "-" + String(DateLineArr[1]) + "-" + String(DateLineArr[0])
            objAdjustmentListIn.DtrDatetime = self.arrSelfActivityList[i].InTime
            colAdjustmentList.DTR_Adjustment.append(objAdjustmentListIn)
            
            let objAdjustmentListOUT = AdjustmentList()
            objAdjustmentListOUT.ClientID = ClientID
            objAdjustmentListOUT.UserID = UserID
            objAdjustmentListOUT.WebType = 2
            objAdjustmentListOUT.IsAdjusted = self.arrSelfActivityList[i].IsOutTime
            objAdjustmentListOUT.DateLine = String(DateLineArr[2]) + "-" + String(DateLineArr[1]) + "-" + String(DateLineArr[0])
            objAdjustmentListOUT.DtrDatetime = self.arrSelfActivityList[i].OutTime
            colAdjustmentList.DTR_Adjustment.append(objAdjustmentListOUT)
            
        }
        
        
        objCallBack1 = AjaxCallBack()
        objCallBack1.MethodName = "SaveAttendanceNoteDetails"
        
 
                
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            let ResultJSON = objCallBack1.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack1.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack1.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pAttendanceNoteRequestID",value: "-1")
            objCallBack1.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pNoteRaisedDate",value: self.AdjustmentDate)
            objCallBack1.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pRemarks",value: Reason.text!)
            objCallBack1.ParameterList.append(objparam)
            
            var JsonString : NSString = colAdjustmentList.toJsonString()!
            
      
            
            //Find The index from HTTPResonse String
            
            let Startrange =  JsonString.rangeOfString("[")
            let Endrange = JsonString.rangeOfString("]")
            //SubString the JSON string from HTTPResponse String
                JsonString =  JsonString.substringWithRange(NSRange(location: Startrange.location + Startrange.length , length: Endrange.location - (Startrange.location + Startrange.length)))
       
            
            JsonString = "[" + (JsonString as String) + "]"
            
            objparam = Parameter(Name: "pAdjustDetails",value: String(JsonString))
            objCallBack1.ParameterList.append(objparam)
      
            
            
            objCallBack1.post(SaveAttendanceAdjustmentOnComplete, OnError: SaveAttendanceAdjustmentOnErrorOnError)
            
        }
        
        }
        
    }
    
    func SaveAttendanceAdjustmentOnErrorOnError(Response: NSError ){
        
    }
    
    func SaveAttendanceAdjustmentOnComplete (ResultString :NSString? ) {
        
        dispatch_async(dispatch_get_main_queue(),{
            
            self.objLoadingIndicaor.stopAnimating()
            let Result =  self.objCallBack1.GetJSONResultString(ResultString)
            if Result  == "True"
            {
                DoneHUD.showInView(self.view, message: "Done")
                self.ShowAdjustmentDetails()
            }
            else
            {
                let alert = UIAlertController(title: "Validation", message: Result , preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                }
            }
            
        })
    }
    
    
    func ShowAdjustmentDetails()
    {        
        
            objCallBack1 = AjaxCallBack()
            objCallBack1.MethodName = "GetRequestDetailsBasedonDate"
            
            if let UserInfo = defaults.stringForKey("UserInfo")
            {
                let ResultJSON = objCallBack1.DeserializeJSONString(UserInfo)
                
                var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
                objCallBack1.ParameterList.append(objparam)
             
                objparam = Parameter(Name: "pRequestedDate",value: String(self.AdjustmentDate))
                objCallBack1.ParameterList.append(objparam)
                
                objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
                objCallBack1.ParameterList.append(objparam)
                
                
                objCallBack1.post(ShowAdjustmentDetailsOnComplete, OnError: ShowAdjustmentDetailsErrorOnError)
                
            }
     
        
        
    }
    
    //Response on error callback
    func ShowAdjustmentDetailsErrorOnError(Response: NSError ){
        
    }
    
    func ShowAdjustmentDetailsOnComplete (ResultString :NSString? ) {
        
        dispatch_async(dispatch_get_main_queue(),{
            
            let  objAdjustmentDetails = AdjustmentDetails()
            objAdjustmentDetails.AdjDetails = self.GetAdjRequestObject(JSON(data: self.objCallBack1.GetJSONData(ResultString)!))
            objAdjustmentDetails.Mode = 0
            self.presentViewController(objAdjustmentDetails, animated: true, completion: nil)
            
        })
    }
    
    func GetAdjRequestObject(AdjMentJSON: Talentoz.JSON!) -> AttendanceAdjustmentRequest
    {
        
      
            
            let madj = AttendanceAdjustmentRequest()
            madj.RequestID = AdjMentJSON[0]["RequestID"].int
            madj.EmployeePhoto  = AdjMentJSON[0]["EmployeePhoto"].string
            madj.EmployeeName  = AdjMentJSON[0]["EmployeeName"].string
            madj.PositionName  = AdjMentJSON[0]["PositionName"].string
            madj.DateLineDesc  = AdjMentJSON[0]["DateLineDesc"].string
            madj.WorkLocationName  = AdjMentJSON[0]["WorkLocationName"].string
            madj.StatusDesc  = AdjMentJSON[0]["StatusDesc"].string
            madj.TotalHours  = AdjMentJSON[0]["TotalHours"].string
            madj.DateLine  = AdjMentJSON[0]["DateLine"].string
            madj.Remarks  = AdjMentJSON[0]["Remarks"].string
            madj.UserID  = AdjMentJSON[0]["UserID"].int
            madj.Status  = AdjMentJSON[0]["Status"].int
            madj.Order  = AdjMentJSON[0]["Order"].int
            
       
        return madj
    }
    
    
    func AddRow(Sender:UIButton)
    {
        if self.AdjustmentDate == "Select Date"
        {
          
            let alert = UIAlertController(title: "Validation", message: "Please select adjustment date." , preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.presentViewController(alert, animated: true, completion: nil)
                
            }
        }
        else
        {
            let madj = ActivitiesList()
            madj.TotalTime = ""
            madj.OutTime  = ""
            madj.RwNo  = ""
            madj.InTime  = ""
            madj.ShirtOutTime  = ""
            madj.ShirtInTime  = ""
            madj.Sum_of_TotalTime  = ""
            madj.IsInTime  = 1
            madj.RowID  = 0
            madj.IsOutTime  = 1
            madj.IsNewlyCreated = true
            self.arrSelfActivityList.append(madj)
            ActivityListView.reloadSections(NSIndexSet(index: 0), withRowAnimation: .Automatic)
        }
        

    }
    
    func numberOfSectionsInTableView(ActivityListView: UITableView) -> Int {
        return 1
    }
    
    func tableView(ActivityListView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrSelfActivityList.count
    }
    
    
    func tableView(ActivityListView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // 3
        let cell = UITableViewCell()
        
        
        if indexPath.row % 2 == 0
        {
            cell.backgroundColor = UIColor.whiteColor()
        }
        else
        {
            cell.backgroundColor = UIColor(hexString: "#f5f5f5")
        }
        
      
        if self.arrSelfActivityList[indexPath.row].IsInTime == 0
        {
            let lbl_InTime : UILabel = UILabel(frame: CGRectMake(self.ActivityListView.frame.size.width / 100 * 2  , 5, self.ActivityListView.frame.size.width / 100 * 30 ,20))
            lbl_InTime.backgroundColor = UIColor.clearColor()
            lbl_InTime.textAlignment = .Center
            lbl_InTime.font = UIFont(name:"HelveticaNeue", size: 10.0)
            lbl_InTime.textColor = UIColor(hexString: "#666666")
            lbl_InTime.text = String(self.arrSelfActivityList[indexPath.row].ShirtInTime )
            cell.addSubview(lbl_InTime)
        }
        else
        {
            let lbl_InTime : UIButton = UIButton(frame: CGRectMake(self.ActivityListView.frame.size.width / 100 * 7  , 5, self.ActivityListView.frame.size.width / 100 * 20 ,20))
             lbl_InTime.backgroundColor = UIColor.whiteColor()
            lbl_InTime.layer.borderWidth = 1
            lbl_InTime.layer.cornerRadius = 3
            
            lbl_InTime.setTitleColor(UIColor(hexString: "#666666"), forState: .Normal)
            lbl_InTime.titleLabel!.font =  UIFont(name:"HelveticaNeue", size: 10.0)
            lbl_InTime.tag = indexPath.row
            if self.arrSelfActivityList[indexPath.row].ShirtInTime != ""
            {
                lbl_InTime.setTitle(self.arrSelfActivityList[indexPath.row].ShirtInTime, forState: .Normal)
            }
             lbl_InTime.addTarget(self, action: "InTimeShow:", forControlEvents: .TouchUpInside)
            cell.addSubview(lbl_InTime)
        }
        
        
        if self.arrSelfActivityList[indexPath.row].IsOutTime  == 0
        {
        let lbl_OutTime : UILabel = UILabel(frame: CGRectMake(self.ActivityListView.frame.size.width / 100 * 32, 5, self.ActivityListView.frame.size.width / 100 * 30 ,20))
        lbl_OutTime.backgroundColor = UIColor.clearColor()
        lbl_OutTime.textAlignment = .Center
        lbl_OutTime.textColor = UIColor(hexString: "#666666")
        lbl_OutTime.font = UIFont(name:"HelveticaNeue", size: 10.0)
        lbl_OutTime.text = String(self.arrSelfActivityList[indexPath.row].ShirtOutTime)
        cell.addSubview(lbl_OutTime)
        }
        else
        {
            let lbl_OutTime : UIButton = UIButton(frame: CGRectMake(self.ActivityListView.frame.size.width / 100 * 40, 5, self.ActivityListView.frame.size.width / 100 * 20 ,20))
            lbl_OutTime.backgroundColor = UIColor.whiteColor()
            lbl_OutTime.layer.borderWidth = 1
            lbl_OutTime.layer.cornerRadius = 3
            lbl_OutTime.tag = indexPath.row
           
            lbl_OutTime.titleLabel!.font =  UIFont(name:"HelveticaNeue", size: 10.0)
            lbl_OutTime.setTitleColor(UIColor(hexString: "#666666"), forState: .Normal)
            if self.arrSelfActivityList[indexPath.row].ShirtOutTime != ""
            {
                lbl_OutTime.setTitle(self.arrSelfActivityList[indexPath.row].ShirtOutTime, forState: .Normal)
            }
            lbl_OutTime.addTarget(self, action: "OutTimeShow:", forControlEvents: .TouchUpInside)
            cell.addSubview(lbl_OutTime)
        }
    
       
        if self.arrSelfActivityList[indexPath.row].IsNewlyCreated == false
        {
            let lbl_Hours : UILabel = UILabel(frame: CGRectMake(self.ActivityListView.frame.size.width / 100 * 68, 5, self.ActivityListView.frame.size.width / 100 * 30 ,20))
            lbl_Hours.backgroundColor = UIColor.clearColor()
            lbl_Hours.textAlignment = .Center
            lbl_Hours.textColor = UIColor(hexString: "#666666")
            lbl_Hours.font = UIFont(name:"HelveticaNeue", size: 10.0)
        
                
               self.arrSelfActivityList[indexPath.row].TotalTime =  self.GetTotalHours(String(self.arrSelfActivityList[indexPath.row].InTime),ToDateTime: String(self.arrSelfActivityList[indexPath.row].OutTime))
                
                 lbl_Hours.text = String(self.arrSelfActivityList[indexPath.row].TotalTime)
         
            cell.addSubview(lbl_Hours)
        }
        else
        {
            
            let lbl_Hours : UILabel = UILabel(frame: CGRectMake(self.ActivityListView.frame.size.width / 100 * 68, 5, self.ActivityListView.frame.size.width / 100 * 30 ,20))
            lbl_Hours.backgroundColor = UIColor.clearColor()
            lbl_Hours.textAlignment = .Center
            lbl_Hours.textColor = UIColor(hexString: "#666666")
            lbl_Hours.font = UIFont(name:"HelveticaNeue", size: 10.0)
            
            self.arrSelfActivityList[indexPath.row].TotalTime =  self.GetTotalHours(String(self.arrSelfActivityList[indexPath.row].InTime),ToDateTime: String(self.arrSelfActivityList[indexPath.row].OutTime))
            
            lbl_Hours.text = String(self.arrSelfActivityList[indexPath.row].TotalTime)
            
            cell.addSubview(lbl_Hours)
            
            let btn_RemoveCell : UIButton = UIButton(frame: CGRectMake(self.ActivityListView.frame.size.width / 100 * 95, 5, self.ActivityListView.frame.size.width / 100 * 5 ,20))
            btn_RemoveCell.backgroundColor = UIColor.clearColor()
            btn_RemoveCell.setTitleColor(UIColor.redColor(), forState: .Normal)
            btn_RemoveCell.setFAIcon(FAType.FAMinusCircle, iconSize: 12, forState: .Normal)
            btn_RemoveCell.addTarget(self, action: "DeleteRow:", forControlEvents: .TouchUpInside)
            btn_RemoveCell.tag = indexPath.row + 1
            cell.addSubview(btn_RemoveCell)
        }
        
        if indexPath.row == 0
        {
            TotalHoursInSecond = 0
        }
        
        if self.arrSelfActivityList[indexPath.row].TotalTime  != nil
        {
            if self.arrSelfActivityList[indexPath.row].TotalTime  != ""
            {
                TotalHoursInSecond = TotalHoursInSecond + GetSecondFromHours(self.arrSelfActivityList[indexPath.row].TotalTime)
            }
        }
      
        if indexPath.row == self.arrSelfActivityList.count - 1
        {
            if TotalHoursInSecond > 0
            {
                TotalHours.text =  self.GetTimeFormatStr(TotalHoursInSecond)
            }
            else
            {
                TotalHours.text = "00:00:00"
            }
        }
        
        return cell
    }
    
    
    func GetSecondFromHours(StrTotalHours: String)->Int
    {
        var TotalHours : Int = 0
        
        if StrTotalHours != "nil"
        {
            var TimeArr = String(StrTotalHours).componentsSeparatedByString(":")
        
            TotalHours = (Int(TimeArr[0])! * 60 * 60) + (Int(TimeArr[1])! * 60 ) +  Int(TimeArr[2])!
        }
        return TotalHours
    }
    
    
    func GetTotalHours(FromDateTime: String, ToDateTime: String)->String
    {
        var TotalHoursStr = "00:00:00"
        
        if FromDateTime != "nil"{
            if ToDateTime != "nil"{
        if FromDateTime != ""{
            if ToDateTime != ""{
                
                var TimeArr = FromDateTime.componentsSeparatedByString("T")
                var DateArr = TimeArr[0].componentsSeparatedByString("-")
                
                TimeArr = TimeArr[1].componentsSeparatedByString(":")
                
                
                let calendar = NSCalendar(identifier: NSCalendarIdentifierGregorian)
               calendar!.timeZone = NSTimeZone(name: "UTC")!
                
                let components = NSDateComponents()
                components.year = Int(DateArr[0])!
                components.month = Int(DateArr[1])!
                components.day = Int(DateArr[2])!
                components.hour = Int(TimeArr[0])!
                components.minute = Int(TimeArr[1])!
                components.second = 0
                
                let FromDateTimeCal = (calendar?.dateFromComponents(components))!
                
                
                 TimeArr = ToDateTime.componentsSeparatedByString("T")
                 DateArr = TimeArr[0].componentsSeparatedByString("-")
                
                TimeArr = TimeArr[1].componentsSeparatedByString(":")
                
                
                let calendar1 = NSCalendar(identifier: NSCalendarIdentifierGregorian)
                calendar1!.timeZone = NSTimeZone(name: "UTC")!
                
                let components1 = NSDateComponents()
                components1.year = Int(DateArr[0])!
                components1.month = Int(DateArr[1])!
                components1.day = Int(DateArr[2])!
                components1.hour = Int(TimeArr[0])!
                components1.minute = Int(TimeArr[1])!
                components1.second = 0
                
                let ToDateTimeCal = (calendar1?.dateFromComponents(components1))!
                
                let interval = ToDateTimeCal.timeIntervalSinceDate(FromDateTimeCal)
                
                if interval > 0
                {
                    TotalHoursStr = self.GetTimeFormatStr(Int(interval))
                }
                
            }
                }
            }
        }
        
       
        
        return TotalHoursStr
    }
    
    func GetTimeFormatStr(TotalSecond: Int)->String
    {
        var TotalHoursStr: String!
        var Hour: Int!
        var Minute: Int!
        var Second: Int!
        
        Second = Int(TotalSecond) % 60
        
        if (((Int(TotalSecond) - Second) / 60)>0)
        {
            Minute = ((Int(TotalSecond) - Second) / 60) % 60
        }
        else
        {
            Minute = 0
        }
        
        if (((Int(TotalSecond) - (Int(TotalSecond) % 60)/60)/60)>0)
        {
            Hour = ((Int(TotalSecond) - ((Minute * 60) + Second))/60) / 60
        }
        else
        {
            Hour = 0
        }
        
        if Hour<10
        {
            TotalHoursStr = "0" + String(Hour)
        }
        else
        {
            TotalHoursStr = String(Hour)
        }
        
        if Minute<10
        {
            TotalHoursStr = TotalHoursStr + ":" + "0" + String(Minute)
        }
        else
        {
            TotalHoursStr = TotalHoursStr + ":" + String(Minute)
        }
        
        if Second<10
        {
            TotalHoursStr = TotalHoursStr + ":" + "0" + String(Second)
        }
        else
        {
            TotalHoursStr = TotalHoursStr + ":" + String(Second)
        }
        
        return TotalHoursStr

    }
    
    
    
    
    func DeleteRow(Sender:UIButton)
    {
        self.arrSelfActivityList.removeAtIndex(Sender.tag - 1)
        self.ActivityListView.reloadData()
        
        
    }
    
    func InTimeShow(sender:UIButton)
    {
        self.EnterActivityType = 0
        ShowTimePicker(sender,TagID: sender.tag)
    }
    func OutTimeShow(sender:UIButton)
    {
        self.EnterActivityType = 1
        ShowTimePicker(sender,TagID: sender.tag)
    }
    
    
    func ShowTimePicker(sender:UIButton, TagID: Int)
    {
        SelectedTimeBtn = sender
        
    
        
        FilterBGLayer = UIView(frame: CGRectMake(0, 0, Viewframe.width  ,  Viewframe.height))
        FilterBGLayer.backgroundColor = UIColor(colorLiteralRed: 0 , green: 0, blue: 0, alpha: 0.6)
        let GestureLeaveDetail1 = UITapGestureRecognizer(target: self, action: "DismissFilter:")
        FilterBGLayer.addGestureRecognizer(GestureLeaveDetail1)
        FilterView = UIView(frame: CGRectMake(FilterBGLayer.frame.width / 100 * 10 , FilterBGLayer.frame.height  / 100 * 10 , FilterBGLayer.frame.width  / 100 * 80, FilterBGLayer.frame.height  / 100 * 70))
        FilterView.backgroundColor = UIColor.whiteColor()
        FilterView.layer.cornerRadius = 8
        var DateCur = NSDate()
        
        if self.EnterActivityType == 0
        {
            if self.arrSelfActivityList[TagID].ShirtInTime != nil
            {
                if self.arrSelfActivityList[TagID].ShirtInTime != ""
                {
                    var DateArr = self.AdjustmentDate.componentsSeparatedByString("/")
                    
                    var TimeArr = self.arrSelfActivityList[TagID].InTime.componentsSeparatedByString("T")
                    TimeArr = TimeArr[1].componentsSeparatedByString(":")
                    
                    
                    let calendar = NSCalendar(identifier: NSCalendarIdentifierGregorian)
                   // calendar!.timeZone = NSTimeZone(name: "UTC")!
                    
                    let components = NSDateComponents()
                    components.year = Int(DateArr[2])!
                    components.month = Int(DateArr[1])!
                    components.day = Int(DateArr[0])!
                    components.hour = Int(TimeArr[0])!
                    components.minute = Int(TimeArr[1])!
                    components.second = Int(TimeArr[2])!
                    
                    DateCur = (calendar?.dateFromComponents(components))!
                    
                }
            }
         
        }
        else
        {
            
            if self.arrSelfActivityList[TagID].ShirtOutTime != nil
            {
                if self.arrSelfActivityList[TagID].ShirtOutTime != ""
                {
                    var DateArr = self.AdjustmentDate.componentsSeparatedByString("/")
                    
                    var TimeArr = self.arrSelfActivityList[TagID].OutTime.componentsSeparatedByString("T")
                    TimeArr = TimeArr[1].componentsSeparatedByString(":")
                    
                    
                    let calendar = NSCalendar(identifier: NSCalendarIdentifierGregorian)
                    //calendar!.timeZone = NSTimeZone(name: "UTC")!
                    
                    let components = NSDateComponents()
                    components.year = Int(DateArr[2])!
                    components.month = Int(DateArr[1])!
                    components.day = Int(DateArr[0])!
                    components.hour = Int(TimeArr[0])!
                    components.minute = Int(TimeArr[1])!
                    components.second = Int(TimeArr[2])!
                    
                    DateCur = (calendar?.dateFromComponents(components))!
                }
            }
           
        }
        
        
        
        
        let timeFormatter = NSDateFormatter()
          timeFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
        
        let strDate = timeFormatter.stringFromDate(DateCur)
        
        SelectedTimeBtn.setTitle(String(strDate), forState: .Normal)
        
        timeFormatter.dateFormat = "HH:mm:ss"
        
        var array = self.AdjustmentDate.componentsSeparatedByString("/")
        
        if self.EnterActivityType == 0
        {
        self.arrSelfActivityList[TagID].InTime = String(array[2]) + "-" + String(array[1]) + "-" + String(array[0]) + "T" + timeFormatter.stringFromDate(DateCur)
        
        self.arrSelfActivityList[TagID].ShirtInTime = String(strDate)
        }
        else
        {
            self.arrSelfActivityList[TagID].OutTime = String(array[2]) + "-" + String(array[1]) + "-" + String(array[0]) + "T" + timeFormatter.stringFromDate(DateCur)
            
            self.arrSelfActivityList[TagID].ShirtOutTime = String(strDate)
        }
        
        ActivityListView.reloadSections(NSIndexSet(index: 0), withRowAnimation: .Automatic)
        
       let TimePick : UIDatePicker = UIDatePicker(frame: CGRect(x: 30, y: 5, width: FilterView.frame.width / 100 * 80 ,height: FilterView.frame.width - 30))
        TimePick.datePickerMode = .Time
        TimePick.date = DateCur
        TimePick.tag = TagID
        TimePick.addTarget(self, action: Selector("handler:"), forControlEvents: UIControlEvents.ValueChanged)
        FilterView.addSubview(TimePick)
        
        let btn_ApplyFilter : UIButton = UIButton(frame: CGRectMake(FilterBGLayer.frame.width / 100 * 25, TimePick.frame.height + 10, 100 ,24))
        btn_ApplyFilter.setTitle("Done", forState: .Normal)
        btn_ApplyFilter.backgroundColor = UIColor(hexString: "#2190ea")
        btn_ApplyFilter.layer.cornerRadius = 5
        btn_ApplyFilter.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        btn_ApplyFilter.titleLabel!.font =  UIFont(name:"helvetica", size: 12)
        btn_ApplyFilter.addTarget(self, action: "DismissFilter:", forControlEvents: .TouchUpInside)
        FilterView.addSubview(btn_ApplyFilter)
        
        FilterBGLayer.addSubview(FilterView)
        self.view.addSubview(FilterBGLayer)
        
    }
    
    func handler(sender: UIDatePicker) {
        let timeFormatter = NSDateFormatter()
        timeFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
        
        let strDate = timeFormatter.stringFromDate(sender.date)
        
        SelectedTimeBtn.setTitle(String(strDate), forState: .Normal)
        var array = self.AdjustmentDate.componentsSeparatedByString("/")
        if self.EnterActivityType == 0
        {
            timeFormatter.dateFormat = "HH:mm:ss"
            self.arrSelfActivityList[sender.tag].InTime = String(array[2]) + "-" + String(array[1]) + "-" + String(array[0]) + "T" + timeFormatter.stringFromDate(sender.date)
            self.arrSelfActivityList[sender.tag].ShirtInTime = String(strDate)
        }
        else
        {
            timeFormatter.dateFormat = "HH:mm:ss"
            self.arrSelfActivityList[sender.tag].OutTime =  String(array[2]) + "-" + String(array[1]) + "-" + String(array[0]) + "T" + timeFormatter.stringFromDate(sender.date)
            self.arrSelfActivityList[sender.tag].ShirtOutTime = String(strDate)
        }
        
        ActivityListView.reloadSections(NSIndexSet(index: 0), withRowAnimation: .Automatic)
       
        // do what you want to do with the string.
    }
    
    
    
    func ShowDatePicker(sender:UIButton)
     {
        SelectedTimeBtn = sender
        
        FilterBGLayer = UIView(frame: CGRectMake(0, 0, Viewframe.width  ,  Viewframe.height))
        FilterBGLayer.backgroundColor = UIColor(colorLiteralRed: 0 , green: 0, blue: 0, alpha: 0.6)
        let GestureLeaveDetail1 = UITapGestureRecognizer(target: self, action: "DismissFilter:")
        FilterBGLayer.addGestureRecognizer(GestureLeaveDetail1)
        FilterView = UIView(frame: CGRectMake(FilterBGLayer.frame.width / 100 * 10 , FilterBGLayer.frame.height  / 100 * 10 , FilterBGLayer.frame.width  / 100 * 80, FilterBGLayer.frame.height  / 100 * 70))
        FilterView.backgroundColor = UIColor.whiteColor()
        FilterView.layer.cornerRadius = 8
        let TimePick : UIDatePicker =  UIDatePicker(frame: CGRect(x: 10, y: 5, width: FilterView.frame.width - 10 ,height: FilterView.frame.width - 30))
        TimePick.datePickerMode = .Date
        
        let calendar: NSCalendar = NSCalendar(calendarIdentifier: NSGregorianCalendar)!
        // let calendar: NSCalendar = NSCalendar.currentCalendar()
        calendar.timeZone = NSTimeZone(name: "UTC")!
        var DateStr = ""
        if self.AdjustmentDate == "Select Date"
        {
            let date = NSDate()
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components([.Year,.Month,.Day], fromDate: date)
            
            DateStr = String(components.day) + "/" + String(components.month) + "/" + String(components.year)
        }
        else
        {
            DateStr = self.AdjustmentDate
        }
        
        var array = DateStr.componentsSeparatedByString("/")
        let components: NSDateComponents = NSDateComponents()
        components.year = Int(array[2])!
        components.month = Int(array[1])!
        components.day = Int(array[0])!
        let defaultDate: NSDate = calendar.dateFromComponents(components)!
        TimePick.date = defaultDate
        
        TimePick.addTarget(self, action: Selector("handlerDate:"), forControlEvents: UIControlEvents.ValueChanged)
        
      
        
        let timeFormatter = NSDateFormatter()
        timeFormatter.dateFormat = "dd/MM/yyyy"
        
        SelectedDate = timeFormatter.stringFromDate(defaultDate)
       
        
        let btn_ApplyFilter : UIButton = UIButton(frame: CGRectMake(FilterBGLayer.frame.width / 100 * 25, TimePick.frame.height + 10, 100 ,24))
        btn_ApplyFilter.setTitle("Go", forState: .Normal)
        btn_ApplyFilter.layer.cornerRadius = 5
        btn_ApplyFilter.backgroundColor = UIColor(hexString: "#2190ea")
        btn_ApplyFilter.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        btn_ApplyFilter.titleLabel!.font =  UIFont(name:"helvetica-Bold", size: 12)
        btn_ApplyFilter.addTarget(self, action: "DateChanged:", forControlEvents: .TouchUpInside)
        FilterView.addSubview(btn_ApplyFilter)
        
        FilterView.addSubview(TimePick)
        FilterBGLayer.addSubview(FilterView)
        self.view.addSubview(FilterBGLayer)
        
    }
    

    
    func handlerDate(sender: UIDatePicker) {
        let timeFormatter = NSDateFormatter()
        timeFormatter.dateFormat = "dd/MM/yyyy"
        
        SelectedDate = timeFormatter.stringFromDate(sender.date)
        
       
        // do what you want to do with the string.
    }
  
    func DateChanged(sender: UIButton)
    {
        self.Mode = 1
        SelectedTimeBtn.removeFromSuperview()
        SelectedTimeBtn.setTitle(String(SelectedDate), forState: .Normal)
        self.AdjustmentDate = SelectedDate
        isFilterOpen = false
        FilterBGLayer.removeFromSuperview()
        self.TotalHours.removeFromSuperview()
        LoadAdjustmentInfo()
    }
    
    
    func DismissFilter(sender: UIButton)
    {
        isFilterOpen = false
        FilterBGLayer.removeFromSuperview()
        
    }
    
    func ValidateAdjustmentEntries()->String
    {
        
        var IsValid = "0"
        var  iTempIntime: NSDate!
        var  iTempOuttime: NSDate!
        var  jTempIntime: NSDate!
        var  jTempOuttime: NSDate!
        
        if self.AdjustmentDate == "Select Date"
        {
            IsValid = "Please select adjustment date."
            return IsValid
        }
        
        let date = NSDate()
        
        var DateArr11 = self.AdjustmentDate.componentsSeparatedByString("/")
        
        
        let calendar11 = NSCalendar(identifier: NSCalendarIdentifierGregorian)
       
        
        let components11 = NSDateComponents()
        components11.year = Int(DateArr11[2])!
        components11.month = Int(DateArr11[1])!
        components11.day = Int(DateArr11[0])!
        let date11 = calendar11?.dateFromComponents(components11)
        
         if (date.compare(date11!) == NSComparisonResult.OrderedAscending || date.compare(date11!) == NSComparisonResult.OrderedSame)
         {
            IsValid = "Sorry, You can't raise adjustment for current and future date(s)."
            return IsValid
        }
        
        
        if self.arrSelfActivityList.count == 0
        {
            IsValid = "Please Insert altleast one  In Time & Out Time entry."
            return IsValid
        }
        
        for var i = 0; i < self.arrSelfActivityList.count ; i++ {
            
            if (  self.arrSelfActivityList[i].ShirtInTime ==  nil ||  self.arrSelfActivityList[i].ShirtOutTime == nil) {
                
                IsValid = "Please Select In Time & Out Time"
                return IsValid
            }
            
            if (  self.arrSelfActivityList[i].ShirtInTime ==  "" ||  self.arrSelfActivityList[i].ShirtOutTime == "") {
                
                IsValid = "Please Select In Time & Out Time"
                return IsValid
            }
       
        for var j = 0; j < self.arrSelfActivityList.count ; j++ {
            
            if ( self.arrSelfActivityList[j].ShirtInTime ==  nil ||   self.arrSelfActivityList[j].ShirtOutTime == nil) {
                
                IsValid = "Please Select In Time & Out Time"
                return IsValid
            }
            
            if (self.arrSelfActivityList[j].ShirtInTime == ""   || self.arrSelfActivityList[j].ShirtOutTime == "" ) {
                
                IsValid = "Please Select In Time & Out Time"
                return IsValid
            }
            
            //SimpleDateFormat format = new SimpleDateFormat("hh:mm a");
            
            var DateArr = self.AdjustmentDate.componentsSeparatedByString("/")
            
            var TimeArr = arrSelfActivityList[i].InTime.componentsSeparatedByString("T")
            TimeArr = TimeArr[1].componentsSeparatedByString(":")
            
            
            let calendar = NSCalendar(identifier: NSCalendarIdentifierGregorian)
              calendar!.timeZone = NSTimeZone(name: "UTC")!
            
            let components = NSDateComponents()
            components.year = Int(DateArr[2])!
            components.month = Int(DateArr[1])!
            components.day = Int(DateArr[0])!
            components.hour = Int(TimeArr[0])!
            components.minute = Int(TimeArr[1])!
            components.second = Int(TimeArr[2])!
            
            let date = calendar?.dateFromComponents(components)
            
            iTempIntime = date
           
            
           var TimeArr11 = arrSelfActivityList[i].OutTime.componentsSeparatedByString("T")
            TimeArr11 = TimeArr11[1].componentsSeparatedByString(":")
            
            
           let calendar22 = NSCalendar(identifier: NSCalendarIdentifierGregorian)
            calendar22!.timeZone = NSTimeZone(name: "UTC")!
             let components111 = NSDateComponents()
            components111.year = Int(DateArr[2])!
            components111.month = Int(DateArr[1])!
            components111.day = Int(DateArr[0])!
            components111.hour = Int(TimeArr11[0])!
            components111.minute = Int(TimeArr11[1])!
            components111.second = Int(TimeArr11[2])!
            
            let date11 = calendar22?.dateFromComponents(components111)
            
            
            iTempOuttime  = date11
           
            
            let calendar1 = NSCalendar(identifier: NSCalendarIdentifierGregorian)
            calendar1!.timeZone = NSTimeZone(name: "UTC")!
            
            var TimeArr1 = arrSelfActivityList[j].InTime.componentsSeparatedByString("T")
            TimeArr1 = TimeArr1[1].componentsSeparatedByString(":")
            
            let components1 = NSDateComponents()
            
            components1.year = Int(DateArr[2])!
            components1.month = Int(DateArr[1])!
            components1.day = Int(DateArr[0])!
            components1.hour = Int(TimeArr1[0])!
            components1.minute = Int(TimeArr1[1])!
            components1.second = Int(TimeArr1[2])!
            
            let date1 = calendar1?.dateFromComponents(components1)
            
            jTempIntime = date1
          
            
            let calendar1222 = NSCalendar(identifier: NSCalendarIdentifierGregorian)
            calendar1222!.timeZone = NSTimeZone(name: "UTC")!
            
           var TimeArr122 = arrSelfActivityList[j].OutTime.componentsSeparatedByString("T")
            TimeArr122 = TimeArr122[1].componentsSeparatedByString(":")
            
             let components1sss = NSDateComponents()
            
            components1sss.year = Int(DateArr[2])!
            components1sss.month = Int(DateArr[1])!
            components1sss.day = Int(DateArr[0])!
            components1sss.hour = Int(TimeArr122[0])!
            components1sss.minute = Int(TimeArr122[1])!
            components1sss.second = Int(TimeArr122[2])!
            
            let date12 = calendar1222?.dateFromComponents(components1sss)
            
            jTempOuttime  = date12
            
           
            
          //  FromDate!.compare(ToDate!) == NSComparisonResult.OrderedDescending
            
            
            if (i != j) {
                if (iTempIntime.compare(jTempIntime) == NSComparisonResult.OrderedDescending && iTempOuttime.compare(jTempOuttime) == NSComparisonResult.OrderedDescending && iTempIntime.compare(jTempOuttime) == NSComparisonResult.OrderedAscending) {
                    IsValid = "The given time pairing already falls on previous time pairings."
                    return IsValid
                }
                else if (iTempIntime.compare(jTempIntime) == NSComparisonResult.OrderedAscending && iTempOuttime.compare(jTempOuttime) == NSComparisonResult.OrderedAscending && iTempOuttime.compare(jTempIntime) == NSComparisonResult.OrderedDescending) {
                    IsValid = "The given time pairing already falls on previous time pairings."
                    return IsValid
                }
                else if (iTempOuttime.compare(jTempIntime) == NSComparisonResult.OrderedAscending && iTempOuttime.compare(jTempOuttime) == NSComparisonResult.OrderedDescending) {
                    IsValid = "The given time pairing already falls on previous time pairings."
                    return IsValid
                }
                else if (jTempIntime.compare(iTempIntime) == NSComparisonResult.OrderedAscending && jTempOuttime.compare(iTempOuttime) == NSComparisonResult.OrderedDescending) {
                    IsValid = "The given time pairing already falls on previous time pairings."
                    return IsValid
                }
                else if ((iTempIntime.compare(jTempIntime) == NSComparisonResult.OrderedSame) && iTempOuttime.compare(jTempOuttime) == NSComparisonResult.OrderedSame)
                {
                    IsValid = "The given time pairing already falls on previous time pairings."
                    return IsValid
                }
            }
            
            
            
            }
        }
        
        return IsValid
    }

   


}