//
//  ADJList.swift
//  Talentoz
//
//  Created by forziamac on 27/01/16.
//  Copyright © 2016 Forzia Tech Private Ltd. All rights reserved.
//
 
import UIKit

class ADJList: BaseTabBarVC, UITableViewDataSource ,UITableViewDelegate {
    
//# MARK: Local Variables
    var customSC: UISegmentedControl?
    var SelfAdjListData : Talentoz.JSON!
    var TeamAdjListData : Talentoz.JSON!
    var CurrentAdjListData = [AttendanceAdjustmentRequest]()
    var arrSelfAdjList = [AttendanceAdjustmentRequest]()
    var arrTeamAdjList = [AttendanceAdjustmentRequest]()
    var UserID : Int!
    var objCallBack = AjaxCallBack()
    let defaults = NSUserDefaults.standardUserDefaults()
    var CurrentCell =  UITableViewCell()
    let objRole = RoleManager()
    var CurrentRowIndex : Int!
    var CurrentActionType: Int!
    var AdjListView = UITableView()
    var ContainerTitalView = UIView()
    var objLoadingIndicaor : UIActivityIndicatorView!
    var swipeRight : UISwipeGestureRecognizer!
    var swipeLeft : UISwipeGestureRecognizer!
    var CurrentPageCount : Int!
    var VisibleShowMore : Bool = false
      var LoadMoreClicked : Int = 0
//# MARK: Public Variables
      internal var ModeofShow: Int! = 0
//# MARK: Default Methods
    override func viewDidLoad() {
        
        self.view.backgroundColor = UIColor.whiteColor()
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            self.UserID = Int(ResultJSON!["UserID"]as! NSNumber)
        }
        
        super.UINavigationBarTitle = "Adjustment List"
        super.viewDidLoad()
        BindContextTab()
        
        if objRole.ISManager{
            self.AddGesture()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


//# MARK: Custom Methods
    //Bind Top Context Tab Control
    func BindContextTab()
    {
        var items = ["My Time Requests"]
        if objRole.ISManager{
            items = ["My Time Requests", "Team Time Requests"]
        }
        
        let LeaveView=UIView(frame: CGRectMake(0, 60,  Viewframe.width , 48))
        LeaveView.backgroundColor = UIColor(colorLiteralRed: 28/255, green: 138/255, blue: 255/255, alpha: 1)
        
        
        customSC = UISegmentedControl(items: items)
        customSC!.selectedSegmentIndex = 0
        customSC!.frame = CGRectMake(Viewframe.width / 100 * 2, 5,
            Viewframe.width / 100 * 96,40)
        customSC!.layer.cornerRadius = 0.5
        customSC!.backgroundColor = UIColor(colorLiteralRed: 28/255, green: 138/255, blue: 255/255, alpha: 1)
        customSC!.tintColor = UIColor.whiteColor()
        customSC!.layer.borderColor = UIColor.whiteColor().CGColor
        customSC!.layer.borderWidth = 0
        customSC!.addTarget(self, action: "changeColor:", forControlEvents: .ValueChanged)
        LeaveView.addSubview(customSC!)
        self.view.addSubview(LeaveView)
        customSC!.selectedSegmentIndex = self.ModeofShow
        
        BindAdjList()
        
    }
    
    func LoadTeamLeaveList(gestureReconizer: UISwipeGestureRecognizer) {
        customSC!.selectedSegmentIndex = 1
        BindAdjList()
    }
    
    func LoadSelfLeaveList(gestureReconizer: UISwipeGestureRecognizer) {
        customSC!.selectedSegmentIndex = 0
        BindAdjList()
    }
    
    func changeColor(sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            BindAdjList()
            break;
        case 1:
            BindAdjList()
            break;
        default:
            break;
        }
    }
    
    func BindAdjList()
    {
        if self.customSC!.selectedSegmentIndex == 0
        {
            if let TempData = self.SelfAdjListData
            {
                ConstructAdjList()
            }else
            {
                GetAdjList()
            }
            
        }
        else
        {
            if let TempData = self.TeamAdjListData
            {
                ConstructAdjList()
            }else
            {
                GetAdjList()
            }
            
        }
        
    }
    
//# MARK: Ajax Callback Methods
    func GetAdjList()
    {
        objLoadingIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, Viewframe.width, Viewframe.height))
        
        objLoadingIndicaor.activityIndicatorViewStyle = .WhiteLarge
        let objPosition =  self.view.center
        objLoadingIndicaor.center = objPosition
        objLoadingIndicaor.hidesWhenStopped = true
        objLoadingIndicaor.color = UIColor.grayColor()
        self.view.addSubview(objLoadingIndicaor)
        objLoadingIndicaor.startAnimating()
        
        objCallBack = AjaxCallBack()
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            if customSC!.selectedSegmentIndex == 0
            {
                objCallBack.MethodName = "GetMyTimeRequests"
                
                objparam = Parameter(Name: "pPageIndex",value: "0")
                objCallBack.ParameterList.append(objparam)
                
                objparam = Parameter(Name: "pPageSize",value: "100000")
                objCallBack.ParameterList.append(objparam)
            }
            else
            {
                objCallBack.MethodName = "GetMyTeamTimeRequests"
                
                if self.CurrentPageCount == nil
                {
                    self.CurrentPageCount = 0
                }
                objparam = Parameter(Name: "pPageIndex",value: String(self.CurrentPageCount))
                objCallBack.ParameterList.append(objparam)
                
                objparam = Parameter(Name: "pPageSize",value: "10")
                objCallBack.ParameterList.append(objparam)
            
            }
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            
            
            objCallBack.post(AdjListOnComplete, OnError: LeaveListOnErrorOnError)
            
        }
        
    }
    //Response on error callback
    func LeaveListOnErrorOnError(Response: NSError ){
        
    }
    
    func AdjListOnComplete (ResultString :NSString? ) {
        if self.customSC!.selectedSegmentIndex == 0
        {
            if self.objCallBack.GetJSONData(ResultString) != nil
            {
                self.SelfAdjListData =   JSON(data: self.objCallBack.GetJSONData(ResultString)! )
                self.arrSelfAdjList = self.GetAdjRequestArray(self.SelfAdjListData , IsManager: 0 )
            }
        }
        else
        {
            if self.objCallBack.GetJSONData(ResultString) != nil
            {
                self.TeamAdjListData =   JSON(data: self.objCallBack.GetJSONData(ResultString)!)
                self.arrTeamAdjList = self.GetAdjRequestArray(self.TeamAdjListData , IsManager: 1 )
            }
        }
        ConstructAdjList()
    }
    //JSON Object array convert into Custom Array Collection
    func GetAdjRequestArray(AdjMentJSON: Talentoz.JSON!, IsManager: Int) -> [AttendanceAdjustmentRequest]
    {
        
        var arrAdjTempRequest = [AttendanceAdjustmentRequest]()
        for var i = 0; i < AdjMentJSON.count ; ++i {
            
            let madj = AttendanceAdjustmentRequest()
            madj.RequestID = AdjMentJSON[i]["RequestID"].int
            madj.EmployeePhoto  = AdjMentJSON[i]["EmployeePhoto"].string
            madj.EmployeeName  = AdjMentJSON[i]["EmployeeName"].string
            madj.PositionName  = AdjMentJSON[i]["PositionName"].string
            madj.DateLineDesc  = AdjMentJSON[i]["DateLineDesc"].string
            madj.WorkLocationName  = AdjMentJSON[i]["WorkLocationName"].string
            madj.StatusDesc  = AdjMentJSON[i]["StatusDesc"].string
            madj.TotalHours  = AdjMentJSON[i]["TotalHours"].string
            madj.DateLine  = AdjMentJSON[i]["DateLine"].string
            madj.Remarks  = AdjMentJSON[i]["Remarks"].string
            madj.UserID  = AdjMentJSON[i]["UserID"].int
            madj.Status  = AdjMentJSON[i]["Status"].int
            madj.Order  = AdjMentJSON[i]["Order"].int
            
            
            if IsManager == 0
            {
                arrAdjTempRequest.append(madj)
            }
            else
            {
                self.arrTeamAdjList.append(madj)
                self.CurrentAdjListData.append(madj)
            }
            
        }
        if IsManager == 1
        {
            if AdjMentJSON.count == 10
            {
                VisibleShowMore = true
            }
            else
            {
                VisibleShowMore = false
            }
            
            self.CurrentPageCount = self.CurrentPageCount + 1
            arrAdjTempRequest = self.arrTeamAdjList
        }
        
        return arrAdjTempRequest
    }
    
    //Bind Leave list table
    
    func ConstructAdjList()
    {
        
        dispatch_async(dispatch_get_main_queue(),{
            
            self.objLoadingIndicaor.stopAnimating()
            self.AdjListView.removeFromSuperview()
            self.ContainerTitalView.removeFromSuperview()
            
            if self.customSC!.selectedSegmentIndex == 0
            {
                self.CurrentAdjListData = self.arrSelfAdjList
            }
            else
            {
                self.CurrentAdjListData =  self.arrTeamAdjList
            }
            
            if (self.CurrentAdjListData.count > 0)
            {
            
            if self.customSC!.selectedSegmentIndex == 0
            {            
                self.ContainerTitalView = UIView(frame: CGRectMake(0, 110, self.view.frame.width , 35))
                self.ContainerTitalView.backgroundColor = UIColor(hexString: "#f5f5f5")
                self.view.addSubview(self.ContainerTitalView)
                
                self.AdjListView = UITableView(frame: CGRectMake(0, 145, self.view.frame.width, self.Viewframe.height  - 189 ))
                
                let lbl_DateTitle : UILabel = UILabel(frame: CGRectMake(self.view.frame.size.width / 100 * 12 , 0, self.view.frame.size.width / 100 * 40 ,35))
                lbl_DateTitle.textAlignment = .Left
                lbl_DateTitle.textColor = UIColor(hexString: String("#666666"))
                lbl_DateTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
                lbl_DateTitle.text =  "Date"
                self.ContainerTitalView.addSubview(lbl_DateTitle)
                
                
                let lbl_HoursTitle : UILabel = UILabel(frame: CGRectMake(self.view.frame.size.width / 100 * 45 , 0, self.view.frame.size.width / 100 * 40 , 35))
                lbl_HoursTitle.textAlignment = .Left
                lbl_HoursTitle.textColor = UIColor(hexString: String("#666666"))
                lbl_HoursTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
                lbl_HoursTitle.text =  "Total Hours"
                self.ContainerTitalView.addSubview(lbl_HoursTitle)
                
            }
            else
            {
                self.AdjListView = UITableView(frame: CGRectMake(0, 110, self.view.frame.width, self.Viewframe.height  - 154 ))
            }
            
            self.AdjListView.showsVerticalScrollIndicator = true
            self.AdjListView.delegate = self
            self.AdjListView .dataSource = self
            if self.customSC!.selectedSegmentIndex == 0
            {
                self.AdjListView.rowHeight = 35
            }
            else
            {
                self.AdjListView.rowHeight = 93
            }
            
            self.AdjListView.backgroundColor = UIColor.clearColor()
            self.AdjListView.separatorColor = UIColor.clearColor()
            self.view.addSubview(self.AdjListView )
                
            }
            else
            {
                let lbl_NotFound : UILabel = UILabel(frame: CGRectMake(0, 110, self.view.frame.width, 25 ))
                lbl_NotFound.textAlignment = .Center
                lbl_NotFound.font = UIFont(name:"HelveticaNeue", size: 12.0)
                lbl_NotFound.textColor = UIColor(hexString: String("#ff8877"))
                lbl_NotFound.text = "No time adjustment(s) to show."
                self.view.addSubview(lbl_NotFound)
            }
            
            if self.customSC!.selectedSegmentIndex == 1
            {
                if(self.LoadMoreClicked==1)
                {
                    self.scrollToLastRow()
                }
            }
            
        })
        
        
    }
//# MARK: Table List View Methods
    
    func numberOfSectionsInTableView(AdjListView: UITableView) -> Int {
        return 1
    }
    
    func tableView(AdjListView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        if self.customSC!.selectedSegmentIndex == 1
        {
            if self.VisibleShowMore == true
            {
                return self.CurrentAdjListData.count + 1
            }
            else
            {
                return self.CurrentAdjListData.count
            }
            
        }
        else
        {
            return self.CurrentAdjListData.count
        }
    }
    
    func tableView(AdjListView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
 
        let cell = UITableViewCell()
        ///cell.frame = CGRectMake(0, 30, 55, 400)
        cell.backgroundColor = UIColor(hexString:"#f5f5f5")
        
            if self.customSC!.selectedSegmentIndex == 0
            {
                let ContainerView = UIView(frame: CGRectMake(self.view.frame.size.width / 100 * 5, 0, self.view.frame.width / 100 * 90 , 30))
                ContainerView.backgroundColor = UIColor.whiteColor()
                cell.addSubview(ContainerView)
                
            
                let lbl_DateLine : UILabel = UILabel(frame: CGRectMake(self.view.frame.size.width / 100 * 4 , 5, self.view.frame.size.width / 100 * 40 ,18))
                lbl_DateLine.textAlignment = .Left
                lbl_DateLine.textColor = UIColor(hexString: String("#666666"))
                lbl_DateLine.font = UIFont(name:"HelveticaNeue", size: 12.0)
                lbl_DateLine.text =  String(self.CurrentAdjListData[indexPath.row].DateLineDesc)
                ContainerView.addSubview(lbl_DateLine)
                
                
                
                let lbl_Hours : UILabel = UILabel(frame: CGRectMake(self.view.frame.size.width / 100 * 42 , 5, self.view.frame.size.width / 100 * 23,18))
                lbl_Hours.textAlignment = .Left
                lbl_Hours.textColor = UIColor(hexString: String("#666666"))
                lbl_Hours.font = UIFont(name:"HelveticaNeue", size: 12.0)
                lbl_Hours.text =  String(self.CurrentAdjListData[indexPath.row].TotalHours) + " Hrs"
                ContainerView.addSubview(lbl_Hours)
                
                let lbl_Status : UILabel = UILabel(frame: CGRectMake(self.view.frame.size.width / 100 * 70 , 5, self.view.frame.size.width / 100 * 33,18))
                lbl_Status.textAlignment = .Left
            
            
                if (self.CurrentAdjListData[indexPath.row].Status == 1)
                {
                    lbl_Status.textColor = UIColor(hexString:"#ffbb5c")
                } else if (self.CurrentAdjListData[indexPath.row].Status == 2) {
                
                lbl_Status.textColor = UIColor(hexString: "#48c9b0")
            }
            else
            {
                lbl_Status.textColor = UIColor(hexString: "#ef6565")
            }
                
                
            lbl_Status.font = UIFont(name:"HelveticaNeue", size: 12.0)
            lbl_Status.text =  String(self.CurrentAdjListData[indexPath.row].StatusDesc)
            ContainerView.addSubview(lbl_Status)

            
            let GestureLeaveDetail = UITapGestureRecognizer(target: self, action: "ShowAdjustmentDetail:")
            cell.addGestureRecognizer(GestureLeaveDetail)
   
        }
        else
        {
            var IsShowMoreSection = false
            
            if self.VisibleShowMore == true
            {
                if self.CurrentAdjListData.count   == indexPath.row
                {
                    IsShowMoreSection = true
                }
            }
          
            if IsShowMoreSection == false
            {
                let ContainerView = UIView(frame: CGRectMake(self.view.frame.size.width / 100 * 2, 15, self.view.frame.width / 100 * 96 , 88))
                ContainerView.backgroundColor = UIColor.whiteColor()
                cell.addSubview(ContainerView)
                
                
                if let tmpDomainURL = self.defaults.stringForKey("DomainURL")
                {
                    if String(self.CurrentAdjListData[indexPath.row].EmployeePhoto) == "nil" ||  String(self.CurrentAdjListData[indexPath.row].EmployeePhoto) == ""
                    {
                        let img_EmpPic:  UIButton = UIButton(frame: CGRectMake(10, 20, 50, 50))
                        img_EmpPic.layer.borderWidth = 0.5
                        img_EmpPic.layer.masksToBounds = false
                        img_EmpPic.layer.borderColor = UIColor.lightGrayColor().CGColor
                        img_EmpPic.backgroundColor = UIColor.SetRandomColor()
                        img_EmpPic.layer.cornerRadius = img_EmpPic.frame.height/2
                        img_EmpPic.clipsToBounds = true
                        img_EmpPic.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                        img_EmpPic.setTitle(String(self.CurrentAdjListData[indexPath.row].EmployeeName)[0], forState: UIControlState.Normal)
                        ContainerView.addSubview(img_EmpPic)
                    }
                    else{
                        let img_EmpPic:  UIImageView = UIImageView(frame: CGRectMake(10, 20, 50, 50))
                        img_EmpPic.layer.borderWidth = 0.5
                        img_EmpPic.layer.masksToBounds = false
                        img_EmpPic.layer.borderColor = UIColor.lightGrayColor().CGColor
                        img_EmpPic.backgroundColor = UIColor(hexString: String("#0CDBFF"))
                        img_EmpPic.layer.cornerRadius = img_EmpPic.frame.height/2
                        img_EmpPic.clipsToBounds = true
                        
                        ContainerView.addSubview(img_EmpPic)
                        
                        if self.CurrentAdjListData[indexPath.row].EmployeePhoto != nil
                        {
                            let employeePic = "http://" + tmpDomainURL + "/Uploadedfiles/Client" + String(self.defaults.stringForKey("ClientID")!) + "/Image/" + String(self.CurrentAdjListData[indexPath.row].EmployeePhoto!)
                            img_EmpPic.setImageWithUrl(NSURL(string: employeePic)!)
                        }
                    }
                    
                    
                    
                }
                
                
                let lbl_EmployeeName : UILabel = UILabel(frame: CGRectMake(70, 5, self.view.frame.size.width / 100 * 55 ,18))
                lbl_EmployeeName.textAlignment = .Left
                lbl_EmployeeName.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
                lbl_EmployeeName.textColor = UIColor(hexString: String("#333333"))
                lbl_EmployeeName.text = String(self.CurrentAdjListData[indexPath.row].EmployeeName)
                ContainerView.addSubview(lbl_EmployeeName)
                
                let lbl_PositionName : UILabel = UILabel(frame: CGRectMake(70, 23, self.view.frame.size.width / 100 * 45 ,18))
                lbl_PositionName.textAlignment = .Left
                lbl_PositionName.textColor = UIColor(hexString: String("#666666"))
                lbl_PositionName.font = UIFont(name:"HelveticaNeue", size: 10.0)
                lbl_PositionName.text = String(self.CurrentAdjListData[indexPath.row].PositionName)
                ContainerView.addSubview(lbl_PositionName)
                
                let lbl_DateLine : UILabel = UILabel(frame: CGRectMake(70 , 41, self.view.frame.size.width / 100 * 70,18))
                lbl_DateLine.textAlignment = .Left
                lbl_DateLine.textColor = UIColor(hexString: String("#666666"))
                lbl_DateLine.font = UIFont(name:"HelveticaNeue", size: 10.0)
                lbl_DateLine.setFAText(prefixText: "", icon: FAType.FACalendar, postfixText: "   " + String(self.CurrentAdjListData[indexPath.row].DateLineDesc), size: 10)
                ContainerView.addSubview(lbl_DateLine)
                
                let lbl_Hours : UILabel = UILabel(frame: CGRectMake(70 , 59, self.view.frame.size.width / 100 * 70,18))
                lbl_Hours.textAlignment = .Left
                lbl_Hours.textColor = UIColor(hexString: String("#666666"))
                lbl_Hours.font = UIFont(name:"HelveticaNeue", size: 10.0)
                lbl_Hours.setFAText(prefixText: "", icon: FAType.FAClockO, postfixText: "    " +  String(self.CurrentAdjListData[indexPath.row].TotalHours) + " Hrs", size: 10)
                ContainerView.addSubview(lbl_Hours)
                
                if self.CurrentAdjListData[indexPath.row].Status == 1
                {
                    self.CreateDynamicButton(cell, ButtonMode: 0, Xpos: 70, row: indexPath.row)
                    self.CreateDynamicButton(cell, ButtonMode: 1, Xpos: 85, row: indexPath.row)
                }
                else
                {
                    self.CreateDynamicLabel(cell, row: indexPath.row )
                }
                
                let GestureLeaveDetail = UITapGestureRecognizer(target: self, action: "ShowAdjustmentDetail:")
                cell.addGestureRecognizer(GestureLeaveDetail)
            }
            else
            {
                let lbl_More : UILabel = UILabel(frame: CGRectMake(70, 28, self.view.frame.size.width / 100 * 45 ,22))
                lbl_More.textAlignment = .Center
                lbl_More.textColor = UIColor.whiteColor()
                lbl_More.backgroundColor = UIColor(hexString: "#2190ea")
                lbl_More.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
                lbl_More.text = "Load More . . ."
                cell.addSubview(lbl_More)

                let GestureLeaveDetail = UITapGestureRecognizer(target: self, action: "LoadMore:")
                cell.addGestureRecognizer(GestureLeaveDetail)
            }
            
            
        }
        
        
        
        
        
        return cell
    }
    
    
    func ShowAdjustmentDetail(sender: UITapGestureRecognizer)
    {
        let tapLocation = sender.locationInView(self.AdjListView)
        let indexPath = self.AdjListView.indexPathForRowAtPoint(tapLocation)
        let  objAdjustmentDetails = AdjustmentDetails()
        objAdjustmentDetails.AdjDetails = self.CurrentAdjListData[(indexPath?.row)!]
        objAdjustmentDetails.Mode = self.customSC!.selectedSegmentIndex
        objAdjustmentDetails.OnCompletion = HandleDismissEvent
        objAdjustmentDetails.RowIndex = indexPath?.row
        self.presentViewController(objAdjustmentDetails, animated: true, completion: nil)
        
    }
    
    func LoadMore(sender: UITapGestureRecognizer)
    {
        self.LoadMoreClicked = 1
        GetAdjList()
        //AdjListView.reloadData()
    }
    func scrollToLastRow() {
        let indexPath = NSIndexPath(forRow: self.CurrentAdjListData.count - 1, inSection: 0)
        self.AdjListView.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Bottom, animated: true)
    }
    
    //Handles dismiss event of modal view
    func HandleDismissEvent(RowID:Int , ActionCode:String) -> Void
    {
        if ActionCode == "1"
        {
            self.CurrentCell  =  self.AdjListView.cellForRowAtIndexPath( NSIndexPath(forRow: RowID, inSection: 0))!
            self.arrTeamAdjList[RowID].StatusDesc = "Approved"
            self.arrTeamAdjList[RowID].Status = 2
            
            self.CurrentAdjListData[RowID].StatusDesc = "Approved"
            self.CurrentAdjListData[RowID].Status = 2
            self.CreateDynamicLabel( self.CurrentCell,row: RowID,StatusDesc: "Approved")
            
            dispatch_async(dispatch_get_main_queue(),{
                for subview in   self.CurrentCell.subviews
                {
                    if subview.tag  == RowID + 1
                    {
                        
                        subview.removeFromSuperview()
                    }
                    
                }})
            
        }
        else if ActionCode == "2"
        {
            self.CurrentCell  =  self.AdjListView.cellForRowAtIndexPath( NSIndexPath(forRow: RowID, inSection: 0))!
            self.arrTeamAdjList[RowID].StatusDesc = "Rejected"
            self.arrTeamAdjList[RowID].Status = 3
            
            self.CurrentAdjListData[RowID].StatusDesc = "Rejected"
            self.CurrentAdjListData[RowID].Status = 3
            
            self.CreateDynamicLabel( self.CurrentCell,row: RowID,StatusDesc: "Rejected")
            dispatch_async(dispatch_get_main_queue(),{
                for subview in   self.CurrentCell.subviews
                {
                    if subview.tag   == RowID + 1
                    {
                        
                        subview.removeFromSuperview()
                    }
                    
                }})
            
        }
        
    }
    
    
    
 
    //Bind The status label with respective color
    func CreateDynamicLabel(cell:UITableViewCell , row:Int , StatusDesc: String = "" )
    {
        let lbl_LeaveStatus : UILabel = UILabel(frame: CGRectMake(self.view.frame.size.width / 100 * 50, 40, self.view.frame.size.width / 100 * 45 ,18))
        lbl_LeaveStatus.textAlignment = .Right
        if (self.CurrentAdjListData[row].Status == 1)
        {
            lbl_LeaveStatus.textColor = UIColor(hexString:"#ffbb5c")
            
        } else if (self.CurrentAdjListData[row].Status == 2) {
            lbl_LeaveStatus.textColor = UIColor(hexString: "#48c9b0")
        }
        else {
            lbl_LeaveStatus.textColor = UIColor(hexString: "#ef6565")
        }
        
        lbl_LeaveStatus.font = UIFont(name:"HelveticaNeue", size: 10.0)
        
        if StatusDesc == ""
        {
            lbl_LeaveStatus.text = String(self.CurrentAdjListData[row].StatusDesc)
        }else{
            
            lbl_LeaveStatus.text = StatusDesc
        }
        
        cell.addSubview(lbl_LeaveStatus)
    }
    
    //Create dynamically Approve/Reject Button with action
    func CreateDynamicButton(cell:UITableViewCell , ButtonMode:Int , Xpos: CGFloat, row: Int)
    {
        let btnApprove : UIButton = UIButton(frame: CGRectMake(self.view.frame.size.width / 100 * Xpos , 36, 30,30))
        if ButtonMode == 0
        {
            btnApprove.backgroundColor = UIColor(hexString: String("#48C9B0"))
            btnApprove.setFAIcon(FAType.FACheck, iconSize: 20, forState:  .Normal)
            btnApprove.addTarget(self, action: "ApproveAdj:", forControlEvents: .TouchUpInside)
            btnApprove.tag = row + 1
        }
        else
        {
            btnApprove.backgroundColor = UIColor(hexString: String("#F46868"))
            btnApprove.setFAIcon(FAType.FAClose, iconSize: 20, forState:  .Normal)
            btnApprove.addTarget(self, action: "RejectAdj:", forControlEvents: .TouchUpInside)
            btnApprove.tag = row + 1
        }
        
        btnApprove.layer.borderWidth = 0
        btnApprove.layer.cornerRadius =  btnApprove.frame.height/2
        btnApprove.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        
        cell.addSubview(btnApprove)
    }
    
    //Approve Adjustment Action
    func ApproveAdj(Sender: UIButton!)
    {
        self.arrTeamAdjList[Sender.tag - 1].StatusDesc = "Approved"
        self.arrTeamAdjList[Sender.tag - 1].Status = 2
        
        self.CurrentAdjListData[Sender.tag - 1].StatusDesc = "Approved"
        self.CurrentAdjListData[Sender.tag - 1].Status = 2
        
        self.CurrentRowIndex = Sender.tag
        self.CurrentActionType = 2
        self.CurrentCell  =  self.AdjListView.cellForRowAtIndexPath( NSIndexPath(forRow: Sender.tag - 1, inSection: 0))!
        self.ProcessLeaveRequest("1", RowIndex: Sender.tag - 1)
        
    }
    
    //Reject Adjustment action
    func RejectAdj(Sender: UIButton!)
    {
        self.arrTeamAdjList[Sender.tag - 1].StatusDesc = "Approved"
        self.arrTeamAdjList[Sender.tag - 1].Status = 3
        
        self.CurrentAdjListData[Sender.tag - 1].StatusDesc = "Approved"
        self.CurrentAdjListData[Sender.tag - 1].Status = 3
        
        self.CurrentActionType = 3
        self.CurrentRowIndex = Sender.tag
        self.CurrentCell  =  self.AdjListView.cellForRowAtIndexPath( NSIndexPath(forRow: Sender.tag - 1, inSection: 0))!
        self.ProcessLeaveRequest("2", RowIndex: Sender.tag - 1)
    }
    
    func AddGesture()
    {
        swipeRight = UISwipeGestureRecognizer(target: self, action: "LoadTeamLeaveList:")
        swipeRight.direction = UISwipeGestureRecognizerDirection.Right
        self.view.addGestureRecognizer(swipeRight)
        
        swipeLeft = UISwipeGestureRecognizer(target: self, action: "LoadSelfLeaveList:")
        swipeRight.direction = UISwipeGestureRecognizerDirection.Left
        self.view.addGestureRecognizer(swipeLeft)
    }
    
    func RemoveGesture()
    {
        self.view.removeGestureRecognizer(swipeLeft)
        self.view.removeGestureRecognizer(swipeRight)
    }
    
 //# MARK: Attendance Adjustment Approval Methods
    func ProcessLeaveRequest(ActionCode: String, RowIndex: Int )
    {
        self.RemoveGesture()
        
        //Loading Panel
        objLoadingIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, Viewframe.width, Viewframe.height))
        objLoadingIndicaor.activityIndicatorViewStyle = .WhiteLarge
        let objPosition =  self.view.center
        objLoadingIndicaor.center = objPosition
        objLoadingIndicaor.hidesWhenStopped = true
        objLoadingIndicaor.color = UIColor.grayColor()
        self.view.addSubview(objLoadingIndicaor)
        objLoadingIndicaor.startAnimating()
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            
            objCallBack.MethodName = "ProcessAttendanceNote"
            
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pRequestID",value: String(self.CurrentAdjListData[RowIndex].RequestID))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pAction",value: ActionCode)
            objCallBack.ParameterList.append(objparam)
            
            objCallBack.post(ApproveOnSuccess, OnError: ApproveOnError)
            
        }
        
    }
    
    //Response on error callback
    func ApproveOnError(Response: NSError ){
        
    }
    
    func ApproveOnSuccess (ResultString :NSString? ) {
        
        if   self.CurrentActionType == 2
        {
            dispatch_async(dispatch_get_main_queue(),{
                self.arrTeamAdjList[self.CurrentRowIndex - 1].StatusDesc = "Approved"
                self.arrTeamAdjList[self.CurrentRowIndex - 1].Status = 2
                
                self.CurrentAdjListData[self.CurrentRowIndex - 1].StatusDesc = "Approved"
                self.CurrentAdjListData[self.CurrentRowIndex - 1].Status = 2
                
                self.CreateDynamicLabel( self.CurrentCell,row: self.CurrentRowIndex - 1,StatusDesc: "Approved")
                dispatch_async(dispatch_get_main_queue(),{
                    self.objLoadingIndicaor.stopAnimating()
                    self.AddGesture()
                    DoneHUD.showInView(self.view, message: "Done")
                    for subview in   self.CurrentCell.subviews
                    {
                        if subview.tag  == self.CurrentRowIndex
                        {
                            
                            subview.removeFromSuperview()
                        }
                        
                    }
                })
                
            })
        }else{
            dispatch_async(dispatch_get_main_queue(),{
                self.arrTeamAdjList[self.CurrentRowIndex - 1].StatusDesc = "Rejected"
                self.arrTeamAdjList[self.CurrentRowIndex - 1].Status = 3
                
                self.CurrentAdjListData[self.CurrentRowIndex - 1].StatusDesc = "Rejected"
                self.CurrentAdjListData[self.CurrentRowIndex - 1].Status = 3
                
                self.CreateDynamicLabel( self.CurrentCell,row: self.CurrentRowIndex - 1,StatusDesc: "Rejected")
                dispatch_async(dispatch_get_main_queue(),{
                    self.objLoadingIndicaor.stopAnimating()
                    self.AddGesture()
                    DoneHUD.showInView(self.view, message: "Done")
                    for subview in   self.CurrentCell.subviews
                    {
                        if subview.tag  == self.CurrentRowIndex
                        {
                            
                            subview.removeFromSuperview()
                        }
                        
                    }
                    
                })
            })
            
        }
        
    }
    
    
}



