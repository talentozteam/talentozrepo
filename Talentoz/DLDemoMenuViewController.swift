//
//  DLDemoMenuViewController.swift
//  DLHamburguerMenu
//
//  Created by Nacho on 5/3/15.
//  Copyright (c) 2015 Ignacio Nieto Carvajal. All rights reserved.
//

import UIKit


class DLDemoMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    // outlets
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet var EmployeePic: UIImageView!
    
    @IBOutlet var lblUserName: UILabel!
    
    @IBOutlet var lblPosition: UILabel!
    
    @IBOutlet var lblBusinessUnit: UILabel!
    
    @IBOutlet var lblWorkLocation: UILabel!
    
    @IBOutlet var MenuView: UITableView!
    
    internal var UINavigationBarTitle: String!
    internal var navigationBar : UINavigationBar!
    
    @IBOutlet var stView: UIStackView!
    
    @IBOutlet var MenuTable: UITableViewCell!
   
    
    @IBOutlet var MenuTablee: UIStackView!
    // data
    //var segues = ["About Us" , "My Profile", "My Team", "Leaves" , "Log out" ]
     var segues = ["About Us" , "My Profile", "My Team", "Leaves","Attendance","Claims", "Log out" ]
    
    let objCallBack = AjaxCallBack()
    let defaults = NSUserDefaults.standardUserDefaults()
    
    override func viewDidLoad() {
        
        self.title = "Home"
        
        let objRole = RoleManager()
        
        if !objRole.ISManager{
       // segues = [ "About Us" , "My Profile", "Leaves" , "Log out"]
          segues = [ "About Us" , "My Profile", "Leaves","Attendance","Claims", "Log out"]
        
        }
        
        dispatch_async(dispatch_get_main_queue(),{
            
            self.EmployeePic.frame = CGRectMake(self.view.frame.size.width / 100 * 35 , 30  ,65, 65)
            
            self.lblUserName.frame = CGRectMake(self.view.frame.size.width / 100 * 5, 100 ,self.view.frame.size.width / 100 * 80, 15)
            
            self.lblPosition.frame = CGRectMake(self.view.frame.size.width / 100 * 5,  120 ,self.view.frame.size.width / 100 * 80, 15)
            
            self.lblBusinessUnit.frame = CGRectMake(self.view.frame.size.width / 100 * 5,  140 ,self.view.frame.size.width / 100 * 80, 15)
            
            self.lblWorkLocation.frame = CGRectMake(self.view.frame.size.width / 100 * 5,  160	 ,self.view.frame.size.width / 100 * 80, 15)
            
            
            self.MenuView.frame = CGRectMake(self.view.frame.size.width / 100 * 5,  250	 ,self.view.frame.size.width / 100 * 80, 15)
            
            self.stView.frame = CGRectMake(self.stView.frame.origin.x,  self.stView.frame.origin.y	 ,self.stView.frame.width, 300)
            
             self.MenuTablee.frame = CGRectMake(self.MenuTablee.frame.origin.x,  self.MenuTablee.frame.origin.y	 ,self.MenuTablee.frame.width, 150)
            
        })
        
        super.viewDidLoad()
      // self.LoadTopNavigation( )
        // Do any additional setup after loading the view.
        self.BindEmployeeProfile()
    }
    
    //Binds employee profile details
    func BindEmployeeProfile() -> Void{
        let defaults = NSUserDefaults.standardUserDefaults()
        
   
 
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            objCallBack.MethodName = "GetEmployeeProfile"
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
          
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objCallBack.post(EmpProfileOnComplete, OnError: EmpProfileOnErrorOnError)
          
        }
    
    }
    
    

    
    
    //Response on complete callback
    func EmpProfileOnComplete(ResultString: NSString? ){
        
        
        
        dispatch_async(dispatch_get_main_queue(),{
            
           //  let objUserInfo = self.objCallBack.GetJSONArrayResult(ResultString)
           let objUserInfo =   JSON(data: self.objCallBack.GetJSONData(ResultString)!)
            
          if (objUserInfo.count > 0 )
          {
            
            if let tmpDomainURL = self.defaults.stringForKey("DomainURL")
            {
            
                
                if  (objUserInfo[0]["EmployeePhoto"] == nil  )
                {               
                    let img_EmpPic:  UIButton = UIButton(frame: CGRectMake(self.view.frame.size.width / 100 * 35 , 30  ,65, 65))
                    img_EmpPic.layer.borderWidth = 0.5
                    img_EmpPic.layer.masksToBounds = false
                    img_EmpPic.layer.borderColor = UIColor.lightGrayColor().CGColor
                    img_EmpPic.backgroundColor = UIColor(hexString: String("#0CDBFF"))
                    img_EmpPic.layer.cornerRadius = img_EmpPic.frame.height/2
                    img_EmpPic.clipsToBounds = true
                    img_EmpPic.titleLabel!.font = UIFont(name: "helvetica-bold", size: 25)
                    img_EmpPic.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                    img_EmpPic.setTitle( (objUserInfo[0]["EmployeeName"].string)![0], forState: UIControlState.Normal)
                    self.view.addSubview(img_EmpPic)
                     }
                else
                  {
                    
                        let employeePic = "http://" + tmpDomainURL + "/Uploadedfiles/Client" + self.defaults.stringForKey("ClientID")! + "/Image/" + objUserInfo[0]["EmployeePhoto"].string!
                        self.EmployeePic.setImageWithUrl(NSURL(string: (employeePic))!)
                        self.EmployeePic.layer.borderWidth = 1
                        self.EmployeePic.layer.masksToBounds = false
                        self.EmployeePic.layer.borderColor = UIColor.whiteColor().CGColor
                        self.EmployeePic.layer.cornerRadius = self.EmployeePic.frame.height/2
                        self.EmployeePic.clipsToBounds = true
                   
                    
                    
                   
                    
                }
                    
                self.lblUserName.text = objUserInfo[0]["EmployeeName"].string
                if (objUserInfo[0]["PositionName"] != nil)
                {
                self.lblPosition.text =  objUserInfo[0]["PositionName"].string
                }
                
                
                if  (objUserInfo[0]["BusinessUnitName"] != nil)
                {
                     self.lblBusinessUnit.text =  objUserInfo[0]["BusinessUnitName"].string
                }
                if  (objUserInfo[0]["WorkLocationName"] != nil)
                {
                     self.lblWorkLocation.text =  objUserInfo[0]["WorkLocationName"].string
                }
                
                
            }
            
            }
           	
            
        })
        
    }
    
    
    
    //Response on error callback
    func EmpProfileOnErrorOnError(Response: NSError ){
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UITableViewDelegate&DataSource methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return segues.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MenuCell", forIndexPath: indexPath) 
        
        
        switch String(segues[indexPath.row]) {
        case "My Profile":
          
            cell.textLabel?.setFAText(prefixText: "", icon: FAType.FAUser, postfixText: "   " + segues[indexPath.row], size: 17)
            
            
        case "My Team":
               cell.textLabel?.setFAText(prefixText: "", icon: FAType.FAGroup, postfixText: "   " + segues[indexPath.row], size: 17)
        case "Leaves":
               cell.textLabel?.setFAText(prefixText: "", icon: FAType.FACalendar, postfixText: "   " + segues[indexPath.row], size: 17)
        case "Attendance":
            cell.textLabel?.setFAText(prefixText: "", icon: FAType.FAClockO, postfixText: "   " + segues[indexPath.row], size: 17)
        case "Claims":
            cell.textLabel?.setFAText(prefixText: "", icon: FAType.FAMoney, postfixText: "   " + segues[indexPath.row], size: 17)
        case "Log out":
               cell.textLabel?.setFAText(prefixText: "", icon: FAType.FAPowerOff, postfixText: "   " + segues[indexPath.row], size: 17)
            
        case "About Us":
            cell.textLabel?.setFAText(prefixText: "", icon: FAType.FAInfoCircle, postfixText: "   " + segues[indexPath.row], size: 17)
            
        default:  break
            
        }
     
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let id = tableView.cellForRowAtIndexPath(indexPath)
        
        if (String(id!.textLabel!.text!).containsString("My Profile")) == true {
           let  ProfileVCon = ProfileVC()
           self.presentViewController(ProfileVCon, animated: true, completion: nil)
        }
        else  if (String(id!.textLabel!.text!).containsString("My Team")) == true {
            let  vwMyTeam = MyTeam()
            self.presentViewController(vwMyTeam, animated: true, completion: nil)
        }
        else  if (String(id!.textLabel!.text!).containsString("Leaves")) == true {
            let storyboard = UIStoryboard(name: "LMSDashBoard", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("LMSDashBoardVW")
            self.presentViewController(vc, animated: true, completion: nil)
        }
        else  if (String(id!.textLabel!.text!).containsString("Attendance")) == true {
            let storyboard1 = UIStoryboard(name: "TADashBoard", bundle: nil)
            let vc1 = storyboard1.instantiateViewControllerWithIdentifier("TADashBoardVW")
            self.presentViewController(vc1, animated: true, completion: nil)
            
        }
        else  if (String(id!.textLabel!.text!).containsString("Claims")) == true {
            
            let Claimstoryboard = UIStoryboard(name: "ClaimsDashBoard", bundle: nil)
            let vc1 = Claimstoryboard.instantiateViewControllerWithIdentifier("ClaimsDashBoardVC")
            self.presentViewController(vc1, animated: true, completion: nil)
        }
        else  if (String(id!.textLabel!.text!).containsString("Log out")) == true {
            
            defaults.removeObjectForKey("UserName")
            defaults.removeObjectForKey("Password")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("Main")
            self.presentViewController(vc, animated: true, completion: nil)
            
        }
         else  if (String(id!.textLabel!.text!).containsString("About Us")) == true {            
            
            let  abtUs = AboutUs()
            self.presentViewController(abtUs, animated: true, completion: nil)
        }
        
        
 
                
        
        
    }
    
    // MARK: - Navigation
    
    func mainNavigationController() -> DLHamburguerNavigationController {
        return self.storyboard?.instantiateViewControllerWithIdentifier("DLDemoNavigationViewController") as! DLHamburguerNavigationController
    }
    
   
    
}
