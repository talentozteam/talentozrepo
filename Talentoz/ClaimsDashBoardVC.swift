//
//  ClaimsDashBoardVC.swift
//  Talentoz
//
//  Created by forziamac on 19/04/16.
//  Copyright © 2016 Forzia Tech Private Ltd. All rights reserved.
//

import UIKit

class ClaimsDashBoardVC: BaseTabBarVC,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate  , UIPageViewControllerDataSource      {
    
     //# MARK: Public Variables
    private var pageViewController: UIPageViewController?
    var objCallBackTA = AjaxCallBack()
     var objCallBackClaim = AjaxCallBack()
    var objCallBack = AjaxCallBack()
    var objCallBackAP = AjaxCallBack()
    let defaults = NSUserDefaults.standardUserDefaults()
    var ClaimQuickInfo: Talentoz.JSON!
    var ClaimsApprovalJSON: Talentoz.JSON!
    var dClaimsAdjCollectionView : UICollectionView!
    var CurrentActionCode : String!
    var iSlideIndex: Int!
    var iSlideCount: Int!
    let objRole = RoleManager()
    var objLBActivityIndicaor : UIActivityIndicatorView!
    var objLRActivityIndicaor : UIActivityIndicatorView!
    let vscrollView = UIScrollView(frame: UIScreen.mainScreen().bounds)
    var iDraftTodosLinkHeight:CGFloat = 0
    var lbl_Approvalref : UILabel! = nil
   
    
     //# MARK: Default Methods
    override func viewDidLoad() {
        super.UINavigationBarTitle = "Claims"
        self.view.addSubview(vscrollView)
        
        self.vscrollView.contentSize = CGSize(width: self.view.frame.width, height: 745)
        self.vscrollView.backgroundColor = UIColor.whiteColor()
        super.viewDidLoad()
        GetSliderPageDetails()
        LoadQuickAction()
        ShowDraftingClaimTodo()
        
    }
    
      //# MARK: Page Controller
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        var index = (viewController as! ClaimsSliderB).IndexNo
        
        if (index == 0) || (index == NSNotFound) || (self.iSlideCount == 0) {
            return nil
        }
        
        index = index - 1
        
        return LoadSlide(index)
        
        
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        
       
        var index = (viewController as! ClaimsSliderB).IndexNo
        
        if index == NSNotFound || (self.iSlideCount == 0) {
            return nil
        }
        
        index = index + 1
        
        if( index == self.iSlideCount) {
            return nil
        }
        
        return LoadSlide(index)
        
    }
    
    // Enables pagination dots
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        if( self.ClaimQuickInfo != nil)
        {
            if (self.ClaimQuickInfo.count == 0)
            {
                self.iSlideCount = 0
                return 1
            }
            else
            {
                self.iSlideCount = self.ClaimQuickInfo.count
                return self.ClaimQuickInfo.count
            }
        }
        else
        {
            self.iSlideCount = 0
            return 1
        }
        
    }
    
    // This only gets called once, when setViewControllers is called
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    
    func LoadSlide(IndexRow: Int) -> ClaimsSliderB {
        
        let vc = storyboard!.instantiateViewControllerWithIdentifier("ClaimsSliderVC") as! ClaimsSliderB
        vc.IndexNo = IndexRow
        vc.ClaimDetail = self.ClaimQuickInfo[IndexRow]
        self.iSlideIndex = IndexRow
        return vc
    }
    
 

    
    private func createPageViewController(Mode: Int, CalculatedTime: Int) {
        
        
        
        let pageController = self.storyboard!.instantiateViewControllerWithIdentifier("PageViewMainVc") as! UIPageViewController
        pageController.dataSource = self
        
        let Vc = storyboard!.instantiateViewControllerWithIdentifier("ClaimsSliderVC")  as! ClaimsSliderB
        
        if(self.ClaimQuickInfo != nil)
        {
        if (self.ClaimQuickInfo.count > 0)
        {
            Vc.IndexNo = 0
            Vc.ClaimDetail = self.ClaimQuickInfo[0]
            
        }
        }
        self.iSlideIndex = 0
        
        pageController.setViewControllers([Vc] , direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
        pageViewController = pageController
        addChildViewController(pageViewController!)
        self.vscrollView.addSubview(pageViewController!.view)
        self.pageViewController!.view.frame = CGRectMake(0,60, self.vscrollView.frame.width, 200)
        
        
    }
    
    private func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = UIColor(hexString: "#cccccc")
        appearance.currentPageIndicatorTintColor = UIColor(hexString: "#2190ea")
        appearance.backgroundColor = UIColor(hexString: "#fffffe")
        appearance.frameForAlignmentRect(self.vscrollView.frame)
        appearance.frame = CGRectMake(0,25, self.vscrollView.frame.width, 200)
        
    }
    
    
    
    func GetSliderPageDetails()
    {
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            
            objCallBackClaim.MethodName = "GetClaimAmountDashboard"
            let ResultJSON = objCallBackClaim.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBackClaim.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBackClaim.ParameterList.append(objparam)
            
            objCallBackClaim.post(SliderInfoOnComplete, OnError: SliderInfoOnErrorOnError)
            
        }
    }
    
    //Response on error callback
    func SliderInfoOnErrorOnError(Response: NSError ){
        
    }
    
    func SliderInfoOnComplete (ResultString :NSString? ) {
        
        dispatch_async(dispatch_get_main_queue(),{
            if let tempdata = self.objCallBackClaim.GetJSONData(ResultString)
           {
                self.ClaimQuickInfo = JSON(data: self.objCallBackClaim.GetJSONData(ResultString)!)
            }
            self.createPageViewController(0, CalculatedTime: 0)
            
            self.setupPageControl()
            
            if self.objRole.ISManager {
                
                let ContainerView2 = UIView(frame: CGRectMake(0, 425 + self.iDraftTodosLinkHeight, self.view.frame.size.width, 1))
                ContainerView2.layer.borderColor = UIColor(hexString: "#cccccc")?.CGColor
                ContainerView2.layer.borderWidth = 1
                self.vscrollView.addSubview(ContainerView2)
                
                
                self.objLBActivityIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, ContainerView2.frame.size.width, ContainerView2.frame.size.height))
                self.objLBActivityIndicaor.activityIndicatorViewStyle = .WhiteLarge
                var objPosition =  ContainerView2.center
                objPosition.y =  objPosition.y.advancedBy(20)
                self.objLBActivityIndicaor.center = objPosition
                self.objLBActivityIndicaor.hidesWhenStopped = true
                self.objLBActivityIndicaor.color = UIColor.grayColor()
                self.vscrollView.addSubview(self.objLBActivityIndicaor)
                self.objLBActivityIndicaor.startAnimating()
                
                self.FetEmployeeApprovalAdj()
            }
            else
            {
                self.vscrollView.contentSize = CGSize(width: self.view.frame.width, height: 500)
            }
            
        })
        
    }
    
    //# MARK: Quick Action Process
    
    func LoadQuickAction()
    {
        
        let lbl_Title : UILabel = UILabel(frame: CGRectMake(10, 260, self.view.frame.size.width,25))
        lbl_Title.backgroundColor = UIColor.whiteColor()
        lbl_Title.textAlignment = .Left
        lbl_Title.font = UIFont(name:"helvetica-bold", size: 14)
        lbl_Title.text = "Quick Actions"
        vscrollView.addSubview(lbl_Title)
        
        let ContainerView = UIView(frame: CGRectMake(0,285, self.view.frame.size.width, 1))
        ContainerView.layer.borderColor = UIColor(hexString: "#cccccc")?.CGColor
        ContainerView.layer.borderWidth = 1
        vscrollView.addSubview(ContainerView)
        
        
        
        
        let AddClaimView=UIView(frame: CGRectMake((self.view.frame.width / 100 * 4), 300, (self.view.frame.width / 100 * 40), 75))
        AddClaimView.backgroundColor = UIColor(hexString: "#426F42")
        let gestureAdjList = UITapGestureRecognizer(target: self, action: #selector(ClaimsDashBoardVC.AddClaimAction(_:)))
        let gestureAdjList1 = UITapGestureRecognizer(target: self, action: #selector(ClaimsDashBoardVC.AddClaimAction(_:)))
        
        let lbl_ClaimListViewIcon : UIButton = UIButton(frame: CGRectMake((AddClaimView.frame.width / 100 * 2), 15, AddClaimView.frame.width,23))
        lbl_ClaimListViewIcon.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        lbl_ClaimListViewIcon.setFAIcon(FAType.FAFile, iconSize: 25, forState: .Normal)
        lbl_ClaimListViewIcon.addGestureRecognizer(gestureAdjList1)
        AddClaimView.addSubview(lbl_ClaimListViewIcon)
        
        let lbl_ClaimListViewTitle : UILabel = UILabel(frame: CGRectMake((AddClaimView.frame.width / 100 * 2), 45, AddClaimView.frame.width,18))
        lbl_ClaimListViewTitle.textColor = UIColor.whiteColor()
        lbl_ClaimListViewTitle.textAlignment = .Center
        lbl_ClaimListViewTitle.font = lbl_Title.font.fontWithSize(14.0)
        lbl_ClaimListViewTitle.text = "Add Claim"
        AddClaimView.addSubview(lbl_ClaimListViewTitle)
        AddClaimView.addGestureRecognizer(gestureAdjList)
        
        
        vscrollView.addSubview(AddClaimView)
        
        
        let ClaimRequestView=UIView(frame: CGRectMake((self.view.frame.width / 100 * 56), 300, (self.view.frame.width / 100 * 40), 75))
        ClaimRequestView.backgroundColor = UIColor(hexString: "#71C671")
        let gestureHoliday = UITapGestureRecognizer(target: self, action: #selector(ClaimsDashBoardVC.ClaimReqListAction(_:)))
        let gestureHoliday1 = UITapGestureRecognizer(target: self, action: #selector(ClaimsDashBoardVC.ClaimReqListAction(_:)))
        
        
        let lbl_ClaimReqListViewIcon : UIButton = UIButton(frame: CGRectMake((ClaimRequestView.frame.width / 100 * 2), 15, ClaimRequestView.frame.width,23))
        lbl_ClaimReqListViewIcon.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        lbl_ClaimReqListViewIcon.setFAIcon(FAType.FAList, iconSize: 25, forState: .Normal)
        ClaimRequestView.addGestureRecognizer(gestureHoliday)
        ClaimRequestView.addSubview(lbl_ClaimReqListViewIcon)
        
        
        let lbl_ClaimReqListViewTitle : UILabel = UILabel(frame: CGRectMake((ClaimRequestView.frame.width / 100 * 2), 45, ClaimRequestView.frame.width,18))
        lbl_ClaimReqListViewTitle.textColor = UIColor.whiteColor()
        lbl_ClaimReqListViewTitle.textAlignment = .Center
        lbl_ClaimReqListViewTitle.font = lbl_Title.font.fontWithSize(14.0)
        lbl_ClaimReqListViewTitle.text = "Claim Requests"
        lbl_ClaimReqListViewIcon.addGestureRecognizer(gestureHoliday1)
        ClaimRequestView.addSubview(lbl_ClaimReqListViewTitle)
        
        
        vscrollView.addSubview(ClaimRequestView)
        
        
        
    }
    
    func ShowTeamClaimList(Sender: UIButton!)
    {
        let ClaimListObj = ClaimsRequestListB()
        ClaimListObj.ModeofShow = 1
        self.presentViewController(ClaimListObj, animated: true, completion: nil)
    }
    
    func ClaimReqListAction(sender: UIView) {
        
        let  ClaimListView = ClaimsRequestListB()
        self.presentViewController(ClaimListView, animated: true, completion: nil)
        
        
    }
    
    func AddClaimAction(sender: UIView) {
        let  AddClaimView = AddClaim()
        self.presentViewController(AddClaimView, animated: true, completion: nil)
        
    }
    
    //# MARK: Drafting Claims Count
    
    func ShowDraftingClaimTodo()
    {
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            objCallBackTA.MethodName = "GetClaimUserItemsCount"
            let ResultJSON = objCallBackTA.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBackTA.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBackTA.ParameterList.append(objparam)
            
            objCallBackTA.post(ShowDraftingClaimTodoInfoOnComplete, OnError: ShowDraftingClaimTodoInfoOnError)
            
        }

    }
    
    //Response on error callback
    func ShowDraftingClaimTodoInfoOnError(Response: NSError ){
        
    }
    
    func ShowDraftingClaimTodoInfoOnComplete (ResultString :NSString? ) {
        
        dispatch_async(dispatch_get_main_queue(),{
              let DraftCount = self.objCallBackTA.GetJSONResultString(ResultString)
            
              if (DraftCount != "0" )
              {
                let DraftView=UIView(frame: CGRectMake(0, 397,self.vscrollView.frame.width, 35))
                DraftView.backgroundColor = UIColor.whiteColor()
                
                let gesture = UITapGestureRecognizer(target: self, action: #selector(ClaimsDashBoardVC.SubmitClaim(_:)))
                DraftView.addGestureRecognizer(gesture)
                
                
                self.vscrollView.addSubview(DraftView)
                DraftView.layer.borderWidth = 1;
                DraftView.layer.borderColor = UIColor(hexString: "#eeeeee")?.CGColor
                
                
                
                self.iDraftTodosLinkHeight = 50
                
                let lbl_ClaimCount : UILabel = UILabel(frame: CGRectMake(10 , 2, DraftView.frame.width / 100 * 10, 30	))
                lbl_ClaimCount.textColor = UIColor(hexString: "#2190ea")
                lbl_ClaimCount.textAlignment = .Left
                
                lbl_ClaimCount.font = UIFont(name: "HelveticaNeue-bold", size: 24)
                lbl_ClaimCount.text = String(DraftCount!)
                DraftView.addSubview(lbl_ClaimCount)
                
                var CalPercenct : CGFloat = 0.0
                
                if(DraftCount?.characters.count > 1)
                {
                    CalPercenct = 2.5
                }
                
                let lbl_ClaimText : UILabel = UILabel(frame: CGRectMake(DraftView.frame.width / 100 * (8.5 + CalPercenct), 3, DraftView.frame.width / 100 * 70, 30))
                lbl_ClaimText.textColor = UIColor(hexString: "#2190ea")
                
                lbl_ClaimText.font = UIFont(name: "HelveticaNeue-bold", size: 14)
                lbl_ClaimText.text = String(" Claims to be submitted ")
                DraftView.addSubview(lbl_ClaimText)
                
                let lbl_DirSymbol : UILabel = UILabel(frame: CGRectMake(DraftView.frame.width / 100 * 95 , 2, DraftView.frame.width / 100 * 20, 30))
                lbl_DirSymbol.textColor = UIColor(hexString: "#333333")
                lbl_DirSymbol.font = lbl_DirSymbol.font.fontWithSize(18.0)
                lbl_DirSymbol.setFAIcon(FAType.FAAngleRight, iconSize: 18.0)
                
                
                DraftView.addSubview(lbl_DirSymbol)
                
                
              }
            
            
            
        })
    }
    
    func SubmitClaim(sender:UITapGestureRecognizer) {
        let objClaimList = ClaimListByCurrency()
        self.presentViewController(objClaimList, animated: true, completion: nil)
    }
    
     override func goBack() {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("HomeInitialView")
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    //# MARK: Quick Approval Process
    
    // Adj Approval Slider
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return  1
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            
            return CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height / 100 * 30)
            
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell : UICollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("cellIdentifierAdj", forIndexPath: indexPath)
        
        cell.backgroundColor = UIColor.whiteColor()
        cell.frame = CGRectMake(0, 0, dClaimsAdjCollectionView.frame.size.width, dClaimsAdjCollectionView.frame.size.height  )
        
        let scroll: UIScrollView = UIScrollView(frame: CGRectMake(0, 0, dClaimsAdjCollectionView.frame.size.width, 220  ))
        scroll.delegate = self
        scroll.backgroundColor = UIColor.whiteColor()
        cell.addSubview(scroll)
        
        var xbase : CGFloat = 10
        
        var CountMore: Int = 0
        
        if self.ClaimsApprovalJSON.count > 5
        {
            CountMore = 5
        }
        else
        {
            CountMore = self.ClaimsApprovalJSON.count
        }
        
        
        
        for i in 0 ..< CountMore  {
            
                
                let ClaimsApprView=UIView(frame: CGRectMake(xbase, 0, scroll.frame.width / 100 * 80 , scroll.frame.height))
                ClaimsApprView.backgroundColor = UIColor(hexString: "#e9e9e9")
                
                ClaimsApprView.tag = self.ClaimsApprovalJSON[i]["RequestID"].int!
                scroll.addSubview(ClaimsApprView)
                
                let imgView : UIImageView = UIImageView(frame: CGRectMake(10, 12, 50, 50))
                imgView.layer.borderWidth = 1
                imgView.layer.masksToBounds = false
                imgView.layer.borderColor = UIColor.whiteColor().CGColor
                imgView.layer.cornerRadius = imgView.frame.height/2
                imgView.clipsToBounds = true
                ClaimsApprView.addSubview(imgView)
                
                
                
                let lbl_EmpName : UILabel = UILabel(frame: CGRectMake( 70 , 10, ClaimsApprView.frame.width / 100 * 50, 18))
                lbl_EmpName.textColor = UIColor(hexString: "#333333")
                
                
                lbl_EmpName.font = lbl_EmpName.font.fontWithSize(14.0)
                ClaimsApprView.addSubview(lbl_EmpName)
                lbl_EmpName.text = String(self.ClaimsApprovalJSON[i]["EmployeeName"])
                
                
                let lbl_PositionName : UILabel = UILabel(frame: CGRectMake(70 , 28, ClaimsApprView.frame.width / 100 * 72, 18))
                lbl_PositionName.textColor = UIColor(hexString: "#666666")
                
                lbl_PositionName.font = lbl_PositionName.font.fontWithSize(12.0)
                lbl_PositionName.text = String(self.ClaimsApprovalJSON[i]["PositionName"])
                ClaimsApprView.addSubview(lbl_PositionName)
                
                let lbl_WorkLocationName : UILabel = UILabel(frame: CGRectMake(70 , 45, ClaimsApprView.frame.width / 100 * 72, 18))
                lbl_WorkLocationName.textColor = UIColor(hexString: "#666666")
                
                lbl_WorkLocationName.font = lbl_WorkLocationName.font.fontWithSize(12.0)
                ClaimsApprView.addSubview(lbl_WorkLocationName)
                lbl_WorkLocationName.text = String(self.ClaimsApprovalJSON[i]["WorkLocationName"])
                
                let btn_ShowDetail : UIButton = UIButton(frame: CGRectMake(160 , 46, 90, 18))
                btn_ShowDetail.backgroundColor = UIColor.clearColor()
                btn_ShowDetail.setTitleColor(UIColor(hexString: "#2190ea"), forState: UIControlState.Normal)
                btn_ShowDetail.setTitle("Show Detail" , forState: .Normal)
                btn_ShowDetail.titleLabel?.font = UIFont(name: "HelveticaNeue-bold", size: 12)
                btn_ShowDetail.addTarget(self, action: #selector(ClaimsDashBoardVC.ShowClaimDetail(_:)), forControlEvents: .TouchUpInside)
                btn_ShowDetail.tag = i
                ClaimsApprView.addSubview(btn_ShowDetail)
                
                
                if let tmpDomainURL = self.defaults.stringForKey("DomainURL")
                {
                    
                    if self.ClaimsApprovalJSON[i]["EmployeePhoto"] == nil
                    {
                        let img_EmpPic:  UIButton = UIButton(frame: CGRectMake(10, 12, 50, 50))
                        img_EmpPic.layer.borderWidth = 0.5
                        img_EmpPic.layer.masksToBounds = false
                        img_EmpPic.layer.borderColor = UIColor.lightGrayColor().CGColor
                        img_EmpPic.backgroundColor = UIColor.SetRandomColor()
                        img_EmpPic.layer.cornerRadius = img_EmpPic.frame.height/2
                        img_EmpPic.clipsToBounds = true
                        img_EmpPic.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                        img_EmpPic.setTitle(String(self.ClaimsApprovalJSON[i]["EmployeeName"])[0], forState: UIControlState.Normal)
                        ClaimsApprView.addSubview(img_EmpPic)
                    }
                    else{
                        let imgView : UIImageView = UIImageView(frame: CGRectMake(10, 12, 50, 50))
                        imgView.layer.borderWidth = 1
                        imgView.layer.masksToBounds = false
                        imgView.layer.borderColor = UIColor.whiteColor().CGColor
                        imgView.layer.cornerRadius = imgView.frame.height/2
                        imgView.clipsToBounds = true
                        ClaimsApprView.addSubview(imgView)
                        
                        let employeePic = "http://" + tmpDomainURL + "/Uploadedfiles/Client" + String(self.defaults.stringForKey("ClientID")!) + "/Image/" + String(self.ClaimsApprovalJSON[i]["EmployeePhoto"])
                        imgView.setImageWithUrl(NSURL(string: employeePic)!)
                    }
                }
                
                
                
                
                let PeriodView=UIView(frame: CGRectMake(ClaimsApprView.frame.width / 100 * 4, 80, (ClaimsApprView.frame.width) / 100 * 92, 90))
                PeriodView.backgroundColor = UIColor.whiteColor()
                ClaimsApprView.addSubview(PeriodView)
                
                let lbl_CurrencySymbol : UILabel = UILabel(frame: CGRectMake( PeriodView.frame.width / 100 * 4, 10, PeriodView.frame.width / 100 * 20, 20))
                lbl_CurrencySymbol.textAlignment = .Left
                lbl_CurrencySymbol.textColor = UIColor(hexString: "#2190ea")
                lbl_CurrencySymbol.font = UIFont(name: "HelveticaNeue-bold", size: 14)
                lbl_CurrencySymbol.text = String(self.ClaimsApprovalJSON[i]["CurrencyShortName"])
                PeriodView.addSubview(lbl_CurrencySymbol)
            
                let lbl_Amount : UILabel = UILabel(frame: CGRectMake( PeriodView.frame.width / 100 * 20 , 10, PeriodView.frame.width / 100 * 79 , 20))
                lbl_Amount.textAlignment = .Left
                lbl_Amount.textColor = UIColor(hexString: "#2190ea")
                lbl_Amount.font = UIFont(name: "HelveticaNeue-bold", size: 18)
                lbl_Amount.text =  ConvertValueWithDecimal( String(self.ClaimsApprovalJSON[i]["TotalAmount"]),NoofDecimal: 2)
                PeriodView.addSubview(lbl_Amount)
            
            
                let lbl_ClaimTitle : UILabel = UILabel(frame: CGRectMake(PeriodView.frame.width / 100 * 4 , 32, PeriodView.frame.width / 100 * 90, 20))
            
                lbl_ClaimTitle.textAlignment = .Left             
                lbl_ClaimTitle.textColor = UIColor(hexString: "#444444")
                lbl_ClaimTitle.font = UIFont(name: "HelveticaNeue", size: 14)
                lbl_ClaimTitle.text = String(self.ClaimsApprovalJSON[i]["ProgramTitle"])
                PeriodView.addSubview(lbl_ClaimTitle)
            
            let ContainerView =  UIView(frame:CGRectMake(PeriodView.frame.width / 100 * 5, 55, PeriodView.frame.width / 100 * 90 , 1))
            ContainerView.layer.borderColor = UIColor(hexString: "#eeeeee")?.CGColor
            ContainerView.layer.borderWidth = 1
            PeriodView.addSubview(ContainerView)
            
                let lbl_ClaimPeriod : UILabel = UILabel(frame: CGRectMake(PeriodView.frame.width / 100 * 4 , 60, PeriodView.frame.width, 20))
                   lbl_ClaimPeriod.textAlignment = .Left
            lbl_ClaimPeriod.font = UIFont(name: "HelveticaNeue", size: 14)
                lbl_ClaimPeriod.textColor = UIColor(hexString: "#666666")
                lbl_ClaimPeriod.setFAText(prefixText: "", icon: FAType.FACalendar, postfixText: "  " + String(self.ClaimsApprovalJSON[i]["StartDateString"]) + " - " + String(self.ClaimsApprovalJSON[i]["EndDateString"]), size: 14)
                PeriodView.addSubview(lbl_ClaimPeriod)
            
            
                
                
                
                let btnApprove : UIButton = UIButton(frame: CGRectMake(ClaimsApprView.frame.width / 100 * 4 ,180, ClaimsApprView.frame.width / 100 * 45, 30))
                
                btnApprove.layer.borderWidth = 0
                btnApprove.setTitle("Reject", forState: UIControlState.Normal)
                
                btnApprove.addTarget(self, action: #selector(ClaimsDashBoardVC.ApproveTouch(_:)), forControlEvents: .TouchUpInside)
                btnApprove.tag = self.ClaimsApprovalJSON[i]["RequestID"].int!
                btnApprove.titleLabel!.font =  UIFont(name:"HelveticaNeue-Bold", size: 14.0)
                btnApprove.setFAText(prefixText: "", icon: FAType.FACheck, postfixText: "  Approve " , size: 14 , forState: .Normal )
                btnApprove.backgroundColor = UIColor(hexString: "#48C9B0")
                //btnApprove.layer.cornerRadius=5
                btnApprove.setFATitleColor(UIColor.whiteColor())
                
                ClaimsApprView.addSubview(btnApprove)
                
                
                let btnReject : UIButton = UIButton(frame: CGRectMake(ClaimsApprView.frame.width / 100 * 52,180, ClaimsApprView.frame.width / 100 * 44, 30	))
                
                btnReject.layer.borderWidth = 0
                btnReject.setTitle("Reject", forState: UIControlState.Normal)
                
                btnReject.addTarget(self, action: #selector(ClaimsDashBoardVC.RejectTouch(_:)), forControlEvents: .TouchUpInside)
                btnReject.tag = self.ClaimsApprovalJSON[i]["RequestID"].int!
                btnReject.titleLabel!.font =  UIFont(name:"HelveticaNeue-Bold", size: 14.0)
                btnReject.setFAText(prefixText: "", icon: FAType.FATimes, postfixText: "  Reject " , size: 14 , forState: .Normal )
                //btnReject.layer.cornerRadius=5
                btnReject.backgroundColor = UIColor(hexString: "#F46868")
                btnReject.setFATitleColor(UIColor.whiteColor())
                
                ClaimsApprView.addSubview(btnReject)
                
                
                
                
                xbase += ClaimsApprView.frame.width  + 20
                
                
           
            
            
            
        }
        
        // Bind Load More Tiles
        if self.ClaimsApprovalJSON.count > 5
        {
            
            let ClaimsApprView=UIView(frame: CGRectMake(xbase, 0, scroll.frame.width / 100 * 80 , scroll.frame.height))
            ClaimsApprView.backgroundColor = UIColor(hexString: "#e9e9e9")
            scroll.addSubview(ClaimsApprView)
            let btnButton : UIButton = UIButton(frame: CGRectMake(ClaimsApprView.frame.width / 100 * 25  ,ClaimsApprView.frame.height / 100 * 45 , ClaimsApprView.frame.width / 100 * 50 , ClaimsApprView.frame.height / 100 * 16 ))
            btnButton.backgroundColor = UIColor(hexString: "#ffffff")
            btnButton.layer.cornerRadius = 5
            btnButton.layer.borderWidth = 1
            btnButton.titleLabel!.font = UIFont(name: "helvetica-bold", size: 14)
            btnButton.layer.borderColor = UIColor(hexString: "#FFFFFF")!.CGColor
            btnButton.addTarget(self, action: #selector(ClaimsDashBoardVC.ShowTeamClaimList(_:)), forControlEvents: .TouchUpInside)
            btnButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            btnButton.setTitle("Load More...", forState: UIControlState.Normal)
            ClaimsApprView.addSubview(btnButton)
            xbase += ClaimsApprView.frame.width  + 35
            
          
        }
        
          scroll.contentSize = CGSizeMake(xbase, scroll.frame.size.height)
        
        
        return cell
        
    }
    
    func ShowClaimDetail(sender: UIButton) {
        
        let  objClaimsDetails = ClaimsDetails()
        objClaimsDetails.ClaimDetailsObj = GetAdjRequestArray(sender.tag)
        objClaimsDetails.Mode = 1
        self.presentViewController(objClaimsDetails, animated: true, completion: nil)
        
        
        
        
    }

    func GetAdjRequestArray(i:Int) -> ClaimRequest
    {
        
        let iClaim = ClaimRequest()

        iClaim.RequestID = self.ClaimsApprovalJSON[i]["RequestID"].int
        iClaim.EmployeePhoto  = self.ClaimsApprovalJSON[i]["EmployeePhoto"].string
        iClaim.EmployeeName  = self.ClaimsApprovalJSON[i]["EmployeeName"].string
        iClaim.PositionName  = self.ClaimsApprovalJSON[i]["PositionName"].string
        iClaim.StartDateString  = self.ClaimsApprovalJSON[i]["StartDateString"].string
        iClaim.EndDateString  = self.ClaimsApprovalJSON[i]["EndDateString"].string
        iClaim.StatusText  = self.ClaimsApprovalJSON[i]["StatusText"].string
        iClaim.WorkLocationName  = self.ClaimsApprovalJSON[i]["WorkLocationName"].string
        
        iClaim.BusinessUnitName = self.ClaimsApprovalJSON[i]["BusinessUnitName"].string
        iClaim.CurrencyShortName = self.ClaimsApprovalJSON[i]["CurrencyShortName"].string
        iClaim.CurrencySymbol = self.ClaimsApprovalJSON[i]["CurrencySymbol"].string
        iClaim.ProgramTitle = self.ClaimsApprovalJSON[i]["ProgramTitle"].string
        iClaim.RequestedOnString = self.ClaimsApprovalJSON[i]["RequestedOnString"].string
        iClaim.TotalAmount = String(self.ClaimsApprovalJSON[i]["TotalAmount"])
        
        iClaim.UserID = self.ClaimsApprovalJSON[i]["UserID"].int
        iClaim.Status = self.ClaimsApprovalJSON[i]["Status"].int
        iClaim.Access = self.ClaimsApprovalJSON[i]["Access"].int
        iClaim.Type = self.ClaimsApprovalJSON[i]["Type"].int
        iClaim.BaseStatus = self.ClaimsApprovalJSON[i]["BaseStatus"].int
        iClaim.IsLastStep = self.ClaimsApprovalJSON[i]["IsLastStep"].int
        
        iClaim.RequestedFor = self.ClaimsApprovalJSON[i]["RequestedFor"].int
        iClaim.LastActionBy = self.ClaimsApprovalJSON[i]["LastActionBy"].int
        iClaim.RequestedBy = self.ClaimsApprovalJSON[i]["RequestedBy"].int
        iClaim.ProcessType = self.ClaimsApprovalJSON[i]["ProcessType"].int
        
        
        
        return iClaim
    }
    
    
    func FetEmployeeApprovalAdj() -> Void
    {
        
        
        
        let defaults = NSUserDefaults.standardUserDefaults()
        
        
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            objCallBack.MethodName = "GetClaimRequestToDoList"
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objCallBack.post(ApprovalAdjListOncomplete, OnError: ApprovalAdjListOnErrorOnError)
            
        }
        
    }
    
    //Response on error callback
    func ApprovalAdjListOnErrorOnError(Response: NSError ){
        
    }
    
    
    func ApprovalAdjListOncomplete (ResultString :NSString? ) {
        
        dispatch_async(dispatch_get_main_queue(),{
            self.objLBActivityIndicaor.stopAnimating()
            if self.objCallBack.GetJSONData(ResultString) != nil
            {
                
                let lbl_Approval : UILabel = UILabel(frame: CGRectMake(10, CGFloat(400) + self.iDraftTodosLinkHeight, self.view.frame.size.width,18))
                lbl_Approval.backgroundColor = UIColor.whiteColor()
                lbl_Approval.textAlignment = .Left
                lbl_Approval.font = UIFont(name:"helvetica-bold", size: 14)
                lbl_Approval.text = "Claim Approvals"
                self.lbl_Approvalref = lbl_Approval
                self.vscrollView.addSubview(lbl_Approval)
                
                let btnViewAll : UIButton = UIButton(frame: CGRectMake(self.view.frame.size.width / 100 * 70 , CGFloat(399) + self.iDraftTodosLinkHeight, self.view.frame.size.width / 100 * 23,20))
                btnViewAll.backgroundColor = UIColor(hexString: "#2190ea")
                btnViewAll.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                btnViewAll.titleLabel!.font =  UIFont(name:"helvetica-Bold", size: 14)
                btnViewAll.setTitle("View All", forState: .Normal)
                btnViewAll.addTarget(self, action: #selector(ClaimsDashBoardVC.ShowTeamClaimList(_:)), forControlEvents: .TouchUpInside)
                self.vscrollView.addSubview(btnViewAll)
                self.ClaimsApprovalJSON = JSON(data: self.objCallBack.GetJSONData(ResultString)!)
                
                self.lbl_Approvalref.text = "Claim Approvals(" + String(self.ClaimsApprovalJSON.count) + ")"
                if self.dClaimsAdjCollectionView != nil {
                    self.dClaimsAdjCollectionView.removeFromSuperview()
                }
                
                let dCollectionViewFlowLayout : UICollectionViewFlowLayout = UICollectionViewFlowLayout ()
                self.dClaimsAdjCollectionView = UICollectionView (frame: self.view.frame, collectionViewLayout: dCollectionViewFlowLayout)
                self.dClaimsAdjCollectionView.delegate = self
                self.dClaimsAdjCollectionView.dataSource = self
                self.dClaimsAdjCollectionView.backgroundColor = UIColor.blackColor()
                self.dClaimsAdjCollectionView.scrollEnabled=false
                self.dClaimsAdjCollectionView.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "cellIdentifierAdj")
                self.vscrollView.addSubview(self.dClaimsAdjCollectionView)
                
                
                self.dClaimsAdjCollectionView.frame = CGRectMake(0, CGFloat(445) + self.iDraftTodosLinkHeight, self.view.frame.size.width,220)
            }else
            {
                self.ClaimsApprovalJSON = nil
                if(self.dClaimsAdjCollectionView != nil)
                {
                    self.dClaimsAdjCollectionView.removeFromSuperview()
                
                }
                
                let ContainerView2 = UIView(frame: CGRectMake(0, 425 + self.iDraftTodosLinkHeight, self.view.frame.size.width, 1))
                ContainerView2.layer.borderColor = UIColor.whiteColor().CGColor
                ContainerView2.layer.borderWidth = 1
                self.vscrollView.addSubview(ContainerView2)
                
                self.vscrollView.contentSize = CGSize(width: self.view.frame.width, height: 500)
            }
            
            
        })
        
        
        
        
    }
    
    //Handles Adj approval event
    func ApproveTouch(sender: UIButton) {
        
        objLRActivityIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
        objLRActivityIndicaor.activityIndicatorViewStyle = .WhiteLarge
        let objPosition =  self.view.center
        //  objPosition.y =  objPosition.y.advancedBy(-40.0)
        objLRActivityIndicaor.center = objPosition
        //objActivityIndicaor.transform = CGAffineTransformMakeScale(1.5, 1.5);
        objLRActivityIndicaor.hidesWhenStopped = true
        objLRActivityIndicaor.color = UIColor.grayColor()
        self.view.addSubview(objLRActivityIndicaor)
        objLRActivityIndicaor.startAnimating()
        self.ProcessAttendanceAdjRequest("1", pReqeustID: sender.tag)
        
    }
    
    //Handles Adj reject event
    func RejectTouch(sender: UIButton) {
        
        objLRActivityIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
        
        objLRActivityIndicaor.activityIndicatorViewStyle = .WhiteLarge
        let objPosition =  self.view.center
        objLRActivityIndicaor.center = objPosition
        objLRActivityIndicaor.hidesWhenStopped = true
        objLRActivityIndicaor.color = UIColor.grayColor()
        self.view.addSubview(objLRActivityIndicaor)
        objLRActivityIndicaor.startAnimating()
        self.ProcessAttendanceAdjRequest("2", pReqeustID: sender.tag)
        
    }
    func ProcessAttendanceAdjRequest(ActionCode: String , pReqeustID : Int)
    {
        
        self.CurrentActionCode = ActionCode
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            
            objCallBackAP = AjaxCallBack()
            let ResultJSON = objCallBackAP.DeserializeJSONString(UserInfo)
            
            
            objCallBackAP.MethodName = "ProcessAttendanceNote"
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBackAP.ParameterList.append(objparam)
            
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBackAP.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pRequestID",value: String(pReqeustID))
            objCallBackAP.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pAction",value: ActionCode)
            objCallBackAP.ParameterList.append(objparam)
            
            
            
            
            
            objCallBackAP.post(ApproveOnSuccess, OnError: ApproveOnError)
            
            
            
        }
        
    }
    
    //Response on error callback
    func ApproveOnError(Response: NSError ){
        
        
    }
    
    //Respone on success callback
    func ApproveOnSuccess(ResultString :NSString? ) {
        
        dispatch_async(dispatch_get_main_queue(),{
            self.objLRActivityIndicaor.stopAnimating()
            DoneHUD.showInView(self.view, message: "Done")
            self.FetEmployeeApprovalAdj()
            self.dClaimsAdjCollectionView.reloadData()
            
        })
        
    }
    
    
    
    
}