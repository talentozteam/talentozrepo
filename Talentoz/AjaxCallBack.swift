//Ajax call back
//Serves as a API for requesting webservices

import UIKit

 
class AjaxCallBack   {
  
     var error: NSError?
    var WebSerViceURL: String = ""
    internal var MethodName: String = ""
    internal var ParameterList = [Parameter]()
    internal var JSONString: String = ""
    
     init(){

    }
    
   
    
    //Constructs parameter string based on the specified parameters
    func GetParameterString() ->String{
        var Params : String = ""
        for param in ParameterList
        {
        
            Params +=  "<\(param.ParameterName)>\(param.ParameterValue)</\(param.ParameterName)>"
            
        }
        return Params
    }

    //Sends a post request to the webservice
    //onCompletion is the request completion method callback reference
    //OnError is the error method callback reference
    func post(OnCompletion: (ResultString: NSString? ) -> Void, OnError:(error: NSError) -> Void){
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let tmpDomainURL = defaults.stringForKey("DomainURL")
        {
            WebSerViceURL = "http://\(tmpDomainURL)/mobileservices/core/authentication.asmx"
        }
        
   //     if let AuthKey = defaults.stringForKey("AuthKey")
    //    {
     //       var objparam = Parameter(Name: "pOKey",value: AuthKey)
     //       ParameterList.append(objparam)
      //  }
      //  else{
        
     //       var objparam = Parameter(Name: "pOKey",value: "")
      //      ParameterList.append(objparam)
      //  }
        
        let soapMessage = "<?xml version='1.0' encoding='utf-8'?>" +
            "<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>" +
            "<soap:Body> <" + MethodName + " xmlns='http://forzia.com/'>" +
            GetParameterString() + "</" + MethodName + ">    </soap:Body>    </soap:Envelope>"
        
     
        let urlString =  WebSerViceURL + "?op=" + MethodName
        
        let url = NSURL(string: urlString)
    if url != nil
        {
        let theRequest = NSMutableURLRequest(URL: url!)
        let msgLength = String(soapMessage.characters.count)
        
        theRequest.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        theRequest.addValue(msgLength, forHTTPHeaderField: "Content-Length")
        theRequest.addValue("http://forzia.com/" + MethodName , forHTTPHeaderField: "SOAPAction")
        theRequest.HTTPMethod = "POST"
        theRequest.HTTPBody = soapMessage.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        
     
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(theRequest) {
            data, response, error in
            if error != nil {
                //Error call back
                OnError(error: error!)
                return
            }
            // Covert NSData to Type to NSString
            let ResponseStr = NSString(data: data!, encoding: NSUTF8StringEncoding)
       
            
            //Success call back
            OnCompletion(ResultString: ResponseStr)
            
        }
        
        task.resume()
        }
        else
        {
            OnError(error: NSError(domain: "-404", code: 123, userInfo: nil))
        return
        }
    }
    
    
    internal func GetJSONResult(ResponseStr: NSString?) ->  Dictionary<String,AnyObject>?
    {
        do{
            
            //Find The index from HTTPResonse String
            let Startrange =  ResponseStr!.rangeOfString("<" + self.MethodName + "Result>")
            let Endrange = ResponseStr!.rangeOfString("</" + self.MethodName + "Result>")
            //SubString the JSON string from HTTPResponse String
            let  JSONStr =  ResponseStr!.substringWithRange(NSRange(location: Startrange.location + Startrange.length , length: Endrange.location - (Startrange.location + Startrange.length)))
         
            self.JSONString = JSONStr
            
            if let data = self.JSONString.dataUsingEncoding(NSUTF8StringEncoding) {
                var error: NSError?
                let json = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments)
                
                if error != nil {
                    print(error)
                }
             
                return json as? Dictionary<String,AnyObject>
            }
            return nil
        }
        catch{
            return nil
        }
     
    }
    
    internal func GetJSONArrayResult(ResponseStr: NSString?)->  Dictionary<String,AnyObject>?
    {
        do{
            
            //Find The index from HTTPResonse String
            let Startrange =  ResponseStr!.rangeOfString("<" + self.MethodName + "Result>")
            let Endrange = ResponseStr!.rangeOfString("</" + self.MethodName + "Result>")
            //SubString the JSON string from HTTPResponse String
            let  JSONStr =  ResponseStr!.substringWithRange(NSRange(location: Startrange.location + Startrange.length , length: Endrange.location - (Startrange.location + Startrange.length)))
            
            self.JSONString = JSONStr
            
            let StrippedJSON  =  String(JSONStr.characters.dropFirst().dropLast())
            
            if let data = StrippedJSON.dataUsingEncoding(NSUTF8StringEncoding) {
                var error: NSError?
                let json = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments)
                
                if error != nil {
                    print(error)
                }
                
                return json as! Dictionary<String,AnyObject>
            }
            return nil
        }
        catch{
            return nil
        }
        
    }
    
    internal func GetJSONData(ResponseStr: NSString?)->  NSData?
    {
        do{
            
            //Find The index from HTTPResonse String
            let Startrange =  ResponseStr!.rangeOfString("<" + self.MethodName + "Result>")
            let Endrange = ResponseStr!.rangeOfString("</" + self.MethodName + "Result>")
            //SubString the JSON string from HTTPResponse String
            if Startrange.location == Endrange.location
            {
                return nil
            }
            else
            {
            let  JSONStr =  ResponseStr!.substringWithRange(NSRange(location: Startrange.location + Startrange.length , length: Endrange.location - (Startrange.location + Startrange.length)))
        
            
            if let data = JSONStr.dataUsingEncoding(NSUTF8StringEncoding) {
                
                return data
            }
            }
            return nil
        }
        catch{
            return nil
        }
        
    }
    
    internal func GetJSONResultString(ResponseStr: NSString?)->  String?
    {
        do{
            
            //Find The index from HTTPResonse String
            let Startrange =  ResponseStr!.rangeOfString("<" + self.MethodName + "Result>")
            let Endrange = ResponseStr!.rangeOfString("</" + self.MethodName + "Result>")
            //SubString the JSON string from HTTPResponse String
            if Startrange.location == Endrange.location
            {
                return nil
            }
            else
            {
                let  JSONStr =  ResponseStr!.substringWithRange(NSRange(location: Startrange.location + Startrange.length , length: Endrange.location - (Startrange.location + Startrange.length)))
                
                return JSONStr
                
            }
            return nil
        }
        catch{
            return nil
        }
        
    }
    
    
    internal func DeserializeJSONString(ResponseStr: String?) ->  Dictionary<String,AnyObject>?
    {
        do{
            
            if let data = ResponseStr!.dataUsingEncoding(NSUTF8StringEncoding) {
                var error: NSError?
                let json = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments)
                if error != nil {
                    print(error)
                }               
                
              
                
                return json as? Dictionary<String,AnyObject>
            }
            return nil
        }
        catch{
            return nil
        }
        
        
    }
    
    
   
    
    
    
    
}





