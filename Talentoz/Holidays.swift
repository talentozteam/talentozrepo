//
//  Holidays.swift
//  Talentoz
//
//  Created by forziamac on 24/12/15.
//  Copyright © 2015 Forzia Tech Private Ltd. All rights reserved.
//

import UIKit

class Holidays: BaseTabBarVC, UITableViewDataSource ,UITableViewDelegate {

    let objCallBack = AjaxCallBack()
    let defaults = NSUserDefaults.standardUserDefaults()
    var SourceData: Talentoz.JSON!
    var HolidayListView = UITableView()
    
    
    override func viewDidLoad() {
        
        
        
        super.UINavigationBarTitle = "Holiday List"
       // super.UINavigatorWidth = self.view.frame.width
       // self.view.backgroundColor = UIColor.lightGrayColor()
   
         super.viewDidLoad()
    
        
        
        BindHolidays()
   
       
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    func BindHolidays()
    {
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            objCallBack.MethodName = "GetHolidayList"
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objCallBack.post(HolidayListOnComplete, OnError: HolidayListOnErrorOnError)
            
        }
    
    }
    
    //Response on error callback
    func HolidayListOnErrorOnError(Response: NSError ){
        
    }
    
    func HolidayListOnComplete (ResultString :NSString? ) {
        
        dispatch_async(dispatch_get_main_queue(),{
          self.SourceData =   JSON(data: self.objCallBack.GetJSONData(ResultString)!)
            
            
            let date = NSDate()
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components([.Year], fromDate: date)
            
            let lbl_Approval : UILabel = UILabel(frame: CGRectMake(0 , 60, self.view.frame.width ,25 ))
            lbl_Approval.backgroundColor = UIColor(hexString: "#f5f5f5")
            lbl_Approval.font = UIFont(name:"HelveticaNeue-Bold", size: 17.0)
             lbl_Approval.textAlignment = .Center
            lbl_Approval.font = lbl_Approval.font.fontWithSize(14.0)
            lbl_Approval.text = String(components.year)
           self.view.addSubview(lbl_Approval)
            
            self.HolidayListView = UITableView(frame: CGRectMake(0,  85, self.view.frame.width, self.view.frame.height - 129 ))
            self.HolidayListView.showsVerticalScrollIndicator = true
            self.HolidayListView.delegate = self
            self.HolidayListView .dataSource = self
            self.HolidayListView .rowHeight = 60
            self.view.addSubview(self.HolidayListView )
            
        })
        
        
    }
    
    
     func numberOfSectionsInTableView(HolidayListView: UITableView) -> Int {
       
        return 1
    }
    
     func tableView(HolidayListView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        return self.SourceData.count
    }
    
    
     func tableView(HolidayListView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // 3
        let cell = UITableViewCell()
        
        if indexPath.row % 2 == 0
        {
            cell.backgroundColor = UIColor.whiteColor()
        }
        else
        {
            cell.backgroundColor = UIColor(hexString: "#f5f5f5")
        }
        
        let lbl_Approval : UILabel = UILabel(frame: CGRectMake(17 , 10, 50 ,18))
        lbl_Approval.backgroundColor = UIColor.clearColor()
        lbl_Approval.textAlignment = .Left
        lbl_Approval.font = UIFont(name:"HelveticaNeue-Bold", size: 16.0)
        lbl_Approval.textColor = UIColor(hexString: "#333333")
        lbl_Approval.text = String(self.SourceData[indexPath.row]["HolidayDate"])
        cell.addSubview(lbl_Approval)
        
        let lbl_Approval1 : UILabel = UILabel(frame: CGRectMake(15, 30, 32 ,18))
        lbl_Approval1.backgroundColor = UIColor.clearColor()
        lbl_Approval1.textAlignment = .Left
        lbl_Approval1.textColor = UIColor(hexString: "#2A2A2A")
        lbl_Approval1.font = UIFont(name:"HelveticaNeue", size: 12.0)
        lbl_Approval1.text = String(self.SourceData[indexPath.row]["HolidayMonth"])
        cell.addSubview(lbl_Approval1)
        
        let lbl_Approval2 : UILabel = UILabel(frame: CGRectMake(60 , 20, 250 ,18))
        lbl_Approval2.backgroundColor = UIColor.clearColor()
        lbl_Approval2.textAlignment = .Left
        lbl_Approval2.textColor = UIColor(hexString: "#2A2A2A")
       
        lbl_Approval2.font = UIFont(name:"HelveticaNeue", size: 14.0)
        lbl_Approval2.text = String(self.SourceData[indexPath.row]["HolidayName"])
        cell.addSubview(lbl_Approval2)
        
        
        
        return cell
    }
 

}
