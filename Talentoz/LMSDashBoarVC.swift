//
//  LMSDashBoarVC.swift
//  Talentoz
//
//  Created by forziamac on 18/12/15.
//  Copyright © 2015 Forzia Tech Private Ltd. All rights reserved.
//

import UIKit

class LMSDashBoarVC:  BaseTabBarVC,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate    {

  let vscrollView = UIScrollView(frame: UIScreen.mainScreen().bounds)
    
    var objCallBack = AjaxCallBack()
    let defaults = NSUserDefaults.standardUserDefaults()
    var LeaveBalJSON: Talentoz.JSON!
    var LeaveApprovalJSON: Talentoz.JSON!
    
    var dCollectionView : UICollectionView!
    var dLeaveCollectionView : UICollectionView!
    var objLBActivityIndicaor : UIActivityIndicatorView!
    var objLRActivityIndicaor : UIActivityIndicatorView!
    var CurrentActionCode : String!
    let objRole = RoleManager()
    
    override func viewDidLoad() {
        super.UINavigationBarTitle = "Leaves"
        self.view.addSubview(vscrollView)
        self.vscrollView.contentSize = CGSize(width: self.view.frame.width, height: 700)
        self.vscrollView.backgroundColor = UIColor.whiteColor()
        
        super.viewDidLoad()
        
       
        
        // Do any additional setup after loading the view.
        self.FetEmployeeLeaveBalance()
        LoadQuickAction()
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func FetEmployeeLeaveBalance() -> Void
    {
        
        let lbl_Title : UILabel = UILabel(frame: CGRectMake(15, 75, self.view.frame.size.width,20))
        lbl_Title.backgroundColor = UIColor.whiteColor()
        lbl_Title.textAlignment = .Left
        lbl_Title.font = UIFont(name:"helvetica-bold", size: 13)
        vscrollView.addSubview(lbl_Title)
        lbl_Title.text = "My Leave Balance"
    
        let ContainerView = UIView(frame: CGRectMake(0,100, self.view.frame.size.width, 1))
        ContainerView.layer.borderColor = UIColor(hexString: "#cccccc")?.CGColor
        ContainerView.layer.borderWidth = 1
        
        vscrollView.addSubview(ContainerView)
        
        objLBActivityIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, ContainerView.frame.size.width, ContainerView.frame.size.height))
        
        
        
        objLBActivityIndicaor.activityIndicatorViewStyle = .WhiteLarge
        
        var objPosition =  ContainerView.center
        
        objPosition.y =  objPosition.y.advancedBy(-40.0)
        objLBActivityIndicaor.center = objPosition
        
        //objActivityIndicaor.transform = CGAffineTransformMakeScale(1.5, 1.5);
        objLBActivityIndicaor.hidesWhenStopped = true
        objLBActivityIndicaor.color = UIColor.grayColor()
        
        ContainerView.addSubview(objLBActivityIndicaor)
        objLBActivityIndicaor.startAnimating()
        
        let defaults = NSUserDefaults.standardUserDefaults()
        
        
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            objCallBack.MethodName = "GetEligebleLeaves"
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objCallBack.post(setupCollectionView, OnError: EmpProfileOnErrorOnError)
            
        }
        
    }
    
    //Response on error callback
    func EmpProfileOnErrorOnError(Response: NSError ){
        
    }
    
    func LoadQuickAction()
    {
    
        let lbl_Title : UILabel = UILabel(frame: CGRectMake(10, 210, self.view.frame.size.width,25))
        lbl_Title.backgroundColor = UIColor.whiteColor()
        lbl_Title.textAlignment = .Left
        lbl_Title.font = UIFont(name:"helvetica-bold", size: 13)
         lbl_Title.text = "Quick Actions"
        vscrollView.addSubview(lbl_Title)
        
        let ContainerView = UIView(frame: CGRectMake(0,235, self.view.frame.size.width, 1))
        ContainerView.layer.borderColor = UIColor(hexString: "#cccccc")?.CGColor
        ContainerView.layer.borderWidth = 1
        vscrollView.addSubview(ContainerView)
        
        //let navigationBar = UIContentContainer() // Offset by 20 pixels
        
        objLRActivityIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, ContainerView.frame.size.width, ContainerView.frame.size.height))
        
        
        
        objLRActivityIndicaor.activityIndicatorViewStyle = .WhiteLarge
        
        let objPosition =  ContainerView.center
        
      //  objPosition.y =  objPosition.y.advancedBy(-40.0)
        objLRActivityIndicaor.center = objPosition
        
        //objActivityIndicaor.transform = CGAffineTransformMakeScale(1.5, 1.5);
        objLRActivityIndicaor.hidesWhenStopped = true
        objLRActivityIndicaor.color = UIColor.grayColor()
        
        ContainerView.addSubview(objLRActivityIndicaor)
        if(objRole.ISManager)
        {
        objLRActivityIndicaor.startAnimating()
        }
        
        
        
        let LeaveListView=UIView(frame: CGRectMake((self.view.frame.width / 100 * 6), 245, (self.view.frame.width / 100 * 24), 75))
        LeaveListView.backgroundColor = UIColor(hexString: "#0CD6F5")

     
        
        let lbl_LeaveListViewIcon : UILabel = UILabel(frame: CGRectMake((LeaveListView.frame.width / 100 * 3), 15, LeaveListView.frame.width,23))
        lbl_LeaveListViewIcon.textColor = UIColor.whiteColor()
       lbl_LeaveListViewIcon.textAlignment = .Center
        lbl_LeaveListViewIcon.setFAIcon(FAType.FAFileText, iconSize: 25)
        LeaveListView.addSubview(lbl_LeaveListViewIcon)
        
        let lbl_LeaveListViewTitle : UILabel = UILabel(frame: CGRectMake((LeaveListView.frame.width / 100 * 4), 45, LeaveListView.frame.width,18))
        lbl_LeaveListViewTitle.textColor = UIColor.whiteColor()
        lbl_LeaveListViewTitle.textAlignment = .Center
        lbl_LeaveListViewTitle.font = lbl_Title.font.fontWithSize(11.0)
        lbl_LeaveListViewTitle.text = "Leave List"
        LeaveListView.addSubview(lbl_LeaveListViewTitle)
        
        let gestureLeaveList = UITapGestureRecognizer(target: self, action: "LeaveListAction:")
        LeaveListView.addGestureRecognizer(gestureLeaveList)
        vscrollView.addSubview(LeaveListView)
        
           vscrollView.addSubview(LeaveListView)
        
        
        let HolidayListView=UIView(frame: CGRectMake((self.view.frame.width / 100 * 37), 245, (self.view.frame.width / 100 * 24), 75))
        HolidayListView.backgroundColor = UIColor(hexString: "#F50CA0")
        let gestureHoliday = UITapGestureRecognizer(target: self, action: "HolidayListAction:")
        
         let gestureHoliday1 = UITapGestureRecognizer(target: self, action: "HolidayListAction:")
       
        
        let lbl_HolidayListViewIcon : UIButton = UIButton(frame: CGRectMake((HolidayListView.frame.width / 100 * 3), 15, HolidayListView.frame.width,23))
       // lbl_HolidayListViewIcon.textColor = UIColor.whiteColor()
       // lbl_HolidayListViewIcon.textAlignment = .Center
      //  lbl_HolidayListViewIcon.setFAIcon(FAType.FACar, iconSize: 25)
        
        lbl_HolidayListViewIcon.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        lbl_HolidayListViewIcon.setImage(UIImage(named: "Beach-White.png"), forState: UIControlState.Normal)
        lbl_HolidayListViewIcon.addGestureRecognizer(gestureHoliday1)
        HolidayListView.addSubview(lbl_HolidayListViewIcon)
        
        
        let lbl_HolidayListViewTitle : UILabel = UILabel(frame: CGRectMake((HolidayListView.frame.width / 100 * 4), 45, HolidayListView.frame.width,18))
        lbl_HolidayListViewTitle.textColor = UIColor.whiteColor()
        lbl_HolidayListViewTitle.textAlignment = .Center
        lbl_HolidayListViewTitle.font = lbl_Title.font.fontWithSize(11.0)
        lbl_HolidayListViewTitle.text = "Holidays"
        HolidayListView.addSubview(lbl_HolidayListViewTitle)
         HolidayListView.addGestureRecognizer(gestureHoliday)
      
     
        
        vscrollView.addSubview(HolidayListView)
        
        
        
        let LeavePlannerView=UIView(frame: CGRectMake((self.view.frame.width / 100 * 68), 245, (self.view.frame.width / 100 * 24), 75))
        LeavePlannerView.backgroundColor = UIColor(hexString: "#F5940C")
   
        
        let lbl_LeavePlannerViewIcon : UILabel = UILabel(frame: CGRectMake((LeavePlannerView.frame.width / 100 * 3), 15, LeavePlannerView.frame.width,23))
        lbl_LeavePlannerViewIcon.textColor = UIColor.whiteColor()
        lbl_LeavePlannerViewIcon.textAlignment = .Center
        lbl_LeavePlannerViewIcon.setFAIcon(FAType.FACalendar, iconSize: 25)
        LeavePlannerView.addSubview(lbl_LeavePlannerViewIcon)
        
        let lbl_LeavePlannerViewTitle : UILabel = UILabel(frame: CGRectMake((LeavePlannerView.frame.width / 100 * 4), 45, LeavePlannerView.frame.width,18))
        lbl_LeavePlannerViewTitle.textColor = UIColor.whiteColor()
        lbl_LeavePlannerViewTitle.textAlignment = .Center
        lbl_LeavePlannerViewTitle.font = lbl_Title.font.fontWithSize(11.0)
        lbl_LeavePlannerViewTitle.text = "Apply Leave"
        LeavePlannerView.addSubview(lbl_LeavePlannerViewTitle)
        
        let gesture = UITapGestureRecognizer(target: self, action: "ApplyLeaveAction:")
        LeavePlannerView.addGestureRecognizer(gesture)
        vscrollView.addSubview(LeavePlannerView)
        
        
       
        
 
        
    }
    
    func ShowTeamLeaveList(Sender: UIButton!)
    {
        let LeaveListObj = LeaveList()
        LeaveListObj.ModeofShow = 1
        self.presentViewController(LeaveListObj, animated: true, completion: nil)
    }
    
    func ApplyLeaveAction(sender: UIView) {

        let  ApplyCon = ApplyLeave()
        self.presentViewController(ApplyCon, animated: true, completion: nil)
        
        
    }
    
    func LeaveListAction(sender: UIView) {
        
        let  LeaveListView = LeaveList()
        self.presentViewController(LeaveListView, animated: true, completion: nil)
        
        
    }
    
    func HolidayListAction(sender: UIView) {
        
        let  Holiday = Holidays()
        self.presentViewController(Holiday, animated: true, completion: nil)
        
        
    }
    
    
    //Leave Balance Slider
    
    func setupCollectionView (ResultString :NSString? ) {
        
        dispatch_async(dispatch_get_main_queue(),{
            self.LeaveBalJSON = JSON(data: self.objCallBack.GetJSONData(ResultString)!)
            
            let dCollectionViewFlowLayout : UICollectionViewFlowLayout = UICollectionViewFlowLayout ()
            self.dCollectionView = UICollectionView (frame: self.view.frame, collectionViewLayout: dCollectionViewFlowLayout)
            self.dCollectionView.delegate = self
            self.dCollectionView.dataSource = self
            self.dCollectionView.backgroundColor = UIColor.blackColor()
            
            self.dCollectionView.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "cellIdentifier")
            self.vscrollView.addSubview(self.dCollectionView)
            
            self.dCollectionView.frame = CGRectMake(0,115, self.view.frame.size.width,100)
            self.objLBActivityIndicaor.stopAnimating()
            if self.objRole.ISManager {
                self.FetEmployeeApprovalLeave()
            }
            
            
        })
        
        
        
    }
    
    
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return  1
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            if collectionView == self.dCollectionView {
                 return CGSize(width: self.view.frame.size.width, height: 55)
            }
            else
            {
                 return CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height / 100 * 30)
            }
           
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        if collectionView == self.dCollectionView {
            let cell : UICollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("cellIdentifier", forIndexPath: indexPath)
            
            cell.backgroundColor = UIColor.whiteColor()
            
            let scroll: UIScrollView = UIScrollView(frame: CGRectMake(0, 0, self.dCollectionView.frame.size.width, self.dCollectionView.frame.size.height))
            scroll.delegate = self
            scroll.backgroundColor = UIColor.whiteColor()
            cell.addSubview(scroll)
            
            var xbase : CGFloat = 10
            
            
        
            
            for i in 0 ..< self.LeaveBalJSON.count  {
                
                
                let btnButton : UIButton = UIButton(frame: CGRectMake(xbase + 20,0, 60, 60))
                btnButton.backgroundColor = UIColor.clearColor()
                btnButton.layer.cornerRadius = btnButton.frame.height/2
                btnButton.layer.borderWidth = 3
                btnButton.titleLabel!.font = UIFont(name: "helvetica-bold", size: 15)
                if self.LeaveBalJSON[i]["LeaveColorCode"] != nil
                {
                    btnButton.layer.borderColor = UIColor(hexString: String(self.LeaveBalJSON[i]["LeaveColorCode"]))!.CGColor
                }
                else
                {
                    btnButton.layer.borderColor = UIColor(hexString: "#FFFFFF")!.CGColor
                }
                btnButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
                btnButton.setTitle(String(self.LeaveBalJSON[i]["LeaveBalance"]), forState: UIControlState.Normal)
                scroll.addSubview(btnButton)
                
                
                let lbl_Title : UILabel = UILabel(frame: CGRectMake(xbase, 65, 100, 15))
                lbl_Title.backgroundColor = UIColor.whiteColor()
                lbl_Title.textAlignment = .Center
                lbl_Title.font = lbl_Title.font.fontWithSize(12.0)
                scroll.addSubview(lbl_Title)
                lbl_Title.text = String(self.LeaveBalJSON[i]["LeaveType"])
                
                xbase += 100 + 10
                
            }
            
            
            scroll.contentSize = CGSizeMake(xbase, scroll.frame.size.height)
            
            
            return cell
        }
        else
        {
            let cell : UICollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("cellIdentifierLeave", forIndexPath: indexPath)
            
            cell.backgroundColor = UIColor.whiteColor()
            cell.frame = CGRectMake(0, 0, dLeaveCollectionView.frame.size.width, dLeaveCollectionView.frame.size.height  )
            
            let scroll: UIScrollView = UIScrollView(frame: CGRectMake(0, 0, dLeaveCollectionView.frame.size.width, 200  ))
            scroll.delegate = self
            scroll.backgroundColor = UIColor.whiteColor()
            cell.addSubview(scroll)
            
            var xbase : CGFloat = 10
            
            var CountMore: Int = 0
            
            if self.LeaveApprovalJSON.count > 5
            {
                CountMore = 5
            }
            else
            {
                CountMore = self.LeaveApprovalJSON.count
            }
            
            
            for i in 0 ..< CountMore  {
                
                let LeavePlannerView=UIView(frame: CGRectMake(xbase, 0, scroll.frame.width / 100 * 80 , scroll.frame.height))
                LeavePlannerView.backgroundColor = UIColor(hexString: "#e9e9e9")
                
                LeavePlannerView.tag = self.LeaveApprovalJSON[i]["RequestID"].int!
                scroll.addSubview(LeavePlannerView)
                
        
                
                
                
                let lbl_EmpName : UILabel = UILabel(frame: CGRectMake( 70 , 10, LeavePlannerView.frame.width / 100 * 50, 15))
                lbl_EmpName.textColor = UIColor(hexString: "#333333")
                
                lbl_EmpName.font = lbl_EmpName.font.fontWithSize(12.0)
                LeavePlannerView.addSubview(lbl_EmpName)
                lbl_EmpName.text = String(self.LeaveApprovalJSON[i]["EmployeeName"])
                
                
                let lbl_PositionName : UILabel = UILabel(frame: CGRectMake(70 , 25, LeavePlannerView.frame.width / 100 * 50, 15))
                lbl_PositionName.textColor = UIColor(hexString: "#666666")
                
                lbl_PositionName.font = lbl_PositionName.font.fontWithSize(10.0)
                LeavePlannerView.addSubview(lbl_PositionName)
                lbl_PositionName.text = String(self.LeaveApprovalJSON[i]["PositionName"])
                
                LeavePlannerView.addSubview(lbl_PositionName)
                
                let lbl_WorkLocationName : UILabel = UILabel(frame: CGRectMake(70 , 40, 75, 15))
                lbl_WorkLocationName.textColor = UIColor(hexString: "#666666")
                lbl_WorkLocationName.font = lbl_WorkLocationName.font.fontWithSize(10.0)
                lbl_WorkLocationName.text = String(self.LeaveApprovalJSON[i]["WorkLocationName"])
                LeavePlannerView.addSubview(lbl_WorkLocationName)
                
           
                
                let btn_ShowDetail : UIButton = UIButton(frame: CGRectMake(160 , 40, 60, 15))
                btn_ShowDetail.backgroundColor = UIColor.clearColor()
                btn_ShowDetail.setTitleColor(UIColor(hexString: "#2190ea"), forState: UIControlState.Normal)
                btn_ShowDetail.setTitle("Show Detail" , forState: .Normal)
                btn_ShowDetail.titleLabel?.font = UIFont(name: "HelveticaNeue", size: 10)
                btn_ShowDetail.addTarget(self, action: #selector(LMSDashBoarVC.ShowLeaveDetail(_:)), forControlEvents: .TouchUpInside)
                btn_ShowDetail.tag = i
                LeavePlannerView.addSubview(btn_ShowDetail)
                
                if let tmpDomainURL = self.defaults.stringForKey("DomainURL")
                {
                    
                    if self.LeaveApprovalJSON[i]["EmployeePhoto"] == nil
                    {
                        let img_EmpPic:  UIButton = UIButton(frame: CGRectMake(10, 7, 50, 50))
                        img_EmpPic.layer.borderWidth = 0.5
                        img_EmpPic.layer.masksToBounds = false
                        img_EmpPic.layer.borderColor = UIColor.lightGrayColor().CGColor
                        img_EmpPic.backgroundColor = UIColor.SetRandomColor()
                        img_EmpPic.layer.cornerRadius = img_EmpPic.frame.height/2
                        img_EmpPic.clipsToBounds = true
                        img_EmpPic.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                        img_EmpPic.setTitle(String(self.LeaveApprovalJSON[i]["EmployeeName"])[0], forState: UIControlState.Normal)
                        LeavePlannerView.addSubview(img_EmpPic)
                    }
                    else{
                        let imgView : UIImageView = UIImageView(frame: CGRectMake(10, 7, 50, 50))
                        imgView.layer.borderWidth = 1
                        imgView.layer.masksToBounds = false
                        imgView.layer.borderColor = UIColor.whiteColor().CGColor
                        imgView.layer.cornerRadius = imgView.frame.height/2
                        imgView.clipsToBounds = true
                        LeavePlannerView.addSubview(imgView)
                        
                        let employeePic = "http://" + tmpDomainURL + "/Uploadedfiles/Client" + String(self.defaults.stringForKey("ClientID")!) + "/Image/" + String(self.LeaveApprovalJSON[i]["EmployeePhoto"])
                        imgView.setImageWithUrl(NSURL(string: employeePic)!)
                    }
                    
                 
                }
                
                
                let NoOfDaysView=UIView(frame: CGRectMake(10, 70, (LeavePlannerView.frame.width / 100 * 20), 50))
                NoOfDaysView.backgroundColor = UIColor.whiteColor()
                LeavePlannerView.addSubview(NoOfDaysView)
                
                let lbl_NoOfDays : UILabel = UILabel(frame: CGRectMake(0 , 10, NoOfDaysView.frame.width, 15))
                lbl_NoOfDays.backgroundColor = UIColor.clearColor()
                lbl_NoOfDays.textAlignment = .Center
               // lbl_NoOfDays.font = lbl_NoOfDays.font.fontWithSize(14.0)
                lbl_NoOfDays.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
                lbl_NoOfDays.textColor = UIColor(hexString: "#666666")
                NoOfDaysView.addSubview(lbl_NoOfDays)
                lbl_NoOfDays.text = String(self.LeaveApprovalJSON[i]["NOOfDays"])
                
                let lbl_Days : UILabel = UILabel(frame: CGRectMake(0 , 30, NoOfDaysView.frame.width, 15))
                lbl_Days.backgroundColor = UIColor.clearColor()
                lbl_Days.textAlignment = .Center
                lbl_Days.font = lbl_Days.font.fontWithSize(12.0)
                lbl_Days.textColor = UIColor(hexString: "#666666")
                NoOfDaysView.addSubview(lbl_Days)
                lbl_Days.text = "days"
                
                let PeriodView=UIView(frame: CGRectMake((LeavePlannerView.frame.width / 100 * 28), 70, (LeavePlannerView.frame.width / 100 * 65), 50))
              PeriodView.backgroundColor = UIColor.whiteColor()
                LeavePlannerView.addSubview(PeriodView)
                
                let lbl_LeaveDate : UILabel = UILabel(frame: CGRectMake(0 , 10, PeriodView.frame.width, 15))
                lbl_LeaveDate.backgroundColor = UIColor.clearColor()
                lbl_LeaveDate.textAlignment = .Center
                lbl_LeaveDate.font = lbl_LeaveDate.font.fontWithSize(10.0)
                lbl_LeaveDate.textColor = UIColor(hexString: "#333333")
                PeriodView.addSubview(lbl_LeaveDate)
                lbl_LeaveDate.text = String(self.LeaveApprovalJSON[i]["StartDateString"]) + " - " + String(self.LeaveApprovalJSON[i]["EndDateString"])
                
                let lbl_LeaveType : UILabel = UILabel(frame: CGRectMake(0 , 30, PeriodView.frame.width, 15))
                lbl_LeaveType.backgroundColor = UIColor.clearColor()
                
                lbl_LeaveType.textAlignment = .Center
                lbl_LeaveType.font = lbl_LeaveType.font.fontWithSize(10.0)
                lbl_LeaveType.textColor = UIColor(hexString: "#333333")
                PeriodView.addSubview(lbl_LeaveType)
                lbl_LeaveType.text = String(self.LeaveApprovalJSON[i]["RequestDescription"])
                
                let btnApprove : UIButton = UIButton(frame: CGRectMake(LeavePlannerView.frame.width / 100 * 4 ,135, LeavePlannerView.frame.width / 100 * 40, 25))
                btnApprove.layer.borderWidth = 0
                    btnApprove.setTitle("Approve", forState: UIControlState.Normal)
                btnApprove.titleLabel!.font =  UIFont(name:"HelveticaNeue-Bold", size: 12.0)
                 btnApprove.addTarget(self, action: "ApproveTouch:", forControlEvents: .TouchUpInside)
                btnApprove.tag = self.LeaveApprovalJSON[i]["RequestID"].int!
             
               
                btnApprove.backgroundColor = UIColor(hexString: "#48C9B0")
                btnApprove.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Reserved)
                
                LeavePlannerView.addSubview(btnApprove)
                
                let btnReject : UIButton = UIButton(frame: CGRectMake(LeavePlannerView.frame.width / 100 * 52,135, LeavePlannerView.frame.width / 100 * 40, 25))
                
                btnReject.layer.borderWidth = 0
               btnReject.setTitle("Reject", forState: UIControlState.Normal)
                
                btnReject.addTarget(self, action: "RejectTouch:", forControlEvents: .TouchUpInside)
                btnReject.tag = self.LeaveApprovalJSON[i]["RequestID"].int!
               btnReject.titleLabel!.font =  UIFont(name:"HelveticaNeue-Bold", size: 12.0)
                btnReject.backgroundColor = UIColor(hexString: "#F46868")
                 btnReject.setFATitleColor(UIColor.whiteColor())
                
                LeavePlannerView.addSubview(btnReject)
                
                LeavePlannerView.tag = i
                
            
                
                xbase += LeavePlannerView.frame.width  + 20
                
           
                
            }
            
            // Bind Load More Tiles
            if self.LeaveApprovalJSON.count > 5
            {
                let LeavePlannerView=UIView(frame: CGRectMake(xbase, 0, scroll.frame.width / 100 * 80 , scroll.frame.height))
                LeavePlannerView.backgroundColor = UIColor(hexString: "#e9e9e9")
                scroll.addSubview(LeavePlannerView)
                let btnButton : UIButton = UIButton(frame: CGRectMake(LeavePlannerView.frame.width / 100 * 30  ,LeavePlannerView.frame.height / 100 * 40 , LeavePlannerView.frame.width / 100 * 40 , LeavePlannerView.frame.height / 100 * 16 ))
                btnButton.backgroundColor = UIColor(hexString: "#ffffff")
                btnButton.layer.cornerRadius = 5
                btnButton.layer.borderWidth = 1
                
                
                btnButton.titleLabel!.font = UIFont(name: "helvetica-bold", size: 12)
                btnButton.layer.borderColor = UIColor(hexString: "#FFFFFF")!.CGColor
                btnButton.addTarget(self, action: "ShowTeamLeaveList:", forControlEvents: .TouchUpInside)
                btnButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                btnButton.setTitle("Load More...", forState: UIControlState.Normal)
                LeavePlannerView.addSubview(btnButton)
                xbase += LeavePlannerView.frame.width  + 35
            }
            
            
            scroll.contentSize = CGSizeMake(xbase, scroll.frame.size.height)
            
            
            return cell
        }
   
        
    }
    
    func ShowLeaveDetail(sender: UIButton) {
 
        
        let  objLeaveDetail = LeaveDetail()
        
        objLeaveDetail.LeaveDetails = GetLeaveRequestArray(sender.tag)
        objLeaveDetail.UserID = objLeaveDetail.LeaveDetails.RequestedFor
        objLeaveDetail.Mode = -1
        self.presentViewController(objLeaveDetail, animated: true, completion: nil)
       
        
      
        
    }
    
    func GetLeaveRequestArray(i:Int) -> LeaveRequest
    {
        
       
            
            let mleave = LeaveRequest()
    
            mleave.Access = LeaveApprovalJSON[i]["Access"].int
            mleave.BaseStatus = LeaveApprovalJSON[i]["BaseStatus"].int
            mleave.Type  = LeaveApprovalJSON[i]["Type"].int
            mleave.ISLastStep  = LeaveApprovalJSON[i]["ISLastStep"].int
            mleave.ProcessType  = LeaveApprovalJSON[i]["ProcessType"].int
            mleave.ISChildCreated  = LeaveApprovalJSON[i]["ISChildCreated"].int
            mleave.ISHRApply  = LeaveApprovalJSON[i]["ISHRApply"].int
            
            mleave.RequestID = LeaveApprovalJSON[i]["RequestID"].int
            mleave.EmployeePhoto  = LeaveApprovalJSON[i]["EmployeePhoto"].string
            mleave.EmployeeName  = LeaveApprovalJSON[i]["EmployeeName"].string
            mleave.PositionName  = LeaveApprovalJSON[i]["PositionName"].string
            mleave.RequestDescription  = LeaveApprovalJSON[i]["RequestDescription"].string
            mleave.NOOfDays  = LeaveApprovalJSON[i]["NOOfDays"].double
            mleave.StartDateString  = LeaveApprovalJSON[i]["StartDateString"].string
            mleave.EndDateString  = LeaveApprovalJSON[i]["EndDateString"].string
            mleave.RequestedFor  = LeaveApprovalJSON[i]["RequestedFor"].int
            mleave.LastActionBy  = LeaveApprovalJSON[i]["LastActionBy"].int
            mleave.StatusText  = LeaveApprovalJSON[i]["StatusText"].string
            mleave.ParentID  = LeaveApprovalJSON[i]["ParentID"].string
            mleave.RequestedBY  = LeaveApprovalJSON[i]["RequestedBY"].int
            
            mleave.WorkLocationName  = LeaveApprovalJSON[i]["WorkLocationName"].string
            mleave.Reason  = LeaveApprovalJSON[i]["Reason"].string
            
            //String(objLeaveRequest["Access"]) as Int
        
        
        return mleave
    }
    
    
    // Leave Approval Slider
    
    func FetEmployeeApprovalLeave() -> Void
    {
        
        let defaults = NSUserDefaults.standardUserDefaults()
        
        
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            objCallBack.MethodName = "GetLeaveList"
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objCallBack.post(ApprovalLeaveListOncomplete, OnError: ApprovalLeaveListOnErrorOnError)
            
        }
        
    }
    
    //Response on error callback
    func ApprovalLeaveListOnErrorOnError(Response: NSError ){
        
    }
    
    override func goBack() {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("HomeInitialView")
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    
    func ApprovalLeaveListOncomplete (ResultString :NSString? ) {
        
        dispatch_async(dispatch_get_main_queue(),{
            self.objLRActivityIndicaor.stopAnimating()
        if let tempdata = self.objCallBack.GetJSONData(ResultString)
            {
              
                let lbl_Approval : UILabel = UILabel(frame: CGRectMake(10, 350, self.view.frame.size.width,18))
                lbl_Approval.backgroundColor = UIColor.whiteColor()
                lbl_Approval.textAlignment = .Left
                lbl_Approval.font = UIFont(name:"helvetica-bold", size: 13)
                lbl_Approval.text = "Leave Approval"
                
               
                
                self.vscrollView.addSubview(lbl_Approval)
                
                let btnViewAll : UIButton = UIButton(frame: CGRectMake(self.view.frame.size.width / 100 * 70 , 350, self.view.frame.size.width / 100 * 23,20))
                btnViewAll.backgroundColor = UIColor(hexString: "#2190ea")
                btnViewAll.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                
                btnViewAll.titleLabel!.font =  UIFont(name:"helvetica", size: 10)
                btnViewAll.setTitle("View All", forState: .Normal)
                btnViewAll.addTarget(self, action: "ShowTeamLeaveList:", forControlEvents: .TouchUpInside)
                // btnViewAll.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
                self.vscrollView.addSubview(btnViewAll)
                
                self.LeaveApprovalJSON = JSON(data: self.objCallBack.GetJSONData(ResultString)!)
                
                if self.dLeaveCollectionView != nil {
                    self.dLeaveCollectionView.removeFromSuperview()
                }
                
                
                
                let dCollectionViewFlowLayout : UICollectionViewFlowLayout = UICollectionViewFlowLayout ()
                self.dLeaveCollectionView = UICollectionView (frame: self.view.frame, collectionViewLayout: dCollectionViewFlowLayout)
                self.dLeaveCollectionView.delegate = self
                self.dLeaveCollectionView.dataSource = self
                self.dLeaveCollectionView.backgroundColor = UIColor.blackColor()
                self.dLeaveCollectionView.scrollEnabled=false
                self.dLeaveCollectionView.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "cellIdentifierLeave")
                self.vscrollView.addSubview(self.dLeaveCollectionView)
                
                let ContainerView2 = UIView(frame: CGRectMake(0, 375, self.view.frame.size.width,1))
                ContainerView2.layer.borderColor = UIColor(hexString: "#cccccc")?.CGColor
                ContainerView2.layer.borderWidth = 1
                
                self.vscrollView.addSubview(ContainerView2)
                
                self.dLeaveCollectionView.frame = CGRectMake(0,395, self.view.frame.size.width,200)
        }else
        {
            self.LeaveApprovalJSON = nil
            if(self.dLeaveCollectionView != nil)
            {
                self.dLeaveCollectionView.removeFromSuperview()
            }
            
            
            
        }
           
            
        })
        
        
        
        
    }
    
    //Handles leave approval event
     func ApproveTouch(sender: UIButton) {
        
        objLRActivityIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
        
        objLRActivityIndicaor.activityIndicatorViewStyle = .WhiteLarge
        let objPosition =  self.view.center
        //  objPosition.y =  objPosition.y.advancedBy(-40.0)
        objLRActivityIndicaor.center = objPosition
        //objActivityIndicaor.transform = CGAffineTransformMakeScale(1.5, 1.5);
        objLRActivityIndicaor.hidesWhenStopped = true
        objLRActivityIndicaor.color = UIColor.grayColor()
        self.view.addSubview(objLRActivityIndicaor)
        objLRActivityIndicaor.startAnimating()
        self.ProcessLeaveRequest("2",sender: sender)
        
    }
    
    //Handles leave reject event
     func RejectTouch(sender: UIButton) {
        
        objLRActivityIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
        
        objLRActivityIndicaor.activityIndicatorViewStyle = .WhiteLarge
        let objPosition =  self.view.center
        objLRActivityIndicaor.center = objPosition
        objLRActivityIndicaor.hidesWhenStopped = true
        objLRActivityIndicaor.color = UIColor.grayColor()
        self.view.addSubview(objLRActivityIndicaor)
        objLRActivityIndicaor.startAnimating()
        self.ProcessLeaveRequest("3",sender: sender)
      
    }
    func ProcessLeaveRequest(ActionCode: String , sender: UIButton)
    {
        self.CurrentActionCode = ActionCode
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            objCallBack = AjaxCallBack()
            
            
            objCallBack.MethodName = "ProcessAttendanceNote"
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pRequestID",value: String(sender.tag))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pAction",value: ActionCode)
            objCallBack.ParameterList.append(objparam)
            
            objCallBack.post(ApproveOnSuccess, OnError: ApproveOnError)
            
            
            
        }
        
    }
    
    //Response on error callback
    func ApproveOnError(Response: NSError ){
        
        
    }
    
    //Respone on success callback
    func ApproveOnSuccess(ResultString :NSString? ) {
        
      dispatch_async(dispatch_get_main_queue(),{
        self.objLRActivityIndicaor.stopAnimating()
        DoneHUD.showInView(self.view, message: "Done")
        self.FetEmployeeApprovalLeave()
        self.dLeaveCollectionView.reloadData()
      })
        
    }

    
}
