//
//  CommonMethods.swift
//  Talentoz
//
//  Created by forziamac on 13/05/16.
//  Copyright © 2016 Forzia Tech Private Ltd. All rights reserved.
//


import UIKit

public extension String {
    
    subscript (i: Int) -> Character {
        return self[self.startIndex.advancedBy(i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        return substringWithRange(Range(start: startIndex.advancedBy(r.startIndex), end: startIndex.advancedBy(r.endIndex)))
    }
}

 

public func ConvertValueWithDecimal(inputString: String, NoofDecimal: Int) -> String
{
    var ResultString:String! = ""
    
    var FormatStr:String = ""
    
    FormatStr = "%." + String(NoofDecimal) + "f"
    
    
    let DateLineArr = inputString.componentsSeparatedByString(".")
    
    if DateLineArr.count > 1
    {
        ResultString = String(format: FormatStr, Double(inputString)!)
    }
    else
    {
        ResultString = inputString + ".0"
        
        ResultString = String(format: FormatStr,  Double(inputString)!)
        
    }
    
    
    return ResultString
}

public func CalculateWidthPercentage(pAmount:String , pHoleWidth: CGFloat , Type: Int , Gap : CGFloat ) -> CGFloat  {
    let charcount = pAmount.characters.count
    var CurrencySymbolWidth:CGFloat = 0.0
    var CurrencyWidth : CGFloat = 0.0
    
    
    if charcount <= 4        {
        
        CurrencySymbolWidth = (pHoleWidth - Gap) / 100 * 39.5
        CurrencyWidth = (pHoleWidth - Gap) / 100 * 60.5
    }
    else if charcount == 5
    {
        CurrencySymbolWidth = (pHoleWidth - Gap) / 100 * 38.5
        CurrencyWidth = (pHoleWidth - Gap) / 100 * 61.5
    }
    else if charcount == 6
    {
        CurrencySymbolWidth = (pHoleWidth - Gap) / 100 * 37.5
        CurrencyWidth = (pHoleWidth - Gap) / 100 * 62.5
    }
    else if charcount == 7
    {
        CurrencySymbolWidth = (pHoleWidth - Gap) / 100 * 36.5
        CurrencyWidth = (pHoleWidth - Gap) / 100 * 63.5
    }
    else if charcount == 8
    {
        CurrencySymbolWidth = (pHoleWidth - Gap) / 100 * 35.5
        CurrencyWidth = (pHoleWidth - Gap) / 100 * 64.5
    }
    else if charcount == 9
    {
        CurrencySymbolWidth = (pHoleWidth - Gap) / 100 * 30
        CurrencyWidth = (pHoleWidth - Gap) / 100 * 70
    }
    else if charcount == 10
    {
        CurrencySymbolWidth = (pHoleWidth - Gap) / 100 * 28
        CurrencyWidth = (pHoleWidth - Gap) / 100 * 72
    }
    else if charcount == 11
    {
        CurrencySymbolWidth = (pHoleWidth - Gap) / 100 * 26
        CurrencyWidth = (pHoleWidth - Gap) / 100 * 74
    }
    else
    {
        CurrencySymbolWidth = (pHoleWidth - Gap) / 100 * 24
        CurrencyWidth = (pHoleWidth - Gap) / 100 * 76
    }
    
    if (Type == 0)// Currency Symbol
    {
        return CurrencySymbolWidth
    }
    else // Amount
    {
        return CurrencyWidth
    }
    
    
}