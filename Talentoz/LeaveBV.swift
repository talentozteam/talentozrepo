//
//  LeaveBV.swift
//  Talentoz
//
//  Created by forziamac on 22/01/16.
//  Copyright © 2016 Forzia Tech Private Ltd. All rights reserved.
//

import UIKit

class LeaveBV : UIViewController  {
    internal var LeaveDays: Int!
    
    @IBOutlet var lblLeaveDays: UILabel!
    
    @IBOutlet var leavebtn5: UIButton!
    @IBOutlet var leavedaystxt: UILabel!
    @IBOutlet var totalleavelbltitle: UILabel!
    override func viewDidLoad() {
        lblLeaveDays.text = String(LeaveDays)
        
        
        
        
        self.totalleavelbltitle.frame = CGRectMake(0 , 5 ,self.view.frame.size.width, 25)
        
        self.lblLeaveDays.frame = CGRectMake(self.view.frame.size.width / 100 * 30 , 60 , self.view.frame.size.width / 100 * 30 , 35)
        self.leavedaystxt.frame = CGRectMake(self.view.frame.size.width / 100 * 50 , 70 , self.view.frame.size.width / 100 * 20 , 25)
        
        
        
        self.leavebtn5.frame = CGRectMake(self.view.frame.size.width / 100 * 30 ,  120 ,self.view.frame.size.width / 100 * 40, 30)
        
        
        self.leavebtn5.backgroundColor = UIColor(hexString: "#455A64")
        
        
    }
    
    @IBAction func btnLeaveDetail_Click(sender: AnyObject) {
        let LeaveListObj = LeaveList()        
        self.presentViewController(LeaveListObj, animated: true, completion: nil)
    }
    
}

