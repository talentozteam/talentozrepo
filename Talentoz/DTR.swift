
//
//  DTR.swift
//  Talentoz
//
//  Created by forziamac on 29/01/16.
//  Copyright © 2016 Forzia Tech Private Ltd. All rights reserved.
//


import UIKit

class DTRList: BaseTabBarVC, UITableViewDataSource ,UITableViewDelegate {
    
    var customSC: UISegmentedControl?
    var SelfDTRListData : Talentoz.JSON!
    var TeamDTRListData : Talentoz.JSON!
    var CurrentDTRListData : Talentoz.JSON!
    internal var ModeofShow: Int! = 0
    var UserID : Int!
    var RequestID : Int!
    var objCallBack = AjaxCallBack()
    let defaults = NSUserDefaults.standardUserDefaults()
    let objRole = RoleManager()
    var DTRListView = UITableView()
    var objLoadingIndicaor : UIActivityIndicatorView!
    var swipeRight : UISwipeGestureRecognizer!
    var swipeLeft : UISwipeGestureRecognizer!
    var MonthYear: String!
    var ContainerTitalView = UIView()
    var Month: Int!
    var Year: Int!
    
    internal var FilterView : UIView!
    internal var FilterBGLayer : UIView!
    var isFilterOpen = false
    
    override func viewDidLoad() {
        
        
        self.view.backgroundColor = UIColor(hexString: "#f5f5f5")
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            self.UserID = Int(ResultJSON!["UserID"]as! NSNumber)
            
        }
        
        super.UINavigationBarTitle = "Time Record"
        super.viewDidLoad()
        
        let date = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Year,.Month], fromDate: date)
        
        self.Month = Int(components.month)
        self.Year = Int(components.year)
        
        BindContextTab()
        
        if objRole.ISManager{
            
            self.AddGesture()
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func BindContextTab()
    {
        var items = ["My Time Record"]
        if objRole.ISManager{
            items = ["My Time Record", "Team Time Record"]
        }
        
        
        
        
        let DTRListView=UIView(frame: CGRectMake(0, 60,  Viewframe.width , 48))
        DTRListView.backgroundColor = UIColor(colorLiteralRed: 28/255, green: 138/255, blue: 255/255, alpha: 1)
        
        
        customSC = UISegmentedControl(items: items)
        customSC!.selectedSegmentIndex = 0
        
        
        customSC!.frame = CGRectMake(Viewframe.width / 100 * 2, 5,
            Viewframe.width / 100 * 96,40)
        
        customSC!.layer.cornerRadius = 0.5  // Don't let background bleed
        customSC!.backgroundColor = UIColor(colorLiteralRed: 28/255, green: 138/255, blue: 255/255, alpha: 1)
        customSC!.tintColor = UIColor.whiteColor()
        customSC!.layer.borderColor = UIColor.whiteColor().CGColor
        customSC!.layer.borderWidth = 0
        
        customSC!.addTarget(self, action: "changeColor:", forControlEvents: .ValueChanged)
        
        DTRListView.addSubview(customSC!)
        
        self.view.addSubview(DTRListView)
        
        customSC!.selectedSegmentIndex = self.ModeofShow
        BindDTRList()
        
    }
    
    func LoadTeamDTRList(gestureReconizer: UISwipeGestureRecognizer) {
        customSC!.selectedSegmentIndex = 1
        BindDTRList()
    }
    
    func LoadSelfDTRList(gestureReconizer: UISwipeGestureRecognizer) {
        customSC!.selectedSegmentIndex = 0
        BindDTRList()
    }
    
    
    func changeColor(sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            BindDTRList()
            break;
        case 1:
            BindDTRList()
            break;
        default:
            break;
        }
    }
    
    
    
    func BindDTRList()
    {
        
        if self.customSC!.selectedSegmentIndex == 0
        {
            if let TempData = self.SelfDTRListData
            {
                ConstructDTRList()
            }else
            {
                GetDTRList(0)
            }
            
        }
        else
        {
            if let TempData = self.TeamDTRListData
            {
                ConstructDTRList()
            }else
            {
                GetDTRList(1)
            }
            
        }
        
    }
    
    func GetDTRList(Modeofview : Int)
    {
        objLoadingIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, Viewframe.width, Viewframe.height))
        
        objLoadingIndicaor.activityIndicatorViewStyle = .WhiteLarge
        let objPosition =  self.view.center
        objLoadingIndicaor.center = objPosition
        objLoadingIndicaor.hidesWhenStopped = true
        objLoadingIndicaor.color = UIColor.grayColor()
        self.view.addSubview(objLoadingIndicaor)
        objLoadingIndicaor.startAnimating()
        
        objCallBack = AjaxCallBack()
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            if customSC!.selectedSegmentIndex == 0
            {
                objCallBack.MethodName = "GetEmployeeDTR"
                
                
                objparam = Parameter(Name: "pMonth",value: String(self.Month))
                objCallBack.ParameterList.append(objparam)
                
                objparam = Parameter(Name: "pYear",value: String(self.Year))
                objCallBack.ParameterList.append(objparam)
          
            }
            else
            {
                objCallBack.MethodName = "GetSubOrdinatesTodayDTR"
                
            }
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            
            objCallBack.post(DTRListOnComplete, OnError: DTRListOnErrorOnError)
            
        }
        
    }
    //Bind Leave list table
    
    //Response on error callback
    func DTRListOnErrorOnError(Response: NSError ){
        
    }
    
    
    func DTRListOnComplete (ResultString :NSString? ) {
        dispatch_async(dispatch_get_main_queue(),{
            
            self.objLoadingIndicaor.stopAnimating()
            
            if self.customSC!.selectedSegmentIndex == 0
            {
                self.SelfDTRListData =   JSON(data: self.objCallBack.GetJSONData(ResultString)!)
                self.CurrentDTRListData = self.SelfDTRListData
                self.ConstructDTRList()
                
            }
            else
            {
                self.TeamDTRListData =   JSON(data: self.objCallBack.GetJSONData(ResultString)!)
                self.CurrentDTRListData = self.TeamDTRListData
                self.ConstructDTRList()
                
            }
        })
            
        
        
    }
    
    
    func ConstructDTRList()
    {
        
      
            self.ContainerTitalView.removeFromSuperview()
            self.DTRListView.removeFromSuperview()
        
        
        
            if self.customSC!.selectedSegmentIndex == 0
            {
                self.CurrentDTRListData = self.SelfDTRListData
                self.DTRListView = UITableView(frame: CGRectMake(0, 183, self.view.frame.width, self.Viewframe.height  - 217 ))
                
            }
            else
            {
                self.CurrentDTRListData =  self.TeamDTRListData
                
                self.DTRListView = UITableView(frame: CGRectMake(0, 133, self.view.frame.width, self.Viewframe.height  - 177 ))
            }
            
          
           
            self.DTRListView.showsVerticalScrollIndicator = true
            self.DTRListView.delegate = self
            self.DTRListView .dataSource = self
            if self.customSC!.selectedSegmentIndex == 0
            {
                self.DTRListView.rowHeight = 50
                 self.DTRListView.separatorColor = UIColor.whiteColor()
            }
            else
            {
                self.ContainerTitalView.removeFromSuperview()
                self.DTRListView.rowHeight = 102
                self.DTRListView.separatorColor = UIColor.whiteColor()
            }
            
            self.DTRListView.backgroundColor = UIColor.clearColor()
            self.view.addSubview(self.DTRListView )
            
     
        
        
    }
    
    
 
    
    func numberOfSectionsInTableView(DTRListView: UITableView) -> Int {
        return 1
    }
    
    func tableView(DTRListView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return self.CurrentDTRListData.count
        
    }
    
    
    func tableView(DTRListView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // 3
        let cell = UITableViewCell()
        cell.frame = CGRectMake(0, 30, self.view.frame.width, 400)
        
        cell.backgroundColor = UIColor(hexString: "#f5f5f5")
        //Employee image
        
        var strDate : String!
        
        
        if Int((self.customSC?.selectedSegmentIndex)!) == 0
        {
           
            if indexPath.row % 2 == 0
            {
                cell.backgroundColor = UIColor.whiteColor()
            }
            else
            {
                cell.backgroundColor = UIColor(hexString: "#f5f5f5")
            }
            
           strDate = String(self.CurrentDTRListData[indexPath.row]["ReportingDate"])
           var array = strDate.componentsSeparatedByString("T")
           strDate = array[0]
           array = strDate.componentsSeparatedByString("-")
            
            
            if indexPath.row == 0
            {
                self.MonthYear = GetMonthName(strDate) + "   " + String(array[0])
                self.ContainerTitalView = UIView(frame: CGRectMake(0, 108, self.view.frame.width , 65))
                
                self.ContainerTitalView.backgroundColor = UIColor(hexString: "#f5f5f5")
                self.view.addSubview(self.ContainerTitalView)
                
                
               let  btn_MonthYear : UIButton = UIButton(frame:CGRectMake(self.ContainerTitalView.frame.size.width / 100 * 4 , 10, self.ContainerTitalView.frame.size.width / 100 * 92 ,30))
                btn_MonthYear.setTitle( self.MonthYear , forState: .Normal)
             
                
                btn_MonthYear.backgroundColor = UIColor.whiteColor()
                btn_MonthYear.setTitleColor(UIColor(hexString: "#666666"),forState: UIControlState.Normal)
                btn_MonthYear.titleLabel!.font =  UIFont(name:"helvetica-Bold", size: 14)
                btn_MonthYear.addTarget(self, action: "openFilter:", forControlEvents: .TouchUpInside)
                self.ContainerTitalView.addSubview(btn_MonthYear)
                
                let btn_dropdownsymbol : UIButton = UIButton(frame: CGRectMake(self.ContainerTitalView.frame.size.width / 100 * 70 , 10 ,self.ContainerTitalView.frame.size.width * 0.05, 30))
                btn_dropdownsymbol.setFAIcon(FAType.FAAngleDown, iconSize: CGFloat(20),forState: .Normal)
                btn_dropdownsymbol.backgroundColor = UIColor.clearColor()
                btn_dropdownsymbol.setTitleColor(UIColor(hexString: "#666666"), forState: UIControlState.Normal)
                btn_dropdownsymbol.addTarget(self, action: "openFilter:", forControlEvents: .TouchUpInside)
                
                self.ContainerTitalView.addSubview(btn_dropdownsymbol)
                
                
                let lbl_DayTitle : UILabel = UILabel(frame: CGRectMake(0 , 45, self.view.frame.size.width / 100 * 25 ,30))
                lbl_DayTitle.backgroundColor = UIColor(hexString: "#eeeeee")
                lbl_DayTitle.textAlignment = .Center
                
                lbl_DayTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
                lbl_DayTitle.text = "Day"
                lbl_DayTitle.textColor = UIColor(hexString: "#333333")
                self.ContainerTitalView.addSubview(lbl_DayTitle)
                
                
                let lbl_InTimeTitle : UILabel = UILabel(frame: CGRectMake(self.view.frame.size.width / 100 * 25 , 45, self.view.frame.size.width / 100 * 25 ,30))
                lbl_InTimeTitle.backgroundColor = UIColor(hexString: "#eeeeee")
                lbl_InTimeTitle.textAlignment = .Center
                lbl_InTimeTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
                lbl_InTimeTitle.text = "In Time"
                lbl_InTimeTitle.textColor = UIColor(hexString: "#333333")
                self.ContainerTitalView.addSubview(lbl_InTimeTitle)
                
                let lbl_OutTimeTitle : UILabel = UILabel(frame: CGRectMake(self.view.frame.size.width / 100 * 50 , 45, self.view.frame.size.width / 100 * 25 ,30))
              lbl_OutTimeTitle.backgroundColor = UIColor(hexString: "#eeeeee")
                lbl_OutTimeTitle.textAlignment = .Center
                lbl_OutTimeTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
                lbl_OutTimeTitle.text = "Out Time"
                self.ContainerTitalView.addSubview(lbl_OutTimeTitle)
                 lbl_OutTimeTitle.textColor = UIColor(hexString: "#333333")
                
                let lbl_TotalHrsTitle : UILabel = UILabel(frame: CGRectMake(self.view.frame.size.width / 100 * 75 , 45, self.view.frame.size.width / 100 * 25 ,30))
                lbl_TotalHrsTitle.backgroundColor = UIColor(hexString: "#eeeeee")
                lbl_TotalHrsTitle.textAlignment = .Center
                lbl_TotalHrsTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
                lbl_TotalHrsTitle.text = "Total Hours"
                lbl_TotalHrsTitle.textColor = UIColor(hexString: "#333333")
                self.ContainerTitalView.addSubview(lbl_TotalHrsTitle)
            }
            
            let lbl_Month : UILabel = UILabel(frame: CGRectMake(0 , 5, self.view.frame.size.width / 100 * 23 ,20))
            lbl_Month.backgroundColor = UIColor.clearColor()
            lbl_Month.textAlignment = .Center
            lbl_Month.font = UIFont(name:"HelveticaNeue", size: 20.0)
            lbl_Month.textColor = UIColor(hexString: "#666666")
            lbl_Month.text = String(array[2])
            cell.addSubview(lbl_Month)
            
            let lbl_Day : UILabel = UILabel(frame: CGRectMake(0, 27, self.view.frame.size.width / 100 * 23 ,20))
            lbl_Day.backgroundColor = UIColor.clearColor()
            lbl_Day.textAlignment = .Center
            lbl_Day.textColor = UIColor(hexString: "#666666")
            lbl_Day.font = UIFont(name:"HelveticaNeue", size: 12.0)
            lbl_Day.text = String(self.GetWeekDayName(strDate))
            cell.addSubview(lbl_Day)
            
            var HalfDayHeightMove : CGFloat = 0.0
            
            if self.CurrentDTRListData[indexPath.row]["IsHalfDayLeave"] == 1
            {
                if(self.CurrentDTRListData[indexPath.row]["LeaveID"] != nil)
                {
                    
                    if(Int(self.CurrentDTRListData[indexPath.row]["LeaveID"].int!) > 0)
                    {
                        HalfDayHeightMove = 7
                    }
                }
            }
            
            
            let lbl_InTime : UILabel = UILabel(frame: CGRectMake(self.view.frame.size.width / 100 * 25 , 15 - HalfDayHeightMove, self.view.frame.size.width / 100 * 25 ,20))
            lbl_InTime.backgroundColor = UIColor.clearColor()
            lbl_InTime.textAlignment = .Center
            
            lbl_InTime.font = UIFont(name:"HelveticaNeue", size: 12.0)
           lbl_InTime.textColor = UIColor.blackColor()
            
            
            if self.CurrentDTRListData[indexPath.row]["InTime"] != nil
            {
            lbl_InTime.text = String(self.CurrentDTRListData[indexPath.row]["InTime"])
            }
            cell.addSubview(lbl_InTime)
            
            let lbl_OutTime : UILabel = UILabel(frame: CGRectMake(self.view.frame.size.width / 100 * 50 , 15 - HalfDayHeightMove, self.view.frame.size.width / 100 * 25 ,20))
            lbl_OutTime.backgroundColor = UIColor.clearColor()
            lbl_OutTime.textAlignment = .Center
            lbl_OutTime.textColor = UIColor.blackColor()
            lbl_OutTime.font = UIFont(name:"HelveticaNeue", size: 12.0)
            if self.CurrentDTRListData[indexPath.row]["OutTime"] != nil
            {
                  lbl_OutTime.text = String(self.CurrentDTRListData[indexPath.row]["OutTime"])
            }
          
            cell.addSubview(lbl_OutTime)
            
            let lbl_TotalHrs : UILabel = UILabel(frame: CGRectMake(self.view.frame.size.width / 100 * 75 , 15 - HalfDayHeightMove, self.view.frame.size.width / 100 * 25 ,20))
            lbl_TotalHrs.backgroundColor = UIColor.clearColor()
            lbl_TotalHrs.textAlignment = .Center
            lbl_TotalHrs.textColor = UIColor.blackColor()
            lbl_TotalHrs.font = UIFont(name:"HelveticaNeue", size: 12.0)
            if String(self.CurrentDTRListData[indexPath.row]["TotalHours"]) != "00:00"
            {
                lbl_TotalHrs.text = String(self.CurrentDTRListData[indexPath.row]["TotalHours"])
            }
            
            
            cell.addSubview(lbl_TotalHrs)
            
            let GestureLeaveDetail = UITapGestureRecognizer(target: self, action: "ShowActivitiesDetail:")
            cell.addGestureRecognizer(GestureLeaveDetail)
            
            
            
            // Start
            if (self.CurrentDTRListData[indexPath.row]["InTime"] != nil ){
                
                if(self.CurrentDTRListData[indexPath.row]["Tardiness"] != nil)
                {
                    
                    if (self.CurrentDTRListData[indexPath.row]["HolidayID"]  < 4 && self.CurrentDTRListData[indexPath.row]["LeaveID"] == nil || self.CurrentDTRListData[indexPath.row]["LeaveID"] != nil && self.CurrentDTRListData[indexPath.row]["LeaveStatus"] != "2")   {
                        if self.CurrentDTRListData[indexPath.row]["Tardiness"] != nil{
                            if (Int(self.CurrentDTRListData[indexPath.row]["Tardiness"].int!) > 0) {
                                lbl_InTime.textColor = UIColor(hexString: "#cc0000")
                            }
                        }
                        
                    }
                }
                else
                {
                    lbl_InTime.textColor = UIColor(hexString: "#000000")
                    
                }
            }
            else
            {
                lbl_InTime.text = "-"
                lbl_InTime.textColor = UIColor(hexString: "#000000")
               
                if(self.CurrentDTRListData[indexPath.row]["OutTime"] == nil)
                {
                    if(self.CurrentDTRListData[indexPath.row]["HolidayID"] != nil)
                    {
                        if(Int(self.CurrentDTRListData[indexPath.row]["HolidayID"].int!) > 3)
                        {
                             lbl_InTime.text =  self.CurrentDTRListData[indexPath.row]["HolidayDesc"].string
                             lbl_InTime.textAlignment = .Left
                             lbl_InTime.frame = CGRectMake(self.view.frame.size.width / 100 * 30 , 15, self.view.frame.size.width / 100 * 70 ,20)
                            
                            
                        }
                        else
                        {
                            if(self.CurrentDTRListData[indexPath.row]["LeaveID"] != nil)
                            {
                        
                            if(Int(self.CurrentDTRListData[indexPath.row]["LeaveID"].int!) > 0)
                            {
                                lbl_InTime.text = self.CurrentDTRListData[indexPath.row]["LeaveDesc"].string
                                lbl_InTime.textAlignment = .Left
                                lbl_InTime.frame = CGRectMake(self.view.frame.size.width / 100 * 30 , 15, self.view.frame.size.width / 100 * 70 ,20)
                            }
                            }
                            else
                            {
                                if(self.CurrentDTRListData[indexPath.row]["AbsentText"] == "1")
                                {
                                    
                                    lbl_InTime.text =  "Absent"
                                    lbl_InTime.textColor = UIColor(hexString: "#cc0000")
                                    lbl_InTime.textAlignment = .Left
                                    lbl_InTime.frame = CGRectMake(self.view.frame.size.width / 100 * 30 , 15, self.view.frame.size.width / 100 * 70 ,20)
                                }
                            }
                        }
                    }
                        
                    else if(self.CurrentDTRListData[indexPath.row]["LeaveID"] != nil)
                    {
                        if(Int(self.CurrentDTRListData[indexPath.row]["LeaveID"].int!)>0)
                        {
                            lbl_InTime.text = self.CurrentDTRListData[indexPath.row]["LeaveDesc"].string
                            lbl_InTime.textAlignment = .Left
                            lbl_InTime.frame = CGRectMake(self.view.frame.size.width / 100 * 30 , 15, self.view.frame.size.width / 100 * 70 ,20)
                        }
                    }
                        
                    else
                    {
                        
                        if(self.CurrentDTRListData[indexPath.row]["AbsentText"] == "1")
                        {
                            
                            lbl_InTime.text =  "Absent"
                            lbl_InTime.textColor = UIColor(hexString: "#cc0000")
                            lbl_InTime.textAlignment = .Left
                             lbl_InTime.frame = CGRectMake(self.view.frame.size.width / 100 * 30 , 15, self.view.frame.size.width / 100 * 70 ,20)
                        }
                        
                    }
                    
                }
                
            }
            
            
            if(self.CurrentDTRListData[indexPath.row]["OutTime"] != nil) {
                lbl_OutTime.text =  self.CurrentDTRListData[indexPath.row]["OutTime"].string
                if (self.CurrentDTRListData[indexPath.row]["PeakHoursUnderTime"] != nil) {
                    if (self.CurrentDTRListData[indexPath.row]["HolidayID"] < 4 && (self.CurrentDTRListData[indexPath.row]["LeaveID"] == nil || (self.CurrentDTRListData[indexPath.row]["LeaveID"] != nil && self.CurrentDTRListData[indexPath.row]["LeaveStatus"] != "2"))) {
                        
                        if (Int(self.CurrentDTRListData[indexPath.row]["PeakHoursUnderTime"].int!) > 0) {
                            lbl_OutTime.textColor = UIColor(hexString: "#cc0000")
                        }
                    }
                    
                }
            }
            else
            {
                if(self.CurrentDTRListData[indexPath.row]["OutTime"] == nil){
                    
                }
                else
                {
                    lbl_OutTime.text =  "-"
                }
                
                
                lbl_OutTime.textColor = UIColor(hexString: "#000000")
            }
            
            if(self.CurrentDTRListData[indexPath.row]["TotalHours"].string == "00:00")
            {
                
                if(self.CurrentDTRListData[indexPath.row]["InTime"] == nil && self.CurrentDTRListData[indexPath.row]["OutTime"] == nil ){
                    
                }
                else
                {
                    lbl_TotalHrs.text =  "-"
                    
                }
                
                
            }else
            {
                if (self.CurrentDTRListData[indexPath.row]["UnderTime"] != nil) {
                    if (self.CurrentDTRListData[indexPath.row]["HolidayID"] < 4  && (self.CurrentDTRListData[indexPath.row]["LeaveID"] == nil || (self.CurrentDTRListData[indexPath.row]["LeaveID"] != nil && self.CurrentDTRListData[indexPath.row]["LeaveStatus"] != "2"))) {
                        
                        if (Int(self.CurrentDTRListData[indexPath.row]["UnderTime"].int!) > 0) {
                           
                            lbl_TotalHrs.textColor = UIColor(hexString: "#cc0000")
                        }
                    }
                    
                }
                lbl_TotalHrs.text = self.CurrentDTRListData[indexPath.row]["TotalHours"].string
            }
            
            
            if HalfDayHeightMove > 0
            {
                let lbl_HalfDayLeaveDesc : UILabel = UILabel(frame: CGRectMake(self.view.frame.size.width / 100 * 30 , 25, self.view.frame.size.width / 100 * 70 ,20))
                lbl_HalfDayLeaveDesc.backgroundColor = UIColor.clearColor()
                lbl_HalfDayLeaveDesc.textAlignment = .Left
                lbl_HalfDayLeaveDesc.font = UIFont(name:"HelveticaNeue", size: 10.0)
                lbl_HalfDayLeaveDesc.textColor = UIColor.blackColor()
                lbl_HalfDayLeaveDesc.text = self.CurrentDTRListData[indexPath.row]["LeaveDesc"].string
               
                cell.addSubview(lbl_HalfDayLeaveDesc)
                
            }

            // End
            
            
        }
        else
        {
            
            if indexPath.row == 0
            {
                
                let HeaderView : UIView = UIView(frame: CGRectMake(0, 108, self.view.frame.width , 25))
                HeaderView.backgroundColor = UIColor(hexString: "#f5f5f5")
                self.view.addSubview(HeaderView)
                
                let lbl_TodayTitle : UILabel = UILabel(frame: CGRectMake(0, 0, HeaderView.frame.width ,30))
                lbl_TodayTitle.backgroundColor = UIColor.clearColor()
                lbl_TodayTitle.textAlignment = .Center
                lbl_TodayTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
                lbl_TodayTitle.textColor = UIColor(hexString: "#333")
                lbl_TodayTitle.text =  "Today"
                HeaderView.addSubview(lbl_TodayTitle)
                
            }
            
           let Containerdtr = UIView(frame: CGRectMake(cell.frame.width / 100 * 2, 10, cell.frame.width / 100 * 96 , 100))
            Containerdtr.backgroundColor = UIColor.whiteColor()
            cell.addSubview(Containerdtr)
            
            
            if let tmpDomainURL = self.defaults.stringForKey("DomainURL")
            {
                
                if  (self.CurrentDTRListData[indexPath.row]["EmployeePhoto"].type == Talentoz.Type.Null || self.CurrentDTRListData[indexPath.row]["EmployeePhoto"].string == ""  )
                {
                    let img_EmpPic:  UIButton = UIButton(frame: CGRectMake(10, 25, 50, 50))
                    img_EmpPic.layer.borderWidth = 0.5
                    img_EmpPic.layer.masksToBounds = false
                    img_EmpPic.layer.borderColor = UIColor.lightGrayColor().CGColor
                    img_EmpPic.backgroundColor = UIColor.SetRandomColor()
                    img_EmpPic.layer.cornerRadius = img_EmpPic.frame.height/2
                    img_EmpPic.clipsToBounds = true
                    img_EmpPic.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                    img_EmpPic.setTitle(String(self.CurrentDTRListData[indexPath.row]["EmployeeName"])[0], forState: UIControlState.Normal)
                    Containerdtr.addSubview(img_EmpPic)
                }
                else{
                    let img_EmpPic:  UIImageView = UIImageView(frame: CGRectMake(10, 25, 50, 50))
                    img_EmpPic.layer.borderWidth = 0.5
                    img_EmpPic.layer.masksToBounds = false
                    img_EmpPic.layer.borderColor = UIColor.lightGrayColor().CGColor
                    img_EmpPic.backgroundColor = UIColor(hexString: String("#0CDBFF"))
                    img_EmpPic.layer.cornerRadius = img_EmpPic.frame.height/2
                    img_EmpPic.clipsToBounds = true
                    
                    Containerdtr.addSubview(img_EmpPic)
                    
                    if self.CurrentDTRListData[indexPath.row]["EmployeePhoto"] != nil
                    {
                        let employeePic = "http://" + tmpDomainURL + "/Uploadedfiles/Client" + String(self.defaults.stringForKey("ClientID")!) + "/Image/" + String(self.CurrentDTRListData[indexPath.row]["EmployeePhoto"])
                        img_EmpPic.setImageWithUrl(NSURL(string: employeePic)!)
                    }
                }
                
                
                
            }
            
            
            let lbl_EmployeeName : UILabel = UILabel(frame: CGRectMake(70, 25, (self.view.frame.size.width - 70) / 100 * 90 ,18))
            // lbl_EmployeeName.backgroundColor = UIColor.whiteColor()
            lbl_EmployeeName.textAlignment = .Left
            lbl_EmployeeName.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
            lbl_EmployeeName.textColor = UIColor(hexString: String("#333333"))
            lbl_EmployeeName.text =  String(self.CurrentDTRListData[indexPath.row]["EmployeeName"])
            Containerdtr.addSubview(lbl_EmployeeName)
            
            let lbl_PositionName : UILabel = UILabel(frame: CGRectMake(70, 43, (self.view.frame.size.width - 70) / 100 * 55 ,18))
            // lbl_PositionName.backgroundColor = UIColor.whiteColor()
            lbl_PositionName.textAlignment = .Left
            lbl_PositionName.textColor = UIColor(hexString: String("#666666"))
            lbl_PositionName.font = UIFont(name:"HelveticaNeue", size: 12.0)
            lbl_PositionName.text = String(self.CurrentDTRListData[indexPath.row]["PositionName"])
            Containerdtr.addSubview(lbl_PositionName)
            
            
            
            if self.CurrentDTRListData[indexPath.row]["Status"] != 1
            {
                let lbl_FirstChckIntxt : UILabel = UILabel(frame: CGRectMake((self.view.frame.size.width ) / 100 * 55 , 10, (self.view.frame.size.width) / 100 * 20 ,20))
                //lbl_LeaveType.backgroundColor = UIColor.whiteColor()
                lbl_FirstChckIntxt.textAlignment = .Left
                lbl_FirstChckIntxt.textColor = UIColor(hexString: String("#666666"))
                lbl_FirstChckIntxt.font = UIFont(name:"HelveticaNeue", size: 12.0)
                lbl_FirstChckIntxt.text =  "First Check In"
                Containerdtr.addSubview(lbl_FirstChckIntxt)
            }
            
            
            
            
            let lbl_FirstCheckIN : UILabel = UILabel(frame: CGRectMake(self.view.frame.size.width / 100 * 80  , 10, (self.view.frame.size.width) / 100 * 20 ,20))
            //lbl_LeaveDate.backgroundColor = UIColor.whiteColor()
            lbl_FirstCheckIN.textAlignment = .Left
            lbl_FirstCheckIN.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
            lbl_FirstCheckIN.textColor = UIColor(hexString: "#666666")

            if self.CurrentDTRListData[indexPath.row]["Status"] == 1
            {
                lbl_FirstCheckIN.textColor = UIColor(hexString: "#cc0000")
            }
            else if self.CurrentDTRListData[indexPath.row]["Status"] == 0
            {
                if self.CurrentDTRListData[indexPath.row]["Tardiness"] != nil
                {
                     if self.CurrentDTRListData[indexPath.row]["Tardiness"] > 0
                    {
                        lbl_FirstCheckIN.textColor = UIColor(hexString: "#cc0000")
                    }
                }
            }
            
            lbl_FirstCheckIN.text =  String(self.CurrentDTRListData[indexPath.row]["StatusDesc"])
          
            Containerdtr.addSubview(lbl_FirstCheckIN)
            
            if self.CurrentDTRListData[indexPath.row]["Status"].int == 0
            {
                
            let btn_ViewTodayAct : UIButton = UIButton(frame: CGRectMake((self.view.frame.size.width - 70 ) / 100 * 28 , 70, (self.view.frame.size.width ) / 100 * 35,20))
            btn_ViewTodayAct.backgroundColor = UIColor(hexString: "#48c9b0")
            btn_ViewTodayAct.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            btn_ViewTodayAct.titleLabel!.font =  UIFont(name:"helvetica", size: 12)
            btn_ViewTodayAct.setTitle("Today's Activities", forState: .Normal)
            btn_ViewTodayAct.tag = indexPath.row
                btn_ViewTodayAct.layer.cornerRadius=5
            btn_ViewTodayAct.addTarget(self, action: "ShowTodaysActivities:", forControlEvents: .TouchUpInside)
            Containerdtr.addSubview(btn_ViewTodayAct)
            
       
            let btn_MonthlyDTR : UIButton = UIButton(frame: CGRectMake((self.view.frame.size.width ) / 100 * 63 , 70, (self.view.frame.size.width ) / 100 * 35,20))
            btn_MonthlyDTR.setTitle("View Monthly DTR", forState: .Normal)
            btn_MonthlyDTR.backgroundColor = UIColor(hexString: "#2190ea")
            btn_MonthlyDTR.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            btn_MonthlyDTR.titleLabel!.font =  UIFont(name:"helvetica", size: 12)
            btn_MonthlyDTR.tag = indexPath.row
                btn_MonthlyDTR.layer.cornerRadius=5
            btn_MonthlyDTR.addTarget(self, action: "ShowMonthlyDTR:", forControlEvents: .TouchUpInside)
            Containerdtr.addSubview(btn_MonthlyDTR)
            
            }
            else
            {
                
                let btn_MonthlyDTR : UIButton = UIButton(frame: CGRectMake((self.view.frame.size.width - 70	) / 100 * 28 , 70, (self.view.frame.size.width ) / 100 * 35,18))
                btn_MonthlyDTR.setTitle("View Monthly DTR", forState: .Normal)
                btn_MonthlyDTR.backgroundColor = UIColor(hexString: "#2190ea")
                btn_MonthlyDTR.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                btn_MonthlyDTR.titleLabel!.font =  UIFont(name:"helvetica", size: 12)
                btn_MonthlyDTR.tag = indexPath.row
                btn_MonthlyDTR.layer.cornerRadius=5
                btn_MonthlyDTR.addTarget(self, action: "ShowMonthlyDTR:", forControlEvents: .TouchUpInside)
                Containerdtr.addSubview(btn_MonthlyDTR)
                
            }
            
          
        }
        
        

        
        return cell
    }
    
    
 
    
 
   
    
   
    
    func openFilter(sender: UIButton)
    {
        if isFilterOpen
        {
            isFilterOpen = false
            FilterBGLayer.removeFromSuperview()
        }
        else{          
            
            
            isFilterOpen = true
            FilterBGLayer = UIView(frame: CGRectMake(0, 0, Viewframe.width  ,  Viewframe.height))
            FilterBGLayer.backgroundColor = UIColor(colorLiteralRed: 0 , green: 0, blue: 0, alpha: 0.6)
            let GestureLeaveDetail1 = UITapGestureRecognizer(target: self, action: "DismissFilter:")
            FilterBGLayer.addGestureRecognizer(GestureLeaveDetail1)
            FilterView = UIView(frame: CGRectMake(FilterBGLayer.frame.width / 100 * 10 ,100 , FilterBGLayer.frame.width  / 100 * 80, 150))
            FilterView.backgroundColor = UIColor.whiteColor()
            FilterView.layer.cornerRadius = 8
            
            let expiryDatePicker = MonthYearPickerView(frame: CGRectMake(0, 20, FilterView.frame.width   ,  80))
            let year = NSCalendar(identifier: NSCalendarIdentifierGregorian)!.component(.Year, fromDate: NSDate())
            expiryDatePicker.FromYear = year - 5
            expiryDatePicker.LengthOfYear = 7
            expiryDatePicker.commonSetup()
            expiryDatePicker.tintColor = UIColor.yellowColor()
            
            expiryDatePicker.onDateSelected = { (month,year) in
                self.MonthYear = String(expiryDatePicker.months[month - 1 ]) + " " + String(year)
                self.Month = month
                self.Year = year
                
            }
            FilterView.addSubview(expiryDatePicker)
            
            
            let btn_ApplyFilter : UIButton = UIButton(frame: CGRectMake(FilterBGLayer.frame.width / 100 * 32, 110, 50 ,20))
            btn_ApplyFilter.setTitle("Go", forState: .Normal)
            btn_ApplyFilter.backgroundColor = UIColor(hexString: "#2190ea")
            btn_ApplyFilter.layer.cornerRadius = 6
            btn_ApplyFilter.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            btn_ApplyFilter.titleLabel!.font =  UIFont(name:"helvetica", size: 12)
            btn_ApplyFilter.addTarget(self, action: "ApplyFilter:", forControlEvents: .TouchUpInside)
            FilterView.addSubview(btn_ApplyFilter)
            
            
            FilterBGLayer.addSubview(FilterView)
            
            
            self.view.addSubview(FilterBGLayer)
        }
        

    }
    
    func DismissFilter(sender: UIButton)
    {
        isFilterOpen = false
        FilterBGLayer.removeFromSuperview()
        
    }
    
    func ApplyFilter(sender: UIButton) {
    

        
        isFilterOpen = false
        FilterBGLayer.removeFromSuperview()
        GetDTRList(0)
    }
    func ShowActivitiesDetail(sender: UITapGestureRecognizer)
    {
        
        FilterBGLayer = UIView(frame: CGRectMake(0, 0, Viewframe.width  ,  Viewframe.height))
        FilterBGLayer.backgroundColor = UIColor(colorLiteralRed: 0 , green: 0, blue: 0, alpha: 0.6)
        let GestureLeaveDetail1 = UITapGestureRecognizer(target: self, action: "DismissFilter:")
        FilterBGLayer.addGestureRecognizer(GestureLeaveDetail1)
        FilterView = UIView(frame: CGRectMake(FilterBGLayer.frame.width / 100 * 10 , FilterBGLayer.frame.height  / 100 * 10 , FilterBGLayer.frame.width  / 100 * 80, FilterBGLayer.frame.height  / 100 * 70))
        FilterView.backgroundColor = UIColor.whiteColor()
        FilterView.layer.cornerRadius = 8
        
        let tapLocation = sender.locationInView(self.DTRListView)
        
        let indexPath = self.DTRListView.indexPathForRowAtPoint(tapLocation)
        
        let  objActivities = ActivitiesListB()
        objActivities.ShowRaiseButtonVisible = true
        var strDate = String(self.CurrentDTRListData[(indexPath?.row)!]["ReportingDate"])
        var array = strDate.componentsSeparatedByString("T")
        strDate = array[0]
        array = strDate.componentsSeparatedByString("-")
        objActivities.UserId = self.UserID
        objActivities.ActivityDate = array[2] + "/" + array[1] + "/" + array[0]
        
        if self.CurrentDTRListData[(indexPath?.row)!]["RequestID"].int > 0
        {
            objActivities.ShowRaiseButtonVisible = true
            objActivities.ShowDetailButtonVisible = true
            
            self.RequestID = self.CurrentDTRListData[(indexPath?.row)!]["RequestID"].int
        }
        else
        {
        
        let calendar: NSCalendar = NSCalendar(calendarIdentifier: NSGregorianCalendar)!
        // let calendar: NSCalendar = NSCalendar.currentCalendar()
        calendar.timeZone = NSTimeZone(name: "UTC")!
        
        let components: NSDateComponents = NSDateComponents()
        components.year = Int(array[0])!
        components.month = Int(array[1])!
        components.day = Int(array[2])!
        let defaultDate: NSDate = calendar.dateFromComponents(components)!
      
      
        let date = NSDate()
        let cal = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        
        // Swift 2:
        let components1 = cal.components([.Day , .Month, .Year ], fromDate: date)
        components1.hour = 0
        components1.minute = 0
        components1.second = 0
        components1.nanosecond = 0
        let newDate = cal.dateFromComponents(components1)
        
        if (newDate!.compare(defaultDate) == NSComparisonResult.OrderedAscending ||
        newDate!.compare(defaultDate) == NSComparisonResult.OrderedSame)
        {
            objActivities.ShowRaiseButtonVisible = false
        }
            
        }
        objActivities.MainView = self.FilterView
        objActivities.MainView1 = self.FilterBGLayer
        objActivities.GetActivitiesList()
        objActivities.OnCompletion = RaiseNewAdjustmenEvent
        FilterView.addSubview(objActivities)
        FilterBGLayer.addSubview(FilterView)
        self.view.addSubview(FilterBGLayer)
        
    }
    //Handles dismiss event of modal view
    func RaiseNewAdjustmenEvent(ActivityDate:String , Mode:Int) -> Void
    {
        isFilterOpen = false
        FilterBGLayer.removeFromSuperview()       
        
        if Mode == 0
        {
            let objRaiseNewAdjustment = RaiseAdjustment()
            objRaiseNewAdjustment.AdjustmentDate = ActivityDate
            objRaiseNewAdjustment.Mode = 1
            self.presentViewController(objRaiseNewAdjustment, animated: true, completion: nil)
        }
        else
        {
            ShowAdjustmentDetails(self.RequestID)
            
        }
  
    }
    
    
    func ShowAdjustmentDetails(RequestID : Int )
    {
        objLoadingIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, Viewframe.width, Viewframe.height))
        
        objLoadingIndicaor.activityIndicatorViewStyle = .WhiteLarge
        let objPosition =  self.view.center
        objLoadingIndicaor.center = objPosition
        objLoadingIndicaor.hidesWhenStopped = true
        objLoadingIndicaor.color = UIColor.grayColor()
        self.view.addSubview(objLoadingIndicaor)
        objLoadingIndicaor.startAnimating()
        
        objCallBack = AjaxCallBack()
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
             objCallBack.MethodName = "GetTimeAdjustmentRequestDetails"
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pRequestID",value: String(RequestID))
            objCallBack.ParameterList.append(objparam)
            
            objCallBack.post(ShowAdjustmentDetailsOnComplete, OnError: ShowAdjustmentDetailsOnErrorOnError)
            
        }
        
    }
    //Bind Leave list table
    
    //Response on error callback
    func ShowAdjustmentDetailsOnErrorOnError(Response: NSError ){
        
    }
    
    
    func ShowAdjustmentDetailsOnComplete (ResultString :NSString? ) {
        dispatch_async(dispatch_get_main_queue(),{
            
            self.objLoadingIndicaor.stopAnimating()            
      
            
            let  objAdjustmentDetails = AdjustmentDetails()
             var arrTempLeaveRequest = [AttendanceAdjustmentRequest]()
            arrTempLeaveRequest = self.GetAdjRequestArray(JSON(data: self.objCallBack.GetJSONData(ResultString)!))
            objAdjustmentDetails.AdjDetails = arrTempLeaveRequest[0]            
            objAdjustmentDetails.Mode = 0
            self.presentViewController(objAdjustmentDetails, animated: true, completion: nil)
          
        })
        
        
        
    }
    
    func GetAdjRequestArray(AdjMentJSON: Talentoz.JSON!) -> [AttendanceAdjustmentRequest]
    {
        
        var arrTempLeaveRequest = [AttendanceAdjustmentRequest]()
        for var i = 0; i < AdjMentJSON.count ; ++i {
            
            let madj = AttendanceAdjustmentRequest()
            madj.RequestID = AdjMentJSON[i]["RequestID"].int
            madj.EmployeePhoto  = AdjMentJSON[i]["EmployeePhoto"].string
            madj.EmployeeName  = AdjMentJSON[i]["EmployeeName"].string
            madj.PositionName  = AdjMentJSON[i]["PositionName"].string
            madj.DateLineDesc  = AdjMentJSON[i]["DateLineDesc"].string
            madj.WorkLocationName  = AdjMentJSON[i]["WorkLocationName"].string
            madj.StatusDesc  = AdjMentJSON[i]["StatusDesc"].string
            madj.TotalHours  = AdjMentJSON[i]["TotalHours"].string
            madj.DateLine  = AdjMentJSON[i]["DateLine"].string
            madj.Remarks  = AdjMentJSON[i]["Remarks"].string
            madj.UserID  = AdjMentJSON[i]["UserID"].int
            madj.Status  = AdjMentJSON[i]["Status"].int
            madj.Order  = AdjMentJSON[i]["Order"].int
            
            arrTempLeaveRequest.append(madj)
            
        }
        
        return arrTempLeaveRequest
    }
    

    
    func ShowTodaysActivities(Sender: UIButton!)
    {
        
 
        FilterBGLayer = UIView(frame: CGRectMake(0, 0, Viewframe.width  ,  Viewframe.height))
        FilterBGLayer.backgroundColor = UIColor(colorLiteralRed: 0 , green: 0, blue: 0, alpha: 0.6)
        let GestureLeaveDetail1 = UITapGestureRecognizer(target: self, action: "DismissFilter:")
        FilterBGLayer.addGestureRecognizer(GestureLeaveDetail1)
        FilterView = UIView(frame: CGRectMake(FilterBGLayer.frame.width / 100 * 10 , FilterBGLayer.frame.height  / 100 * 10 , FilterBGLayer.frame.width  / 100 * 80, FilterBGLayer.frame.height  / 100 * 60))
        FilterView.backgroundColor = UIColor.whiteColor()
        FilterView.layer.cornerRadius = 8
        
        
        
        let  objActivities = ActivitiesListB()
        let date = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Year,.Month,.Day], fromDate: date)
        
        objActivities.UserId = self.CurrentDTRListData[Sender.tag]["UserID"].int!
     
        objActivities.ActivityDate = String(components.day) + "/" + String(components.month) + "/" + String(components.year)
        objActivities.MainView = self.FilterView
        objActivities.GetActivitiesList()
        
        FilterView.addSubview(objActivities)
        FilterBGLayer.addSubview(FilterView)
        self.view.addSubview(FilterBGLayer)
        
        
    }
    
    func ShowMonthlyDTR(Sender: UIButton!)
    {
        let MonthlyDTR = SubordinateDTRDetails()
        MonthlyDTR.UserID =  self.CurrentDTRListData[Sender.tag]["UserID"].int!
        MonthlyDTR.SelfEmpData =  self.CurrentDTRListData[Sender.tag]
        self.presentViewController(MonthlyDTR, animated: true , completion: nil)
        
        
    }
    
    func AddGesture()
    {
        swipeRight = UISwipeGestureRecognizer(target: self, action: "LoadTeamDTRList:")
        swipeRight.direction = UISwipeGestureRecognizerDirection.Right
        self.view.addGestureRecognizer(swipeRight)
        
        swipeLeft = UISwipeGestureRecognizer(target: self, action: "LoadSelfDTRList:")
        swipeRight.direction = UISwipeGestureRecognizerDirection.Left
        self.view.addGestureRecognizer(swipeLeft)
    }
    
    func RemoveGesture()
    {
        self.view.removeGestureRecognizer(swipeLeft)
        self.view.removeGestureRecognizer(swipeRight)
    }
    
    
    func GetWeekDayName(DateStr: String) ->String
    {
    
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = NSTimeZone.localTimeZone()
        
        // convert string into date
        let dateValue = dateFormatter.dateFromString(DateStr) as NSDate!
   
        
        dateFormatter.dateFormat = "EEE"
        let dayOfWeekString = dateFormatter.stringFromDate(dateValue)
      
        
        return String(dayOfWeekString)
        
    }
    
    func GetMonthName(DateStr: String) ->String
    {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = NSTimeZone.localTimeZone()
        
        // convert string into date
        let dateValue = dateFormatter.dateFromString(DateStr) as NSDate!
        
        
        dateFormatter.dateFormat = "MMMM"
        let MonthNameString = dateFormatter.stringFromDate(dateValue)
        
        
        return String(MonthNameString)
        
    }
        
        
    
    
        
 
    
    
}



