//
//  DLHamburguerNavigationController.swift
//  DLHamburguerMenu
//
//  Created by Nacho on 5/3/15.
//  Copyright (c) 2015 Ignacio Nieto Carvajal. All rights reserved.
//

import UIKit

class DLHamburguerNavigationController: UINavigationController {

    @IBOutlet var HomeNavigationBar: UINavigationBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.view.addGestureRecognizer(UIScreenEdgePanGestureRecognizer(target: self, action: "panGestureRecognized:"))
        let leftEdgeGesture: UIScreenEdgePanGestureRecognizer = UIScreenEdgePanGestureRecognizer(target: self, action: "panGestureRecognized:")
        leftEdgeGesture.edges = .Left
        self.view.addGestureRecognizer(leftEdgeGesture)
        
       // HomeNavigationBar = UINavigationBar(frame: CGRectMake(0, 0, self.view.frame.size.width, 44)) // Offset by 20 pixels vertically to take the status bar into account
        
        HomeNavigationBar.barTintColor =    UIColor(colorLiteralRed: 28/255, green: 138/255, blue: 255/255, alpha: 1)
        
        let navigationBarApp = UINavigationBar.appearance()
        navigationBarApp.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
     

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func panGestureRecognized(sender: UIScreenEdgePanGestureRecognizer!) {
        // dismiss keyboard
        self.view.endEditing(true)
        self.findHamburguerViewController()?.view.endEditing(true)
        
        // pass gesture to hamburguer view controller.
        self.findHamburguerViewController()?.panGestureRecognized(sender)
    }
    
   

    
}
