//
//  PageItemController.swift
//  Talentoz
//
//  Created by forziamac on 16/12/15.
//  Copyright © 2015 Forzia Tech Private Ltd. All rights reserved.
//

import Foundation
import UIKit

class PageItemController: UIViewController {
    
       let objCallBack = AjaxCallBack()
    // MARK: - Variables
    var itemIndex: Int!
    var EmpQuickInfo : Dictionary<String,AnyObject>?
    
    //Page Item Cotroller Property for Total Hours quick Info
    @IBOutlet var TotalHoursWorkedThisWeek: UILabel!
    
    @IBOutlet var lbltotalhours: UILabel!
    
    @IBOutlet var btnviewdtr: UIButton!
    
    @IBOutlet var lblhourstext: UILabel!
    
    @IBAction func BtnViewTimeRecord(sender: AnyObject) {
        let objectDTRList = DTRList()
        self.presentViewController(objectDTRList, animated: true, completion: nil)
    }
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    //Page Item Cotroller Property for Subordinate Leave quick Info
    
    @IBOutlet var lblNoofsubTitle: UILabel!
    @IBOutlet var NoOfSubordinateLeaveToday: UILabel!
    @IBOutlet var TotalSubordinate: UILabel!
    @IBOutlet var lblOfText: UILabel!
    @IBOutlet var BtnViewTeamLeaves: UIButton!
    
    
     //Page Item Cotroller Property for Session quick Info
    @IBOutlet var TotalTrainingHours: UILabel!
    
    @IBOutlet var lblTrainingHrsTitle: UILabel!
    
    @IBOutlet var lblTrsHrsText: UILabel!
    
    @IBOutlet var btnViewSessHis: UIButton!
    @IBAction func BtnViewSessionHistory(sender: AnyObject) {
    }
    
    
    @IBAction func btnViewTeamLeaveclk(sender: AnyObject) {
        
        let LeaveListObj = LeaveList()
        LeaveListObj.ModeofShow = 1
        self.presentViewController(LeaveListObj, animated: true, completion: nil)
    }
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        
               super.viewDidLoad()
        
        dispatch_async(dispatch_get_main_queue(),{
            
         
            if   self.restorationIdentifier == "TotalHoursController"
            {
                self.lbltotalhours.frame = CGRectMake(0 , 135 / 100 * 18  ,self.view.frame.size.width , 20)
                
                self.TotalHoursWorkedThisWeek.frame = CGRectMake(0 , 135 / 100 * 40  , self.view.frame.size.width / 100 * 90 , 30)
                
                self.TotalHoursWorkedThisWeek.textAlignment = .Center
                
                self.lblhourstext.frame = CGRectMake(self.view.frame.size.width / 100 * 60  , 135 / 100 * 47  ,self.view.frame.size.width / 100 * 10 , 20)
                
                self.btnviewdtr.frame = CGRectMake(self.view.frame.size.width / 100 * 30 , 135 / 100 * 70  ,self.view.frame.size.width / 100 * 40, 30)
                
                                 
                
            }
            
            else if   self.restorationIdentifier == "TotalLeaveController"
            {
                self.lblNoofsubTitle.frame = CGRectMake(0 , 135 / 100 * 18  ,self.view.frame.size.width , 20)
                
                self.NoOfSubordinateLeaveToday.frame = CGRectMake(0 , 135 / 100 * 40  , self.view.frame.size.width / 100 * 75 , 30)
                
                self.NoOfSubordinateLeaveToday.textAlignment = .Center
                
                self.lblOfText.frame = CGRectMake(self.view.frame.size.width / 100 * 42  , 135 / 100 * 40  ,self.view.frame.size.width / 100 * 10 , 30)
                
                self.lblOfText.textAlignment = .Center
                
                
                self.TotalSubordinate.frame = CGRectMake(self.view.frame.size.width / 100 * 50  , 135 / 100 * 40  ,self.view.frame.size.width / 100 * 20 , 30)
                
                  self.TotalSubordinate.textAlignment = .Center
                
                self.BtnViewTeamLeaves.frame = CGRectMake(self.view.frame.size.width / 100 * 30 , 135 / 100 * 70  ,self.view.frame.size.width / 100 * 40, 30)
                
            }
            
            else if   self.restorationIdentifier == "TotalTrainingHoursController"
            {
                self.lblTrainingHrsTitle.frame = CGRectMake(0 , 135 / 100 * 18  ,self.view.frame.size.width , 20)
                
                self.TotalTrainingHours.frame = CGRectMake(0 , 135 / 100 * 40  , self.view.frame.size.width / 100 * 90 , 30)
                
                self.TotalTrainingHours.textAlignment = .Center
                
                self.lblTrsHrsText.frame = CGRectMake(self.view.frame.size.width / 100 * 60  , 135 / 100 * 47  ,self.view.frame.size.width / 100 * 10 , 20)
                
                self.btnViewSessHis.frame = CGRectMake(self.view.frame.size.width / 100 * 25 , 135 / 100 * 70  ,self.view.frame.size.width / 100 * 50, 30)
                 self.btnViewSessHis.hidden = true
              
            }
            
            if let a = self.EmpQuickInfo  {
                
                self.BindUI()
            }
            else
            {
                self.FetchEmployeeQuickInfo()
                
            }
            
            })
 
        
    
        
                
        
        
        
    
    }
    
    func FetchEmployeeQuickInfo() -> Void
    {
        
        
        
        
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            objCallBack.MethodName = "FetchQuickInfoDetail"
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objCallBack.post(EmpQuickInfoOnComplete, OnError: EmpProfileOnErrorOnError)
            
        }
        
    }
    
    //Response on complete callback
    func EmpQuickInfoOnComplete(ResultString: NSString? ){
        
       self.EmpQuickInfo = self.objCallBack.GetJSONArrayResult(ResultString)        
           dispatch_async(dispatch_get_main_queue(),{
          self.BindUI()
        })
    }
    
    func BindUI() -> Void
    
    {
        if   self.restorationIdentifier == "TotalHoursController"
        {
            if !(self.EmpQuickInfo!["WorkingHours"] is NSNull)
            {
            self.TotalHoursWorkedThisWeek.text = (String(self.EmpQuickInfo!["WorkingHours"]as! String))
            }
        }
        
        if   self.restorationIdentifier == "TotalLeaveController"
        {
            if !(self.EmpQuickInfo!["SubOrdinateCount"] is NSNull)
            {
                if   self.EmpQuickInfo!["SubOrdinateCount"]as! Int != 0 {
                    
                    self.TotalSubordinate.text = (String(self.EmpQuickInfo!["SubOrdinateCount"]as! Int))
                    
                    if !(self.EmpQuickInfo!["SubOrdinateLeaveCount"] is NSNull)
                    {
                        if   self.EmpQuickInfo!["SubOrdinateLeaveCount"]as! Int != 0 {
                            self.NoOfSubordinateLeaveToday.text = (String(self.EmpQuickInfo!["SubOrdinateLeaveCount"]as! Int))
                        }
                        else
                        {
                            self.NoOfSubordinateLeaveToday.text = "0"
                        }
                    }
                    
                }
                
            
            }
          
            
        }
        
        if   self.restorationIdentifier == "TotalTrainingHoursController"
        {
            if !(self.EmpQuickInfo!["TrainingHours"] is NSNull)
            {
                self.TotalTrainingHours.text = (String(self.EmpQuickInfo!["TrainingHours"]as! String))
            }
        }

    }
    //Response on error callback
    func EmpProfileOnErrorOnError(Response: NSError ){
        
    }
}