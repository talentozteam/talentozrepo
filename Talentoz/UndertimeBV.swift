//
//  UndertimeBV.swift
//  Talentoz
//
//  Created by forziamac on 22/01/16.
//  Copyright © 2016 Forzia Tech Private Ltd. All rights reserved.
//
import UIKit

class UndertimeBV  : UIViewController
{
    internal var UnderTime: String!
    internal var HomeView:UIView!
    @IBOutlet var lblUnderTime: UILabel!
    
    @IBOutlet var undertimelbltitle: UILabel!
    
    @IBOutlet var underdtrbtn: UIButton!
    @IBOutlet var undertimehrslbl: UILabel!
    
    internal var FilterView : UIView!
    internal var FilterBGLayer : UIView!
    var isFilterOpen = false
    let Viewframe = UIScreen.mainScreen().bounds
    override func viewDidLoad() {
        lblUnderTime.text = UnderTime
        
        
        
        self.undertimelbltitle.frame = CGRectMake(0 , 5 ,self.view.frame.size.width, 25)
        
        self.lblUnderTime.frame = CGRectMake(self.view.frame.size.width / 100 * 30 , 60 , self.view.frame.size.width / 100 * 35 , 35)
        self.undertimehrslbl.frame = CGRectMake(self.view.frame.size.width / 100 * 66 , 70 , self.view.frame.size.width / 100 * 20 , 25)
        
        
        
        self.underdtrbtn.frame = CGRectMake(self.view.frame.size.width / 100 * 25 ,  120 ,self.view.frame.size.width / 100 * 50, 30)
        
        
        self.underdtrbtn.backgroundColor = UIColor(hexString: "#43A047")
        
    }
    
    @IBAction func btnUnderTime_Click(sender: AnyObject) {
        FilterBGLayer = UIView(frame: CGRectMake(0, 0, HomeView.frame.width  ,  HomeView.frame.height))
        FilterBGLayer.backgroundColor = UIColor(colorLiteralRed: 0 , green: 0, blue: 0, alpha: 0.6)
        let GestureLeaveDetail1 = UITapGestureRecognizer(target: self, action: "DismissFilter:")
        FilterBGLayer.addGestureRecognizer(GestureLeaveDetail1)
        FilterView = UIView(frame: CGRectMake(FilterBGLayer.frame.width / 100 * 10 , FilterBGLayer.frame.height  / 100 * 10 , FilterBGLayer.frame.width  / 100 * 80, FilterBGLayer.frame.height  / 100 * 60	))
        FilterView.backgroundColor = UIColor.whiteColor()
        FilterView.layer.cornerRadius = 8
        
        
        
        let  objActivities = IrregularityListB()
        objActivities.Mode = 2
        objActivities.MainView = self.FilterView
        objActivities.GetIrregularityList()
        
        FilterView.addSubview(objActivities)
        FilterBGLayer.addSubview(FilterView)
        HomeView.addSubview(FilterBGLayer)
        
        
    }
    
    func DismissFilter(sender: UIButton)
    {
        isFilterOpen = false
        FilterBGLayer.removeFromSuperview()
        
    }
    
}

