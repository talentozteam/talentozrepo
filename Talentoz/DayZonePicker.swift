//
//  DayZonePicker.swift
//  Talentoz
//
//  Created by forziamac on 23/12/15.
//  Copyright © 2015 Forzia Tech Private Ltd. All rights reserved.
//

import UIKit

class DayZonePicker: UIViewController, UIPickerViewDataSource,UIPickerViewDelegate {

    
    
    var pickerptions = ["Forenoon","Afternoon"]
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerptions.count
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return  String(self.pickerptions[row])
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
      
    }
}
