//
//  ViewController.swift
//  Talentoz
//
//  Created by forziamac on 27/11/15.
//  Copyright © 2015 Forzia Tech Private Ltd. All rights reserved.


import UIKit
import SystemConfiguration

class ViewController: UIViewController , UITextFieldDelegate{
    
    //# MARK: Controls Outlets
    @IBOutlet var TalentOZIcon: UIImageView!
    @IBOutlet  var UserName: UITextField!
    @IBOutlet  var Password: UITextField!
    @IBOutlet var DomainURL: UITextField!
    @IBOutlet var btnButton: UIButton!
    @IBOutlet var lblsampledomain: UILabel!
    
    //# MARK: Local Variables
    let defaults = NSUserDefaults.standardUserDefaults()//User Info Session Details
    var objLoginActivityIndicator : UIActivityIndicatorView!
    var isUserDetailsSaved : Bool = false
    var LoadingPanelViewRef : UIView!
    
    //# MARK: Default Methods
    
    override func viewDidLoad() {
        
        LoadUserInfo()
        
        Password.delegate = self
        DomainURL.delegate = self

    }
    
    func Upgrade()
    {
        UIApplication.sharedApplication().openURL(NSURL(string: "itms-apps://itunes.apple.com/us/app/talentoz/id1076498957?mt=8")!)
    }
    
    func LoadUserInfo()
    {
        if let strUserName = defaults.stringForKey("UserName")//Checking for user already logged in the application
        {
            if (self.CheckInternetConnection() == false)
            {
                dispatch_async(dispatch_get_main_queue(),{
                    
                    // UI controls design from code
                    self.TalentOZIcon.frame = CGRectMake(self.view.frame.size.width / 100 * 20 , self.view.frame.size.height / 100 * 18  ,self.view.frame.size.width / 100 * 60, 45)
                    
                    self.UserName.frame = CGRectMake(self.view.frame.size.width / 100 * 10, self.view.frame.size.height / 100 * 33 ,self.view.frame.size.width / 100 * 80, 40)
                    
                    self.Password.frame = CGRectMake(self.view.frame.size.width / 100 * 10, self.view.frame.size.height / 100 * 43 ,self.view.frame.size.width / 100 * 80, 40)
                    
                    self.DomainURL.frame = CGRectMake(self.view.frame.size.width / 100 * 10, self.view.frame.size.height / 100 * 53 ,self.view.frame.size.width / 100 * 80, 40)
                    
                    self.lblsampledomain.frame = CGRectMake(self.view.frame.size.width / 100 * 10, self.view.frame.size.height / 100 * 62 ,self.view.frame.size.width / 100 * 80, 15)
                    
                    self.btnButton.frame = CGRectMake(self.view.frame.size.width / 100 * 30, self.view.frame.size.height / 100 * 78 ,self.view.frame.size.width / 100 * 40, 40)
                    self.btnButton.layer.cornerRadius = 5
                    
                })
                
                let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard))
                view.addGestureRecognizer(tap)
                super.viewDidLoad()
                
                
                let alert = UIAlertController(title: "Required", message: "Not able to connect to server, kindly check your internet connection", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                }
                return
                
            }
            else
            {
                isUserDetailsSaved = true
                //Loading Panel
                let LoadingPanelView = UIView(frame: CGRectMake(0, 0, self.view.frame.width, self.view.frame.height))
                LoadingPanelView.backgroundColor = UIColor.whiteColor()
                self.view.addSubview(LoadingPanelView)
                LoadingPanelViewRef = LoadingPanelView
                
                objLoginActivityIndicator = UIActivityIndicatorView(frame: CGRectMake(0,0, 200, 200))
                objLoginActivityIndicator.activityIndicatorViewStyle = .WhiteLarge
                objLoginActivityIndicator.center = self.view.center
                objLoginActivityIndicator.transform = CGAffineTransformMakeScale(1.5, 1.5);
                objLoginActivityIndicator.hidesWhenStopped = true
                objLoginActivityIndicator.color = UIColor.grayColor()
                LoadingPanelView.addSubview(objLoginActivityIndicator)
                objLoginActivityIndicator.startAnimating()
                self.view.backgroundColor = UIColor.whiteColor()
                
                
                
                
                let objCallBack = AjaxCallBack()
                objCallBack.MethodName = "CheckAuthentication"
                
                var objparam = Parameter(Name: "un",value: strUserName)
                objCallBack.ParameterList.append(objparam)
                
                objparam = Parameter(Name: "pn",value: defaults.stringForKey("Passsword")!)
                objCallBack.ParameterList.append(objparam)
                
                objCallBack.post(LoginOnComplete, OnError: LoginOnError)
            }
        }
        else{
            
            dispatch_async(dispatch_get_main_queue(),{
                
                // UI controls design from code
                self.TalentOZIcon.frame = CGRectMake(self.view.frame.size.width / 100 * 20 , self.view.frame.size.height / 100 * 18  ,self.view.frame.size.width / 100 * 60, 45)
                
                self.UserName.frame = CGRectMake(self.view.frame.size.width / 100 * 10, self.view.frame.size.height / 100 * 33 ,self.view.frame.size.width / 100 * 80, 40)
                
                self.Password.frame = CGRectMake(self.view.frame.size.width / 100 * 10, self.view.frame.size.height / 100 * 43 ,self.view.frame.size.width / 100 * 80, 40)
                
                self.DomainURL.frame = CGRectMake(self.view.frame.size.width / 100 * 10, self.view.frame.size.height / 100 * 53 ,self.view.frame.size.width / 100 * 80, 40)
                
                self.lblsampledomain.frame = CGRectMake(self.view.frame.size.width / 100 * 10, self.view.frame.size.height / 100 * 62 ,self.view.frame.size.width / 100 * 80, 15)
                
                self.btnButton.frame = CGRectMake(self.view.frame.size.width / 100 * 30, self.view.frame.size.height / 100 * 78 ,self.view.frame.size.width / 100 * 40, 40)
                self.btnButton.layer.cornerRadius = 5
                
            })
            
            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
            view.addGestureRecognizer(tap)
            super.viewDidLoad()
            
        }
    }
 
    
     //# MARK: Control Action
  
    @IBAction func LoginAuthentication(sender: AnyObject) {
        if (self.CheckInternetConnection() == false)
        {
            let alert = UIAlertController(title: "Required", message: "Not able to connect to server, kindly check your internet connection", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.presentViewController(alert, animated: true, completion: nil)
                
            }
            return
            
        }
        else
        {
        
        //Input Validation
        if UserName.text == ""
        {
            let alert = UIAlertController(title: "Required", message: "Please enter the user name", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.presentViewController(alert, animated: true, completion: nil)
                
            }
            return
        }
        
        if Password.text == ""
        {
            let alert = UIAlertController(title: "Required", message: "Please enter the password", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.presentViewController(alert, animated: true, completion: nil)
                
            }
            
             return
        }
        
        if DomainURL.text == ""
        {
            let alert = UIAlertController(title: "Required", message: "Please enter the domain url", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.presentViewController(alert, animated: true, completion: nil)
                
            }
            
             return
        }
        
        var DomainURLStr : String!
        DomainURLStr = String(DomainURL.text!).lowercaseString
        DomainURLStr = DomainURLStr!.stringByReplacingOccurrencesOfString("http://", withString: "", options: .LiteralSearch, range: nil)
        DomainURLStr = DomainURLStr!.stringByReplacingOccurrencesOfString("www.", withString: "", options: .LiteralSearch, range: nil)
     
    
        defaults.setObject(DomainURLStr, forKey: "DomainURL")
        
        // Login callback
        let objCallBack = AjaxCallBack()
        objCallBack.MethodName = "CheckAuthentication"
        
        var objparam = Parameter(Name: "un",value: UserName.text!)
        objCallBack.ParameterList.append(objparam)
        
        objparam = Parameter(Name: "pn",value: Password.text!)
        objCallBack.ParameterList.append(objparam)
        objLoginActivityIndicator = UIActivityIndicatorView(frame: CGRectMake(0,0, 200, 200))
        
        
        
        objLoginActivityIndicator.activityIndicatorViewStyle = .WhiteLarge
        objLoginActivityIndicator.center = self.view.center
        
        objLoginActivityIndicator.transform = CGAffineTransformMakeScale(1.5, 1.5);
        objLoginActivityIndicator.hidesWhenStopped = true
 
        
        self.view.addSubview(objLoginActivityIndicator)
        objLoginActivityIndicator.startAnimating()
        self.view.backgroundColor = UIColor.whiteColor()
        
        objCallBack.post(LoginOnComplete, OnError: LoginOnError)
        
        }
        
    }
    
    //Response on complete callback
    func LoginOnComplete(ResultString: NSString? ){
        
        
          dispatch_async(dispatch_get_main_queue(),{
            
            if (self.objLoginActivityIndicator != nil)
            {
                self.objLoginActivityIndicator.stopAnimating() // Stop the Loading Panel
            }
        
        if ((ResultString?.containsString("Invalid")) == true || (ResultString?.containsString("The resource cannot be found")) == true)
        {
            
            
            //Show alert for invalid users
            let alert = UIAlertController(title: "Invalid Login", message: "The user name or the password you have entered is incorrect", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
            NSOperationQueue.mainQueue().addOperationWithBlock {
            self.presentViewController(alert, animated: true, completion: nil)
                
                self.defaults.removeObjectForKey("UserName")
                self.defaults.removeObjectForKey("Password")
                
                if self.isUserDetailsSaved == true
                {
                    self.LoadingPanelViewRef.removeFromSuperview()
                    self.LoadUserInfo()
                }
                
            }
            
        }else{
            
            //Store the User Information in Local Defaults
            //Find The index from HTTPResonse String
            let Startrange =  ResultString!.rangeOfString("<" + "CheckAuthentication" + "Result>")
            let Endrange = ResultString!.rangeOfString("</" + "CheckAuthentication" + "Result>")
            //SubString the JSON string from HTTPResponse String
            let  JSONStr =  ResultString!.substringWithRange(NSRange(location: Startrange.location + Startrange.length , length: Endrange.location - (Startrange.location + Startrange.length)))
            
            self.defaults.setObject(JSONStr, forKey: "UserInfo")
            
            let objCallBack = AjaxCallBack()
            let ResultJSON = objCallBack.DeserializeJSONString(JSONStr)
            
            let ClientID : String = String(ResultJSON!["ClientID"]as! NSNumber)
            
            self.defaults.setObject( ClientID , forKey: "ClientID")
            
           
            
            if let strUserName = self.defaults.stringForKey("UserName")
            {
          
            }
            else{
                self.defaults.setObject(self.UserName.text!, forKey: "UserName")
                self.defaults.setObject(self.Password.text!, forKey: "Passsword")
            }
            
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("HomeInitialView")
            self.presentViewController(vc, animated: true, completion: nil)
          
        }
        })
     
   
    }
    
    //Response on error callback
    func LoginOnError(Response: NSError ){
        if Response.domain == "-404"
        {
            let alert = UIAlertController(title: "Error", message: "Incorrect URL Format. Please check the domain URL.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.presentViewController(alert, animated: true, completion: nil)
                
                if (self.objLoginActivityIndicator != nil)
                {
                    self.objLoginActivityIndicator.stopAnimating()
                }
                
            }
        }
        else
        {
            let alert = UIAlertController(title: "Error", message: "Unable to contact the server. Please check the domain URL.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.presentViewController(alert, animated: true, completion: nil)
                
                if (self.objLoginActivityIndicator != nil)
                {
                    self.objLoginActivityIndicator.stopAnimating()
                }
                
            }
        }
     

        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func CheckInternetConnection()->Bool{
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    
    func textFieldDidBeginEditing(textField: UITextField) {
        animateViewMoving(true, moveValue: 100)
    }
    func textFieldDidEndEditing(textField: UITextField) {
        animateViewMoving(false, moveValue: 100)
    }
    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:NSTimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = CGRectOffset(self.view.frame, 0,  movement)
        UIView.commitAnimations()
    }
    
}





