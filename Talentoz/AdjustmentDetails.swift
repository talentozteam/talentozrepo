//
//  AdjustmentDetails.swift
//  Talentoz
//
//  Created by forziamac on 08/02/16.
//  Copyright © 2016 Forzia Tech Private Ltd. All rights reserved.
//

import UIKit

class AdjustmentDetails: BaseTabBarVC , UITableViewDataSource, UITableViewDelegate {
    
    //# MARK: Local Variables
    var objLoadingIndicaor : UIActivityIndicatorView!
    var objCallBack = AjaxCallBack()
    let UserInfo = NSUserDefaults.standardUserDefaults()
    var Activities : Talentoz.JSON!
    var CurrentActionCode : String = "0"
    
    
    //# MARK: Public Variables
    internal var AdjDetails = AttendanceAdjustmentRequest()
    internal var Mode : Int!
    internal var RowIndex: Int!
    var OnCompletion: ((RowID:Int , ActionCode:String) -> Void )!
    
    //# MARK: Outlets
    
    @IBOutlet var ScrollViewUI: UIScrollView!
    @IBOutlet var MainView: UIView!
    @IBOutlet var imgEmployeePhoto: UIImageView!
    @IBOutlet var lblEmpName: UILabel!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var lblPosition: UILabel!
    @IBOutlet var lblWorkLocation: UILabel!
    @IBOutlet var lblDateTitle: UILabel!
    @IBOutlet var lblDateTxt: UILabel!
    @IBOutlet var lblInTime: UILabel!
    @IBOutlet var lblOutTime: UILabel!
    @IBOutlet var lblTotalHrs: UILabel!
    @IBOutlet var lblSumHrsTitle: UILabel!
    @IBOutlet var lblSummaryHrs: UILabel!
    @IBOutlet var lblReasonTitle: UILabel!
    @IBOutlet var lblReasonTxt: UILabel!
    @IBOutlet var btnApprove: UIButton!
    @IBOutlet var btnReject: UIButton!
    
    //# MARK: Default Methods
    override func viewDidLoad() {
        
        
        for subview in   self.view.subviews
        {
            subview.hidden = true
            
        }
        
        super.viewDidLoad()
       // self.ScrollViewUI.contentSize = CGSize(0, 60, width: Viewframe.width, height: 1100)
       // self.ScrollViewUI.frame = CGRectMake(0, 0 ,Viewframe.width, 1100)
        if(Viewframe.width==320)
        {
            self.ScrollViewUI.contentSize = CGSize(  width: Viewframe.width, height: 1100)
        }
        else
        {
            self.ScrollViewUI.frame = CGRectMake(0, 0 ,Viewframe.width, 1100)
        }
        self.ScrollViewUI.backgroundColor = UIColor.whiteColor()
        self.view.backgroundColor = UIColor.whiteColor()
        super.UINavigationBarTitle = "Adjustment Details"
    
    }
    
    override func viewDidAppear(animated: Bool) {
        
        
        super.viewDidAppear(animated)
       
        for subview in   self.view.subviews
        {
            subview.hidden = false
            
        }
        if self.Mode == 0
        {
            self.btnApprove.hidden = true
            self.btnReject.hidden = true
        }
        PositionControls()
        BindAdjustInfo()
    }
    
    //# MARK: Button Action
    
    @IBAction func btnApprove_Click(sender: AnyObject) {
       
         self.ProcessLeaveRequest("1")
    }
    
    @IBAction func btnReject_Click(sender: AnyObject) {
        
        self.ProcessLeaveRequest("2")
    }

    
   //# MARK: Design UI Controls
    func PositionControls()
    {
        self.MainView.frame=CGRectMake(0, 0 ,Viewframe.width, self.view.frame.size.height)
        
        self.imgEmployeePhoto.frame = CGRectMake(self.view.frame.size.width / 100 * 5, 80  , 60, 60)
        
        self.lblEmpName.frame = CGRectMake((self.view.frame.size.width  / 100 * 10) + 60, 80  ,(self.view.frame.size.width - 60) / 100 * 70, 20)
        self.lblEmpName.textColor = UIColor(hexString: "#555555")
        self.lblEmpName.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        
        self.lblStatus.frame = CGRectMake((self.view.frame.size.width - 60) / 100 * 80, 65  ,(self.view.frame.size.width - 50) / 100 * 30, 20)
        self.lblStatus.font =  UIFont(name:"HelveticaNeue-Bold", size: 16.0)
        self.lblStatus.textAlignment = .Right
        
        self.lblPosition.frame = CGRectMake((self.view.frame.size.width   / 100 * 10) + 60 , 100  ,(self.view.frame.size.width - 50) / 100 * 70, 20)
        self.lblPosition.textColor = UIColor(hexString: "#666666")
        self.lblPosition.font = UIFont(name:"HelveticaNeue", size: 12.0)
        
        self.lblWorkLocation.frame = CGRectMake((self.view.frame.size.width  / 100 * 10) + 60, 120  ,(self.view.frame.size.width - 50) / 100 * 70, 20)
        self.lblWorkLocation.textColor = UIColor(hexString: "#666666")
        self.lblWorkLocation.font = UIFont(name:"HelveticaNeue", size: 12.0)
        
        self.lblDateTitle.frame = CGRectMake(self.view.frame.size.width  / 100 * 5, 150  ,self.view.frame.size.width / 100 * 20, 20)
        self.lblDateTitle.textColor = UIColor(hexString: "#666666")
        self.lblDateTitle.font = UIFont(name:"HelveticaNeue-bold", size: 12.0)
        
        self.lblDateTxt.frame = CGRectMake(self.view.frame.size.width  / 100 * 20, 150  ,self.view.frame.size.width / 100 * 70, 20)
        self.lblDateTxt.textColor = UIColor(hexString: "#444444")
        self.lblDateTxt.font = UIFont(name:"HelveticaNeue-bold", size: 12.0)
        
        self.lblInTime.frame = CGRectMake(self.view.frame.size.width  / 100 * 5, 185 ,self.view.frame.size.width / 100 * 30, 20)
        self.lblInTime.backgroundColor = UIColor(hexString:"#2190ea")
        self.lblInTime.textAlignment = .Center
        self.lblInTime.font = UIFont(name:"HelveticaNeue-Bold", size: 10.0)
        self.lblInTime.textColor = UIColor.whiteColor()
        self.lblInTime.text = "In Time"
        
        self.lblOutTime.frame = CGRectMake(self.view.frame.size.width  / 100 * 35, 185 ,self.view.frame.size.width / 100 * 30, 20)
        self.lblOutTime.backgroundColor = UIColor(hexString:"#2190ea")
        self.lblOutTime.textAlignment = .Center
        self.lblOutTime.font = UIFont(name:"HelveticaNeue-Bold", size: 10.0)
        self.lblOutTime.textColor = UIColor.whiteColor()
        self.lblOutTime.text = "Out Time"
        
        self.lblTotalHrs.frame = CGRectMake(self.view.frame.size.width  / 100 * 65, 185 ,self.view.frame.size.width / 100 * 30, 20)
        self.lblTotalHrs.backgroundColor = UIColor(hexString:"#2190ea")
        self.lblTotalHrs.textAlignment = .Center
        self.lblTotalHrs.font = UIFont(name:"HelveticaNeue-Bold", size: 10.0)
        self.lblTotalHrs.textColor = UIColor.whiteColor()
        self.lblTotalHrs.text = "Hours"
        
        self.lblSumHrsTitle.frame = CGRectMake(self.view.frame.size.width  / 100 * 5, 320 ,self.view.frame.size.width / 100 * 59.2, 20)
        self.lblSumHrsTitle.backgroundColor = UIColor(hexString: "#f0f0f0")
        self.lblSumHrsTitle.textAlignment = .Right
        self.lblSumHrsTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 10.0)
        self.lblSumHrsTitle.textColor = UIColor(hexString: "#666666")
        self.lblSumHrsTitle.text = "Total Hours"
        
        self.lblSummaryHrs.frame = CGRectMake(self.view.frame.size.width  / 100 * 64, 320 ,self.view.frame.size.width / 100 * 30, 20)
        self.lblSummaryHrs.backgroundColor = UIColor(hexString: "#f0f0f0")
        self.lblSummaryHrs.textAlignment = .Center
        self.lblSummaryHrs.font = UIFont(name:"HelveticaNeue-Bold", size: 10.0)
        self.lblSummaryHrs.textColor = UIColor(hexString: "#666666")

        self.lblReasonTitle.frame = CGRectMake(self.view.frame.size.width  / 100 * 5, 360 ,self.view.frame.size.width/100 * 90, 20)
        self.lblReasonTitle.textAlignment = .Left
        self.lblReasonTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        self.lblReasonTitle.textColor = UIColor(hexString: "#555555")
        
        self.lblReasonTxt.frame = CGRectMake(self.view.frame.size.width  / 100 * 5, 380 ,self.view.frame.size.width / 100 * 90, 40)
        self.lblReasonTxt.textAlignment = .Left
        self.lblReasonTxt.lineBreakMode = .ByWordWrapping
        self.lblReasonTxt.numberOfLines = 0
        self.lblReasonTxt.font = UIFont(name:"HelveticaNeue", size: 12.0)
        self.lblReasonTxt.textColor = UIColor(hexString: "#555555")
        
        self.btnApprove.frame = CGRectMake((self.view.frame.size.width / 100 * 5 ) , 440 ,self.view.frame.size.width / 100 * 40, 30)
        self.btnApprove.setFAText(prefixText: "", icon: FAType.FACheck, postfixText: "   Approve", size: CGFloat(12), forState: .Normal)
        self.btnApprove.backgroundColor = UIColor(hexString: "#48C9B0")
        self.btnApprove.layer.cornerRadius=5
        self.btnApprove.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        
        self.btnReject.frame = CGRectMake((self.view.frame.size.width / 100 * 55 ) , 440 ,self.view.frame.size.width / 100 * 40, 30)
        self.btnReject.setFAText(prefixText: "", icon: FAType.FAClose, postfixText: "   Reject", size: CGFloat(12), forState: .Normal)
        self.btnReject.backgroundColor = UIColor(hexString: "#F46868")
         self.btnReject.layer.cornerRadius=5
        self.btnReject.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)

        
    }
    
    override func goBack() {
        if self.Mode != 0
        {
            if(self.RowIndex != nil)
            {
               self.OnCompletion(RowID: self.RowIndex,ActionCode: self.CurrentActionCode )
            }
            
        }
        dispatch_async(dispatch_get_main_queue(),{
            self.dismissViewControllerAnimated(true, completion: nil)
        })
    }
    
    //Bind Adjustment Details
    func BindAdjustInfo()
    {
        
        if let tmpDomainURL = self.UserInfo.stringForKey("DomainURL")
        {
            
            if  (self.AdjDetails.EmployeePhoto == nil )
            {
                
                let img_EmpPic:  UIButton = UIButton(frame: CGRectMake(self.view.frame.size.width / 100 * 5, 80  , 60, 60))
                img_EmpPic.layer.masksToBounds = false
                //img_EmpPic.layer.borderWidth = 1
                
                img_EmpPic.backgroundColor = UIColor(hexString: "#f5f5f5")
                img_EmpPic.clipsToBounds = true
                img_EmpPic.layer.borderWidth = 1
                img_EmpPic.layer.borderColor = UIColor(hexString: "#cccccc")?.CGColor
                // img_EmpPic.titleLabel!.font = UIFont(name: "helvetica-bold", size: 25)
                
                img_EmpPic.setTitleColor(UIColor(hexString: "#666666"), forState: UIControlState.Normal)
                img_EmpPic.setFAIcon(FAType.FAUser, iconSize: 35, forState: .Normal)
                self.view.addSubview(img_EmpPic)
                
            }
            else
            {
                let employeePic = "http://" + tmpDomainURL + "/Uploadedfiles/Client" + String(self.UserInfo.stringForKey("ClientID")!) + "/Image/" + String(self.AdjDetails.EmployeePhoto)
                imgEmployeePhoto.setImageWithUrl(NSURL(string: employeePic)!)
            }
          
        }
        
        lblEmpName.text = String(self.AdjDetails.EmployeeName)
        lblPosition.text = String(self.AdjDetails.PositionName)
        lblWorkLocation.text = String(self.AdjDetails.WorkLocationName)
        lblDateTxt.text = String(self.AdjDetails.DateLineDesc)
        if(self.AdjDetails.Remarks != nil)
        {
        lblReasonTxt.text = String(self.AdjDetails.Remarks)
        }
        self.lblStatus.textAlignment = .Right
        if (self.AdjDetails.Status == 1)
        {
            self.lblStatus.textColor = UIColor(hexString:"#ffbb5c")
        } else if (self.AdjDetails.Status == 2) {
            self.btnApprove.hidden = true
            self.btnReject.hidden = true
            self.lblStatus.textColor = UIColor(hexString: "#48c9b0")
        }
        else {
            self.btnApprove.hidden = true
            self.btnReject.hidden = true
            self.lblStatus.textColor = UIColor(hexString: "#ef6565")
        }
        
        
        self.lblStatus.font = UIFont(name:"HelveticaNeue", size: 10.0)
        self.lblStatus.text = String(self.AdjDetails.StatusDesc)
        
        
        BindAdjustmentList()
    }
    
    
   
   //Load and Bind Adjustment Timings
    func BindAdjustmentList()
    {
        objLoadingIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, Viewframe.width, Viewframe.height))
        objLoadingIndicaor.activityIndicatorViewStyle = .WhiteLarge
        let objPosition =  self.view.center
        objLoadingIndicaor.center = objPosition
        objLoadingIndicaor.hidesWhenStopped = true
        objLoadingIndicaor.color = UIColor.grayColor()
        self.view.addSubview(objLoadingIndicaor)
        self.objLoadingIndicaor.startAnimating()
        
        objCallBack = AjaxCallBack()
        objCallBack.MethodName = "GetBiometricDataonDate"
        
        if let UserInfo = UserInfo.stringForKey("UserInfo")
        {
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pMode",value: "1")
            objCallBack.ParameterList.append(objparam)
            
            var ArrayResult = self.AdjDetails.DateLine.componentsSeparatedByString("T")
            ArrayResult = ArrayResult[0].componentsSeparatedByString("-")
            
            
            objparam = Parameter(Name: "pDate",value: String(ArrayResult[2]) + "/" + String(ArrayResult[1]) + "/" + String(ArrayResult[0]) )
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pUserID",value: String(self.AdjDetails.UserID))
            objCallBack.ParameterList.append(objparam)
            
            
            objCallBack.post(BindAdjustmentListOnComplete, OnError:BindAdjustmentListOnError)
            
        }
        
    }
    
    func BindAdjustmentListOnError(Response: NSError ){
        
    }
    
    
    func BindAdjustmentListOnComplete (ResultString :NSString? ) {
        dispatch_async(dispatch_get_main_queue(),{          
            self.objLoadingIndicaor.stopAnimating()
            self.Activities = JSON(data: self.objCallBack.GetJSONData(ResultString)!)
            
            if self.Activities.count > 0
            {
                if self.Activities[0]["Sum_of_TotalTime"] != nil
                {
                    self.lblSummaryHrs.text = String(self.Activities[0]["Sum_of_TotalTime"])
                }
                else
                {
                    self.lblSummaryHrs.text = "00:00"
                }
                
            }
            else
            {
                self.lblSummaryHrs.text = "00:00"
            }
            
            
            let ActivityList : UITableView = UITableView(frame : CGRectMake(self.view.frame.size.width  / 100 * 5, 215 ,self.view.frame.size.width / 100 * 90, 100))
            ActivityList.showsVerticalScrollIndicator = true
            ActivityList.delegate = self
            ActivityList.dataSource = self
            ActivityList.rowHeight = 25
            ActivityList.separatorColor = UIColor.whiteColor()
            ActivityList.backgroundColor = UIColor.clearColor()
            self.MainView.addSubview(ActivityList)
            
        })
    }

    
 
    //# MARK: Approval
    func ProcessLeaveRequest(ActionCode: String  )
    {
        
        self.CurrentActionCode = ActionCode
        objLoadingIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, Viewframe.width, Viewframe.height))
        
        objLoadingIndicaor.activityIndicatorViewStyle = .WhiteLarge
        let objPosition =  self.view.center
        objLoadingIndicaor.center = objPosition
        objLoadingIndicaor.hidesWhenStopped = true
        objLoadingIndicaor.color = UIColor.grayColor()
        self.view.addSubview(objLoadingIndicaor)
        objLoadingIndicaor.startAnimating()
        
        if let UserInfo = UserInfo.stringForKey("UserInfo")
        {
            objCallBack = AjaxCallBack()
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            
            objCallBack.MethodName = "ProcessAttendanceNote"
            
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pRequestID",value: String(self.AdjDetails.RequestID))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pAction",value: ActionCode)
            objCallBack.ParameterList.append(objparam)
            
            objCallBack.post(ProcessLeaveRequestOnComplete, OnError: ProcessLeaveRequestOnError)
            
        }
        
    }
    
    
    func ProcessLeaveRequestOnError(Response: NSError ){
        
    }
    
    func ProcessLeaveRequestOnComplete (ResultString :NSString? ) {
        dispatch_async(dispatch_get_main_queue(),{
            
            dispatch_async(dispatch_get_main_queue(),{
                self.objLoadingIndicaor.stopAnimating()
                if(self.CurrentActionCode == "1")
                {
                    self.lblStatus.textColor = UIColor(hexString: "#48c9b0")
                    self.lblStatus.text = String("Approved")
                }
                else
                {
                    self.lblStatus.textColor = UIColor(hexString: "#ef6565")
                    self.lblStatus.text = String("Rejected")
                }
                self.btnApprove.hidden = true
                self.btnReject.hidden = true
                
                DoneHUD.showInView(self.view, message: "Done")
            })
            
        })
        
        
    }
    
    //# MARK: Table List Bind Method
    func numberOfSectionsInTableView(ActivityList: UITableView) -> Int {
        return 1
    }
    
    func tableView(ActivityList: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.Activities.count
        
    }
    
    
    func tableView(ActivityList: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        //Alternate Color
        if indexPath.row % 2 == 0
        {
            cell.backgroundColor = UIColor.whiteColor()
        }
        else
        {
            cell.backgroundColor = UIColor(hexString: "#f5f5f5")
        }
        
        
        let lbl_InTime : UILabel = UILabel(frame: CGRectMake(0  , 5, ActivityList.frame.size.width / 100 * 30 ,20))
        lbl_InTime.backgroundColor = UIColor.clearColor()
        lbl_InTime.textAlignment = .Center
        lbl_InTime.font = UIFont(name:"HelveticaNeue", size: 10.0)
        lbl_InTime.textColor = UIColor(hexString: "#666666")
        if self.Activities[indexPath.row]["IsInTime"] == 1
        {
            lbl_InTime.textColor = UIColor(hexString: "#cc0000")
        }
        if self.Activities[indexPath.row]["ShirtInTime"] != nil
        {
            lbl_InTime.text = String(self.Activities[indexPath.row]["ShirtInTime"])
        }
        
        cell.addSubview(lbl_InTime)
        
        let lbl_OutTime : UILabel = UILabel(frame: CGRectMake(ActivityList.frame.size.width / 100 * 35, 5, ActivityList.frame.size.width / 100 * 30 ,20))
        lbl_OutTime.backgroundColor = UIColor.clearColor()
        lbl_OutTime.textAlignment = .Center
        lbl_OutTime.textColor = UIColor(hexString: "#666666")
        lbl_OutTime.font = UIFont(name:"HelveticaNeue", size: 10.0)
        if self.Activities[indexPath.row]["IsOutTime"] == 1
        {
            lbl_OutTime.textColor = UIColor(hexString: "#cc0000")
        }
        if self.Activities[indexPath.row]["ShirtOutTime"] != nil
        {
            lbl_OutTime.text = String(self.Activities[indexPath.row]["ShirtOutTime"])
        }
        
        cell.addSubview(lbl_OutTime)
        
        
        let lbl_Hours : UILabel = UILabel(frame: CGRectMake(ActivityList.frame.size.width / 100 * 68, 5, ActivityList.frame.size.width / 100 * 30 ,20))
        lbl_Hours.backgroundColor = UIColor.clearColor()
        lbl_Hours.textAlignment = .Center
        lbl_Hours.textColor = UIColor(hexString: "#666666")
        lbl_Hours.font = UIFont(name:"HelveticaNeue", size: 10.0)
        if self.Activities[indexPath.row]["TotalTime"] != nil
        {
            lbl_Hours.text = String(self.Activities[indexPath.row]["TotalTime"])
        }
        cell.addSubview(lbl_Hours)
        
        
        
        
        
        
        return cell
    }
    

    
}

