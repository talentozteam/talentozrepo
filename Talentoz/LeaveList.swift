
    

//
//  LeaveList.swift
//  Talentoz
//
//  Created by forziamac on 24/12/15.
//  Copyright © 2015 Forzia Tech Private Ltd. All rights reserved.


import UIKit

class LeaveList: BaseTabBarVC, UITableViewDataSource ,UITableViewDelegate {
    
    //# MARK: Local Variables
    var customSC: UISegmentedControl?
    var SelfLeaveListData : Talentoz.JSON!
    var TeamLeaveListData : Talentoz.JSON!
    var CurrentLeaveListData = [LeaveRequest]()
    var arrSelfLeaveList = [LeaveRequest]()
    var arrTeamLeaveList = [LeaveRequest]()
    var UserID : Int!
    var objCallBack = AjaxCallBack()
    let defaults = NSUserDefaults.standardUserDefaults()
    var CurrentCell =  UITableViewCell()
    let objRole = RoleManager()
    var CurrentRowIndex : Int!
    var CurrentActionType: Int!
    var LeaveListView = UITableView()
    var objLoadingIndicaor : UIActivityIndicatorView!
    var swipeRight : UISwipeGestureRecognizer!
    var swipeLeft : UISwipeGestureRecognizer!
    var CurrentPageCount : Int!
    var VisibleShowMore : Bool = false
    //# MARK: Public Variables
    internal var ModeofShow: Int! = 0
     var LoadMoreClicked : Int = 0
    //# MARK: Default Methods
    override func viewDidLoad() {
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            self.UserID = Int(ResultJSON!["UserID"]as! NSNumber)
        }
        
        super.UINavigationBarTitle = "Leave List"
        super.viewDidLoad()
        BindContextTab()
        
        if objRole.ISManager{
            self.AddGesture()
        }
        
    }
    
  
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //# MARK: Tab Controls
    
    func BindContextTab()
    {
        var items = ["My Leave"]
        if objRole.ISManager{
              items = ["My Leave", "Team Leave"]
        }
       
      
        let LeaveView=UIView(frame: CGRectMake(0, 60,  Viewframe.width , 48))
        LeaveView.backgroundColor = UIColor(colorLiteralRed: 28/255, green: 138/255, blue: 255/255, alpha: 1)
        
     
        customSC = UISegmentedControl(items: items)
        customSC!.selectedSegmentIndex = 0
        customSC!.frame = CGRectMake(Viewframe.width / 100 * 2, 5,Viewframe.width / 100 * 96,40)
        customSC!.layer.cornerRadius = 0.5
        customSC!.backgroundColor = UIColor(colorLiteralRed: 28/255, green: 138/255, blue: 255/255, alpha: 1)
        customSC!.tintColor = UIColor.whiteColor()
        customSC!.layer.borderColor = UIColor.whiteColor().CGColor
        customSC!.layer.borderWidth = 0
        customSC!.addTarget(self, action: "changeColor:", forControlEvents: .ValueChanged)
        
        LeaveView.addSubview(customSC!)
        self.view.addSubview(LeaveView)
        
        customSC!.selectedSegmentIndex = self.ModeofShow
        BindLeaveList()
        
    }
    
    func LoadTeamLeaveList(gestureReconizer: UISwipeGestureRecognizer) {
        customSC!.selectedSegmentIndex = 1
         BindLeaveList()
    }
    
    func LoadSelfLeaveList(gestureReconizer: UISwipeGestureRecognizer) {
        customSC!.selectedSegmentIndex = 0
         BindLeaveList()
    }
    
    
    func changeColor(sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            BindLeaveList()
            break;
        case 1:
            BindLeaveList()
            break;
        default:
            break;
        }
    }
    
 
    
    func BindLeaveList()
    {
        if self.customSC!.selectedSegmentIndex == 0
        {
            if let TempData = self.SelfLeaveListData
            {
            ConstructLeaveList()
            }else
            {
              GetLeaveList()
            }
            
        }
        else
        {
            if let TempData = self.TeamLeaveListData
            {
                ConstructLeaveList()
            }else
            {
               GetLeaveList()
            }
            
        }
    
    }
    
     //# MARK: Callback Methods
    
    func GetLeaveList()
    {
        objLoadingIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, Viewframe.width, Viewframe.height))
        
        objLoadingIndicaor.activityIndicatorViewStyle = .WhiteLarge
        let objPosition =  self.view.center
        objLoadingIndicaor.center = objPosition
        objLoadingIndicaor.hidesWhenStopped = true
        objLoadingIndicaor.color = UIColor.grayColor()
        self.view.addSubview(objLoadingIndicaor)
        objLoadingIndicaor.startAnimating()
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            
            objCallBack = AjaxCallBack()
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            if customSC!.selectedSegmentIndex == 0
            {
                objCallBack.MethodName = "GetSelfLeaveList"
                
                objparam = Parameter(Name: "pRoleID",value: "1")
                objCallBack.ParameterList.append(objparam)
                
                objparam = Parameter(Name: "pPageIndex",value: "0")
                objCallBack.ParameterList.append(objparam)
                
                objparam = Parameter(Name: "pPageSize",value: "100000")
                objCallBack.ParameterList.append(objparam)
            }
            else
            {
                objCallBack.MethodName = "GetTeamLeaveList"
                
                objparam = Parameter(Name: "pRoleID",value: "2")
                objCallBack.ParameterList.append(objparam)
                
                if self.CurrentPageCount == nil
                {
                    self.CurrentPageCount = 0
                }
                objparam = Parameter(Name: "pPageIndex",value: String(self.CurrentPageCount))
                objCallBack.ParameterList.append(objparam)
                
                objparam = Parameter(Name: "pPageSize",value: "10")
                objCallBack.ParameterList.append(objparam)
            }
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
         
            
            objCallBack.post(LeaveListOnComplete, OnError: LeaveListOnErrorOnError)
            
        }
        
    }
    
    //Response on output callback
    func LeaveListOnComplete (ResultString :NSString? ) {
        if self.customSC!.selectedSegmentIndex == 0
        {
            self.SelfLeaveListData =   JSON(data: self.objCallBack.GetJSONData(ResultString)!)
            self.arrSelfLeaveList = self.GetLeaveRequestArray(self.SelfLeaveListData , IsManager: 0 )
        }
        else
        {
            self.TeamLeaveListData =   JSON(data: self.objCallBack.GetJSONData(ResultString)!)
            self.arrTeamLeaveList = self.GetLeaveRequestArray(self.TeamLeaveListData , IsManager: 1 )
        }
        ConstructLeaveList()
    }
    
    //Response on error callback
    func LeaveListOnErrorOnError(Response: NSError ){
        
    }
    
    // Convert JSON Array Object To Custom object collection
    func GetLeaveRequestArray(LeaveJSON: Talentoz.JSON!, IsManager: Int) -> [LeaveRequest]
    {
        
        var arrTempLeaveRequest = [LeaveRequest]()
       
            for var i = 0; i < LeaveJSON.count ; ++i {
                
                let mleave = LeaveRequest()
                
                mleave.Access = LeaveJSON[i]["Access"].int
                mleave.BaseStatus = LeaveJSON[i]["BaseStatus"].int
                mleave.Type  = LeaveJSON[i]["Type"].int
                mleave.ISLastStep  = LeaveJSON[i]["ISLastStep"].int
                mleave.ProcessType  = LeaveJSON[i]["ProcessType"].int
                mleave.ISChildCreated  = LeaveJSON[i]["ISChildCreated"].int
                mleave.ISHRApply  = LeaveJSON[i]["ISHRApply"].int
                
                mleave.RequestID = LeaveJSON[i]["RequestID"].int
                mleave.EmployeePhoto  = LeaveJSON[i]["EmployeePhoto"].string
                mleave.EmployeeName  = LeaveJSON[i]["EmployeeName"].string
                mleave.PositionName  = LeaveJSON[i]["PositionName"].string
                mleave.RequestDescription  = LeaveJSON[i]["RequestDescription"].string
                mleave.NOOfDays  = LeaveJSON[i]["NOOfDays"].double
                mleave.StartDateString  = LeaveJSON[i]["StartDateString"].string
                mleave.EndDateString  = LeaveJSON[i]["EndDateString"].string
                mleave.RequestedFor  = LeaveJSON[i]["RequestedFor"].int
                mleave.LastActionBy  = LeaveJSON[i]["LastActionBy"].int
                mleave.StatusText  = LeaveJSON[i]["StatusText"].string
                mleave.ParentID  = LeaveJSON[i]["ParentID"].string
                mleave.RequestedBY  = LeaveJSON[i]["RequestedBY"].int
                
                mleave.WorkLocationName  = LeaveJSON[i]["WorkLocationName"].string
                mleave.Reason  = LeaveJSON[i]["Reason"].string
                if IsManager == 0
                {
                    arrTempLeaveRequest.append(mleave)
                }
                else
                {
                    self.arrTeamLeaveList.append(mleave)
                    self.CurrentLeaveListData.append(mleave)
                }
                
            }
       
            if IsManager == 1
            {
                if LeaveJSON.count == 10
                {
                    VisibleShowMore = true
                }
                else
                {
                    VisibleShowMore = false
                }
                
                self.CurrentPageCount = self.CurrentPageCount + 1
                arrTempLeaveRequest = self.arrTeamLeaveList
            }
 
        
            return arrTempLeaveRequest
    }
    
    //Bind Leave list table
    func ConstructLeaveList()
    {
        
        dispatch_async(dispatch_get_main_queue(),{
            
            self.objLoadingIndicaor.stopAnimating()
            
            if self.customSC!.selectedSegmentIndex == 0
            {
               self.CurrentLeaveListData = self.arrSelfLeaveList
            }
            else
            {
               self.CurrentLeaveListData =  self.arrTeamLeaveList
            }
            
            if ( self.CurrentLeaveListData.count > 0 )
            {
                self.LeaveListView.removeFromSuperview()
                self.LeaveListView = UITableView(frame: CGRectMake(0, 110, self.view.frame.width, self.Viewframe.height  - 154 ))
                self.LeaveListView.showsVerticalScrollIndicator = true
                self.LeaveListView.delegate = self
                self.LeaveListView .dataSource = self
                self.LeaveListView.rowHeight = 90
                self.LeaveListView.backgroundColor = UIColor.clearColor()
                self.LeaveListView.separatorColor = UIColor.lightGrayColor()
                self.view.addSubview(self.LeaveListView )
            }
            else
            {
                let lbl_NotFound : UILabel = UILabel(frame: CGRectMake(0, 110, self.view.frame.width, 25 ))
                lbl_NotFound.textAlignment = .Center
                lbl_NotFound.font = UIFont(name:"HelveticaNeue", size: 12.0)
                lbl_NotFound.textColor = UIColor(hexString: String("#ff8877"))
                lbl_NotFound.text = "No leave(s) to show."
                self.view.addSubview(lbl_NotFound)
            }
     
       
            if self.customSC!.selectedSegmentIndex == 1
            {
            if(self.LoadMoreClicked==1)
            {
                self.scrollToLastRow()
            }
            }
         
        })
    
    
    }
    
    
    //# MARK: Table List View Methods
    
    func numberOfSectionsInTableView(LeaveListView: UITableView) -> Int {        
        return 1
    }
    
    func tableView(LeaveListView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.customSC!.selectedSegmentIndex == 1
        {
            if self.VisibleShowMore == true
            {
                return self.CurrentLeaveListData.count + 1
            }
            else
            {
                return self.CurrentLeaveListData.count
            }
            
        }
        else
        {
            return self.CurrentLeaveListData.count
        }
        
        
        
    }
    
    func tableView(LeaveListView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
      
        let cell = UITableViewCell()
        cell.backgroundColor = UIColor.clearColor()
        
        var IsShowMoreSection = false
        
        if self.customSC!.selectedSegmentIndex == 1
        {
            if self.VisibleShowMore == true
            {
                if self.CurrentLeaveListData.count   == indexPath.row
                {
                    IsShowMoreSection = true
                }
            }
        }
        
        if IsShowMoreSection == false
        {
            if let tmpDomainURL = self.defaults.stringForKey("DomainURL")
            {
                if String(self.CurrentLeaveListData[indexPath.row].EmployeePhoto) == "nil" ||  String(self.CurrentLeaveListData[indexPath.row].EmployeePhoto) == ""
                {
                    let img_EmpPic:  UIButton = UIButton(frame: CGRectMake(10, 25, 50, 50))
                    img_EmpPic.layer.borderWidth = 0.5
                    img_EmpPic.layer.masksToBounds = false
                    img_EmpPic.layer.borderColor = UIColor.lightGrayColor().CGColor
                    img_EmpPic.backgroundColor = UIColor.SetRandomColor()
                    img_EmpPic.layer.cornerRadius = img_EmpPic.frame.height/2
                    img_EmpPic.clipsToBounds = true
                    img_EmpPic.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                    img_EmpPic.setTitle(String(self.CurrentLeaveListData[indexPath.row].EmployeeName)[0], forState: UIControlState.Normal)
                    cell.addSubview(img_EmpPic)
                }
                else{
                    let img_EmpPic:  UIImageView = UIImageView(frame: CGRectMake(10, 25, 50, 50))
                    img_EmpPic.layer.borderWidth = 0.5
                    img_EmpPic.layer.masksToBounds = false
                    img_EmpPic.layer.borderColor = UIColor.lightGrayColor().CGColor
                    img_EmpPic.backgroundColor = UIColor(hexString: String("#0CDBFF"))
                    img_EmpPic.layer.cornerRadius = img_EmpPic.frame.height/2
                    img_EmpPic.clipsToBounds = true
                    
                    cell.addSubview(img_EmpPic)
                    
                    if self.CurrentLeaveListData[indexPath.row].EmployeePhoto != nil
                    {
                        let employeePic = "http://" + tmpDomainURL + "/Uploadedfiles/Client" + String(self.defaults.stringForKey("ClientID")!) + "/Image/" + String(self.CurrentLeaveListData[indexPath.row].EmployeePhoto!)
                        img_EmpPic.setImageWithUrl(NSURL(string: employeePic)!)
                    }
                }
                
                
                
            }
            
            
            let lbl_EmployeeName : UILabel = UILabel(frame: CGRectMake(70, 10, self.view.frame.size.width / 100 * 55 ,18))
            lbl_EmployeeName.textAlignment = .Left
            lbl_EmployeeName.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
            lbl_EmployeeName.textColor = UIColor(hexString: String("#333333"))
            lbl_EmployeeName.text = String(self.CurrentLeaveListData[indexPath.row].EmployeeName)
            cell.addSubview(lbl_EmployeeName)
            
            if ( self.CurrentLeaveListData[indexPath.row].PositionName != nil)
            {
            let lbl_PositionName : UILabel = UILabel(frame: CGRectMake(70, 28, self.view.frame.size.width / 100 * 45 ,18))
            lbl_PositionName.textAlignment = .Left
            lbl_PositionName.textColor = UIColor(hexString: String("#666666"))
            lbl_PositionName.font = UIFont(name:"HelveticaNeue", size: 10.0)
            lbl_PositionName.text = String(self.CurrentLeaveListData[indexPath.row].PositionName)
            cell.addSubview(lbl_PositionName)
            }
            let lbl_LeaveType : UILabel = UILabel(frame: CGRectMake(70 , 46, self.view.frame.size.width / 100 * 50,18))
            lbl_LeaveType.textAlignment = .Left
            lbl_LeaveType.textColor = UIColor(hexString: String("#666666"))
            lbl_LeaveType.font = UIFont(name:"HelveticaNeue", size: 10.0)
            lbl_LeaveType.text = String(self.CurrentLeaveListData[indexPath.row].RequestDescription) + " (" + String(self.CurrentLeaveListData[indexPath.row].NOOfDays) + ") Days"
            cell.addSubview(lbl_LeaveType)
            
            
            let lbl_LeaveDate : UILabel = UILabel(frame: CGRectMake(70 , 64, self.view.frame.size.width / 100 * 70,18))
            lbl_LeaveDate.textAlignment = .Left
            lbl_LeaveDate.textColor = UIColor(hexString: String("#333333"))
            lbl_LeaveDate.font = UIFont(name:"HelveticaNeue", size: 10.0)
            lbl_LeaveDate.setFAText(prefixText: "", icon: FAType.FACalendar, postfixText: "  " + String(self.CurrentLeaveListData[indexPath.row].StartDateString) + " - " + String(self.CurrentLeaveListData[indexPath.row].EndDateString) , size: 10.0)
            
            if self.customSC!.selectedSegmentIndex == 1
            {
                self.ModifyButtons(cell,row: indexPath.row )
            }else
            {
                self.CreateDynamicLabel(cell, row: indexPath.row )
            }
            
            cell.addSubview(lbl_LeaveDate)
            
            
            let GestureLeaveDetail = UITapGestureRecognizer(target: self, action: "ShowLeaveDetail:")
            cell.addGestureRecognizer(GestureLeaveDetail)
        }
        else
        {
            let lbl_LoadMore : UILabel = UILabel(frame: CGRectMake(70, 28, self.view.frame.size.width / 100 * 45 ,22))
            lbl_LoadMore.textAlignment = .Left
            lbl_LoadMore.textColor = UIColor.whiteColor()
            lbl_LoadMore.font = UIFont(name:"HelveticaNeue-bold", size: 10.0)
            lbl_LoadMore.text = "Load More. . ."
            lbl_LoadMore.textAlignment = .Center
            lbl_LoadMore.backgroundColor = UIColor(hexString: "#2190ea")
            lbl_LoadMore.layer.cornerRadius = 5
            cell.addSubview(lbl_LoadMore)
            let GestureLeaveDetail = UITapGestureRecognizer(target: self, action: "LoadMore:")
            cell.addGestureRecognizer(GestureLeaveDetail)
            
            
        }
        
        
        
        return cell
    }
    
    
    func ShowLeaveDetail(sender: UITapGestureRecognizer)
    {
        
        
        let tapLocation = sender.locationInView(self.LeaveListView)
      
        let indexPath = self.LeaveListView.indexPathForRowAtPoint(tapLocation)
        
        let  objLeaveDetail = LeaveDetail()
        
        objLeaveDetail.LeaveDetails = self.CurrentLeaveListData[(indexPath?.row)!]
        objLeaveDetail.UserID = self.UserID
        objLeaveDetail.Mode = self.customSC!.selectedSegmentIndex
        objLeaveDetail.OnCompletion = HandleDismissEvent
        objLeaveDetail.RowIndex = indexPath?.row
        self.presentViewController(objLeaveDetail, animated: true, completion: nil)
    
    }
    
    func LoadMore(sender: UITapGestureRecognizer)
    {
      
        GetLeaveList()
        self.LoadMoreClicked = 1
        //LeaveListView.reloadData()
        
       
       
        
      
     
    }
    
    func scrollToLastRow() {
        let indexPath = NSIndexPath(forRow: self.CurrentLeaveListData.count - 1, inSection: 0)
        self.LeaveListView.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Bottom, animated: true)
    }
    
     //# MARK: Custom Methods
    
    //Handles dismiss event of modal view
    func HandleDismissEvent(RowID:Int , ActionCode:String) -> Void
    {
    if ActionCode == "2" // After approve
    {
        
        self.CurrentCell  =  self.LeaveListView.cellForRowAtIndexPath( NSIndexPath(forRow: RowID, inSection: 0))!
        
        self.arrTeamLeaveList[RowID].StatusText = "Approved"
        self.arrTeamLeaveList[RowID].Access = 0
        
        self.CurrentLeaveListData[RowID].StatusText = "Approved"
        self.CurrentLeaveListData[RowID].Access = 0
        
        self.CreateDynamicLabel( self.CurrentCell,row: RowID,StatusText: "Approved")
        
         dispatch_async(dispatch_get_main_queue(),{
        for subview in   self.CurrentCell.subviews
        {
            if subview.tag  == RowID + 1
            {
                
                subview.removeFromSuperview()
            }
            
            }})
    
    }else if ActionCode == "3" // After Reject
    {
        self.CurrentCell  =  self.LeaveListView.cellForRowAtIndexPath( NSIndexPath(forRow: RowID, inSection: 0))!
        
        self.arrTeamLeaveList[RowID].StatusText = "Rejected"
        self.arrTeamLeaveList[RowID].Access = 0
        
        self.CurrentLeaveListData[RowID].StatusText = "Rejected"
        self.CurrentLeaveListData[RowID].Access = 0
        
        self.CreateDynamicLabel( self.CurrentCell,row: RowID,StatusText: "Rejected")
         dispatch_async(dispatch_get_main_queue(),{
        for subview in   self.CurrentCell.subviews
        {
            if subview.tag   == RowID + 1
            {
                
                subview.removeFromSuperview()
            }
            
        }
        })
        
    }
    
    }
    
    
    //Check privilleges of show Approve Reject Button in the leave list
    func ModifyButtons (cell:UITableViewCell , row:Int)
    {
   
    if (self.CurrentLeaveListData[row].ISHRApply == 0 ) {
    if self.CurrentLeaveListData[row].Access == 1 {
    if self.CurrentLeaveListData[row].Type == 2 {
        self.CreateDynamicButton(cell, ButtonMode: 0, Xpos: 70, row: row)
        self.CreateDynamicButton(cell, ButtonMode: 1, Xpos: 85, row: row)
    } else if self.CurrentLeaveListData[row].Type == 3 && self.CurrentLeaveListData[row].ISLastStep == 1 {
        self.CreateDynamicLabel(cell, row: row )
    }
    else if (self.CurrentLeaveListData[row].ISLastStep != 1) {
        self.CreateDynamicButton(cell, ButtonMode: 0, Xpos: 70, row: row)
    }
    
    } else if (self.CurrentLeaveListData[row].Access == 0) {
    if ((self.CurrentLeaveListData[row].RequestedFor == self.CurrentLeaveListData[row].LastActionBy) && ( String(self.CurrentLeaveListData[row].RequestedFor) == String(self.UserID)))  {
    if self.customSC!.selectedSegmentIndex != 3 && self.CurrentLeaveListData[row].StatusText != "Closed"  {
    self.CreateDynamicLabel(cell, row: row )
    
    } else if self.customSC!.selectedSegmentIndex == 1 && self.CurrentLeaveListData[row].StatusText == "Closed" && self.CurrentLeaveListData[row].ParentID == "null" {
    self.CreateDynamicLabel(cell, row: row )
    }
    } else if String(self.UserID) == String(self.CurrentLeaveListData[row].RequestedBY) {
    if self.customSC!.selectedSegmentIndex != 3 && self.CurrentLeaveListData[row].StatusText == "Submitted" {
        self.CreateDynamicLabel(cell, row: row )
    } else {
    if self.CurrentLeaveListData[row].StatusText == "Closed" {
        self.CreateDynamicLabel(cell, row: row )
    } else if self.CurrentLeaveListData[row].ISLastStep == 1 && self.customSC!.selectedSegmentIndex == 2 {
        self.CreateDynamicLabel(cell, row: row )
    }
    
    }
    }
    else if (self.CurrentLeaveListData[row].ProcessType == 1 && String(self.CurrentLeaveListData[row].RequestedBY) == String(self.UserID) && self.CurrentLeaveListData[row].ISChildCreated != 1) {
    if (self.customSC!.selectedSegmentIndex != 3) {
    if (self.CurrentLeaveListData[row].ParentID == "null") {
        self.CreateDynamicLabel(cell, row: row )
    }
    }
    }else{
        self.CreateDynamicLabel(cell, row: row )
    }
    }
    }
    else if ( (self.CurrentLeaveListData[row].ISHRApply == 1)) {
    if (self.CurrentLeaveListData[row].LastActionBy == self.CurrentLeaveListData[row].RequestedBY) {
        self.CreateDynamicLabel(cell, row: row )
    }
    }
    
    
    
    }
    
    
    // Bind the status label with corresponding color
    func CreateDynamicLabel(cell:UITableViewCell , row:Int , StatusText: String = "" )
    {
        let lbl_LeaveStatus : UILabel = UILabel(frame: CGRectMake(self.view.frame.size.width / 100 * 50, 36, self.view.frame.size.width / 100 * 45 ,18))
        lbl_LeaveStatus.textAlignment = .Right
        if (self.CurrentLeaveListData[row].BaseStatus == 1)
        {
            lbl_LeaveStatus.textColor = UIColor(hexString: "#e67f22")
        } else if (self.CurrentLeaveListData[row].BaseStatus == 2) {
            
            lbl_LeaveStatus.textColor = UIColor(hexString: "#399a10")
        } else if (self.CurrentLeaveListData[row].BaseStatus == 3) {
            lbl_LeaveStatus.textColor = UIColor(hexString: "#95a5a5")
        } else {
            lbl_LeaveStatus.textColor = UIColor(hexString: "#ff8877")
        }
        
        lbl_LeaveStatus.font = UIFont(name:"HelveticaNeue", size: 10.0)
        if StatusText == ""
        {
        lbl_LeaveStatus.text = String(self.CurrentLeaveListData[row].StatusText)
        }else{
        
        lbl_LeaveStatus.text = StatusText
        }
        
        cell.addSubview(lbl_LeaveStatus)
    }
    
    
    // Bind Approve/Reject Button in the list
    func CreateDynamicButton(cell:UITableViewCell , ButtonMode:Int , Xpos: CGFloat, row: Int)
    {
      
        if ButtonMode == 0
        {
              let btnApprove : UIButton = UIButton(frame: CGRectMake(self.view.frame.size.width / 100 * Xpos , 36, 30,30))
             btnApprove.backgroundColor = UIColor(hexString: String("#48C9B0"))
             btnApprove.setFAIcon(FAType.FACheck, iconSize: 20, forState:  .Normal)
             btnApprove.addTarget(self, action: "ApproveLeave:", forControlEvents: .TouchUpInside)
             btnApprove.tag = row + 1
            btnApprove.layer.borderWidth = 0
            btnApprove.layer.cornerRadius =  btnApprove.frame.height/2
            btnApprove.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            
            cell.addSubview(btnApprove)
        }
        else
        {
            let btnReject : UIButton = UIButton(frame: CGRectMake(self.view.frame.size.width / 100 * Xpos , 36, 30,30))
             btnReject.backgroundColor = UIColor(hexString: String("#F46868"))
             btnReject.setFAIcon(FAType.FAClose, iconSize: 20, forState:  .Normal)
             btnReject.addTarget(self, action: "RejectLeave:", forControlEvents: .TouchUpInside)
             btnReject.tag = row + 1 
             btnReject.layer.borderWidth = 0
             btnReject.layer.cornerRadius =  btnReject.frame.height/2
             btnReject.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            
            cell.addSubview(btnReject)
        }
       
   
    }
    
    
    func ApproveLeave(Sender: UIButton!)
    {
        self.CurrentRowIndex = Sender.tag
        self.CurrentActionType = 2
        self.CurrentCell  =  self.LeaveListView.cellForRowAtIndexPath( NSIndexPath(forRow: Sender.tag - 1, inSection: 0))!
        self.ProcessLeaveRequest("2", RowIndex: Sender.tag - 1)
    
    }
    
    func RejectLeave(Sender: UIButton!)
    {
        self.CurrentActionType = 3
        self.CurrentRowIndex = Sender.tag
        self.CurrentCell  =  self.LeaveListView.cellForRowAtIndexPath( NSIndexPath(forRow: Sender.tag - 1, inSection: 0))!
        self.ProcessLeaveRequest("3", RowIndex: Sender.tag - 1)
    }
    
    func AddGesture()
    {
        swipeRight = UISwipeGestureRecognizer(target: self, action: "LoadTeamLeaveList:")
        swipeRight.direction = UISwipeGestureRecognizerDirection.Right
        self.view.addGestureRecognizer(swipeRight)
        
        swipeLeft = UISwipeGestureRecognizer(target: self, action: "LoadSelfLeaveList:")
        swipeRight.direction = UISwipeGestureRecognizerDirection.Left
        self.view.addGestureRecognizer(swipeLeft)
    }
    
    func RemoveGesture()
    {
          self.view.removeGestureRecognizer(swipeLeft)
          self.view.removeGestureRecognizer(swipeRight)
    }
    
    // Approve or Reject the leave by callback
    func ProcessLeaveRequest(ActionCode: String, RowIndex: Int )
    {
        self.RemoveGesture()
        
        objLoadingIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, Viewframe.width, Viewframe.height))
        
        objLoadingIndicaor.activityIndicatorViewStyle = .WhiteLarge
        let objPosition =  self.view.center
        objLoadingIndicaor.center = objPosition
        objLoadingIndicaor.hidesWhenStopped = true
        objLoadingIndicaor.color = UIColor.grayColor()
        self.view.addSubview(objLoadingIndicaor)
        objLoadingIndicaor.startAnimating()
    
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            
            objCallBack.MethodName = "ApproveLeave"
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
         
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pRequestID",value: String(self.CurrentLeaveListData[RowIndex].RequestID))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pAction",value: ActionCode)
            objCallBack.ParameterList.append(objparam)
            
            objCallBack.post(ApproveOnSuccess, OnError: ApproveOnError)
            
        }
    
    }
    
    //Response on error callback
    func ApproveOnError(Response: NSError ){
        
    }
    
    func ApproveOnSuccess (ResultString :NSString? ) {
        
         if   self.CurrentActionType == 2
         {
              dispatch_async(dispatch_get_main_queue(),{
                self.arrTeamLeaveList[self.CurrentRowIndex - 1].StatusText = "Approved"
                self.arrTeamLeaveList[self.CurrentRowIndex - 1].Access = 0
        
                self.CurrentLeaveListData[self.CurrentRowIndex - 1].StatusText = "Approved"
                self.CurrentLeaveListData[self.CurrentRowIndex - 1].Access = 0
        
                self.CreateDynamicLabel( self.CurrentCell,row: self.CurrentRowIndex - 1,StatusText: "Approved")
                 dispatch_async(dispatch_get_main_queue(),{
                        self.objLoadingIndicaor.stopAnimating()
                        self.AddGesture()
                        DoneHUD.showInView(self.view, message: "Done")
                        for subview in   self.CurrentCell.subviews
                        {
                            if subview.tag  == self.CurrentRowIndex
                            {
                                subview.removeFromSuperview()
                            }
                    
                        }
                    })
              })
         }
         else
         {
            dispatch_async(dispatch_get_main_queue(),{
                self.arrTeamLeaveList[self.CurrentRowIndex - 1].StatusText = "Rejected"
                self.arrTeamLeaveList[self.CurrentRowIndex - 1].Access = 0
            
                self.CurrentLeaveListData[self.CurrentRowIndex - 1].StatusText = "Rejected"
                self.CurrentLeaveListData[self.CurrentRowIndex - 1].Access = 0
            
                self.CreateDynamicLabel( self.CurrentCell,row: self.CurrentRowIndex - 1,StatusText: "Rejected")
                    dispatch_async(dispatch_get_main_queue(),{
                        self.objLoadingIndicaor.stopAnimating()
                        self.AddGesture()
                        DoneHUD.showInView(self.view, message: "Done")
                        for subview in   self.CurrentCell.subviews
                        {               
                            if subview.tag  == self.CurrentRowIndex
                            {
                                subview.removeFromSuperview()
                            }
                
                        }
                
                    })
                })
        }
      
    }
    
    
}



