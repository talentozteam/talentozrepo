//
//  DLDemoMainContentViewController.swift
//  DLHamburguerMenu
//
//  Created by Nacho on 5/3/15.
//  Copyright (c) 2015 Ignacio Nieto Carvajal. All rights reserved.
//

import UIKit

class DLDemoMainContentViewController: UIViewController ,UIPageViewControllerDataSource ,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate , UITableViewDataSource ,UITableViewDelegate  {
    
    
    // MARK: - Variables
    private var pageViewController: UIPageViewController?
    internal var QuickActionMenu : UIView!
    internal var QuickActionBGLayer : UIView!
    internal var QuickActionList : UIView!
    @IBOutlet var lblMenu: UIBarButtonItem!
    // Initialize it right away here
    
    let objCallBack = AjaxCallBack()
    let defaults = NSUserDefaults.standardUserDefaults()
    var  toolbar :UIToolbar!
    var SubJson: Talentoz.JSON!
    var QuickActionData: Talentoz.JSON!
    let vscrollView = UIScrollView(frame: UIScreen.mainScreen().bounds)
    @IBOutlet var NavigationBarItem: UINavigationItem!
    var QuickActionTableView = UITableView()
    
    var dCollectionView : UICollectionView!
    @IBOutlet var MenuStripView: UIView!
    let objRole = RoleManager()
    var TeamHeight : CGFloat!
    let objCallBack1 = AjaxCallBack()
    let Viewframe = UIScreen.mainScreen().bounds
    
    var isOpen = false
    

    
    override func viewDidLoad() {
        
        
       
        
        self.view.addSubview(vscrollView)
        
        self.vscrollView.contentSize = CGSize(width: self.view.frame.width, height: 700)
        self.vscrollView.backgroundColor = UIColor.whiteColor()
        
        super.viewDidLoad()
        lblMenu.setFAIcon(FAType.FABars, iconSize:16)
       //self.vscrollView.backgroundColor = UIColor.whiteColor()
        
         TeamHeight = 198.0
        
        lblMenu.tintColor = UIColor.whiteColor()
        MenuStripView.backgroundColor = UIColor.whiteColor()
        NavigationBarItem.title = "Home"
        
 
        
        
        toolbar = UIToolbar()
        toolbar.items = [
            
        
            
            UIBarButtonItem(image : UIImage(named: "Home-60.png"), style: UIBarButtonItemStyle.Plain, target: self, action: "HomeAction:"),
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil),
            UIBarButtonItem(image : UIImage(named: "Circled User Male-60.png"), style: UIBarButtonItemStyle.Plain, target: self, action: "ProfileAction:"),
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil),
            UIBarButtonItem(image : UIImage(named: "Beach-60.png"), style: UIBarButtonItemStyle.Plain, target: self, action: "HolidayAction:"),
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil),
            UIBarButtonItem(image : UIImage(named: "Quickaction.png"), style: UIBarButtonItemStyle.Plain, target: self, action: "deleteAction:")
           
        ]
    self.view.addSubview(toolbar)
     
       
        createPageViewController()
        setupPageControl()
        
        let QuickInfoLabel : UILabel = UILabel(frame: CGRectMake(0, 0, self.vscrollView.frame.width , 30))
        QuickInfoLabel.backgroundColor = UIColor(hexString: "#f5f5f5")
        QuickInfoLabel.textAlignment = .Left
        //QuickInfoLabel.font = QuickInfoLabel.font.fontWithSize(13.0)
        QuickInfoLabel.font = UIFont(name:"helvetica-bold", size: 13)
        QuickInfoLabel.text = "  Quick Info"
        self.vscrollView.addSubview(QuickInfoLabel)
        
       
        
        if objRole.ISManager{
           FetchTeamSubordinateDetails()
        }
        else
        {
            TeamHeight = 0
        }
        
        self.BindQuickActions()
        
        let lblQuickAction : UILabel = UILabel(frame: CGRectMake(0, 197 + TeamHeight, self.vscrollView.frame.width , 30))
        lblQuickAction.backgroundColor = UIColor(hexString: "#f5f5f5")
        lblQuickAction.textAlignment = .Left
        lblQuickAction.font =  UIFont(name:"helvetica-bold", size: 13)
        lblQuickAction.text = "  Quick Actions"
        self.vscrollView.addSubview(lblQuickAction)
       

        // Do any additional setup after loading the view.
    }
    
  
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // bounds is now correctly set
        toolbar.frame = CGRectMake(0, view.bounds.height-44, view.bounds.width, 44)
        
        //toolbar.barStyle = UIBarStyle.BlackTranslucent
    }
    
    func HomeAction(sender: UIBarButtonItem) {
        
       // let storyboard = UIStoryboard(name: "Home", bundle: nil)
       // let vc = storyboard.instantiateViewControllerWithIdentifier("HomeMainView")
       // self.presentViewController(vc, animated: true, completion: nil)
        
   
        
    }
    
    func HolidayAction(sender: UIBarButtonItem) {
       let  Holiday = Holidays()
        self.presentViewController(Holiday, animated: true, completion: nil)
       
    }
    
    func ProfileAction(sender: UIBarButtonItem) {

        let  ProfileVCon = ProfileVC()
        self.presentViewController(ProfileVCon, animated: true, completion: nil)
    }
    
    func deleteAction(sender: UIBarButtonItem) {
        BindQuickActionMenu()
 
    }
    
    func BindQuickActionMenu()
    {
        
        if isOpen
        {
            isOpen = false
            QuickActionBGLayer.removeFromSuperview()
        }
        else{
            
            QuickActionBGLayer = UIView(frame: CGRectMake(0, 0, Viewframe.width  ,  Viewframe.height))
            QuickActionBGLayer.backgroundColor = UIColor(colorLiteralRed: 0 , green: 0, blue: 0, alpha: 0.6)
            let GestureLeaveDetail1 = UITapGestureRecognizer(target: self, action: #selector(DLDemoMainContentViewController.DismissQuickActionMenu(_:)))
            QuickActionBGLayer.addGestureRecognizer(GestureLeaveDetail1)
            QuickActionMenu = UIView(frame: CGRectMake(10, QuickActionBGLayer.frame.height - 55 , QuickActionBGLayer.frame.width - 20  , 50))
            QuickActionMenu.backgroundColor = UIColor(colorLiteralRed: 0.95, green: 0.95, blue: 0.95, alpha: 1)
            QuickActionMenu.layer.cornerRadius = 8
            let UICancelButton = UIButton(frame: CGRectMake(0, 0, QuickActionMenu.frame.width  ,  50 ))
            UICancelButton.setTitle("Dismiss", forState: .Normal)
            UICancelButton.setTitleColor(UIColor.redColor(), forState: .Normal)
            
            UICancelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Center
            UICancelButton.contentVerticalAlignment = UIControlContentVerticalAlignment.Center
            UICancelButton.addTarget(self, action: #selector(DLDemoMainContentViewController.UnBindQuickActionMenu(_:)), forControlEvents: .TouchUpInside)
            QuickActionMenu.addSubview(UICancelButton)
            QuickActionBGLayer.addSubview(QuickActionMenu)
            
            
            
            
            QuickActionList = UIView(frame: CGRectMake(10, QuickActionBGLayer.frame.height - 225 , QuickActionBGLayer.frame.width - 20  , 152))
            QuickActionList.backgroundColor = UIColor(colorLiteralRed: 0.95, green: 0.95, blue: 0.95, alpha: 1)
            QuickActionList.layer.cornerRadius = 7
            QuickActionBGLayer.addSubview(QuickActionList)
            
            let UIApplyButton = UIButton(frame: CGRectMake(5, 0, QuickActionList.frame.width - 10  ,  50 ))
            UIApplyButton.setTitle("Apply Leave", forState: .Normal)
            
            UIApplyButton.setTitleColor(UIColor(hexString: "#0793F7"), forState: .Normal)
            UIApplyButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Center
            UIApplyButton.contentVerticalAlignment = UIControlContentVerticalAlignment.Center
            UIApplyButton.addTarget(self, action: #selector(DLDemoMainContentViewController.ApplyLeaveInvoke(_:)), forControlEvents: .TouchUpInside)
            QuickActionList.addSubview(UIApplyButton)
            
            let sepview = UIView(frame: CGRectMake(0, 50,  QuickActionList.frame.width  , 1))
            sepview.backgroundColor = UIColor.lightGrayColor()
            QuickActionList.addSubview(sepview)
            
            let UIApplyAdjustment = UIButton(frame: CGRectMake(5, 55 , QuickActionList.frame.width - 10  ,  50 ))
            UIApplyAdjustment.setTitle("Raise Adjustment", forState: .Normal)
            
            UIApplyAdjustment.setTitleColor(UIColor(hexString: "#0793F7"), forState: .Normal)
            UIApplyAdjustment.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Center
            UIApplyAdjustment.contentVerticalAlignment = UIControlContentVerticalAlignment.Center
            UIApplyAdjustment.addTarget(self, action: #selector(DLDemoMainContentViewController.RaiseNewAdjustment(_:)), forControlEvents: .TouchUpInside)
            QuickActionList.addSubview(UIApplyAdjustment)
            
            
            
            let sepview1 = UIView(frame: CGRectMake(0, 105,  QuickActionList.frame.width  , 1))
            sepview1.backgroundColor = UIColor.lightGrayColor()
            QuickActionList.addSubview(sepview1)
            
            let UIApplyAddClaim = UIButton(frame: CGRectMake(5, 105 , QuickActionList.frame.width - 10  ,  50 ))
            UIApplyAddClaim.setTitle("Add Claim", forState: .Normal)
            
            UIApplyAddClaim.setTitleColor(UIColor(hexString: "#0793F7"), forState: .Normal)
            UIApplyAddClaim.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Center
            UIApplyAddClaim.contentVerticalAlignment = UIControlContentVerticalAlignment.Center
            UIApplyAddClaim.addTarget(self, action: #selector(DLDemoMainContentViewController.AddAClaim(_:)), forControlEvents: .TouchUpInside)
            QuickActionList.addSubview(UIApplyAddClaim)
            
            
            
            
            self.view.addSubview(QuickActionBGLayer)
            
            
        }
        
    }
    
    func AddAClaim(sender: UIButton)
    {
        let objClaim = AddClaim()
        self.presentViewController(objClaim, animated: true, completion: nil)
    }
    
    func RaiseNewAdjustment(sender: UIButton)
    {
        isOpen = false
        QuickActionBGLayer.removeFromSuperview()
               
        let  ObjApplyAdjustment = RaiseAdjustment()
        ObjApplyAdjustment.AdjustmentDate = "Select Date"
        
        self.presentViewController(ObjApplyAdjustment, animated: true, completion: nil)
        
    }
    
    func UnBindQuickActionMenu(sender: UIButton)
    {
        isOpen = false
        QuickActionBGLayer.removeFromSuperview()
    	
    }
    
    func DismissQuickActionMenu(sender: UITapGestureRecognizer)
    {
        isOpen = false
        QuickActionBGLayer.removeFromSuperview()
        
    }
    
    func ApplyLeaveInvoke(sender: UIButton)
    {
        isOpen = false
        QuickActionBGLayer.removeFromSuperview()
        let  ObjApplyLeave = ApplyLeave()
        self.presentViewController(ObjApplyLeave, animated: true, completion: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 

    @IBAction func menuButtonTouched(sender: AnyObject) {
  
        self.findHamburguerViewController()?.showMenuViewController()
    }
    
    //Quick info related code begins here
    private func createPageViewController() {
        
        let pageController = self.storyboard!.instantiateViewControllerWithIdentifier("PageController") as! UIPageViewController
        pageController.dataSource = self
    
    
    
        
        if 3 > 0 {
            let firstController = getItemController(0)!
            let startingViewControllers: NSArray = [firstController]
            pageController.setViewControllers(startingViewControllers as? [UIViewController], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)            
        }
        
        pageViewController = pageController
        addChildViewController(pageViewController!)
        self.vscrollView.addSubview(pageViewController!.view)
        
        self.pageViewController!.view.frame =
            CGRectMake(0,20, self.vscrollView.frame.width, 180)
        
       
    }
    
    private func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = UIColor(hexString: "#cccccc")
        appearance.currentPageIndicatorTintColor = UIColor(hexString: "#2190ea")
        appearance.backgroundColor = UIColor(hexString: "#fffffe")
        appearance.frameForAlignmentRect(self.vscrollView.frame)
        appearance.frame = CGRectMake(0,25, self.vscrollView.frame.width, 180)
        
        
       
        
    
    }
    
    // MARK: - UIPageViewControllerDataSource
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! PageItemController
        
        if itemController.itemIndex > 0 {
            return getItemController(itemController.itemIndex-1)
        }
        
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! PageItemController
        
        if itemController.itemIndex + 1 < 3 {
            return getItemController(itemController.itemIndex + 1)
        }
        
        return nil
    }
    
    private func getItemController(itemIndex: Int) -> PageItemController? {
        
       // if itemIndex < 3 {
        
        var pageItemController : PageItemController!
        
        if objRole.ISManager{
      
   
            if itemIndex == 0 {
                
             pageItemController = self.storyboard!.instantiateViewControllerWithIdentifier("TotalHoursController") as! PageItemController
            pageItemController.itemIndex = itemIndex
               
           // dispatch_async(dispatch_get_main_queue(),{
           //      let appearance = UIPageControl.appearance()
          //  appearance.backgroundColor = UIColor.init(colorLiteralRed:142/255, green:182/255, blue:201/255 , alpha:1.0)
                
           //     })
               
                
            }
            
            else if itemIndex == 1 {
                
                 pageItemController = self.storyboard!.instantiateViewControllerWithIdentifier("TotalLeaveController") as! PageItemController
                pageItemController.itemIndex = itemIndex
                
              //  dispatch_async(dispatch_get_main_queue(),{
              //      let appearance = UIPageControl.appearance()
               //     appearance.backgroundColor = UIColor.init(colorLiteralRed:139/255, green:190/255, blue:164/255 , alpha:1.0)})
                
               // return pageItemController
                
            }
            
          else  if itemIndex == 2 {
                
                 pageItemController = self.storyboard!.instantiateViewControllerWithIdentifier("TotalTrainingHoursController") as! PageItemController
                pageItemController.itemIndex = itemIndex
               // dispatch_async(dispatch_get_main_queue(),{
               //     let appearance = UIPageControl.appearance()
               //     appearance.backgroundColor = UIColor.init(colorLiteralRed:109/255, green:171/255, blue:208/255 , alpha:1.0)})

                //return pageItemController
                
            }
        }
        else
        {
            if itemIndex == 0 {
                
                pageItemController = self.storyboard!.instantiateViewControllerWithIdentifier("TotalHoursController") as! PageItemController
                pageItemController.itemIndex = itemIndex
                
                // dispatch_async(dispatch_get_main_queue(),{
                //      let appearance = UIPageControl.appearance()
                //  appearance.backgroundColor = UIColor.init(colorLiteralRed:142/255, green:182/255, blue:201/255 , alpha:1.0)
                
                //     })
                
                
            }
                
            else if itemIndex == 1 {
                
                pageItemController = self.storyboard!.instantiateViewControllerWithIdentifier("TotalTrainingHoursController") as! PageItemController
                pageItemController.itemIndex = itemIndex
                
                //  dispatch_async(dispatch_get_main_queue(),{
                //      let appearance = UIPageControl.appearance()
                //     appearance.backgroundColor = UIColor.init(colorLiteralRed:139/255, green:190/255, blue:164/255 , alpha:1.0)})
                
                // return pageItemController
                
            }
        }
        
          return pageItemController
           
          
        //}
        
        //return nil
    }
    
    // MARK: - Page Indicator
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
          if objRole.ISManager{
        return 3
        }
        else
          {
            return 2
        }
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    //Quick info related code ends here
    
   

    
    //Response on error callback
    func EmpProfileOnErrorOnError(Response: NSError ){
        
    }
    
    // Subordinate Slider

    
    func FetchTeamSubordinateDetails() -> Void
    {
        
        let defaults = NSUserDefaults.standardUserDefaults()
        
        
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            objCallBack1.MethodName = "GetFullSubList"
            let ResultJSON = objCallBack1.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack1.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack1.ParameterList.append(objparam)
            
            objCallBack1.post(setupCollectionView, OnError: EmpProfileOnErrorOnError)
            
        }
        
    }
    
    

   
    
    func setupCollectionView (ResultString :NSString? ) {
        
        dispatch_async(dispatch_get_main_queue(),{
            self.SubJson = JSON(data: self.objCallBack1.GetJSONData(ResultString)!)
            
            let lblSubordinateTitle : UILabel = UILabel(frame: CGRectMake(0, 197, self.vscrollView.frame.width , 30))
            lblSubordinateTitle.backgroundColor = UIColor(hexString: "#f5f5f5")
            lblSubordinateTitle.textAlignment = .Left
            lblSubordinateTitle.font =  UIFont(name:"helvetica-bold", size: 13)
            lblSubordinateTitle.text = "  My Team"
            self.vscrollView.addSubview(lblSubordinateTitle)
            
            
            
            let dCollectionViewFlowLayout : UICollectionViewFlowLayout = UICollectionViewFlowLayout ()
            self.dCollectionView = UICollectionView (frame: self.vscrollView.frame, collectionViewLayout: dCollectionViewFlowLayout)
            self.dCollectionView.delegate = self
            self.dCollectionView.dataSource = self
            self.dCollectionView.backgroundColor = UIColor.blackColor()
            
            self.dCollectionView.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "cellIdentifier")
            self.vscrollView.addSubview(self.dCollectionView)
            
            self.dCollectionView.frame = CGRectMake(0,237, self.vscrollView.frame.size.width,150)
            
    
            
           
       
        })
        
        
        
    }
    
    
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return  1
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            return CGSize(width: self.vscrollView.frame.size.width, height: 150)
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        
        let cell : UICollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("cellIdentifier", forIndexPath: indexPath)
        
      cell.backgroundColor = UIColor.whiteColor()
        
        let scroll: UIScrollView = UIScrollView(frame: CGRectMake(0, 0, dCollectionView.frame.size.width, dCollectionView.frame.size.height))
        scroll.delegate = self
        scroll.backgroundColor = UIColor.whiteColor()
        cell.addSubview(scroll)
        
        var xbase : CGFloat = 10
        
      for var i = 0; i < self.SubJson.count; ++i {
        
             let ContainerView = UIView(frame: CGRectMake(xbase + 3, 7, 100, 130))
             ContainerView.layer.borderColor = UIColor(hexString: "#cccccc")?.CGColor
             ContainerView.layer.borderWidth = 1
        
        if  (self.SubJson[i]["EmployeePhoto"].type == Talentoz.Type.Null || self.SubJson[i]["EmployeePhoto"].string == ""  )
        {
            let img_EmpPic:  UIButton = UIButton(frame: CGRectMake(12.5, 7, 75, 75))           
            img_EmpPic.layer.masksToBounds = false
            //img_EmpPic.layer.borderWidth = 1
            img_EmpPic.layer.borderColor = UIColor.blackColor().CGColor
            img_EmpPic.layer.cornerRadius = img_EmpPic.frame.height/2
            img_EmpPic.clipsToBounds = true
              img_EmpPic.tag = self.SubJson[i]["UserID"].int!
           // img_EmpPic.titleLabel!.font = UIFont(name: "helvetica-bold", size: 25)
            img_EmpPic.addTarget(self, action: "ShowEmpProfileBtn:", forControlEvents: .TouchUpInside)
            img_EmpPic.setTitleColor(UIColor(hexString: "#666666"), forState: UIControlState.Normal)
            img_EmpPic.setFAIcon(FAType.FAUser, iconSize: 75, forState: .Normal)
            ContainerView.addSubview(img_EmpPic)
            
        }
        else
        {
            let imgView : UIImageView = UIImageView(frame: CGRectMake(12.5, 7, 75, 75))
            //CGRectMake(x,y,width,height)
            imgView.layer.cornerRadius = 0.5
        
            //  imgView.layer.borderColor = UIColor.whiteColor().CGColor
            imgView.layer.borderWidth = 0
            ContainerView.addSubview(imgView)
        
        if let tmpDomainURL = self.defaults.stringForKey("DomainURL")
        {      
            let employeePic = "http://" + tmpDomainURL + "/Uploadedfiles/Client" + String(self.defaults.stringForKey("ClientID")!) + "/Image/" + String(self.SubJson[i]["EmployeePhoto"])
            imgView.setImageWithUrl(NSURL(string: employeePic)!)
        }
        }
        
        ContainerView.layer.backgroundColor = UIColor(hexString: "#ededed")?.CGColor
            
        
            
            let lbl_Title : UILabel = UILabel(frame: CGRectMake( 5, 87, 90	, 15))
        
            lbl_Title.textAlignment = .Center
            lbl_Title.font = lbl_Title.font.fontWithSize(11.0)
           lbl_Title.textColor = UIColor(hexString: "#333333")
            ContainerView.addSubview(lbl_Title)
            lbl_Title.text = String(self.SubJson[i]["EmployeeName"])
            
            let lbl_Title1 : UILabel = UILabel(frame: CGRectMake(5 , 100, 90, 15))
        
            lbl_Title1.textAlignment = .Center
            lbl_Title1.font = lbl_Title1.font.fontWithSize(9.0)
           lbl_Title1.textColor = UIColor(hexString: "#666666")
            ContainerView.addSubview(lbl_Title1)
            if(self.SubJson[i]["PositionName"] != nil)
            {
                lbl_Title1.text = String(self.SubJson[i]["PositionName"])
            }
       
        
        
            ContainerView.tag = self.SubJson[i]["UserID"].int!
        
        
        let GestureLeaveDetail1 = UITapGestureRecognizer(target: self, action: #selector(DLDemoMainContentViewController.ShowEmpProfile(_:)))
       
        ContainerView.addGestureRecognizer(GestureLeaveDetail1)
        
             scroll.addSubview(ContainerView)
            xbase += 100 + 10
        
    
      
      
        }
        
        
        scroll.contentSize = CGSizeMake(xbase, scroll.frame.size.height)
        
        
        return cell
        
    }
    
    func ShowEmpProfile(sender : UITapGestureRecognizer)
    {
       let  ProfileVCon = ProfileVC()
        ProfileVCon.UserID = (sender.view?.tag)!
     
       self.presentViewController(ProfileVCon, animated: true, completion: nil)
    }
    func ShowEmpProfileBtn(sender : UIButton)
    {
        let  ProfileVCon = ProfileVC()
        ProfileVCon.UserID = (sender.tag)
        
        self.presentViewController(ProfileVCon, animated: true, completion: nil)
    }
    
    func BindQuickActions()
    {
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            objCallBack.MethodName = "GetPendingCountDetails"
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objCallBack.post(QuickActionOnComplete, OnError: QuickActionOnErrorOnError)
            
        }
    }
    
    //Response on error callback
    func QuickActionOnErrorOnError(Response: NSError ){
        
    }
    
    func QuickActionOnComplete (ResultString :NSString? ) {
        
        dispatch_async(dispatch_get_main_queue(),{
            self.QuickActionData = JSON(data: self.objCallBack.GetJSONData(ResultString)!)
        
            self.QuickActionTableView = UITableView(frame: CGRectMake(0, 242 + self.TeamHeight, self.vscrollView.frame.width, 150 ))
            self.QuickActionTableView.showsVerticalScrollIndicator = true
            self.QuickActionTableView.delegate = self
            self.QuickActionTableView .dataSource = self
            self.QuickActionTableView.separatorColor = UIColor.clearColor()
            self.vscrollView.addSubview(self.QuickActionTableView )
            
        })
        
        
    }
    
    
    func numberOfSectionsInTableView(QuickActionTableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(QuickActionTableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if objRole.ISManager{
            
            //return self.QuickActionData.count
          return 3
        
        } else {
            //return self.QuickActionData.count - 1
            return 0
        }
        
    }
    
    
    func tableView(QuickActionTableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
     
        let cell = UITableViewCell()
       
     
        
         var Message: String!=""
        
        let btnButton : UIButton = UIButton(frame: CGRectMake(10 , 2,  30 ,30))
       
        btnButton.layer.cornerRadius = btnButton.frame.height/2
        btnButton.layer.borderWidth = 0
      //  btnButton.layer.borderColor = UIColor(hexString: String(self.LeaveBalJSON[i]["LeaveColorCode"]))!.CGColor
        btnButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
       
        if indexPath.row == 0//self.QuickActionData[indexrow]["FeatureID"] == 1
        {
             btnButton.backgroundColor = UIColor(hexString: "#FFC958")
            btnButton.setFAIcon(FAType.FAFileO, iconSize: 15, forState: .Normal)
            Message = "  leaves to approve"
            
        }
        else if indexPath.row == 2 //self.QuickActionData[indexrow]["FeatureID"] == 2
        {
            btnButton.backgroundColor = UIColor(hexString: "#847CC5")
            btnButton.setFAIcon(FAType.FAMoney	, iconSize: 15, forState: .Normal)
            Message = "  claims to approve"
        }
        else if indexPath.row == 3 //self.QuickActionData[indexrow]["FeatureID"] == 2
        {
            btnButton.backgroundColor = UIColor(hexString: "#847CC5")
            btnButton.setFAIcon(FAType.FAStar	, iconSize: 15, forState: .Normal)
            Message = "  upcoming interviews"
        }
        else if indexPath.row == 4 //self.QuickActionData[indexrow]["FeatureID"] == 3
        {
            btnButton.backgroundColor = UIColor(hexString: "#58C9F3")
            btnButton.setFAIcon(FAType.FAClone, iconSize: 15, forState: .Normal)
            Message = "  sessions to attend"
        }
        else if indexPath.row == 1//if self.QuickActionData[indexrow]["FeatureID"] == 4
        {
            btnButton.backgroundColor = UIColor(hexString: "#58C9F3")
            btnButton.setFAIcon(FAType.FAClone, iconSize: 15, forState: .Normal)
            Message = "  adjustment to approve"
        }
        
        //cell.tag = self.QuickActionData[indexPath.row]["FeatureID"].int!
        cell.tag = indexPath.row
        cell.addSubview(btnButton)
        
        let lbl_Approval1 : UILabel = UILabel(frame: CGRectMake( self.vscrollView.frame.size.width / 100 * 18, 10,  self.vscrollView.frame.size.width / 100 * 25 ,15))
        lbl_Approval1.textAlignment = .Left
        lbl_Approval1.textColor = UIColor(hexString: "#666666")
        lbl_Approval1.font = UIFont(name:"HelveticaNeue", size: 13.0)
        
        
        let lbl_Approval12 : UILabel = UILabel(frame: CGRectMake( self.vscrollView.frame.size.width / 100 * 36, 10,  self.vscrollView.frame.size.width / 100 * 12 ,15))
        lbl_Approval12.textAlignment = .Left
        lbl_Approval12.textColor = UIColor(hexString: "#333333")
        lbl_Approval12.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
        
        
        
        let lbl_Approval13 : UILabel = UILabel(frame: CGRectMake( self.vscrollView.frame.size.width / 100 * 40, 10,  self.vscrollView.frame.size.width / 100 * 55 ,15))
        lbl_Approval13.textAlignment = .Left
        lbl_Approval13.textColor = UIColor(hexString: "#666666")
        lbl_Approval13.font = UIFont(name:"HelveticaNeue", size: 13.0)
        
    
        
      
        
        lbl_Approval1.text = "You have  "
        cell.addSubview(lbl_Approval1)
        
         lbl_Approval12.text =  String(self.QuickActionData[indexPath.row]["FeatureCount"])
         if indexPath.row == 2
         {
             lbl_Approval12.text =  String(self.QuickActionData[4]["FeatureCount"])
         }
        
        cell.addSubview(lbl_Approval12)
        
        lbl_Approval13.text = Message
        cell.addSubview(lbl_Approval13)
        
        
        
        let lbl_Approval2 : UILabel = UILabel(frame: CGRectMake( self.vscrollView.frame.size.width / 100 * 85 , 12	, self.vscrollView.frame.size.width / 100 * 5,15))
       
        lbl_Approval2.textAlignment = .Center
        lbl_Approval2.textColor = UIColor.lightGrayColor()
        
        lbl_Approval2.setFAIcon(FAType.FAAngleRight, iconSize: 15)
        cell.addSubview(lbl_Approval2)
        
        
        let GestureLeaveDetail = UITapGestureRecognizer(target: self, action: "ShowDetail:")
        cell.addGestureRecognizer(GestureLeaveDetail)
        
       
        
        return cell
    }
    
    func ShowDetail(sender: UITapGestureRecognizer)
    {
        if sender.view?.tag == 0
        {
            let LeaveListObj = LeaveList()
            LeaveListObj.ModeofShow = 1
            self.presentViewController(LeaveListObj, animated: true, completion: nil)
        }
       else if sender.view?.tag == 1
        {
            let AdjListObj = ADJList()
            AdjListObj.ModeofShow = 1
            self.presentViewController(AdjListObj, animated: true, completion: nil)
        }
        else if sender.view?.tag == 2
        {
            let ClaimListObj = ClaimsRequestListB()
            ClaimListObj.ModeofShow = 1
            self.presentViewController(ClaimListObj, animated: true, completion: nil)
        }
    }

    

}
