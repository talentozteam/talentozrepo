//
//  MyTeam.swift
//  Talentoz
//
//  Created by forziamac on 31/12/15.
//  Copyright © 2015 Forzia Tech Private Ltd. All rights reserved.
//

import UIKit

class MyTeam: BaseTabBarVC, UITableViewDataSource ,UITableViewDelegate  {

    let objCallBack = AjaxCallBack()
    let defaults = NSUserDefaults.standardUserDefaults()
    var SourceData: Talentoz.JSON!
    var SubListView = UITableView()
    
    override func viewDidLoad() {
        super.UINavigationBarTitle = "My Team"
        super.viewDidLoad()
        BindHolidays()
        
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func BindHolidays()
    {
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            objCallBack.MethodName = "GetFullSubList"
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objCallBack.post(MyTeamListOnComplete, OnError: HolidayListOnErrorOnError)
            
        }
        
    }
    
    override func goBack() {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("HomeInitialView")
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    //Response on error callback
    func HolidayListOnErrorOnError(Response: NSError ){
        
    }
    
    func MyTeamListOnComplete (ResultString :NSString? ) {
        
        dispatch_async(dispatch_get_main_queue(),{
            self.SourceData =   JSON(data: self.objCallBack.GetJSONData(ResultString)!)
            
         
            
            self.SubListView = UITableView(frame: CGRectMake(0, 60 , self.view.frame.width, self.Viewframe.height  - 114 ))
            self.SubListView.showsVerticalScrollIndicator = true
            self.SubListView.delegate = self
            self.SubListView .dataSource = self
            self.SubListView.rowHeight = 80
              self.SubListView.separatorColor = UIColor.whiteColor()
       
            self.view.addSubview(self.SubListView )
            
        })
        
        
    }
    
    
    func numberOfSectionsInTableView(HolidayListView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(HolidayListView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.SourceData.count
    }
    
    
    func tableView(HolidayListView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // 3
        let cell = UITableViewCell()
        
        cell.backgroundColor = UIColor.whiteColor()
        cell.layer.borderColor = UIColor.whiteColor().CGColor
        
        if  (self.SourceData[indexPath.row]["EmployeePhoto"].type == Talentoz.Type.Null || self.SourceData[indexPath.row]["EmployeePhoto"].string == ""  )
        {
        
            let img_EmpPic:  UIButton = UIButton(frame: CGRectMake(10, 10, 60, 60))
            img_EmpPic.layer.borderWidth = 0.5
            img_EmpPic.layer.masksToBounds = false
            img_EmpPic.layer.borderColor = UIColor.lightGrayColor().CGColor
            img_EmpPic.backgroundColor = UIColor.SetRandomColor()
            img_EmpPic.layer.cornerRadius = img_EmpPic.frame.height/2
            img_EmpPic.clipsToBounds = true
            img_EmpPic.titleLabel!.font = UIFont(name: "helvetica-bold", size: 25)
            img_EmpPic.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            img_EmpPic.setTitle(String(self.SourceData[indexPath.row]["EmployeeName"])[0], forState: UIControlState.Normal)
            cell.addSubview(img_EmpPic)
        }
        else
        {
            
            let imgView : UIImageView = UIImageView(frame: CGRectMake(10, 10, 60, 60))          
            imgView.layer.borderWidth = 1
            imgView.layer.masksToBounds = false
            imgView.layer.borderColor = UIColor.whiteColor().CGColor
            imgView.layer.cornerRadius = imgView.frame.height/2
            imgView.clipsToBounds = true
            if let tmpDomainURL = self.defaults.stringForKey("DomainURL")
            {
                
                let employeePic = "http://" + tmpDomainURL + "/Uploadedfiles/Client" + String(self.defaults.stringForKey("ClientID")!) + "/Image/" + String( self.SourceData[indexPath.row]["EmployeePhoto"])
                imgView.setImageWithUrl(NSURL(string: employeePic)!)
            }
            cell.addSubview(imgView)
        }
        
        let lbl_EmpName : UILabel = UILabel(frame: CGRectMake(80, 10,  (self.view.frame.size.width - 60) / 100 * 92 ,18))
        lbl_EmpName.backgroundColor = UIColor.whiteColor()
        lbl_EmpName.layer.borderColor = UIColor.whiteColor().CGColor
        lbl_EmpName.textAlignment = .Left
        lbl_EmpName.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
        lbl_EmpName.textColor = UIColor(hexString: "#333333")
        lbl_EmpName.text = String(self.SourceData[indexPath.row]["EmployeeName"])
        cell.addSubview(lbl_EmpName)
        
        let lbl_Email : UILabel = UILabel(frame: CGRectMake(80, 30, (self.view.frame.size.width - 60) / 100 * 92  ,18))
        lbl_Email.backgroundColor = UIColor.whiteColor()
        lbl_Email.textAlignment = .Left
        lbl_Email.textColor = UIColor(hexString: "#666666")
        lbl_Email.font = UIFont(name:"HelveticaNeue", size: 12.0)
        lbl_Email.text = String(self.SourceData[indexPath.row]["Email"])
        cell.addSubview(lbl_Email)
        
        let lbl_Position : UILabel = UILabel(frame: CGRectMake(80 ,50, (self.view.frame.size.width - 60) / 100 * 92 ,18))
        lbl_Position.backgroundColor = UIColor.whiteColor()
        
        lbl_Position.textColor = UIColor(hexString: "#666666")
        lbl_Position.font = UIFont(name:"HelveticaNeue", size: 12.0)
        if( self.SourceData[indexPath.row]["PositionName"] != nil)
        {
            lbl_Position.text = String(self.SourceData[indexPath.row]["PositionName"])
        }
        
        cell.addSubview(lbl_Position)
        
        cell.tag = self.SourceData[indexPath.row]["UserID"].int!
        let GestureLeaveDetail1 = UITapGestureRecognizer(target: self, action: "ShowEmpProfile:")
        
        cell.addGestureRecognizer(GestureLeaveDetail1)
  
     //   if (indexPath.row == self.SourceData.count-1) {
       //     cell.separatorInset = UIEdgeInsetsMake(0, cell.bounds.size.width, 0, 0)
      //  }
        
        return cell
    }
    
    func ShowEmpProfile(sender : UITapGestureRecognizer)
    {
        let  ProfileVCon = ProfileVC()
        ProfileVCon.UserID = (sender.view?.tag)!
        
        self.presentViewController(ProfileVCon, animated: true, completion: nil)
    }

}
