//
//  TotalHoursBV.swift
//  Talentoz
//
//  Created by forziamac on 22/01/16.
//  Copyright © 2016 Forzia Tech Private Ltd. All rights reserved.
//

import UIKit
import CoreLocation

class TotalHoursBV : UIViewController , CLLocationManagerDelegate
{
    
    var Hour: Int!
    var Minute: Int!
    var Second: Int!
    var objCallBack = AjaxCallBack()
    internal var Mode: Int!
    internal var CalculatedTime: Int!
    internal var VisibleCheckInOut = false
    let defaults = NSUserDefaults.standardUserDefaults()
    var LeaveApprovalJSON: Talentoz.JSON!
    var objLRActivityIndicaor : UIActivityIndicatorView!
    @IBOutlet var btnChkInOut: UIButton!
    @IBOutlet var lblHour: UILabel!
    @IBOutlet var lblMinute: UILabel!
    @IBOutlet var lblSecond: UILabel!
    internal var timer = NSTimer()
    @IBOutlet var colonlbl: UILabel!
    @IBOutlet var lbltotalhourstxt: UILabel!
    
    
    @IBOutlet var colon2: UILabel!
    
    @IBOutlet var secview: UIView!
    @IBOutlet var minview: UIView!
    @IBOutlet var hoursview: UIView!
    
    @IBOutlet var lblhourtxt: UILabel!
    
    @IBOutlet var lblminutetxt: UILabel!
    
    @IBOutlet var lblsecondstxt: UILabel!
    var locationManager : CLLocationManager!
   
    
    override func viewDidLoad() {
        
        if (VisibleCheckInOut == false)
        {
            btnChkInOut.hidden = true
        }
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        self.hoursview.frame = CGRectMake(self.view.frame.size.width / 100 * 5 , 45 , self.view.frame.size.width / 100 * 20 , self.view.frame.size.width / 100 * 15)
        self.hoursview.layer.borderColor = UIColor.whiteColor().CGColor
        self.hoursview.layer.borderWidth = 1
        self.hoursview.backgroundColor = UIColor.clearColor()
        self.hoursview.layer.cornerRadius = 5
         self.lbltotalhourstxt.frame = CGRectMake(0 , 7 ,self.view.frame.size.width, 25)
        
        self.lblHour.frame = CGRectMake(0 , self.hoursview.frame.size.height / 100 * 10 , self.hoursview.frame.size.width ,  self.hoursview.frame.size.height / 100 * 80)
        self.lblHour.textAlignment = .Center
        self.lblHour.backgroundColor = UIColor.clearColor()
        self.lblhourtxt.frame = CGRectMake(self.hoursview.frame.size.width / 100 * 36 , self.hoursview.frame.size.height / 100 * 72 , self.hoursview.frame.size.width , 10)
        self.lblhourtxt.textAlignment = .Center
        
        self.colonlbl.frame = CGRectMake(self.view.frame.size.width / 100 * 25 , 30 ,self.view.frame.size.width / 100 * 15, self.view.frame.size.width / 100 * 20)
        self.colonlbl.textAlignment = .Center
        
        
        
        
        
        
        self.minview.frame = CGRectMake(self.view.frame.size.width / 100 * 40 , 45 , self.view.frame.size.width / 100 * 20 , self.view.frame.size.width / 100 * 15)
        self.minview.layer.borderColor = UIColor.whiteColor().CGColor
        self.minview.layer.borderWidth = 1
        self.minview.backgroundColor = UIColor.clearColor()
        self.minview.layer.cornerRadius = 5
        
        
        self.lblMinute.frame = CGRectMake(0 , self.hoursview.frame.size.height / 100 * 10 , self.hoursview.frame.size.width ,  self.hoursview.frame.size.height / 100 * 80)
          self.lblMinute.backgroundColor = UIColor.clearColor()
        
        self.lblminutetxt.frame = CGRectMake(self.hoursview.frame.size.width / 100 * 36 , self.hoursview.frame.size.height / 100 * 72 , self.hoursview.frame.size.width , 10)
        self.lblminutetxt.textAlignment = .Center
        
        
        self.colon2.frame = CGRectMake(self.view.frame.size.width / 100 * 60, 30 ,self.view.frame.size.width / 100 * 15 , self.view.frame.size.width / 100 * 20)
        self.colonlbl.textAlignment = .Center
        
        
        
        self.secview.frame = CGRectMake(self.view.frame.size.width / 100 * 75 , 45 , self.view.frame.size.width / 100 * 20 , self.view.frame.size.width / 100 * 15)
        self.secview.layer.borderColor = UIColor.whiteColor().CGColor
        self.secview.layer.borderWidth = 1
        self.secview.backgroundColor = UIColor.clearColor()
        self.secview.layer.cornerRadius = 5

        
        self.lblSecond.frame = CGRectMake(0 , self.hoursview.frame.size.height / 100 * 10 , self.hoursview.frame.size.width ,  self.hoursview.frame.size.height / 100 * 80)
        self.lblSecond.backgroundColor = UIColor.clearColor()
        
        self.lblsecondstxt.frame = CGRectMake(self.hoursview.frame.size.width / 100 * 36 , self.hoursview.frame.size.height / 100 * 72 , self.hoursview.frame.size.width , 10)
        self.lblsecondstxt.textAlignment = .Center
        
       
        
     
        
        
        
        
        
        self.btnChkInOut.frame = CGRectMake(self.view.frame.size.width / 100 * 35 ,  120 ,self.view.frame.size.width / 100 * 30, 30)
        
        
        self.btnChkInOut.backgroundColor = UIColor(hexString: "#4e8cbf")
        
        
        
        
        
        
        
        if (Mode == 1)
        {
          
            btnChkInOut.setTitle("Check Out", forState: .Normal)
         timer = NSTimer.scheduledTimerWithTimeInterval(1.0,
            target: self,
            selector: Selector("tick"),
            userInfo: nil,
            repeats: true)
            
        }
        else
        {
          btnChkInOut.setTitle("Check In", forState: .Normal)
          tick()
        }
    }
    
    func tick() {
        
        if let tempcal = self.defaults.stringForKey("ModeofCheckIn")
        {
            self.Mode = Int(tempcal)
            
        }
      
        if (Mode == 1)
        {
          
            CalculatedTime = CalculatedTime + 1
            
            self.defaults.setObject(CalculatedTime, forKey: "CalculatedTime")
            
            Second = CalculatedTime % 60
            
            if (((CalculatedTime - Second) / 60)>0)
            {
                Minute = ((CalculatedTime - Second) / 60) % 60
            }
            else
            {
                Minute = 0
            }
            
            if (((CalculatedTime - (CalculatedTime % 60)/60)/60)>0)
            {
                Hour = ((CalculatedTime - ((Minute * 60) + Second))/60) / 60
            }
            else
            {
                Hour = 0
            }
           
            if Hour<10
            {
                lblHour.text = "0" + String(Hour)
            }
            else
            {
                lblHour.text = String(Hour)
            }
            
            if Minute<10
            {
                lblMinute.text = "0" + String(Minute)
            }
            else
            {
                lblMinute.text = String(Minute)
            }
            
            if Second<10
            {
                lblSecond.text = "0" + String(Second)
            }
            else
            {
                lblSecond.text = String(Second)
            }
            
            
            
            
        }
        else
        {
            Second = CalculatedTime % 60
            
            if (((CalculatedTime - Second) / 60)>0)
            {
                Minute = ((CalculatedTime - Second) / 60) % 60
            }
            else
            {
                Minute = 0
            }
            
            if (((CalculatedTime - (CalculatedTime % 60)/60)/60)>0)
            {
                Hour = ((CalculatedTime - ((Minute * 60) + Second))/60) / 60
            }
            else
            {
                Hour = 0
            }
            
            if Hour<10
            {
                lblHour.text = "0" + String(Hour)
            }
            else
            {
                lblHour.text = String(Hour)
            }
            
            if Minute<10
            {
                lblMinute.text = "0" + String(Minute)
            }
            else
            {
                lblMinute.text = String(Minute)
            }
            
            if Second<10
            {
                lblSecond.text = "0" + String(Second)
            }
            else
            {
                lblSecond.text = String(Second)
            }
                self.timer.invalidate()
           
        }
       
        
    }
    
    
    @IBAction func btnCheckInOut_Click(sender: AnyObject) {
        if self.Mode == 1
        {
            
            SaveCheckInOutProcess(2)
             dispatch_async(dispatch_get_main_queue(),{
            self.timer.invalidate()
            self.btnChkInOut.setTitle("Check In", forState: .Normal)
            self.Mode = 2
            self.defaults.setObject(self.Mode, forKey: "ModeofCheckIn")
                self.tick()
             })
        }
        else
        {
            SaveCheckInOutProcess(1)
               dispatch_async(dispatch_get_main_queue(),{
          
            self.Mode = 1
            self.defaults.setObject(self.Mode, forKey: "ModeofCheckIn")
            self.btnChkInOut.setTitle("Check Out", forState: .Normal)
            
            self.timer = NSTimer.scheduledTimerWithTimeInterval(1.0,
                target: self,
                selector: Selector("tick"),
                userInfo: nil,
                repeats: true)
           
            })
            
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        if self.view.window == nil {
            self.timer.invalidate()
        }
    }
    
    
    func SaveCheckInOutProcess(TypeVal: Int)
    {
        

      locationManager.requestWhenInUseAuthorization()
        
     var currentLocation = CLLocation!()
        
      if( CLLocationManager.authorizationStatus() == CLAuthorizationStatus.AuthorizedWhenInUse ||
          CLLocationManager.authorizationStatus() == CLAuthorizationStatus.Authorized){
                
            currentLocation = locationManager.location
            
            objLRActivityIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            objLRActivityIndicaor.activityIndicatorViewStyle = .WhiteLarge
            let objPosition =  self.view.center
            //  objPosition.y =  objPosition.y.advancedBy(-40.0)
            objLRActivityIndicaor.center = objPosition
            //objActivityIndicaor.transform = CGAffineTransformMakeScale(1.5, 1.5);
            objLRActivityIndicaor.hidesWhenStopped = true
            objLRActivityIndicaor.color = UIColor.grayColor()
            self.view.addSubview(objLRActivityIndicaor)
            objLRActivityIndicaor.startAnimating()
            
            
            if let UserInfo = defaults.stringForKey("UserInfo")
            {
                let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
                objCallBack = AjaxCallBack()
                
                
                objCallBack.MethodName = "SaveWebCheckIn"
                
                var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
                objCallBack.ParameterList.append(objparam)
                
                
                objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
                objCallBack.ParameterList.append(objparam)
                
                objparam = Parameter(Name: "pType",value: String(TypeVal))
                objCallBack.ParameterList.append(objparam)
                
                objparam = Parameter(Name: "pXGLC",value: String(currentLocation.coordinate.longitude))
                objCallBack.ParameterList.append(objparam)
                
                objparam = Parameter(Name: "pYGLC",value: String(currentLocation.coordinate.latitude))
                objCallBack.ParameterList.append(objparam)
                
                
                objCallBack.post(SaveCheckInOutProcessOnSuccess, OnError: SaveCheckInOutProcessOnError)
                
                
                
            }
                
       }
        else
      {
        let alert = UIAlertController(title: "Required", message: "Allow GPS Location to track mobile CheckIn/Out." , preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        
        NSOperationQueue.mainQueue().addOperationWithBlock {
            self.presentViewController(alert, animated: true, completion: nil)
            
        }
        
      }
        
     
        
    }
    
    //Response on error callback
    func SaveCheckInOutProcessOnError (Response: NSError ){
        
        
    }
    
    //Respone on success callback
    func SaveCheckInOutProcessOnSuccess(ResultString :NSString? ) {
        
        dispatch_async(dispatch_get_main_queue(),{
            self.objLRActivityIndicaor.stopAnimating()
            DoneHUD.showInView(self.view, message: "Done")
        })
        
    }
}

