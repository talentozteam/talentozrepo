//
//  CheckBox.swift
//  checkbox
//
//  Created by kent on 9/27/14.
//  Copyright (c) 2014 kent. All rights reserved.
//
//Version 2.0 new features
//Added IBInspectable property so the checkbox can be turned on and off in interface builder.

import UIKit

class CheckBox: UIButton {
    
    //images
    let checkedImage = UIImage(named: "checked_checkbox")
    let unCheckedImage = UIImage(named: "unchecked_checkbox")
    var OnCompletion: ((sender:UIButton) -> Void )!
    internal var Checked : Bool!
    //bool propety
    @IBInspectable var isChecked:Bool = false{
        didSet{
            self.updateImage()
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonSetup()
    }
    
    func commonSetup() {
        self.addTarget(self, action: #selector(CheckBox.buttonClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        self.updateImage()
    }
    
    
    func updateImage() {
        if isChecked == true{
            self.setImage(checkedImage, forState: .Normal)
            self.Checked = true
        }else{
            self.setImage(unCheckedImage, forState: .Normal)
            self.Checked = false
        }

    }

    func buttonClicked(sender:UIButton) {
        if(sender == self){
            isChecked = !isChecked
            self.OnCompletion(sender: sender)
        }
    }

}
