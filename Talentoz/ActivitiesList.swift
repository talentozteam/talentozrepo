//
//  ActivitiesList.swift
//  Talentoz
//
//  Created by forziamac on 01/02/16.
//  Copyright © 2016 Forzia Tech Private Ltd. All rights reserved.
//

import UIKit

class ActivitiesListB : UITableView, UITableViewDataSource , UITableViewDelegate  {
    
    //# MARK: Public Variables
    internal var UserId : Int!
    internal var ActivityDate : String!
    internal var ShowDetailButtonVisible = false
    internal var ShowRaiseButtonVisible = false
    internal var MainView : UIView!
    internal var MainView1 : UIView!
    
    //# MARK: Local Variables
    var objCallBack : AjaxCallBack!
    let Viewframe = UIScreen.mainScreen().bounds
    let UserInfo = NSUserDefaults.standardUserDefaults()
    var Activities : Talentoz.JSON!
    var ActivityListView = UITableView()
    var objLoadingIndicaor : UIActivityIndicatorView!
    var OnCompletion: ((ActivityDate:String, Mode : Int) -> Void )!
    
    //# MARK: Custom Methods
    func GetActivitiesList()
    {
        objLoadingIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, Viewframe.width, Viewframe.height))
        
        objLoadingIndicaor.activityIndicatorViewStyle = .WhiteLarge
        let objPosition =  self.MainView.center
        objLoadingIndicaor.center = objPosition
        objLoadingIndicaor.hidesWhenStopped = true
        objLoadingIndicaor.color = UIColor.grayColor()
        self.MainView.addSubview(objLoadingIndicaor)
        objLoadingIndicaor.startAnimating()
        
        objCallBack = AjaxCallBack()
        objCallBack.MethodName = "GetBiometricDataonDate"
        
        if let UserInfo = UserInfo.stringForKey("UserInfo")
        {
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pMode",value: "0")
            objCallBack.ParameterList.append(objparam)
                
            objparam = Parameter(Name: "pDate",value: String(self.ActivityDate))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pUserID",value: String(self.UserId))
            objCallBack.ParameterList.append(objparam)
            
            
            objCallBack.post(GetActivitiesListOnComplete, OnError: GetActivitiesListOnErrorOnError)
            
        }
        
    }
    
    func GetActivitiesListOnErrorOnError(Response: NSError ){
        
    }
    
    func GetActivitiesListOnComplete (ResultString :NSString? ) {
        dispatch_async(dispatch_get_main_queue(),{
            
            self.objLoadingIndicaor.stopAnimating()
            self.Activities = JSON(data: self.objCallBack.GetJSONData(ResultString)!)//Serialiazation
            
            //Draw UI Controls
            
            let lbl_TitleHeader : UILabel = UILabel(frame: CGRectMake( 0  , 0, self.MainView.frame.size.width ,30))
            lbl_TitleHeader.backgroundColor = UIColor.clearColor()
            lbl_TitleHeader.textAlignment = .Center
            lbl_TitleHeader.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
            lbl_TitleHeader.textColor = UIColor(hexString: "#666666")
            lbl_TitleHeader.text = "Activities Report"
            self.MainView.addSubview(lbl_TitleHeader)
            
            let bottomborderview : UIView = UIView(frame: CGRectMake(self.MainView.frame.size.width / 100 * 2  , 32, self.MainView.frame.size.width / 100 * 96 ,1))
            bottomborderview.layer.borderWidth = 1
            bottomborderview.layer.borderColor = UIColor(hexString: "#e5e5e5")?.CGColor
            self.MainView.addSubview(bottomborderview)
            
            let lbl_Day : UILabel = UILabel(frame: CGRectMake(0 , 40 , self.MainView.frame.size.width, 20))
            lbl_Day.backgroundColor = UIColor.clearColor()
            lbl_Day.textAlignment = .Center
            lbl_Day.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
            lbl_Day.textColor = UIColor(hexString: "#666666")
            lbl_Day.text = self.ActivityDate
            self.MainView.addSubview(lbl_Day)
            
            let lbl_InTimeTitle : UILabel = UILabel(frame: CGRectMake(self.MainView.frame.size.width / 100 * 5  , 65 , self.MainView.frame.size.width / 100 * 30 ,24))
            lbl_InTimeTitle.backgroundColor = UIColor(hexString:"#2190ea")
            lbl_InTimeTitle.textAlignment = .Center
            lbl_InTimeTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
            lbl_InTimeTitle.textColor = UIColor.whiteColor()
            lbl_InTimeTitle.text = "In Time"
             self.MainView.addSubview(lbl_InTimeTitle)
            
            let lbl_OutTimeTitle : UILabel = UILabel(frame: CGRectMake(self.MainView.frame.size.width / 100 * 34 , 65 , self.MainView.frame.size.width / 100 * 30 ,24))
             lbl_OutTimeTitle.backgroundColor = UIColor(hexString:"#2190ea")
            lbl_OutTimeTitle.textAlignment = .Center
            lbl_OutTimeTitle.textColor = UIColor.whiteColor()
            lbl_OutTimeTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
            lbl_OutTimeTitle.text = "Out Time"
            self.MainView.addSubview(lbl_OutTimeTitle)
            
            let lbl_HoursTitle : UILabel = UILabel(frame: CGRectMake(self.MainView.frame.size.width / 100 * 63 , 65 , self.MainView.frame.size.width / 100 * 32 , 24))
             lbl_HoursTitle.backgroundColor = UIColor(hexString:"#2190ea")
            lbl_HoursTitle.textAlignment = .Center
            lbl_HoursTitle.textColor = UIColor.whiteColor()
            lbl_HoursTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
            lbl_HoursTitle.text = "Hours"
             self.MainView.addSubview(lbl_HoursTitle)
            
            // Calculate Height by Records count
            var CellHeight : CGFloat!
            if self.Activities.count > 0
            {
                if self.Activities.count > 10
                {
                    CellHeight = 20 * 10
                }
                else
                {
                 CellHeight = 20 * CGFloat(self.Activities.count + 1)
                }
            }
            else
            {
                 CellHeight = 20
            }
            
            self.ActivityListView = UITableView(frame: CGRectMake(self.MainView.frame.width / 100 * 5 , 95 , self.MainView.frame.width / 100 * 90 , CellHeight  ))
            self.ActivityListView.showsVerticalScrollIndicator = true
            self.ActivityListView.delegate = self
            self.ActivityListView .dataSource = self
            self.ActivityListView.rowHeight = 25
            self.ActivityListView.separatorColor = UIColor.whiteColor()
            self.ActivityListView.backgroundColor = UIColor.clearColor()
            self.MainView.addSubview(self.ActivityListView)
            
            
            let lbl_SumofHoursTitle : UILabel = UILabel(frame: CGRectMake(0, 120 + CellHeight, self.MainView.frame.size.width / 100 * 60 ,20))
            lbl_SumofHoursTitle.backgroundColor = UIColor.clearColor()
            lbl_SumofHoursTitle.textAlignment = .Right
            lbl_SumofHoursTitle.textColor = UIColor(hexString: "#666666")
            lbl_SumofHoursTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 10.0)
            lbl_SumofHoursTitle.text = "Total Hours"
            self.MainView.addSubview(lbl_SumofHoursTitle)
            
            
            let lbl_SummaryHours : UILabel = UILabel(frame: CGRectMake(self.MainView.frame.size.width / 100 * 63, 120 + CellHeight, self.MainView.frame.size.width / 100 * 30 ,20))
            lbl_SummaryHours.backgroundColor = UIColor.clearColor()
            lbl_SummaryHours.textAlignment = .Center
            lbl_SummaryHours.textColor = UIColor(hexString: "#666666")
            lbl_SummaryHours.font = UIFont(name:"HelveticaNeue-Bold", size: 10.0)
            if self.Activities.count > 0
            {
                if self.Activities[0]["Sum_of_TotalTime"] != nil
                {
                    lbl_SummaryHours.text = String(self.Activities[0]["Sum_of_TotalTime"])
                }
                else
                {
                     lbl_SummaryHours.text = "00:00:00"
                }
            
            }
            else
            {
                lbl_SummaryHours.text = "00:00:00"
            }
            self.MainView.addSubview(lbl_SummaryHours)
            
            if self.ShowRaiseButtonVisible == true
            {
                let btnViewAll : UIButton = UIButton(frame: CGRectMake(self.MainView.frame.size.width / 100 * 20 , 190 + CellHeight, self.MainView.frame.size.width / 100 * 60 ,24))
                btnViewAll.backgroundColor = UIColor(hexString: "#48c9b0")
                btnViewAll.layer.cornerRadius = 5
                btnViewAll.setTitle("test", forState: .Normal)
                  btnViewAll.titleLabel!.font = UIFont(name: "helvetica-bold", size: 12)
                if self.ShowDetailButtonVisible == true
                {
                     btnViewAll.setFAText(prefixText: "View Adjustment   ", icon: FAType.FAArrowCircleORight, postfixText: "  ", size: 12, forState: .Normal)
                    btnViewAll.tag = 1
                }
                else
                {
                     btnViewAll.setFAText(prefixText: "Raise Adjustment   ", icon: FAType.FAArrowCircleORight, postfixText: "  ", size: 12, forState: .Normal)
                    btnViewAll.tag = 0
                }
                btnViewAll.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                btnViewAll.addTarget(self, action: "CreateOrViewAdjustment:", forControlEvents: .TouchUpInside)
                btnViewAll.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Center
               
                self.MainView.addSubview(btnViewAll)
            }

        
        })
        
        
    }
    
    //Oncompletion of Create or View Adjustment
    func CreateOrViewAdjustment(sender : UIButton)
    {        
         self.OnCompletion(ActivityDate: self.ActivityDate , Mode: sender.tag)
    }
   
     //# MARK: Activies List View Methods
    func numberOfSectionsInTableView(ActivityListView: UITableView) -> Int {
        return 1
    }
    
      func tableView(ActivityListView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.Activities.count
        
    }
    
    func tableView(ActivityListView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
     
        let cell = UITableViewCell()
        
        //Alternate Color
            if indexPath.row % 2 == 0
            {
                cell.backgroundColor = UIColor.whiteColor()
            }
            else
            {
                cell.backgroundColor = UIColor(hexString: "#f5f5f5")
            }
        
            let lbl_InTime : UILabel = UILabel(frame: CGRectMake( self.ActivityListView.frame.size.width / 100 * 3 , 5, self.ActivityListView.frame.size.width / 100 * 30 ,20))
            lbl_InTime.backgroundColor = UIColor.clearColor()
            lbl_InTime.textAlignment = .Center
            lbl_InTime.font = UIFont(name:"HelveticaNeue", size: 10.0)
            lbl_InTime.textColor = UIColor(hexString: "#666666")
            if self.Activities[indexPath.row]["ShirtInTime"] != nil
            {
             lbl_InTime.text = String(self.Activities[indexPath.row]["ShirtInTime"])
            }
            cell.addSubview(lbl_InTime)
            
            let lbl_OutTime : UILabel = UILabel(frame: CGRectMake(self.ActivityListView.frame.size.width / 100 * 32, 5, self.ActivityListView.frame.size.width / 100 * 30 ,20))
            lbl_OutTime.backgroundColor = UIColor.clearColor()
            lbl_OutTime.textAlignment = .Center
            lbl_OutTime.textColor = UIColor(hexString: "#666666")
            lbl_OutTime.font = UIFont(name:"HelveticaNeue", size: 10.0)
            if self.Activities[indexPath.row]["ShirtOutTime"] != nil
            {
                lbl_OutTime.text = String(self.Activities[indexPath.row]["ShirtOutTime"])
            }
            cell.addSubview(lbl_OutTime)
            
        
            let lbl_Hours : UILabel = UILabel(frame: CGRectMake(self.ActivityListView.frame.size.width / 100 * 66, 5, self.ActivityListView.frame.size.width / 100 * 32 ,20))
            lbl_Hours.backgroundColor = UIColor.clearColor()
            lbl_Hours.textAlignment = .Center
            lbl_Hours.textColor = UIColor(hexString: "#666666")
            lbl_Hours.font = UIFont(name:"HelveticaNeue", size: 10.0)
            if self.Activities[indexPath.row]["TotalTime"] != nil
            {
                lbl_Hours.text = String(self.Activities[indexPath.row]["TotalTime"])
            }
            cell.addSubview(lbl_Hours)
        
        
        return cell
    }
    
    
}



 