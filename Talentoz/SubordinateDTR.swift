//
//  SubordinateDTR.swift
//  Talentoz
//
//  Created by forziamac on 01/02/16.
//  Copyright © 2016 Forzia Tech Private Ltd. All rights reserved.
//


import UIKit

class SubordinateDTRDetails: BaseTabBarVC, UITableViewDataSource ,UITableViewDelegate {
    
    
    internal var SelfEmpData : Talentoz.JSON!
    var CurrentDTRListData : Talentoz.JSON!
    internal var UserID : Int!
    var objCallBack = AjaxCallBack()
    let defaults = NSUserDefaults.standardUserDefaults()
    var DTRListView = UITableView()
    var objLoadingIndicaor : UIActivityIndicatorView!
    var MonthYear: String!
    var ContainerTitalView = UIView()
    var Month: Int!
    var Year: Int!
    
    internal var FilterView : UIView!
    internal var FilterBGLayer : UIView!
    var isFilterOpen = false
    
    override func viewDidLoad() {
        
        self.view.backgroundColor = UIColor.whiteColor()
        super.UINavigationBarTitle = "Time Record"
        super.viewDidLoad()
        
        let date = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Year,.Month], fromDate: date)
        
        self.Month = Int(components.month)
        self.Year = Int(components.year)
        
        GetDTRList()
        
 
        
        
        // Do any additional setup after loading the view.
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 
 
    
    func GetDTRList()
    {
        objLoadingIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, Viewframe.width, Viewframe.height))
        objLoadingIndicaor.activityIndicatorViewStyle = .WhiteLarge
        let objPosition =  self.view.center
        objLoadingIndicaor.center = objPosition
        objLoadingIndicaor.hidesWhenStopped = true
        objLoadingIndicaor.color = UIColor.grayColor()
        self.view.addSubview(objLoadingIndicaor)
        objLoadingIndicaor.startAnimating()
        
        objCallBack = AjaxCallBack()
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
             objCallBack.MethodName = "GetEmployeeDTR"
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
                
            objparam = Parameter(Name: "pMonth",value: String(self.Month))
            objCallBack.ParameterList.append(objparam)
                
            objparam = Parameter(Name: "pYear",value: String(self.Year))
            objCallBack.ParameterList.append(objparam)
          
            
            objparam = Parameter(Name: "pUserID",value: String(self.UserID))
            objCallBack.ParameterList.append(objparam)
            
            
            objCallBack.post(DTRListOnComplete, OnError: DTRListOnErrorOnError)
            
        }
        
    }
    //Bind Leave list table
    
    //Response on error callback
    func DTRListOnErrorOnError(Response: NSError ){
        
    }
    
    
    func DTRListOnComplete (ResultString :NSString? ) {
        dispatch_async(dispatch_get_main_queue(),{
            
            self.objLoadingIndicaor.stopAnimating()
         
            
                self.CurrentDTRListData = JSON(data: self.objCallBack.GetJSONData(ResultString)!)
            
                self.ConstructAdjList()
           
        })
        
        
        
    }
    
    
    func ConstructAdjList()
    {
        self.ContainerTitalView.removeFromSuperview()
        self.DTRListView.removeFromSuperview()
        
           let viewempdet : UIView = UIView(frame: CGRectMake(0, 60, self.view.frame.size.width, 70))
        self.view.addSubview(viewempdet)
        viewempdet.backgroundColor = UIColor(colorLiteralRed: 28/255, green: 138/255, blue: 255/255, alpha: 1)
        
        if let tmpDomainURL = self.defaults.stringForKey("DomainURL")
        {
            if  (self.SelfEmpData["EmployeePhoto"].type == Talentoz.Type.Null || self.SelfEmpData["EmployeePhoto"].string == ""  )
            {
                let img_EmpPic:  UIButton = UIButton(frame: CGRectMake(20, 10, 50, 50))
                img_EmpPic.layer.borderWidth = 1
                img_EmpPic.layer.masksToBounds = false
                img_EmpPic.layer.borderColor = UIColor.whiteColor().CGColor
                img_EmpPic.backgroundColor = UIColor.clearColor()
                img_EmpPic.layer.cornerRadius = img_EmpPic.frame.height/2
                img_EmpPic.clipsToBounds = true
                img_EmpPic.titleLabel!.font = UIFont(name: "helvetica-bold", size: 25)
                img_EmpPic.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                img_EmpPic.setTitle(String(self.SelfEmpData["EmployeeName"])[0], forState: UIControlState.Normal)
                viewempdet.addSubview(img_EmpPic)
            }
            else{
                let img_EmpPic:  UIImageView = UIImageView(frame: CGRectMake(20, 10, 50, 50))
                img_EmpPic.layer.borderWidth = 0.5
                img_EmpPic.layer.masksToBounds = false
                img_EmpPic.layer.borderColor = UIColor.lightGrayColor().CGColor
                img_EmpPic.backgroundColor = UIColor.whiteColor()
                img_EmpPic.layer.cornerRadius = img_EmpPic.frame.height/2
                img_EmpPic.clipsToBounds = true
                
                viewempdet.addSubview(img_EmpPic)
                
                if self.SelfEmpData["EmployeePhoto"] != nil
                {
                    let employeePic = "http://" + tmpDomainURL + "/Uploadedfiles/Client" + String(self.defaults.stringForKey("ClientID")!) + "/Image/" + String(self.SelfEmpData["EmployeePhoto"])
                    img_EmpPic.setImageWithUrl(NSURL(string: employeePic)!)
                }
            }
            
            
            
        }
        
        
        let lbl_EmployeeName : UILabel = UILabel(frame: CGRectMake(80, 20, (self.view.frame.size.width - 70) / 100 * 55 ,20))
        // lbl_EmployeeName.backgroundColor = UIColor.whiteColor()
        lbl_EmployeeName.textAlignment = .Left
        lbl_EmployeeName.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        lbl_EmployeeName.textColor = UIColor.whiteColor()
        lbl_EmployeeName.text =  String(self.SelfEmpData["EmployeeName"])
        viewempdet.addSubview(lbl_EmployeeName)
        
        let lbl_PositionName : UILabel = UILabel(frame: CGRectMake(80, 40, (self.view.frame.size.width - 70) / 100 * 55 ,18))
        // lbl_PositionName.backgroundColor = UIColor.whiteColor()
        lbl_PositionName.textAlignment = .Left
        lbl_PositionName.textColor = UIColor.whiteColor()
        lbl_PositionName.font = UIFont(name:"HelveticaNeue", size: 10.0)
        lbl_PositionName.text = String(self.SelfEmpData["PositionName"])
        viewempdet.addSubview(lbl_PositionName)
      
       self.DTRListView = UITableView(frame: CGRectMake(0, 210, self.view.frame.width, self.Viewframe.height  - 249 ))
       self.DTRListView.showsVerticalScrollIndicator = true
       self.DTRListView.delegate = self
       self.DTRListView .dataSource = self
       self.DTRListView.rowHeight = 50
       self.DTRListView.separatorColor = UIColor.whiteColor()
       self.DTRListView.backgroundColor = UIColor.clearColor()
       self.view.addSubview(self.DTRListView )
      
        
    }
    
    
    
    
    func numberOfSectionsInTableView(DTRListView: UITableView) -> Int {
        return 1
    }
    
    func tableView(DTRListView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.CurrentDTRListData.count
        
    }
    
    
    func tableView(DTRListView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // 3
        let cell = UITableViewCell()
        ///cell.frame = CGRectMake(0, 30, 55, 400)
        
        cell.backgroundColor = UIColor.whiteColor()
        //Employee image
        
        var strDate : String!
        
        
        
        if indexPath.row % 2 == 0
        {
            cell.backgroundColor = UIColor.whiteColor()
        }
        else
        {
            cell.backgroundColor = UIColor(hexString: "#f5f5f5")
        }
        
        strDate = String(self.CurrentDTRListData[indexPath.row]["ReportingDate"])
        var array = strDate.componentsSeparatedByString("T")
        strDate = array[0]
        array = strDate.componentsSeparatedByString("-")
        
        
        if indexPath.row == 0
        {
            self.MonthYear = GetMonthName(strDate) + "   " + String(array[0])
            self.ContainerTitalView = UIView(frame: CGRectMake(0, 130, self.view.frame.width , 65))
            
            self.ContainerTitalView.backgroundColor = UIColor(hexString: "#f5f5f5")
            self.view.addSubview(self.ContainerTitalView)
            
            
            let  btn_MonthYear : UIButton = UIButton(frame:CGRectMake(self.ContainerTitalView.frame.size.width / 100 * 4 , 10, self.ContainerTitalView.frame.size.width / 100 * 92 ,30))
            btn_MonthYear.setTitle( self.MonthYear , forState: .Normal)
            
            
            btn_MonthYear.backgroundColor = UIColor.whiteColor()
            btn_MonthYear.setTitleColor(UIColor(hexString: "#666666"),forState: UIControlState.Normal)
            btn_MonthYear.titleLabel!.font =  UIFont(name:"helvetica-Bold", size: 14)
            btn_MonthYear.addTarget(self, action: "openFilter:", forControlEvents: .TouchUpInside)
            self.ContainerTitalView.addSubview(btn_MonthYear)
            
            let btn_dropdownsymbol : UIButton = UIButton(frame: CGRectMake(self.ContainerTitalView.frame.size.width / 100 * 70 , 10 ,self.ContainerTitalView.frame.size.width * 0.05, 30))
            btn_dropdownsymbol.setFAIcon(FAType.FAAngleDown, iconSize: CGFloat(20),forState: .Normal)
            btn_dropdownsymbol.backgroundColor = UIColor.clearColor()
            btn_dropdownsymbol.setTitleColor(UIColor(hexString: "#666666"), forState: UIControlState.Normal)
            btn_dropdownsymbol.addTarget(self, action: "openFilter:", forControlEvents: .TouchUpInside)
            
            self.ContainerTitalView.addSubview(btn_dropdownsymbol)
            
            
            let lbl_DayTitle : UILabel = UILabel(frame: CGRectMake(0 , 45, self.view.frame.size.width / 100 * 23 ,30))
            lbl_DayTitle.backgroundColor = UIColor(hexString: "#eeeeee")
            lbl_DayTitle.textAlignment = .Center
            
            lbl_DayTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
            lbl_DayTitle.text = "Day"
            lbl_DayTitle.textColor = UIColor(hexString: "#333333")
            self.ContainerTitalView.addSubview(lbl_DayTitle)
            
            
            let lbl_InTimeTitle : UILabel = UILabel(frame: CGRectMake(self.view.frame.size.width / 100 * 22 , 45, self.view.frame.size.width / 100 * 25 ,30))
            lbl_InTimeTitle.backgroundColor = UIColor(hexString: "#eeeeee")
            lbl_InTimeTitle.textAlignment = .Center
            lbl_InTimeTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
            lbl_InTimeTitle.text = "In Time"
            lbl_InTimeTitle.textColor = UIColor(hexString: "#333333")
            self.ContainerTitalView.addSubview(lbl_InTimeTitle)
            
            let lbl_OutTimeTitle : UILabel = UILabel(frame: CGRectMake(self.view.frame.size.width / 100 * 47 , 45, self.view.frame.size.width / 100 * 25 ,30))
            lbl_OutTimeTitle.backgroundColor = UIColor(hexString: "#eeeeee")
            lbl_OutTimeTitle.textAlignment = .Center
            lbl_OutTimeTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
            lbl_OutTimeTitle.text = "Out Time"
            self.ContainerTitalView.addSubview(lbl_OutTimeTitle)
            lbl_OutTimeTitle.textColor = UIColor(hexString: "#333333")
            
            let lbl_TotalHrsTitle : UILabel = UILabel(frame: CGRectMake(self.view.frame.size.width / 100 * 72 , 45, self.view.frame.size.width / 100 * 25 ,30))
            lbl_TotalHrsTitle.backgroundColor = UIColor(hexString: "#eeeeee")
            lbl_TotalHrsTitle.textAlignment = .Center
            lbl_TotalHrsTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
            lbl_TotalHrsTitle.text = "Total Hours"
            lbl_TotalHrsTitle.textColor = UIColor(hexString: "#333333")
            self.ContainerTitalView.addSubview(lbl_TotalHrsTitle)
        }
        
        let lbl_Month : UILabel = UILabel(frame: CGRectMake(0 , 5, self.view.frame.size.width / 100 * 23 ,20))
        lbl_Month.backgroundColor = UIColor.clearColor()
        lbl_Month.textAlignment = .Center
        lbl_Month.font = UIFont(name:"HelveticaNeue", size: 20.0)
        lbl_Month.textColor = UIColor(hexString: "#666666")
        lbl_Month.text = String(array[2])
        cell.addSubview(lbl_Month)
        
        let lbl_Day : UILabel = UILabel(frame: CGRectMake(0, 27, self.view.frame.size.width / 100 * 23 ,20))
        lbl_Day.backgroundColor = UIColor.clearColor()
        lbl_Day.textAlignment = .Center
        lbl_Day.textColor = UIColor(hexString: "#666666")
        lbl_Day.font = UIFont(name:"HelveticaNeue", size: 12.0)
        lbl_Day.text = String(self.GetWeekDayName(strDate))
        cell.addSubview(lbl_Day)
        
        let lbl_InTime : UILabel = UILabel(frame: CGRectMake(self.view.frame.size.width / 100 * 22 , 15, self.view.frame.size.width / 100 * 25 ,20))
        lbl_InTime.backgroundColor = UIColor.clearColor()
        lbl_InTime.textAlignment = .Center
        
        lbl_InTime.font = UIFont(name:"HelveticaNeue", size: 12.0)
        
     
            lbl_InTime.textColor = UIColor.blackColor()
       
        
        
        if self.CurrentDTRListData[indexPath.row]["InTime"] != nil
        {
            lbl_InTime.text = String(self.CurrentDTRListData[indexPath.row]["InTime"])
        }
        cell.addSubview(lbl_InTime)
        
        let lbl_OutTime : UILabel = UILabel(frame: CGRectMake(self.view.frame.size.width / 100 * 47 , 15, self.view.frame.size.width / 100 * 25 ,20))
        lbl_OutTime.backgroundColor = UIColor.clearColor()
        lbl_OutTime.textAlignment = .Center
        
            lbl_OutTime.textColor = UIColor.blackColor()
        
        lbl_OutTime.font = UIFont(name:"HelveticaNeue", size: 12.0)
        if self.CurrentDTRListData[indexPath.row]["OutTime"] != nil
        {
            lbl_OutTime.text = String(self.CurrentDTRListData[indexPath.row]["OutTime"])
        }
        
        cell.addSubview(lbl_OutTime)
        
        let lbl_TotalHrs : UILabel = UILabel(frame: CGRectMake(self.view.frame.size.width / 100 * 72 , 15, self.view.frame.size.width / 100 * 25 ,20))
        lbl_TotalHrs.backgroundColor = UIColor.clearColor()
        lbl_TotalHrs.textAlignment = .Center
       
        lbl_TotalHrs.font = UIFont(name:"HelveticaNeue", size: 12.0)
        if String(self.CurrentDTRListData[indexPath.row]["TotalHours"]) != "00:00"
        {
            lbl_TotalHrs.text = String(self.CurrentDTRListData[indexPath.row]["TotalHours"])
        }
        
        
        let GestureLeaveDetail = UITapGestureRecognizer(target: self, action: "ShowActivitiesDetail:")
        cell.addGestureRecognizer(GestureLeaveDetail)
        
        
        cell.addSubview(lbl_TotalHrs)
        
        // Start
        if (self.CurrentDTRListData[indexPath.row]["InTime"] != nil ){
            
            if(self.CurrentDTRListData[indexPath.row]["Tardiness"] != nil)
            {
                
                if (self.CurrentDTRListData[indexPath.row]["HolidayID"]  < 4 && self.CurrentDTRListData[indexPath.row]["LeaveID"] == nil || self.CurrentDTRListData[indexPath.row]["LeaveID"] != nil && self.CurrentDTRListData[indexPath.row]["LeaveStatus"] != "2")   {
                    if self.CurrentDTRListData[indexPath.row]["Tardiness"] != nil{
                        if (Int(self.CurrentDTRListData[indexPath.row]["Tardiness"].int!) > 0) {
                            lbl_InTime.textColor = UIColor(hexString: "#cc0000")
                        }
                    }
                    
                }
            }
            else
            {
                lbl_InTime.textColor = UIColor(hexString: "#000000")
                
            }
        }
        else
        {
            lbl_InTime.text = "-"
            lbl_InTime.textColor = UIColor(hexString: "#000000")
            
            if(self.CurrentDTRListData[indexPath.row]["OutTime"] == nil)
            {
                if(self.CurrentDTRListData[indexPath.row]["HolidayID"] != nil)
                {
                    if(Int(self.CurrentDTRListData[indexPath.row]["HolidayID"].int!) > 3)
                    {
                        lbl_InTime.text =  self.CurrentDTRListData[indexPath.row]["HolidayDesc"].string
                        lbl_InTime.textAlignment = .Left
                        lbl_InTime.frame = CGRectMake(self.view.frame.size.width / 100 * 30 , 15, self.view.frame.size.width / 100 * 75 ,20)
                        
                        
                    }
                    else
                    {
                        if(self.CurrentDTRListData[indexPath.row]["LeaveID"] != nil)
                        {
                            
                            if(Int(self.CurrentDTRListData[indexPath.row]["LeaveID"].int!) > 0)
                            {
                                lbl_InTime.text = self.CurrentDTRListData[indexPath.row]["LeaveDesc"].string
                                lbl_InTime.textAlignment = .Left
                                lbl_InTime.frame = CGRectMake(self.view.frame.size.width / 100 * 30 , 15, self.view.frame.size.width / 100 * 75 ,20)
                            }
                        }
                        else
                        {
                            if(self.CurrentDTRListData[indexPath.row]["AbsentText"] == "1")
                            {
                                
                                lbl_InTime.text =  "Absent"
                                lbl_InTime.textColor = UIColor(hexString: "#cc0000")
                                lbl_InTime.textAlignment = .Left
                                lbl_InTime.frame = CGRectMake(self.view.frame.size.width / 100 * 30 , 15, self.view.frame.size.width / 100 * 75 ,20)
                            }
                        }
                    }
                }
                    
                else if(self.CurrentDTRListData[indexPath.row]["LeaveID"] != nil)
                {
                    if(Int(self.CurrentDTRListData[indexPath.row]["LeaveID"].int!)>0)
                    {
                        lbl_InTime.text = self.CurrentDTRListData[indexPath.row]["LeaveDesc"].string
                        lbl_InTime.textAlignment = .Left
                        lbl_InTime.frame = CGRectMake(self.view.frame.size.width / 100 * 30 , 15, self.view.frame.size.width / 100 * 75 ,20)
                    }
                }
                    
                else
                {
                    
                    if(self.CurrentDTRListData[indexPath.row]["AbsentText"] == "1")
                    {
                        
                        lbl_InTime.text =  "Absent"
                        lbl_InTime.textColor = UIColor(hexString: "#cc0000")
                        lbl_InTime.textAlignment = .Left
                        lbl_InTime.frame = CGRectMake(self.view.frame.size.width / 100 * 30 , 15, self.view.frame.size.width / 100 * 75 ,20)
                    }
                    
                }
                
            }
            
        }
        
        
        if(self.CurrentDTRListData[indexPath.row]["OutTime"] != nil) {
            lbl_OutTime.text =  self.CurrentDTRListData[indexPath.row]["OutTime"].string
            if (self.CurrentDTRListData[indexPath.row]["PeakHoursUnderTime"] != nil) {
                if (self.CurrentDTRListData[indexPath.row]["HolidayID"] < 4 && (self.CurrentDTRListData[indexPath.row]["LeaveID"] == nil || (self.CurrentDTRListData[indexPath.row]["LeaveID"] != nil && self.CurrentDTRListData[indexPath.row]["LeaveStatus"] != "2"))) {
                    
                    if (Int(self.CurrentDTRListData[indexPath.row]["PeakHoursUnderTime"].int!) > 0) {
                        lbl_OutTime.textColor = UIColor(hexString: "#cc0000")
                    }
                }
                
            }
        }
        else
        {
            if(self.CurrentDTRListData[indexPath.row]["OutTime"] == nil){
                
            }
            else
            {
                lbl_OutTime.text =  "-"
            }
            
            
            lbl_OutTime.textColor = UIColor(hexString: "#000000")
        }
        
        if(self.CurrentDTRListData[indexPath.row]["TotalHours"].string == "00:00")
        {
            
            if(self.CurrentDTRListData[indexPath.row]["InTime"] == nil && self.CurrentDTRListData[indexPath.row]["OutTime"] == nil ){
                
            }
            else
            {
                lbl_TotalHrs.text =  "-"
                
            }
            
            
        }else
        {
            if (self.CurrentDTRListData[indexPath.row]["UnderTime"] != nil) {
                if (self.CurrentDTRListData[indexPath.row]["HolidayID"] < 4  && (self.CurrentDTRListData[indexPath.row]["LeaveID"] == nil || (self.CurrentDTRListData[indexPath.row]["LeaveID"] != nil && self.CurrentDTRListData[indexPath.row]["LeaveStatus"] != "2"))) {
                    
                    if (Int(self.CurrentDTRListData[indexPath.row]["UnderTime"].int!) > 0) {
                        
                        lbl_TotalHrs.textColor = UIColor(hexString: "#cc0000")
                    }
                }
                
            }
            lbl_TotalHrs.text = self.CurrentDTRListData[indexPath.row]["TotalHours"].string
        }
        
        
 
        
        
        return cell
    }
    
    
    
    
    func DismissFilter(sender: UIButton)
    {
        isFilterOpen = false
        FilterBGLayer.removeFromSuperview()
        
    }
    
   
    
    
    
    func openFilter(sender: UIButton)
    {
        if isFilterOpen
        {
            isFilterOpen = false
            FilterBGLayer.removeFromSuperview()
        }
        else{
            
            isFilterOpen = true
            FilterBGLayer = UIView(frame: CGRectMake(0, 0, Viewframe.width  ,  Viewframe.height))
            FilterBGLayer.backgroundColor = UIColor(colorLiteralRed: 0 , green: 0, blue: 0, alpha: 0.6)
            let GestureLeaveDetail1 = UITapGestureRecognizer(target: self, action: "DismissFilter:")
            FilterBGLayer.addGestureRecognizer(GestureLeaveDetail1)
            FilterView = UIView(frame: CGRectMake(FilterBGLayer.frame.width / 100 * 10 ,100 , FilterBGLayer.frame.width  / 100 * 80, 150))
            FilterView.backgroundColor = UIColor.whiteColor()
            FilterView.layer.cornerRadius = 8
            
            let expiryDatePicker = MonthYearPickerView(frame: CGRectMake(0, 20, FilterView.frame.width   ,  80))
            let year = NSCalendar(identifier: NSCalendarIdentifierGregorian)!.component(.Year, fromDate: NSDate())
            expiryDatePicker.FromYear = year - 5
            expiryDatePicker.LengthOfYear = 7
            expiryDatePicker.commonSetup()
            expiryDatePicker.tintColor = UIColor.yellowColor()
            
            expiryDatePicker.onDateSelected = { (month,year) in
                self.MonthYear = String(expiryDatePicker.months[month - 1 ]) + " " + String(year)
                self.Month = month
                self.Year = year
                
                
            }
            FilterView.addSubview(expiryDatePicker)
            
            
            let btn_ApplyFilter : UIButton = UIButton(frame: CGRectMake(FilterBGLayer.frame.width / 100 * 32, 110, 50 ,20))
            btn_ApplyFilter.setTitle("Go", forState: .Normal)
            btn_ApplyFilter.backgroundColor = UIColor(hexString: "#2190ea")
            btn_ApplyFilter.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            btn_ApplyFilter.titleLabel!.font =  UIFont(name:"helvetica", size: 12)
            btn_ApplyFilter.addTarget(self, action: "ApplyFilter:", forControlEvents: .TouchUpInside)
            FilterView.addSubview(btn_ApplyFilter)
            
            
            FilterBGLayer.addSubview(FilterView)
            
            
            self.view.addSubview(FilterBGLayer)
        }
        
        
    }
    
    
    
    
    func ApplyFilter(sender: UIButton) {
        
        
        
        isFilterOpen = false
        FilterBGLayer.removeFromSuperview()
        GetDTRList()
    }
   
    
    func ShowActivitiesDetail(sender: UITapGestureRecognizer)
    {
        
        FilterBGLayer = UIView(frame: CGRectMake(0, 0, Viewframe.width  ,  Viewframe.height))
        FilterBGLayer.backgroundColor = UIColor(colorLiteralRed: 0 , green: 0, blue: 0, alpha: 0.6)
        let GestureLeaveDetail1 = UITapGestureRecognizer(target: self, action: "DismissFilter:")
        FilterBGLayer.addGestureRecognizer(GestureLeaveDetail1)
        FilterView = UIView(frame: CGRectMake(FilterBGLayer.frame.width / 100 * 10 , FilterBGLayer.frame.height  / 100 * 10 , FilterBGLayer.frame.width  / 100 * 80, FilterBGLayer.frame.height  / 100 * 60))
        FilterView.backgroundColor = UIColor.whiteColor()
        FilterView.layer.cornerRadius = 8
        
        let tapLocation = sender.locationInView(self.DTRListView)
        
        let indexPath = self.DTRListView.indexPathForRowAtPoint(tapLocation)
        
        let  objActivities = ActivitiesListB()
        var strDate = String(self.CurrentDTRListData[(indexPath?.row)!]["ReportingDate"])
        var array = strDate.componentsSeparatedByString("T")
        strDate = array[0]
        array = strDate.componentsSeparatedByString("-")   
        objActivities.UserId = self.UserID
        objActivities.ActivityDate = array[2] + "/" + array[1] + "/" + array[0]
        objActivities.MainView = self.FilterView
        objActivities.MainView1 = self.FilterBGLayer
        objActivities.GetActivitiesList()
        
        FilterView.addSubview(objActivities)
        FilterBGLayer.addSubview(FilterView)
        self.view.addSubview(FilterBGLayer)
        
    }
    
  
    
    func GetWeekDayName(DateStr: String) ->String
    {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = NSTimeZone.localTimeZone()
        
        // convert string into date
        let dateValue = dateFormatter.dateFromString(DateStr) as NSDate!
        
        
        dateFormatter.dateFormat = "EEE"
        let dayOfWeekString = dateFormatter.stringFromDate(dateValue)
        
        
        return String(dayOfWeekString)
        
    }
    
    func GetMonthName(DateStr: String) ->String
    {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = NSTimeZone.localTimeZone()
        
        // convert string into date
        let dateValue = dateFormatter.dateFromString(DateStr) as NSDate!
        
        
        dateFormatter.dateFormat = "MMMM"
        let MonthNameString = dateFormatter.stringFromDate(dateValue)
        
        
        return String(MonthNameString)
        
    }
    
    
    
    
    
    
    
}



