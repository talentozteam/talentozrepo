//
//  ApplyLeave.swift
//  Talentoz
//
//  Created by forziamac on 22/12/15.
//  Copyright © 2015 Forzia Tech Private Ltd. All rights reserved.
//

import UIKit


class ApplyLeave: BaseTabBarVC ,UIPickerViewDataSource,UIPickerViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {

    var objCallBack = AjaxCallBack()
     var objCallBack1 = AjaxCallBack()
    var SelectedTimeBtn: UIButton!
    let defaults = NSUserDefaults.standardUserDefaults()
    var TempJson: Talentoz.JSON!
    var FromDate: NSDate? = nil
    var ToDate: NSDate? = nil
    var iLeaveType: Int = -1
     var iDeductFrom: Int = 1
    var iDayZone: Int!
    var iWorkFlowID: Int!
    var AttachFileActualName: String! = ""
    var AttachFileGenerateName: String!  = ""
    var LeaveTypeSelectIndex:Int = -1
    var LeaveType = [String]()
    internal var FilterView : UIView!
    internal var FilterBGLayer : UIView!
    var isFilterOpen = false
    
    var imagePicker: UIImagePickerController!
    

    @IBOutlet var imgOutlet: UIImageView!
    
    //Label outlets 
     var objActivityIndicaor : UIActivityIndicatorView!
    let dropDown = DropDown()
    let dropDown1 = DropDown()
    
    @IBOutlet var fromdatelbl: UILabel!
    var activeField: UITextField?
    
    @IBOutlet var todatelbl: UILabel!
    @IBOutlet var LeaveTypeOutlet: UIButton!
    
    @IBOutlet var dtToDate: UIButton!
    @IBOutlet var ShowHalfDayType: UIButton!
    
    @IBOutlet var lblHalfDay: UILabel!
    @IBOutlet var txtReason: UITextField!
    @IBOutlet var btnSubmit: UIButton!
    
    @IBOutlet var dtFromDate: UIButton!
   
    @IBOutlet var dropdownHT: UIButton!
    //Label outlets

    @IBOutlet var HalfDaySwitch: UISwitch!
   
    @IBOutlet var lblBalance: UILabel!
   
    @IBOutlet var DeductFromVw: UIView!
    @IBOutlet var DeductFromlbl: UILabel!
 
    @IBOutlet var deduct_types: UISegmentedControl!
    
    @IBOutlet var deductseg: UISegmentedControl!
  
    @IBOutlet var dropdownsymbol: UIButton!
    
    var pickerptions = ["Forenoon","Afternoon"]
    
    @IBAction func HalfDayClick(sender: AnyObject) {
        if HalfDaySwitch.on
        {
            ShowHalfDayType.hidden = false
            self.dropdownHT.hidden = false
            
            
        }else{
            ShowHalfDayType.hidden = true
            self.dropdownHT.hidden = true
            
        }
        
    }
     @IBAction func dropdownclick(sender: AnyObject) {
        if dropDown.hidden {
            dropDown.show()
        } else {
            dropDown.hide()
        }
    }
    @IBAction func ShowDismissLT(sender: AnyObject) {
        if dropDown.hidden {
            dropDown.show()
        } else {
            dropDown.hide()
        }
    }
    
    @IBAction func ShowDismissHT(sender: AnyObject) {
        if dropDown1.hidden {
            dropDown1.show()
        } else {
            dropDown1.hide()
        }
    }
 
 
    
    @IBAction func dropDownHTclick(sender: AnyObject) {
        if dropDown1.hidden {
            dropDown1.show()
        } else {
            dropDown1.hide()
        }
    }
    
    
    
    func DesignUI()
    {
        
        
     
    
        // self.HalfDaySwitch.transform = CGAffineTransformMakeScale(1.5, 0.8)
        
        self.LeaveTypeOutlet.layer.borderColor = UIColor(red:222/255.0, green:225/255.0, blue:227/255.0, alpha: 1.0).CGColor
        self.LeaveTypeOutlet.layer.borderWidth = 1.0
        self.LeaveTypeOutlet.frame = CGRectMake(self.view.frame.size.width / 100 * 3 , 65  ,self.view.frame.size.width * 0.94, 35)
        self.LeaveTypeOutlet.layer.cornerRadius = 7
        
        DeductFromlbl.textColor = UIColor(hexString: "#666666")
        DeductFromlbl.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        
        
         self.DeductFromVw.frame = CGRectMake(self.view.frame.size.width / 100 * 3 , self.view.frame.size.height / 100 * 23  ,self.view.frame.size.width , 30)
        
       
        
        
        
        
        
        self.dropdownsymbol.frame = CGRectMake(self.view.frame.size.width / 100 * 89 , 65 ,self.view.frame.size.width * 0.05, 35)
        self.dropdownsymbol.setFAIcon(FAType.FAAngleDown, iconSize: CGFloat(25),forState: .Normal)
        
        
        
       
        
        
        self.lblBalance.frame = CGRectMake(self.view.frame.size.width / 100 * 3 , self.view.frame.size.height / 100 * 28  ,self.view.frame.size.width, 45)
        
        lblBalance.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        
        self.fromdatelbl.frame = CGRectMake(self.view.frame.size.width / 100 * 3 , self.view.frame.size.height / 100 * 35  ,self.view.frame.size.width / 100 * 35, 35)
        fromdatelbl.backgroundColor = UIColor.clearColor()
        //fromdatelbl.textAlignment = .left
        fromdatelbl.textColor = UIColor(hexString: "#666666")
        fromdatelbl.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        
        dtFromDate.addTarget(self, action: "ShowDatePicker:", forControlEvents: .TouchUpInside)
        dtFromDate.tag = 1
        self.dtFromDate.frame = CGRectMake(self.view.frame.size.width / 100 * 40 , self.view.frame.size.height / 100 * 35  ,self.view.frame.size.width / 100 * 50, 35)
        dtFromDate.backgroundColor = UIColor.clearColor()
         
        
       self.todatelbl.frame = CGRectMake(self.view.frame.size.width / 100 * 3 , self.view.frame.size.height / 100 * 41  ,self.view.frame.size.width / 100 * 35, 35)
        todatelbl.backgroundColor = UIColor.clearColor()
        
        todatelbl.textColor = UIColor(hexString: "#666666")
        todatelbl.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        
        
        
        dtToDate.addTarget(self, action: "ShowDatePicker:", forControlEvents: .TouchUpInside)
        self.dtToDate.frame = CGRectMake(self.view.frame.size.width / 100 * 40 , self.view.frame.size.height / 100 * 41  ,self.view.frame.size.width / 100 * 50, 35)
        dtToDate.tag = 2
      
        
        self.lblHalfDay.frame = CGRectMake(self.view.frame.size.width / 100 * 25 , self.view.frame.size.height / 100 * 50  ,self.view.frame.size.width / 100 * 60, 45)
        self.HalfDaySwitch.frame = CGRectMake(self.view.frame.size.width / 100 * 3 , self.view.frame.size.height / 100 * 51  ,self.view.frame.size.width / 100 * 25, 25)
        
        self.ShowHalfDayType.layer.borderColor = UIColor(red:222/255.0, green:225/255.0, blue:227/255.0, alpha: 1.0).CGColor
        self.ShowHalfDayType.layer.borderWidth = 1.0
        self.ShowHalfDayType.frame = CGRectMake(self.view.frame.size.width / 100 * 50 , self.view.frame.size.height / 100 * 51  ,self.view.frame.size.width / 100 * 40, 30)
        self.ShowHalfDayType.layer.cornerRadius = 7
        
        
        self.dropdownHT.frame = CGRectMake(self.view.frame.size.width / 100 * 82 , self.view.frame.size.height / 100 * 51  ,self.view.frame.size.width * 0.05, 30)
        //self.dropdownHT.layer.cornerRadius = 7
        self.dropdownHT.setFAIcon(FAType.FAAngleDown, iconSize: CGFloat(25),forState: .Normal)


        
        self.txtReason.frame = CGRectMake(self.view.frame.size.width / 100 * 3 , self.view.frame.size.height / 100 * 60  ,self.view.frame.size.width/100 * 94, 35)
        txtReason.textColor = UIColor(hexString: "#666666")
        
        let attachlbl : UILabel = UILabel (frame: CGRectMake(self.view.frame.size.width / 100 * 3 , self.view.frame.size.height / 100 * 70  ,self.view.frame.size.width/100 * 25, 20))
        attachlbl.text = "Attachment : "
        attachlbl.textColor = UIColor(hexString: "#666666")
        attachlbl.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        self.view.addSubview(attachlbl)
        
        let btnAttach : UIButton = UIButton (frame: CGRectMake(self.view.frame.size.width / 100 * 25 , self.view.frame.size.height / 100 * 70  ,self.view.frame.size.width/100 * 20, 20))
        btnAttach.setTitle("Attach", forState: .Normal)
        btnAttach.setFAIcon(FAType.FAPaperclip, iconSize: 16, forState: .Normal)
        btnAttach.addTarget(self, action: "AttachFile:", forControlEvents: .TouchUpInside)
        btnAttach.backgroundColor = UIColor.clearColor()
        btnAttach.setTitleColor(UIColor(hexString: "#333333"), forState: .Normal)
        self.view.addSubview(btnAttach)
        
        imgOutlet.frame = CGRectMake(self.view.frame.size.width / 100 * 45 , self.view.frame.size.height / 100 * 70  ,30, 30)
        imgOutlet.layer.borderWidth = 2
        imgOutlet.layer.borderColor = UIColor(hexString: "#cccccc")?.CGColor
        
        
        self.btnSubmit.frame = CGRectMake(self.view.frame.size.width / 100 * 3 , self.view.frame.size.height / 100 * 80  ,self.view.frame.size.width/100 * 94, 30)
        self.btnSubmit.layer.borderWidth = 0.5
        self.btnSubmit.layer.cornerRadius = 5
        self.txtReason.delegate = self
        
         deductseg!.addTarget(self, action: "changeOption:", forControlEvents: .ValueChanged)
        
    }
    
    func changeOption(sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
             self.lblBalance.text = "Leave Balance " + ": " +  String(self.TempJson[self.LeaveTypeSelectIndex]["MainAccountLeaveBalance"])
             self.iDeductFrom = 1
            break;
        case 1:
              self.lblBalance.text = "Leave Balance " + ": " +  String(self.TempJson[self.LeaveTypeSelectIndex]["CarryForwardAccountLeaveBalance"])
              self.iDeductFrom = 2
            break;
        default:
            break;
        }
    }
  
    
    func AttachFile(sender:UIButton)
    {
        ShowAttachmentOption()
    }
    
    func ShowAttachmentOption()
    {
        
        
        FilterBGLayer = UIView(frame: CGRectMake(0, 0, Viewframe.width  ,  Viewframe.height))
        FilterBGLayer.backgroundColor = UIColor(colorLiteralRed: 0 , green: 0, blue: 0, alpha: 0.6)
        let GestureLeaveDetail1 = UITapGestureRecognizer(target: self, action: "DismissFilter:")
        FilterBGLayer.addGestureRecognizer(GestureLeaveDetail1)
        FilterView = UIView(frame: CGRectMake(FilterBGLayer.frame.width / 100 * 10 , FilterBGLayer.frame.height  / 100 * 25 , FilterBGLayer.frame.width  / 100 * 80, FilterBGLayer.frame.height  / 100 * 40))
        FilterView.backgroundColor = UIColor.whiteColor()
        FilterView.layer.cornerRadius = 8
        
        let btnCapture : UIButton = UIButton (frame: CGRectMake(FilterView.frame.width / 100 * 10 ,  70 , FilterView.frame.width / 100 * 80, 30))
        btnCapture.setTitle("Take a Photo", forState: .Normal)
        btnCapture.addTarget(self, action: "OpenCamera:", forControlEvents: .TouchUpInside)
        btnCapture.backgroundColor = UIColor(hexString: "#237ec9")
        btnCapture.layer.cornerRadius = 5
        btnCapture.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        btnCapture.setFAText(prefixText: "", icon: FAType.FACamera, postfixText:"  Take a Photo", size: 12, forState: .Normal)
        FilterView.addSubview(btnCapture)
        
        let btnBrowseFolder : UIButton = UIButton (frame: CGRectMake(FilterView.frame.width / 100 * 10 ,  115 , FilterView.frame.width / 100 * 80, 30))
        btnBrowseFolder.setTitle("Choose From Gallery", forState: .Normal)
        btnBrowseFolder.addTarget(self, action: "BrowseFolder:", forControlEvents: .TouchUpInside)
        btnBrowseFolder.backgroundColor = UIColor(hexString: "#237ec9")
        btnBrowseFolder.setFAText(prefixText: "", icon: FAType.FAFolder, postfixText:"  Choose From Gallery", size: 12, forState: .Normal)
         btnBrowseFolder.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        btnBrowseFolder.layer.cornerRadius = 5
        FilterView.addSubview(btnBrowseFolder)
        
        FilterBGLayer.addSubview(FilterView)
        self.view.addSubview(FilterBGLayer)
        
    }
    
    func OpenCamera(sender:UIButton)
    {
        
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
            imagePicker.allowsEditing = false
            
            self.presentViewController(imagePicker, animated: true, completion: nil)

        }
        
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        imgOutlet.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        imgOutlet.hidden = false
        
        
        if picker.sourceType == UIImagePickerControllerSourceType.PhotoLibrary  {
            let imageURL = info[UIImagePickerControllerReferenceURL] as! NSURL
            self.AttachFileActualName = String(imageURL.path!)
        }
        else
        {
            self.AttachFileActualName = "Captured"
        }
       
        
      
        
        isFilterOpen = false
        FilterBGLayer.removeFromSuperview()
        
        Fileupload()
        
     
    }
  
    func BrowseFolder(sender:UIButton)
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
            
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            imagePicker.allowsEditing = false
            imagePicker.navigationBar.barTintColor  =  UIColor(colorLiteralRed: 64/255, green: 156/255, blue: 255/255, alpha: 1)
            imagePicker.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
            imagePicker.allowsEditing = true
            
            self.presentViewController(imagePicker, animated: true, completion: nil)
            
        }
    }
    
    
    
 
 
    
    //-Text field move up when hidden by KEYBOARD//
    
    func textFieldDidBeginEditing(textField: UITextField) {
        animateViewMoving(true, moveValue: 100)
    }
    func textFieldDidEndEditing(textField: UITextField) {
        animateViewMoving(false, moveValue: 100)
    }
    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:NSTimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = CGRectOffset(self.view.frame, 0,  movement)
        UIView.commitAnimations()
    }
    

    
  
    
    @IBAction func Submit_Click(sender: AnyObject) {
   
        self.btnSubmit.enabled = false
        SaveLeave()
        
    }
    
    
    func CreateLeaveTypeArray()
    {
        for var i = 0; i < self.TempJson.count ; ++i {
            self.LeaveType.append(String(self.TempJson[i]["LeaveType"]))
        }
    }
    
    func Fileupload()
    {
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            
            
            
            objCallBack1 = AjaxCallBack()
            
            if let unwrappedImage = imgOutlet.image {
                let data = imgOutlet.image!.lowQualityJPEGNSData
                
                let ResultJSON = objCallBack1.DeserializeJSONString(UserInfo)
                objCallBack1.MethodName = "FileUploadForLeave"
                
                let base64String = data.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
                
                var objparam = Parameter(Name: "stream",value: String(base64String))
                objCallBack1.ParameterList.append(objparam)
                
                objparam = Parameter(Name: "ext",value: String(self.imageType(data)))
                objCallBack1.ParameterList.append(objparam)
                
                objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
                objCallBack1.ParameterList.append(objparam)
                
                
                objCallBack1.post(FileuploadOnComplete, OnError: FileuploadOnErrorOnError)
            }
            
           
        }
        
    }
    func FileuploadOnComplete(ResultString :NSString? ) {
        dispatch_async(dispatch_get_main_queue(),{
            
            let Result : String = self.objCallBack1.GetJSONResultString(ResultString)!
            
            self.AttachFileGenerateName = String(Result)
         
        })
        
        
    }
    
    func FileuploadOnErrorOnError(Response: NSError ){
        
        
    }
    
    
    func imageType(imgData : NSData) -> String
    {
        var c = [UInt8](count: 1, repeatedValue: 0)
        imgData.getBytes(&c, length: 1)
        
        let ext : String
        
        switch (c[0]) {
        case 0xFF:
            
            ext = "jpg"
            
        case 0x89:
            
            ext = "png"
        case 0x47:
            
            ext = "gif"
        case 0x49, 0x4D :
            ext = "tiff"
        default:
            ext = "" //unknown
        }
        
        return ext
    }
    
    func SaveLeave(){
        
        if self.iLeaveType == -1
        {
            //Show the alert for invalid users
            let alert = UIAlertController(title: "Validation", message: "Please select a leave type.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.presentViewController(alert, animated: true, completion: nil)
                
            }
            self.btnSubmit.enabled = true
            return
        }

    
        if let tempDate = FromDate
        {
 
        }
        else{
            
            //Show the alert for invalid users
            let alert = UIAlertController(title: "Validation", message: "From date can't be empty.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.presentViewController(alert, animated: true, completion: nil)
                
            }
            self.btnSubmit.enabled = true
            return
        }
        
        if let tempDate = ToDate
        {
            
     
        }else{
            //Show the alert for invalid users
            let alert = UIAlertController(title: "Validation", message: "To date can't be empty.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.presentViewController(alert, animated: true, completion: nil)
            }
            self.btnSubmit.enabled = true
            return
        }
        
        if FromDate != nil && ToDate != nil
        {
            if FromDate!.compare(ToDate!) == NSComparisonResult.OrderedDescending
            {
                //Show the alert for invalid users
                let alert = UIAlertController(title: "Validation", message: "From date can't be greater than to date.", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                self.btnSubmit.enabled = true
                return
                
            }
        }
        
        if self.txtReason.text == ""
        {
            //Show the alert for invalid users
            let alert = UIAlertController(title: "Validation", message: "Please enter the reason.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.presentViewController(alert, animated: true, completion: nil)
                
            }
            self.btnSubmit.enabled = true
            return
        }
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            
            
            objActivityIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            
            
            
            objActivityIndicaor.activityIndicatorViewStyle = .WhiteLarge
            objActivityIndicaor.center = self.view.center
            
            objActivityIndicaor.transform = CGAffineTransformMakeScale(1.5, 1.5);
            objActivityIndicaor.hidesWhenStopped = true
            objActivityIndicaor.color = UIColor.grayColor()
            
            self.view.addSubview(objActivityIndicaor)
            objActivityIndicaor.startAnimating()
            
            
            objCallBack = AjaxCallBack()
            
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            objCallBack.MethodName = "ApplyLeaveNew"
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pLeaveType",value: String(self.iLeaveType))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pWorkFlowID",value: String(self.iWorkFlowID))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pStartDate",value: String(dtFromDate.titleLabel!.text!))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pEndDate",value: String(dtToDate.titleLabel!.text!))
            objCallBack.ParameterList.append(objparam)
            
            if HalfDaySwitch.on{
                objparam = Parameter(Name: "ISPartialDay",value: "1")
                objCallBack.ParameterList.append(objparam)
                
                objparam = Parameter(Name: "pDayZone",value: String(self.iDayZone))
                objCallBack.ParameterList.append(objparam)
                
            }
            else
            {
                objparam = Parameter(Name: "ISPartialDay",value: "0")
                objCallBack.ParameterList.append(objparam)
                
                objparam = Parameter(Name: "pDayZone",value: String("1"))
                objCallBack.ParameterList.append(objparam)
            }
            
            objparam = Parameter(Name: "pComment",value: String(txtReason.text!))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pAttachementName",value: String(self.AttachFileActualName))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pAttachementNewName",value: String(self.AttachFileGenerateName))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pDeductFrom",value: String(self.iDeductFrom))
            objCallBack.ParameterList.append(objparam)
            
            objCallBack.post(LeaveListOnComplete, OnError: LeaveListOnErrorOnError)
            
        }
      
    
    
    }
    

    func LeaveListOnComplete(ResultString :NSString? ) {
        dispatch_async(dispatch_get_main_queue(),{
            let Result =  JSON(data: self.objCallBack.GetJSONData(ResultString)!)
         self.objActivityIndicaor.stopAnimating()
        if Result[0]["ErrorNo"] == -1
        {
            
            let alert = UIAlertController(title: "Success", message: "The leave has been applied successfully.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: self.CloseForm))
            
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.presentViewController(alert, animated: true, completion: nil)}
        }
        else
        {
            
            self.btnSubmit.enabled = true
            
            let alert = UIAlertController(title: "Alert", message: String(Result[0]["ErrorMessage"]), preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.presentViewController(alert, animated: true, completion: nil)}
        }
        })
       
    
    }

    func LeaveListOnErrorOnError(Response: NSError ){
   
        
    }
    
    
    
    
 
    
    func CloseForm(sender:UIAlertAction)
    {
    dismissViewControllerAnimated(true, completion: nil)
    
    }
    

    
    
    override func viewDidLoad() {
        
       
        
        super.UINavigationBarTitle = "Apply Leave"
        
        
        
        self.dropDown1.dataSource = ["Forenoon","Afternoon"]
        self.ShowHalfDayType.setTitle("Forenoon", forState: .Normal)
        self.dropDown1.selectionAction = { [unowned self] (index, item) in
            self.ShowHalfDayType.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
            self.dropdownHT.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
            self.ShowHalfDayType.setTitle(item, forState: .Normal)
            
            self.iDayZone = index + 1;
        }
         self.iDayZone =  1;
        self.dropDown1.anchorView = self.ShowHalfDayType
        self.dropDown1.bottomOffset = CGPoint(x: 0, y:self.ShowHalfDayType.bounds.height + 3 )
        
   
        
        self.ShowHalfDayType.hidden = true
        self.dropdownHT.hidden = true

      
        
        
        super.viewDidLoad()
       
        
     
    }
    
      override func viewDidAppear(animated: Bool) {
        
        
        super.viewDidAppear(animated)
        BindLeaveBalance()
         DesignUI()
    }
    
 
 
    
 
    
func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
    return 1
}
func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    if pickerView.tag == 1
    {
      return self.TempJson.count
    }
    else{
       return 2
    }
  
}
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView.tag == 1
        {
                return String(self.TempJson[row]["LeaveType"])
        }
        else{
            
               return  String(self.pickerptions[row])
            
        }
    
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView!) -> UIView
    {
        let pickerLabel = UILabel()
        pickerLabel.textColor = UIColor.grayColor()
        if pickerView.tag == 1
        {
            pickerLabel.text = String(self.TempJson[row]["LeaveType"])
        }
        else{
            
              pickerLabel.text =  String(self.pickerptions[row])
            
        }
        // pickerLabel.font = UIFont(name: pickerLabel.font.fontName, size: 15)
        pickerLabel.font = UIFont(name: "Arial", size: 17) // In this use your custom font
        pickerLabel.textAlignment = NSTextAlignment.Center
        return pickerLabel
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1
        {
            
            lblBalance.text = "Leave Balance " + ": " + String(self.TempJson[row]["LeaveBalance"])
            self.iLeaveType = Int(String(self.TempJson[row]["LeaveTypeID"]))!
            self.iWorkFlowID = Int(String(self.TempJson[row]["WorkFlowID"]))
        }
        else{
            self.iDayZone = row + 1
            
            
        }
        

    }
    
    func BindLeaveBalance(){
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            objActivityIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            
            
            
            objActivityIndicaor.activityIndicatorViewStyle = .WhiteLarge
            objActivityIndicaor.center = self.view.center
            
            objActivityIndicaor.transform = CGAffineTransformMakeScale(1.5, 1.5);
            objActivityIndicaor.hidesWhenStopped = true
            objActivityIndicaor.color = UIColor.grayColor()
            
            self.view.addSubview(objActivityIndicaor)
            objActivityIndicaor.startAnimating()
            
            objCallBack.MethodName = "GetEligebleLeaves"
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objCallBack.post(setupCollectionView, OnError: EmpProfileOnErrorOnError)
            
        }
    }
    
    func setupCollectionView (ResultString :NSString? ) {
        
        dispatch_async(dispatch_get_main_queue(),{
              self.objActivityIndicaor.stopAnimating()
            self.TempJson = JSON(data: self.objCallBack.GetJSONData(ResultString)!)
        
            self.CreateLeaveTypeArray()
            
            self.LeaveTypeOutlet.setTitle("--Select Leave Type--", forState: .Normal)
            self.dropDown.dataSource = self.LeaveType
            
            if (self.LeaveTypeSelectIndex != -1)
            {
                self.LeaveTypeOutlet.setTitle(String(self.TempJson[self.LeaveTypeSelectIndex]["LeaveType"]), forState: .Normal)
                self.lblBalance.text = "Leave Balance " + ": " +  String(self.TempJson[self.LeaveTypeSelectIndex]["LeaveBalance"])
                
                self.iLeaveType =  self.TempJson[self.LeaveTypeSelectIndex]["LeaveTypeID"].int!
                self.iWorkFlowID =  Int(String(self.TempJson[self.LeaveTypeSelectIndex]["WorkFlowID"]))
                
                if (self.TempJson[self.LeaveTypeSelectIndex]["CarryForwardAccountLeaveBalance"].int > 0 )
                {
                    self.DeductFromVw.hidden = false
                    self.lblBalance.text = "Leave Balance " + ": " +  String(self.TempJson[self.LeaveTypeSelectIndex]["MainAccountLeaveBalance"])
                }
                else
                {
                    self.DeductFromVw.hidden = true
                }
               
            }
            
            self.dropDown.selectionAction = { [unowned self] (index, item) in
                
                if (self.TempJson[index]["CarryForwardAccountLeaveBalance"].int > 0 )
                {
                    self.DeductFromVw.hidden = false
                    self.lblBalance.text = "Leave Balance " + ": " +  String(self.TempJson[index]["MainAccountLeaveBalance"])
                }
                else
                {
                      self.DeductFromVw.hidden = true
                }
                
                self.LeaveTypeSelectIndex = index
                self.LeaveTypeOutlet.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
                self.dropdownsymbol.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
                self.LeaveTypeOutlet.setTitle(item, forState: .Normal)
                self.lblBalance.text = "Leave Balance " + ": " + String(self.TempJson[index]["LeaveBalance"])
                
                self.iLeaveType = Int(String(self.TempJson[index]["LeaveTypeID"]))!
                self.iWorkFlowID = Int(String(self.TempJson[index]["WorkFlowID"]))
            }
            
            
            self.dropDown.anchorView = self.LeaveTypeOutlet
            self.dropDown.bottomOffset = CGPoint(x: 0, y:self.LeaveTypeOutlet.bounds.height)
        })
        
        
        
    }
    
    //Response on error callback
    func EmpProfileOnErrorOnError(Response: NSError ){
        
    }
    
    func ShowDatePicker(sender:UIButton)
    {
        SelectedTimeBtn = sender
        
        FilterBGLayer = UIView(frame: CGRectMake(0, 0, Viewframe.width  ,  Viewframe.height))
        FilterBGLayer.backgroundColor = UIColor(colorLiteralRed: 0 , green: 0, blue: 0, alpha: 0.6)
        let GestureLeaveDetail1 = UITapGestureRecognizer(target: self, action: "DismissFilter:")
        FilterBGLayer.addGestureRecognizer(GestureLeaveDetail1)
        FilterView = UIView(frame: CGRectMake(FilterBGLayer.frame.width / 100 * 10 , FilterBGLayer.frame.height  / 100 * 30 , FilterBGLayer.frame.width  / 100 * 80, FilterBGLayer.frame.height  / 100 * 70))
        FilterView.backgroundColor = UIColor.whiteColor()
        FilterView.layer.cornerRadius = 8
        let TimePick : UIDatePicker =  UIDatePicker(frame: CGRect(x: 10, y: 5, width: FilterView.frame.width - 10 ,height: FilterView.frame.width - 30))
        TimePick.datePickerMode = .Date
        TimePick.tag = sender.tag
        let calendar: NSCalendar = NSCalendar(calendarIdentifier: NSGregorianCalendar)!
        // let calendar: NSCalendar = NSCalendar.currentCalendar()
        calendar.timeZone = NSTimeZone(name: "UTC")!
        var DateStr = ""
        if sender.titleLabel?.text == "Choose Date"
        {
            let date = NSDate()
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components([.Year,.Month,.Day], fromDate: date)
            
            DateStr = String(components.day) + "/" + String(components.month) + "/" + String(components.year)
        }
        else
        {
            DateStr = (sender.titleLabel?.text)!
        }
        
        var array = DateStr.componentsSeparatedByString("/")
        let components: NSDateComponents = NSDateComponents()
        components.year = Int(array[2])!
        components.month = Int(array[1])!
        components.day = Int(array[0])!
        let defaultDate: NSDate = calendar.dateFromComponents(components)!
        TimePick.date = defaultDate
        
        TimePick.addTarget(self, action: Selector("handlerDate:"), forControlEvents: UIControlEvents.ValueChanged)
        
        
        
        let timeFormatter = NSDateFormatter()
        timeFormatter.dateFormat = "dd/MM/yyyy"
        
       
        
        if sender.tag == 1
        {
            self.FromDate = defaultDate
            SelectedTimeBtn.setTitle(String(timeFormatter.stringFromDate(defaultDate)), forState: .Normal)
            
        }else{
            self.ToDate = defaultDate
            SelectedTimeBtn.setTitle(String(timeFormatter.stringFromDate(defaultDate)), forState: .Normal)
        }
        
        
        let btn_ApplyFilter : UIButton = UIButton(frame: CGRectMake(FilterBGLayer.frame.width / 100 * 22, TimePick.frame.height + 40, 120 ,24))
        btn_ApplyFilter.setTitle("Go", forState: .Normal)
        btn_ApplyFilter.layer.cornerRadius = 5
        btn_ApplyFilter.tag = sender.tag
        btn_ApplyFilter.backgroundColor = UIColor(hexString: "#2190ea")
        btn_ApplyFilter.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        btn_ApplyFilter.titleLabel!.font =  UIFont(name:"helvetica-Bold", size: 12)
        btn_ApplyFilter.addTarget(self, action: "DateChanged:", forControlEvents: .TouchUpInside)
        FilterView.addSubview(btn_ApplyFilter)
        
        FilterView.addSubview(TimePick)
        FilterBGLayer.addSubview(FilterView)
        self.view.addSubview(FilterBGLayer)
        
    }
    
    func DateChanged(sender: UIButton)
    {
    
           
        isFilterOpen = false
        FilterBGLayer.removeFromSuperview()
        
    }
    
    func DismissFilter(sender: UIButton)
    {
        isFilterOpen = false
        FilterBGLayer.removeFromSuperview()
        
    }
    
    func handlerDate(sender: UIDatePicker) {
        let timeFormatter = NSDateFormatter()
        timeFormatter.dateFormat = "dd/MM/yyyy"
  
        
        if sender.tag == 1
        {
            self.FromDate = sender.date
            
            
        }else{
            self.ToDate = sender.date
            
        }
         SelectedTimeBtn.setTitle(String(timeFormatter.stringFromDate(sender.date)), forState: .Normal)
        
        // do what you want to do with the string.
    }
    


    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
