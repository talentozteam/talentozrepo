//
//  TADashBoardVC.swift
//  Talentoz
//
//  Created by forziamac on 21/01/16.
//  Copyright © 2016 Forzia Tech Private Ltd. All rights reserved.
//
import UIKit

class TADashBoardVC:  BaseTabBarVC,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate  , UIPageViewControllerDataSource      {
    
    private var pageViewController: UIPageViewController?
    private var initViewController: UIViewController?
    let objCallBackTA = AjaxCallBack()
    var objCallBack = AjaxCallBack()
     var objCallBackAP = AjaxCallBack()
    let defaults = NSUserDefaults.standardUserDefaults()
    var TAQuickInfo: Talentoz.JSON!
    var AttendanceApprovalJSON: Talentoz.JSON!
    var dAttendanceAdjCollectionView : UICollectionView!
    var iMode: Int!
    var iCalculatedTime: Int!
    var iTardiness: String!
    var iUnderTime: String!
    var iLeaveDays: Int!
    var CurrentActionCode : String!
    internal var VisibleCheckInOut = false
    
    internal var FilterView : UIView!
    internal var FilterBGLayer : UIView!
    var isFilterOpen = false
    
    @IBOutlet var MainView: UIView!
    let objRole = RoleManager()
    
    var objLBActivityIndicaor : UIActivityIndicatorView!
    var objLRActivityIndicaor : UIActivityIndicatorView!
    
    let vscrollView = UIScrollView(frame: UIScreen.mainScreen().bounds)
    
    override func viewDidLoad() {
        super.UINavigationBarTitle = "Attendance"
        self.view.addSubview(vscrollView)
        
        self.vscrollView.contentSize = CGSize(width: self.view.frame.width, height: 700)
        self.vscrollView.backgroundColor = UIColor.whiteColor()
        super.viewDidLoad()
        GetSliderPageDetails()
        LoadQuickAction()
        
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        if viewController.isKindOfClass(LeaveBV) {
            // 3 -> 2
            return getStepTwo()
        }
        else if viewController.isKindOfClass(UndertimeBV) {
            // 2 -> 1
            return getStepOne()
        } else if viewController.isKindOfClass(TardinessBV) {
            // 1 -> 0
            return getStepZero()
        } else {
            // 0 -> end of the road
            return nil
        }
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        if viewController.isKindOfClass(TotalHoursBV) {
            // 0 -> 1
            return getStepOne()
        } else if viewController.isKindOfClass(TardinessBV) {
            // 1 -> 2
            return getStepTwo()
        }else if viewController.isKindOfClass(UndertimeBV) {
            // 2 -> 3
            return getStepThree()
        }
        else {
            // 2 -> end of the road
            return nil
        }
    }
    
    // Enables pagination dots
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 4
    }
    
    // This only gets called once, when setViewControllers is called
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    
    func getStepZero() -> TotalHoursBV {
       
        let vc = storyboard!.instantiateViewControllerWithIdentifier("TotalWorkedHrsVC") as! TotalHoursBV
        
        vc.VisibleCheckInOut =  self.TAQuickInfo[0]["EnableMobCheckin"].bool!
        
        
        if let tempcal = self.defaults.stringForKey("ModeofCheckIn")
        {
            vc.Mode = Int(tempcal)!
        }
        else
        {
           vc.Mode = self.iMode
        }
        
       if let tempcal = self.defaults.stringForKey("CalculatedTime")
       {
          vc.CalculatedTime = Int(tempcal)! 
        }
        else
       {
            vc.CalculatedTime = self.iCalculatedTime * 60
        }
        return vc
    }
    
    func getStepOne() -> TardinessBV {
        
       
        let vc = storyboard!.instantiateViewControllerWithIdentifier("TotalTardinessVC") as! TardinessBV
        vc.Tardiness = self.iTardiness
        vc.HomeView = self.MainView
        return vc
    }
    
    func getStepTwo() -> UndertimeBV {
      
        let vc = storyboard!.instantiateViewControllerWithIdentifier("TotalUnderTimeVC") as! UndertimeBV
        vc.UnderTime = self.iUnderTime
        vc.HomeView = self.MainView
        return vc
    }
    func getStepThree() -> LeaveBV {

        let vc = storyboard!.instantiateViewControllerWithIdentifier("TotalLeaveVC") as! LeaveBV
        vc.LeaveDays = self.iLeaveDays
        return vc
    }
    
    override func goBack() {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("HomeInitialView")
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    private func createPageViewController(Mode: Int, CalculatedTime: Int) {
        
        let pageController = self.storyboard!.instantiateViewControllerWithIdentifier("PageViewMainVc") as! UIPageViewController
        pageController.dataSource = self
        let Vc = storyboard!.instantiateViewControllerWithIdentifier("TotalWorkedHrsVC")  as! TotalHoursBV
        Vc.Mode = Mode
        Vc.VisibleCheckInOut = self.VisibleCheckInOut
        Vc.CalculatedTime = CalculatedTime * 60
        self.defaults.setObject(Vc.CalculatedTime, forKey: "CalculatedTime")
         self.defaults.setObject(Vc.Mode, forKey: "ModeofCheckIn")
        pageController.setViewControllers([Vc] , direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
        
        pageViewController = pageController
        addChildViewController(pageViewController!)
        self.vscrollView.addSubview(pageViewController!.view)
        
        self.pageViewController!.view.frame =
            CGRectMake(0,60, self.vscrollView.frame.width, 200)
        
        
    }
    
    private func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = UIColor(hexString: "#cccccc")
        appearance.currentPageIndicatorTintColor = UIColor(hexString: "#2190ea")
        appearance.backgroundColor = UIColor(hexString: "#fffffe")
        appearance.frameForAlignmentRect(self.vscrollView.frame)
        appearance.frame = CGRectMake(0,25, self.vscrollView.frame.width, 200)
        
    }
    
 
    
    func GetSliderPageDetails()
    {
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            objCallBackTA.MethodName = "TimeAttendanceQuickInfo"
            let ResultJSON = objCallBackTA.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBackTA.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBackTA.ParameterList.append(objparam)
            
            objCallBackTA.post(SliderInfoOnComplete, OnError: SliderInfoOnErrorOnError)
            
        }
    }
    
    //Response on error callback
    func SliderInfoOnErrorOnError(Response: NSError ){
        
    }
    
    func SliderInfoOnComplete (ResultString :NSString? ) {
        
        dispatch_async(dispatch_get_main_queue(),{
            self.TAQuickInfo = JSON(data: self.objCallBackTA.GetJSONData(ResultString)!)
            
            self.iMode = self.TAQuickInfo[0]["CheckInSts"].int
            self.iCalculatedTime = self.TAQuickInfo[0]["CalculateTime"].int
            self.iTardiness = self.TAQuickInfo[0]["Tardiness"].string
            self.iUnderTime = self.TAQuickInfo[0]["Undertime"].string
            self.iLeaveDays = self.TAQuickInfo[0]["LeaveCount"].int
            self.VisibleCheckInOut =  self.TAQuickInfo[0]["EnableMobCheckin"].bool!
            
            
            self.createPageViewController(self.iMode, CalculatedTime: self.iCalculatedTime)
            
             self.setupPageControl()
            
            if self.objRole.ISManager {
                
                let ContainerView2 = UIView(frame: CGRectMake(0, 425, self.view.frame.size.width, 1))
                ContainerView2.layer.borderColor = UIColor(hexString: "#cccccc")?.CGColor
                ContainerView2.layer.borderWidth = 1
                self.vscrollView.addSubview(ContainerView2)

                
                
                self.objLBActivityIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, ContainerView2.frame.size.width, ContainerView2.frame.size.height))
                
                
                
                self.objLBActivityIndicaor.activityIndicatorViewStyle = .WhiteLarge
                
                var objPosition =  ContainerView2.center
                
                objPosition.y =  objPosition.y.advancedBy(20)
                self.objLBActivityIndicaor.center = objPosition
                
                //objActivityIndicaor.transform = CGAffineTransformMakeScale(1.5, 1.5);
                self.objLBActivityIndicaor.hidesWhenStopped = true
                self.objLBActivityIndicaor.color = UIColor.grayColor()
                
                 self.vscrollView.addSubview(self.objLBActivityIndicaor)
                self.objLBActivityIndicaor.startAnimating()
                
                self.FetEmployeeApprovalAdj()
            }
           
        })
        
    }
    
    func LoadQuickAction()
    {
        
        let lbl_Title : UILabel = UILabel(frame: CGRectMake(10, 260, self.view.frame.size.width,25))
        lbl_Title.backgroundColor = UIColor.whiteColor()
        lbl_Title.textAlignment = .Left
        lbl_Title.font = UIFont(name:"helvetica-bold", size: 13)
        lbl_Title.text = "Quick Actions"
        vscrollView.addSubview(lbl_Title)
        
        let ContainerView = UIView(frame: CGRectMake(0,285, self.view.frame.size.width, 1))
        ContainerView.layer.borderColor = UIColor(hexString: "#cccccc")?.CGColor
        ContainerView.layer.borderWidth = 1
        vscrollView.addSubview(ContainerView)
        
 
        
        
        let DTRView=UIView(frame: CGRectMake((self.view.frame.width / 100 * 6), 300, (self.view.frame.width / 100 * 24), 75))
        DTRView.backgroundColor = UIColor(hexString: "#FF3E96")
        
        let gestureAdjList = UITapGestureRecognizer(target: self, action: "DTRViewAction:")
        
        let gestureAdjList1 = UITapGestureRecognizer(target: self, action: "DTRViewAction:")
        
        
        let lbl_DTRListViewIcon : UIButton = UIButton(frame: CGRectMake((DTRView.frame.width / 100 * 2), 15, DTRView.frame.width,23))
        lbl_DTRListViewIcon.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        lbl_DTRListViewIcon.setImage(UIImage(named: "AttendanceMenu"), forState: UIControlState.Normal)
        lbl_DTRListViewIcon.addGestureRecognizer(gestureAdjList1)
        DTRView.addSubview(lbl_DTRListViewIcon)
        
        let lbl_DTRListViewTitle : UILabel = UILabel(frame: CGRectMake((DTRView.frame.width / 100 * 2), 45, DTRView.frame.width,18))
        lbl_DTRListViewTitle.textColor = UIColor.whiteColor()
        lbl_DTRListViewTitle.textAlignment = .Center
        lbl_DTRListViewTitle.font = lbl_Title.font.fontWithSize(11.0)
        lbl_DTRListViewTitle.text = "DTR"
        DTRView.addSubview(lbl_DTRListViewTitle)
        DTRView.addGestureRecognizer(gestureAdjList)

        
        vscrollView.addSubview(DTRView)
        
        
        let TodayActivityView=UIView(frame: CGRectMake((self.view.frame.width / 100 * 37), 300, (self.view.frame.width / 100 * 24), 75))
        TodayActivityView.backgroundColor = UIColor(hexString: "#00c853")
        
        let gestureHoliday = UITapGestureRecognizer(target: self, action: "ActListAction:")
        
        let gestureHoliday1 = UITapGestureRecognizer(target: self, action: "ActListAction:")
        
        
        let lbl_ActivityListViewIcon : UIButton = UIButton(frame: CGRectMake((TodayActivityView.frame.width / 100 * 2), 15, TodayActivityView.frame.width,23))
        // lbl_HolidayListViewIcon.textColor = UIColor.whiteColor()
        // lbl_HolidayListViewIcon.textAlignment = .Center
        //  lbl_HolidayListViewIcon.setFAIcon(FAType.FACar, iconSize: 25)
        
        lbl_ActivityListViewIcon.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        lbl_ActivityListViewIcon.setImage(UIImage(named: "Historical Filled-44"), forState: .Normal)
        TodayActivityView.addGestureRecognizer(gestureHoliday)
        TodayActivityView.addSubview(lbl_ActivityListViewIcon)
        
        
        let lbl_ActivityListViewTitle : UILabel = UILabel(frame: CGRectMake((TodayActivityView.frame.width / 100 * 2), 45, TodayActivityView.frame.width,18))
        lbl_ActivityListViewTitle.textColor = UIColor.whiteColor()
        lbl_ActivityListViewTitle.textAlignment = .Center
        lbl_ActivityListViewTitle.font = lbl_Title.font.fontWithSize(11.0)
        lbl_ActivityListViewTitle.text = "Activities"
        lbl_ActivityListViewIcon.addGestureRecognizer(gestureHoliday1)
        TodayActivityView.addSubview(lbl_ActivityListViewTitle)
        
        
        vscrollView.addSubview(TodayActivityView)
        
        
        
        let AttenAdjView=UIView(frame: CGRectMake((self.view.frame.width / 100 * 68), 300, (self.view.frame.width / 100 * 24), 75))
        AttenAdjView.backgroundColor = UIColor(hexString: "#f4771d")
        
        
        let lbl_AttAdjViewIcon : UILabel = UILabel(frame: CGRectMake((AttenAdjView.frame.width / 100 * 2), 15, AttenAdjView.frame.width,23))
        lbl_AttAdjViewIcon.textColor = UIColor.whiteColor()
        lbl_AttAdjViewIcon.textAlignment = .Center
        lbl_AttAdjViewIcon.setFAIcon(FAType.FAPencilSquare, iconSize: 25)
        AttenAdjView.addSubview(lbl_AttAdjViewIcon)
        
        let lbl_AttenAdjViewTitle : UILabel = UILabel(frame: CGRectMake((AttenAdjView.frame.width / 100 * 2), 45, AttenAdjView.frame.width,18))
        lbl_AttenAdjViewTitle.textColor = UIColor.whiteColor()
        lbl_AttenAdjViewTitle.textAlignment = .Center
        lbl_AttenAdjViewTitle.font = lbl_Title.font.fontWithSize(11.0)
        lbl_AttenAdjViewTitle.text = "Adjustments"
        AttenAdjView.addSubview(lbl_AttenAdjViewTitle)
        
        let gesture = UITapGestureRecognizer(target: self, action: "AttendanceAdjAction:")
        AttenAdjView.addGestureRecognizer(gesture)
        vscrollView.addSubview(AttenAdjView)
        
        
        
        
        
        
    }
    
    func ShowTeamAdjList(Sender: UIButton!)
    {
        let AdjListObj = ADJList()
        AdjListObj.ModeofShow = 1
        self.presentViewController(AdjListObj, animated: true, completion: nil)
    }
    
    func AttendanceAdjAction(sender: UIView) {
        
        let  AdjListView = ADJList()
        self.presentViewController(AdjListView, animated: true, completion: nil)
        
        
    }
    
    func DTRViewAction(sender: UIView) {
        let  DTRListView = DTRList()
        self.presentViewController(DTRListView, animated: true, completion: nil)     
        
        
    }
    
  
    
    func ActListAction(sender: UIView) {
        
        FilterBGLayer = UIView(frame: CGRectMake(0, 0, Viewframe.width  ,  Viewframe.height))
        FilterBGLayer.backgroundColor = UIColor(colorLiteralRed: 0 , green: 0, blue: 0, alpha: 0.6)
        let GestureLeaveDetail1 = UITapGestureRecognizer(target: self, action: "DismissFilter:")
        FilterBGLayer.addGestureRecognizer(GestureLeaveDetail1)
        FilterView = UIView(frame: CGRectMake(FilterBGLayer.frame.width / 100 * 10 , FilterBGLayer.frame.height  / 100 * 10 , FilterBGLayer.frame.width  / 100 * 80, FilterBGLayer.frame.height  / 100 * 60))
        FilterView.backgroundColor = UIColor.whiteColor()
        FilterView.layer.cornerRadius = 8
 
 
        
        let  objActivities = ActivitiesListB()
        let date = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Year,.Month,.Day], fromDate: date)
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            let ResultJSON = objCallBackTA.DeserializeJSONString(UserInfo)
            objActivities.UserId = Int(ResultJSON!["UserID"]as! NSNumber)
        }
        objActivities.ActivityDate = String(components.day) + "/" + String(components.month) + "/" + String(components.year)
        objActivities.MainView = self.FilterView
        objActivities.GetActivitiesList()

        FilterView.addSubview(objActivities)
        FilterBGLayer.addSubview(FilterView)
        self.view.addSubview(FilterBGLayer)
        
        
    }
    
    func DismissFilter(sender: UIButton)
    {
        isFilterOpen = false
        FilterBGLayer.removeFromSuperview()
        
    }
    
    
    
    // Adj Approval Slider
    
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return  1
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            
                return CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height / 100 * 30)
            
            
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell : UICollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("cellIdentifierAdj", forIndexPath: indexPath)
        
        cell.backgroundColor = UIColor.whiteColor()
        cell.frame = CGRectMake(0, 0, dAttendanceAdjCollectionView.frame.size.width, dAttendanceAdjCollectionView.frame.size.height  )
        
        let scroll: UIScrollView = UIScrollView(frame: CGRectMake(0, 0, dAttendanceAdjCollectionView.frame.size.width, 200  ))
        scroll.delegate = self
        scroll.backgroundColor = UIColor.whiteColor()
        cell.addSubview(scroll)
        
         var xbase : CGFloat = 10
        var CountMore: Int = 0
        
        
        for i in 0 ..< self.AttendanceApprovalJSON.count  {
            
            
 if self.AttendanceApprovalJSON[i]["Status"].int == 1
 {
            
        if CountMore > 5
        {
            let AttendanceAdjView=UIView(frame: CGRectMake(xbase, 0, scroll.frame.width / 100 * 80 , scroll.frame.height))
            AttendanceAdjView.backgroundColor = UIColor(hexString: "#e9e9e9")
            
            AttendanceAdjView.tag = self.AttendanceApprovalJSON[i]["RequestID"].int!
            scroll.addSubview(AttendanceAdjView)
            
            let imgView : UIImageView = UIImageView(frame: CGRectMake(10, 7, 50, 50))
            imgView.layer.borderWidth = 1
            imgView.layer.masksToBounds = false
            imgView.layer.borderColor = UIColor.whiteColor().CGColor
            imgView.layer.cornerRadius = imgView.frame.height/2
            imgView.clipsToBounds = true
            AttendanceAdjView.addSubview(imgView)
            
            
            
            let lbl_EmpName : UILabel = UILabel(frame: CGRectMake( 70 , 10, AttendanceAdjView.frame.width / 100 * 50, 15))
            lbl_EmpName.textColor = UIColor(hexString: "#333333")
            
            
            lbl_EmpName.font = lbl_EmpName.font.fontWithSize(12.0)
            AttendanceAdjView.addSubview(lbl_EmpName)
            lbl_EmpName.text = String(self.AttendanceApprovalJSON[i]["EmployeeName"])
            
            
            let lbl_PositionName : UILabel = UILabel(frame: CGRectMake(70 , 25, AttendanceAdjView.frame.width / 100 * 75, 15))
            lbl_PositionName.textColor = UIColor(hexString: "#666666")
            
            lbl_PositionName.font = lbl_PositionName.font.fontWithSize(12.0)
            AttendanceAdjView.addSubview(lbl_PositionName)
            lbl_PositionName.text = String(self.AttendanceApprovalJSON[i]["PositionName"])
            AttendanceAdjView.addSubview(lbl_PositionName)
            
            let lbl_WorkLocationName : UILabel = UILabel(frame: CGRectMake(70 , 40, 75, 15))
            lbl_WorkLocationName.textColor = UIColor(hexString: "#666666")
            
            lbl_WorkLocationName.font = lbl_WorkLocationName.font.fontWithSize(12.0)
            AttendanceAdjView.addSubview(lbl_WorkLocationName)
            lbl_WorkLocationName.text = String(self.AttendanceApprovalJSON[i]["WorkLocationName"])
            
            let btn_ShowDetail : UIButton = UIButton(frame: CGRectMake(160 , 40, 70, 15))
            btn_ShowDetail.backgroundColor = UIColor.clearColor()
            btn_ShowDetail.setTitleColor(UIColor(hexString: "#2190ea"), forState: UIControlState.Normal)
            btn_ShowDetail.setTitle("Show Detail" , forState: .Normal)
            btn_ShowDetail.titleLabel?.font = UIFont(name: "HelveticaNeue", size: 12)
            btn_ShowDetail.addTarget(self, action: #selector(TADashBoardVC.ShowADJDetail(_:)), forControlEvents: .TouchUpInside)
            btn_ShowDetail.tag = i
            AttendanceAdjView.addSubview(btn_ShowDetail)
            
            
            if let tmpDomainURL = self.defaults.stringForKey("DomainURL")
            {
                
                if self.AttendanceApprovalJSON[i]["EmployeePhoto"] == nil
                {
                    let img_EmpPic:  UIButton = UIButton(frame: CGRectMake(10, 7, 50, 50))
                    img_EmpPic.layer.borderWidth = 0.5
                    img_EmpPic.layer.masksToBounds = false
                    img_EmpPic.layer.borderColor = UIColor.lightGrayColor().CGColor
                    img_EmpPic.backgroundColor = UIColor.SetRandomColor()
                    img_EmpPic.layer.cornerRadius = img_EmpPic.frame.height/2
                    img_EmpPic.clipsToBounds = true
                    img_EmpPic.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                    img_EmpPic.setTitle(String(self.AttendanceApprovalJSON[i]["EmployeeName"])[0], forState: UIControlState.Normal)
                    AttendanceAdjView.addSubview(img_EmpPic)
                }
                else{
                    let imgView : UIImageView = UIImageView(frame: CGRectMake(10, 7, 50, 50))
                    imgView.layer.borderWidth = 1
                    imgView.layer.masksToBounds = false
                    imgView.layer.borderColor = UIColor.whiteColor().CGColor
                    imgView.layer.cornerRadius = imgView.frame.height/2
                    imgView.clipsToBounds = true
                    AttendanceAdjView.addSubview(imgView)
                    
                    let employeePic = "http://" + tmpDomainURL + "/Uploadedfiles/Client" + String(self.defaults.stringForKey("ClientID")!) + "/Image/" + String(self.AttendanceApprovalJSON[i]["EmployeePhoto"])
                    imgView.setImageWithUrl(NSURL(string: employeePic)!)
                }
            }
            
            
            
            
            let PeriodView=UIView(frame: CGRectMake(AttendanceAdjView.frame.width / 100 * 4, 70, (AttendanceAdjView.frame.width) / 100 * 92, 70))
            PeriodView.backgroundColor = UIColor.whiteColor()
            AttendanceAdjView.addSubview(PeriodView)
            
            let lbl_DateLine : UILabel = UILabel(frame: CGRectMake( PeriodView.frame.width / 100 * 4, 10, PeriodView.frame.width, 15))
            
            lbl_DateLine.textAlignment = .Left
            
            lbl_DateLine.textColor = UIColor(hexString: "#333333")
            lbl_DateLine.setFAText(prefixText: "", icon: FAType.FACalendar, postfixText: "  " + String(self.AttendanceApprovalJSON[i]["DateLineDesc"]), size: 14)
            PeriodView.addSubview(lbl_DateLine)
            
            
            let lbl_TotalHours : UILabel = UILabel(frame: CGRectMake(PeriodView.frame.width / 100 * 4 , 40, PeriodView.frame.width, 15))
            lbl_TotalHours.backgroundColor = UIColor.clearColor()
            
            lbl_TotalHours.textAlignment = .Left
            
            
            lbl_TotalHours.textColor = UIColor(hexString: "#333333")
            PeriodView.addSubview(lbl_TotalHours)
            
            lbl_TotalHours.setFAText(prefixText: "", icon: FAType.FAClockO, postfixText: "  " + String(self.AttendanceApprovalJSON[i]["TotalHours"]) + " Hours", size: 14)
            
            
            
            
            
            
            let btnApprove : UIButton = UIButton(frame: CGRectMake(AttendanceAdjView.frame.width / 100 * 4 ,150, AttendanceAdjView.frame.width / 100 * 45, 30))
            
            btnApprove.layer.borderWidth = 0
            btnApprove.setTitle("Reject", forState: UIControlState.Normal)
            
            btnApprove.addTarget(self, action: #selector(TADashBoardVC.ApproveTouch(_:)), forControlEvents: .TouchUpInside)
            btnApprove.tag = self.AttendanceApprovalJSON[i]["RequestID"].int!
            btnApprove.titleLabel!.font =  UIFont(name:"HelveticaNeue-Bold", size: 14.0)
            btnApprove.setFAText(prefixText: "", icon: FAType.FACheck, postfixText: "  Approve " , size: 14 , forState: .Normal )
            btnApprove.backgroundColor = UIColor(hexString: "#48C9B0")
            //btnApprove.layer.cornerRadius=5
            btnApprove.setFATitleColor(UIColor.whiteColor())
            
            AttendanceAdjView.addSubview(btnApprove)
            
            
            let btnReject : UIButton = UIButton(frame: CGRectMake(AttendanceAdjView.frame.width / 100 * 52,150, AttendanceAdjView.frame.width / 100 * 44, 30	))
            
            btnReject.layer.borderWidth = 0
            btnReject.setTitle("Reject", forState: UIControlState.Normal)
            
            btnReject.addTarget(self, action: #selector(TADashBoardVC.RejectTouch(_:)), forControlEvents: .TouchUpInside)
            btnReject.tag = self.AttendanceApprovalJSON[i]["RequestID"].int!
            btnReject.titleLabel!.font =  UIFont(name:"HelveticaNeue-Bold", size: 14.0)
            btnReject.setFAText(prefixText: "", icon: FAType.FATimes, postfixText: "  Reject " , size: 14 , forState: .Normal )
            //btnReject.layer.cornerRadius=5
            btnReject.backgroundColor = UIColor(hexString: "#F46868")
            btnReject.setFATitleColor(UIColor.whiteColor())
            
            AttendanceAdjView.addSubview(btnReject)
            
            
            
            
            xbase += AttendanceAdjView.frame.width  + 20

            
        }
    
    
                CountMore += 1
    
    
            }
            
            
            
        }
        
        // Bind Load More Tiles
        if CountMore > 5
        {
            let AttendanceAdjView=UIView(frame: CGRectMake(xbase, 0, scroll.frame.width / 100 * 80 , scroll.frame.height))
            AttendanceAdjView.backgroundColor = UIColor(hexString: "#e9e9e9")
            scroll.addSubview(AttendanceAdjView)
            let btnButton : UIButton = UIButton(frame: CGRectMake(AttendanceAdjView.frame.width / 100 * 25  ,AttendanceAdjView.frame.height / 100 * 45 , AttendanceAdjView.frame.width / 100 * 50 , AttendanceAdjView.frame.height / 100 * 16 ))
            btnButton.backgroundColor = UIColor(hexString: "#ffffff")
            btnButton.layer.cornerRadius = 5
            btnButton.layer.borderWidth = 1
            btnButton.titleLabel!.font = UIFont(name: "helvetica-bold", size: 12)
            btnButton.layer.borderColor = UIColor(hexString: "#FFFFFF")!.CGColor
            btnButton.addTarget(self, action: "ShowTeamAdjList:", forControlEvents: .TouchUpInside)
            btnButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            btnButton.setTitle("Load More...", forState: UIControlState.Normal)
            AttendanceAdjView.addSubview(btnButton)
            xbase += AttendanceAdjView.frame.width  + 35
            
            
        }
        
        scroll.contentSize = CGSizeMake(xbase, scroll.frame.size.height)
        
   
        
             return cell
        
    }
    
    func ShowADJDetail(sender: UIButton) {
        
        let  objAdjustmentDetails = AdjustmentDetails()
        
        objAdjustmentDetails.AdjDetails = GetAdjRequestArray(sender.tag)        
        objAdjustmentDetails.Mode = 1
        self.presentViewController(objAdjustmentDetails, animated: true, completion: nil)
        
        
        
        
    }
    
    func GetAdjRequestArray(i:Int) -> AttendanceAdjustmentRequest
    {
            
            let madj = AttendanceAdjustmentRequest()
            madj.RequestID = AttendanceApprovalJSON[i]["RequestID"].int
            madj.EmployeePhoto  = AttendanceApprovalJSON[i]["EmployeePhoto"].string
            madj.EmployeeName  = AttendanceApprovalJSON[i]["EmployeeName"].string
            madj.PositionName  = AttendanceApprovalJSON[i]["PositionName"].string
            madj.DateLineDesc  = AttendanceApprovalJSON[i]["DateLineDesc"].string
            madj.WorkLocationName  = AttendanceApprovalJSON[i]["WorkLocationName"].string
            madj.StatusDesc  = AttendanceApprovalJSON[i]["StatusDesc"].string
            madj.TotalHours  = AttendanceApprovalJSON[i]["TotalHours"].string
            madj.DateLine  = AttendanceApprovalJSON[i]["DateLine"].string
            madj.Remarks  = AttendanceApprovalJSON[i]["Remarks"].string
            madj.UserID  = AttendanceApprovalJSON[i]["UserID"].int
            madj.Status  = AttendanceApprovalJSON[i]["Status"].int
            madj.Order  = AttendanceApprovalJSON[i]["Order"].int
            
        
        
        return madj
    }
    
    
    func FetEmployeeApprovalAdj() -> Void
    {
        
        
        
        let defaults = NSUserDefaults.standardUserDefaults()
        
        
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            objCallBack.MethodName = "GetMyTeamTimeRequestsforQI"
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objCallBack.post(ApprovalAdjListOncomplete, OnError: ApprovalAdjListOnErrorOnError)
            
        }
        
    }
    
    //Response on error callback
    func ApprovalAdjListOnErrorOnError(Response: NSError ){
        
    }
    
    
    func ApprovalAdjListOncomplete (ResultString :NSString? ) {
        
        dispatch_async(dispatch_get_main_queue(),{
            self.objLBActivityIndicaor.stopAnimating()
            if let tempdata = self.objCallBack.GetJSONData(ResultString)
            {
                
                let lbl_Approval : UILabel = UILabel(frame: CGRectMake(10, 400, self.view.frame.size.width,18))
                lbl_Approval.backgroundColor = UIColor.whiteColor()
                lbl_Approval.textAlignment = .Left
                lbl_Approval.font = UIFont(name:"helvetica-bold", size: 13)
                lbl_Approval.text = "Time Adjustment Approval"
                
                
                
                self.vscrollView.addSubview(lbl_Approval)
                
                let btnViewAll : UIButton = UIButton(frame: CGRectMake(self.view.frame.size.width / 100 * 70 , 400, self.view.frame.size.width / 100 * 23,20))
                btnViewAll.backgroundColor = UIColor(hexString: "#2190ea")
                btnViewAll.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                
                btnViewAll.titleLabel!.font =  UIFont(name:"helvetica", size: 12)
                btnViewAll.setTitle("View All", forState: .Normal)
                btnViewAll.addTarget(self, action: "ShowTeamAdjList:", forControlEvents: .TouchUpInside)
                // btnViewAll.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
                self.vscrollView.addSubview(btnViewAll)
                
                self.AttendanceApprovalJSON = JSON(data: self.objCallBack.GetJSONData(ResultString)!)
                
                if self.dAttendanceAdjCollectionView != nil {
                    self.dAttendanceAdjCollectionView.removeFromSuperview()
                }
                
                
                
                let dCollectionViewFlowLayout : UICollectionViewFlowLayout = UICollectionViewFlowLayout ()
                self.dAttendanceAdjCollectionView = UICollectionView (frame: self.view.frame, collectionViewLayout: dCollectionViewFlowLayout)
                self.dAttendanceAdjCollectionView.delegate = self
                self.dAttendanceAdjCollectionView.dataSource = self
                self.dAttendanceAdjCollectionView.backgroundColor = UIColor.blackColor()
                self.dAttendanceAdjCollectionView.scrollEnabled=false
                self.dAttendanceAdjCollectionView.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "cellIdentifierAdj")
                self.vscrollView.addSubview(self.dAttendanceAdjCollectionView)
                
                
                self.dAttendanceAdjCollectionView.frame = CGRectMake(0,445, self.view.frame.size.width,200)
            }else
            {
                self.AttendanceApprovalJSON = nil
                if(self.dAttendanceAdjCollectionView != nil)
                {
                    self.dAttendanceAdjCollectionView.removeFromSuperview()
                }
            }
            
            
        })
        
        
        
        
    }
    
    //Handles Adj approval event
    func ApproveTouch(sender: UIButton) {
        
        objLRActivityIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
        objLRActivityIndicaor.activityIndicatorViewStyle = .WhiteLarge
        let objPosition =  self.view.center
        //  objPosition.y =  objPosition.y.advancedBy(-40.0)
        objLRActivityIndicaor.center = objPosition
        //objActivityIndicaor.transform = CGAffineTransformMakeScale(1.5, 1.5);
        objLRActivityIndicaor.hidesWhenStopped = true
        objLRActivityIndicaor.color = UIColor.grayColor()
        self.view.addSubview(objLRActivityIndicaor)
        objLRActivityIndicaor.startAnimating()
        self.ProcessAttendanceAdjRequest("1", pReqeustID: sender.tag)
        
    }
    
    //Handles Adj reject event
    func RejectTouch(sender: UIButton) {
        
        objLRActivityIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
        
        objLRActivityIndicaor.activityIndicatorViewStyle = .WhiteLarge
        let objPosition =  self.view.center
        objLRActivityIndicaor.center = objPosition
        objLRActivityIndicaor.hidesWhenStopped = true
        objLRActivityIndicaor.color = UIColor.grayColor()
        self.view.addSubview(objLRActivityIndicaor)
        objLRActivityIndicaor.startAnimating()
        self.ProcessAttendanceAdjRequest("2", pReqeustID: sender.tag)
        
    }
    func ProcessAttendanceAdjRequest(ActionCode: String , pReqeustID : Int)
    {
        
        self.CurrentActionCode = ActionCode
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            
            objCallBackAP = AjaxCallBack()
            let ResultJSON = objCallBackAP.DeserializeJSONString(UserInfo)
            
            
            objCallBackAP.MethodName = "ProcessAttendanceNote"
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBackAP.ParameterList.append(objparam)
            
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBackAP.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pRequestID",value: String(pReqeustID))
            objCallBackAP.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pAction",value: ActionCode)
            objCallBackAP.ParameterList.append(objparam)
            
         
            
            
            
            objCallBackAP.post(ApproveOnSuccess, OnError: ApproveOnError)
            
            
            
        }
        
    }
    
    //Response on error callback
    func ApproveOnError(Response: NSError ){
        
        
    }
    
    //Respone on success callback
    func ApproveOnSuccess(ResultString :NSString? ) {
        
        dispatch_async(dispatch_get_main_queue(),{
            self.objLRActivityIndicaor.stopAnimating()
            DoneHUD.showInView(self.view, message: "Done")
            self.FetEmployeeApprovalAdj()
             self.dAttendanceAdjCollectionView.reloadData()
           
        })
        
    }
    
    


}

 