//
//  ClaimItemDetails.swift
//  Talentoz
//
//  Created by forziamac on 12/05/16.
//  Copyright © 2016 Forzia Tech Private Ltd. All rights reserved.
//

import UIKit

class ClaimItemDetails: BaseTabBarVC , UITableViewDataSource ,UITableViewDelegate {
    //# MARK: Local Variables
    var ClaimItemsArray = [ClaimDetailsItem]()
    var ClaimItemViewListRef:UITableView!
    let UserInfo = NSUserDefaults.standardUserDefaults()
    var objActivityIndicaor : UIActivityIndicatorView!
    
    var FilterView : UIView!
    var FilterBGLayer : UIView!
    var isFilterOpen = false
    var MultiAttachment : Talentoz.JSON!
    
    //# MARK: Public Variables
    internal var ClaimJSON : Talentoz.JSON!

     //# MARK: Default Methods
    override func viewDidLoad() {
        super.UINavigationBarTitle = "Select Claim for submission"
        super.viewDidLoad()
        
        GetClaimItemArray()
        BindControls()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
      //# MARK: Custom Methods
    func GetClaimItemArray()
    {
        
        for i in 0 ..< ClaimJSON["CliamItemList"].count  {
            
            let iClaimItem = ClaimDetailsItem()
            
            iClaimItem.ClientID = ClaimJSON["CliamItemList"][i]["ClientID"].int
            iClaimItem.UserID  = ClaimJSON["CliamItemList"][i]["UserID"].int
            iClaimItem.ClaimTypeID  = ClaimJSON["CliamItemList"][i]["ClaimTypeID"].int
            iClaimItem.ItemID  = ClaimJSON["CliamItemList"][i]["ItemID"].int
            iClaimItem.CurrencyID  = ClaimJSON["CliamItemList"][i]["CurrencyID"].int
            iClaimItem.Amount  = ConvertValueWithDecimal(String(ClaimJSON["CliamItemList"][i]["Amount"]),NoofDecimal: 2)
            iClaimItem.ClaimDateString  = ClaimJSON["CliamItemList"][i]["ClaimDateString"].string
            iClaimItem.ClaimItemsAttachment  = String(ClaimJSON["CliamItemList"][i]["ClaimItemsAttachments"].count) + " Attachment(s)"
            
            iClaimItem.ClaimTypeName = ClaimJSON["CliamItemList"][i]["ClaimTypeName"].string
            iClaimItem.CurrencyName = ClaimJSON["CliamItemList"][i]["CurrencyName"].string
            iClaimItem.Description = ClaimJSON["CliamItemList"][i]["Description"].string
            iClaimItem.ProjectName = ClaimJSON["CliamItemList"][i]["ProjectName"].string
            iClaimItem.ProjectID = ClaimJSON["CliamItemList"][i]["ProjectID"].int
            iClaimItem.Checked = true
         
            
            ClaimItemsArray.append(iClaimItem)
            
        }
    }
    
    
    func BindControls() {
        
        let totalamountviews : UIView = UIView(frame: CGRectMake(0, 60, Viewframe.width ,60))
        totalamountviews.backgroundColor = UIColor.whiteColor();
        
         self.view.addSubview(totalamountviews)
        
        let lbl_CurrencySymbol : UILabel = UILabel(frame: CGRectMake(Viewframe.width / 100 * 6, 10, Viewframe.width / 100 * 15 ,35))
        lbl_CurrencySymbol.textAlignment = .Left
        lbl_CurrencySymbol.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
        lbl_CurrencySymbol.textColor = UIColor(hexString: String("#666666"))
        lbl_CurrencySymbol.text = self.ClaimJSON["CurrencyName"].string
        
       totalamountviews.addSubview(lbl_CurrencySymbol)
        
        let lbl_ClaimedAmount : UILabel = UILabel(frame: CGRectMake(Viewframe.width / 100 * 21, 10, Viewframe.width / 100 * 45 ,35))
        lbl_ClaimedAmount.textAlignment = .Left
        lbl_ClaimedAmount.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
        lbl_ClaimedAmount.textColor = UIColor(hexString: String("#666666"))
        lbl_ClaimedAmount.text = ConvertValueWithDecimal(String(self.ClaimJSON["TotalAmount"]),NoofDecimal: 2)
        totalamountviews.addSubview(lbl_ClaimedAmount)
        
        
        let btnSubmit : UIButton = UIButton(frame: CGRectMake(Viewframe.width / 100 * 77, 20, Viewframe.width / 100 * 20 ,20))
        btnSubmit.backgroundColor = UIColor(hexString: String("#2190ea"))
        btnSubmit.titleLabel?.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
        btnSubmit.setTitle("Submit", forState: .Normal)
        btnSubmit.addTarget(self, action: #selector(ClaimItemDetails.Submit_Click(_:)), forControlEvents: .TouchUpInside)
        btnSubmit.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        totalamountviews.addSubview(btnSubmit)
        
        let ClaimItemViewList: UITableView = UITableView(frame: CGRectMake(0,110, Viewframe.width, Viewframe.height - 185 ))
        ClaimItemViewList.showsVerticalScrollIndicator = true
        ClaimItemViewList.delegate = self
        ClaimItemViewList.dataSource = self
        ClaimItemViewList.rowHeight = 130
        ClaimItemViewList.backgroundColor = UIColor(hexString: "#f5f5f5")
        ClaimItemViewList.separatorColor = UIColor.clearColor()
       // let blurEffect = UIBlurEffect(style: .Light)
      //  let blurEffectView = UIVisualEffectView(effect: blurEffect)
       // ClaimItemViewList.backgroundView = blurEffectView
        //if you want translucent vibrant table view separator lines
       // ClaimItemViewList.separatorEffect = UIVibrancyEffect(forBlurEffect: blurEffect)
        self.view.addSubview(ClaimItemViewList )
        self.ClaimItemViewListRef = ClaimItemViewList

        
    }
    
    func Submit_Click(sender: UIButton)  {
        
        if self.CheckValidate() == 1
        {
            let objFinalView = ClaimsFinalSubmit()
            objFinalView.ClaimItemsArray = self.ClaimItemsArray
            self.presentViewController(objFinalView, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "Required", message: "Please choose atleast one claim.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.presentViewController(alert, animated: true, completion: nil)}
            
        }
    }
    
    func numberOfSectionsInTableView(ClaimItemViewListRef: UITableView) -> Int {
        return 1
    }
    
    func tableView(ClaimItemViewListRef: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ClaimItemsArray.count
        
    }
    
    func tableView(ClaimItemViewListRef: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        cell.backgroundColor = UIColor(hexString: "#f5f5f5")
        
        let ContentVw: UIView = UIView(frame: CGRectMake( Viewframe.width / 100 * 2, 10, Viewframe.width / 100 * 96, 120	 ))
        cell.addSubview(ContentVw)
        ContentVw.layer.borderWidth = 1
        ContentVw.backgroundColor = UIColor.whiteColor()
        ContentVw.layer.borderColor = UIColor(hexString: "#eeeeee")!.CGColor
        ContentVw.layer.shadowRadius = 2
        ContentVw.layer.shadowOpacity = 0.7
        ContentVw.layer.shadowColor = UIColor(hexString: "#dddddd")?.CGColor
        ContentVw.layer.shadowRadius = 5
     
        
        
       
        let CheckBoxVw: CheckBox = CheckBox(frame: CGRectMake(Viewframe.width / 100 * 85  , 10, 20,20))
        CheckBoxVw.tag = self.ClaimItemsArray[indexPath.row].ItemID
        if self.ClaimItemsArray[indexPath.row].Checked == true
        {
            CheckBoxVw.isChecked = true
        }
        else
        {
             CheckBoxVw.isChecked = false
        }
        CheckBoxVw.OnCompletion = OnChecked
       // CheckBoxVw.addTarget(self, action: Selector("OnChecked"), forControlEvents: .TouchUpInside)
        ContentVw.addSubview(CheckBoxVw)
       
        let lblClaimType : UILabel = UILabel(frame: CGRectMake(10  , 5, Viewframe.width,20))
        lblClaimType.textColor = UIColor(hexString: "#666666")
        lblClaimType.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
        lblClaimType.text = self.ClaimItemsArray[indexPath.row].ClaimTypeName
        ContentVw.addSubview(lblClaimType)
        
        if (self.ClaimItemsArray[indexPath.row].ProjectID > 0)
        {
        let lblProjectName : UILabel = UILabel(frame: CGRectMake(10  , 25, Viewframe.width ,20))
        lblProjectName.textColor = UIColor(hexString: "#666666")
        lblProjectName.font = UIFont(name:"HelveticaNeue", size: 14.0)
        lblProjectName.setFAText(prefixText: "", icon: FAType.FASuitcase, postfixText: " " + self.ClaimItemsArray[indexPath.row].ProjectName, size: 12.0, iconSize: 12.0)
        ContentVw.addSubview(lblProjectName)
        
        let lblClaimDescription : UILabel = UILabel(frame: CGRectMake(10  , 45, Viewframe.width / 100 * 90  ,20))
        lblClaimDescription.textColor = UIColor(hexString: "#666666")
        lblClaimDescription.font = UIFont(name:"HelveticaNeue", size: 14.0)
        lblClaimDescription.text = self.ClaimItemsArray[indexPath.row].Description
        ContentVw.addSubview(lblClaimDescription)
        }
        else
        {
            let lblClaimDescription : UILabel = UILabel(frame: CGRectMake(10  , 25, Viewframe.width  ,40))
            lblClaimDescription.textColor = UIColor(hexString: "#666666")
            lblClaimDescription.lineBreakMode = .ByWordWrapping
            lblClaimDescription.font = UIFont(name:"HelveticaNeue", size: 14.0)
            lblClaimDescription.text = self.ClaimItemsArray[indexPath.row].Description
            ContentVw.addSubview(lblClaimDescription)
            
        }
        
        if( self.ClaimItemsArray[indexPath.row].ClaimItemsAttachment != "0 Attachment(s)")
        {
        
        let lblClaimAttachFile : UILabel = UILabel(frame: CGRectMake(10 , 65, Viewframe.width / 100 * 5  ,20))
        lblClaimAttachFile.textColor = UIColor(hexString: "#666666")
        lblClaimAttachFile.font = UIFont(name:"HelveticaNeue", size: 12.0)
        lblClaimAttachFile.setFAIcon(FAType.FAPaperclip, iconSize: 14.0)
        ContentVw.addSubview(lblClaimAttachFile)
        
        let btnDownload : UIButton = UIButton(frame: CGRectMake(Viewframe.width / 100 * 7   , 65, Viewframe.width / 100 * 90  ,20))
        btnDownload.setTitle( " " + self.ClaimItemsArray[indexPath.row].ClaimItemsAttachment , forState: .Normal)
        btnDownload.setTitleColor(UIColor(hexString: "#2190ea"), forState: .Normal)
        btnDownload.titleLabel!.font =  UIFont(name:"HelveticaNeue", size: 14.0)
        btnDownload.tag = indexPath.row
        btnDownload.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        btnDownload.contentVerticalAlignment = UIControlContentVerticalAlignment.Center
        btnDownload.addTarget(self, action: #selector(ClaimItemDetails.DownLoadModal(_:)), forControlEvents: .TouchUpInside)
        ContentVw.addSubview(btnDownload)
        }
        
        let vwline3 : UIView = UIView(frame: CGRectMake(10 ,  90 , Viewframe.width / 100 * 87,1))
        vwline3.layer.borderColor = UIColor(hexString: "#eeeeee")?.CGColor
        vwline3.layer.borderWidth = 1
        ContentVw.addSubview(vwline3)
        
        
        let lblClaimItemDate : UILabel = UILabel(frame: CGRectMake(10  , 95, Viewframe.width / 100 * 40  ,20))
        lblClaimItemDate.textColor = UIColor(hexString: "#666666")
        lblClaimItemDate.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
        
        ContentVw.addSubview(lblClaimItemDate)
        lblClaimItemDate.setFAText(prefixText: "", icon: FAType.FACalendar, postfixText: "  " + self.ClaimItemsArray[indexPath.row].ClaimDateString, size: 12.0, iconSize: 14.0)
        
        
        let lblClaimCurrSymbol : UILabel = UILabel(frame: CGRectMake(Viewframe.width / 100 * 54 , 95, Viewframe.width / 100 * 15 ,20))
        lblClaimCurrSymbol.textColor = UIColor(hexString: "#2190ea")
        lblClaimCurrSymbol.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
        lblClaimCurrSymbol.text = self.ClaimItemsArray[indexPath.row].CurrencyName
        ContentVw.addSubview(lblClaimCurrSymbol)
        
        
        let lblClaimItemAmount : UILabel = UILabel(frame: CGRectMake(Viewframe.width / 100 * 68 , 95, Viewframe.width / 100 * 30 ,20))
        lblClaimItemAmount.textColor = UIColor(hexString: "#2190ea")
        lblClaimItemAmount.font = UIFont(name:"HelveticaNeue-Bold", size: 20.0)
        lblClaimItemAmount.text = ConvertValueWithDecimal(self.ClaimItemsArray[indexPath.row].Amount,NoofDecimal: 2)
        ContentVw.addSubview(lblClaimItemAmount)
        
        //let btnDelete : UIButton = UIButton(frame: CGRectMake(ClaimItemViewListRef.frame.size.width / 100 * 75 , 100, ClaimItemViewListRef.frame.size.width / 100 * 10 ,20))
       // btnDelete.setTitle("Delete", forState: .Normal)
       // btnDelete.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
       // btnDelete.setFAIcon(FAType.FATrashO, iconSize: 20, forState: .Normal)
       // btnDelete.tag
       // cell.addSubview(btnDelete)
        
        
        
        
        return cell
    }
    
    func OnChecked(sender:UIButton){
        
        for i in 0 ..< self.ClaimItemsArray.count  {
            
            if(sender.tag == self.ClaimItemsArray[i].ItemID)
            {
                if(sender.currentImage == UIImage(named: "checked_checkbox"))
                {
                    self.ClaimItemsArray[i].Checked = true
                }
                else
                {
                    self.ClaimItemsArray[i].Checked = false
                }
                return
            }
        }
        
    }
    
    func CheckValidate() -> Int {
        var iResult:Int = 0
        
        for i in 0 ..< self.ClaimItemsArray.count  {
            if(self.ClaimItemsArray[i].Checked == true)
            {
                iResult = 1
                return iResult
            }

        }
        return iResult
    }
    
    func DownLoadModal(sender:UIButton) {
        
        self.MultiAttachment = ClaimJSON["CliamItemList"][sender.tag]["ClaimItemsAttachments"]
        if( self.MultiAttachment.count > 1 )
        {
            FilterBGLayer = UIView(frame: CGRectMake(0, 0, Viewframe.width  ,  Viewframe.height))
            FilterBGLayer.backgroundColor = UIColor(colorLiteralRed: 0 , green: 0, blue: 0, alpha: 0.6)
            let GestureLeaveDetail1 = UITapGestureRecognizer(target: self, action: #selector(ClaimItemDetails.DismissFilter(_:)))
           FilterBGLayer.addGestureRecognizer(GestureLeaveDetail1)
            FilterView = UIView(frame: CGRectMake(FilterBGLayer.frame.width / 100 * 10 , FilterBGLayer.frame.height  / 100 * 25 , FilterBGLayer.frame.width  / 100 * 80, FilterBGLayer.frame.height  / 100 * 60))
            FilterView.backgroundColor = UIColor.whiteColor()
            FilterView.layer.cornerRadius = 8
            
            let AttachScroll : UIScrollView = UIScrollView(frame: CGRectMake(0,  10, FilterView.frame.width, 300))
            FilterView.addSubview(AttachScroll )
            
            AttachScroll.contentSize = CGSize(  width: FilterView.frame.width, height: 350)
            
            
            let AttachmentListView : UIView = UIView(frame: CGRectMake(AttachScroll.frame.width / 100 * 2, 5 , AttachScroll.frame.width / 100 * 96 , CGFloat(self.MultiAttachment.count * 33) ))
            AttachScroll.addSubview(AttachmentListView )
            
            var HeightCal = CGFloat(5)
            
            for i in 0..<self.MultiAttachment.count {
                
                let AttachmentListViewInn : UIView = UIView(frame: CGRectMake(AttachScroll.frame.width / 100 * 2,  HeightCal, AttachScroll.frame.width / 100 * 96 , 30 ))
                AttachmentListView.addSubview(AttachmentListViewInn )
                
                let lbl_Approval : UILabel = UILabel(frame: CGRectMake(AttachmentListView.frame.size.width / 100 * 2 , 4 , AttachmentListView.frame.size.width / 100 * 60 ,20))
                lbl_Approval.backgroundColor = UIColor.clearColor()
                lbl_Approval.textAlignment = .Left
                lbl_Approval.font = UIFont(name:"HelveticaNeue", size: 14.0)
                lbl_Approval.textColor = UIColor(hexString: "#333333")
                lbl_Approval.text = "Attached File" + String((i+1))
                AttachmentListViewInn.addSubview(lbl_Approval)
                
                let btn_ShowDetail : UIButton = UIButton(frame: CGRectMake(AttachmentListView.frame.size.width / 100 * 50 , 2, AttachmentListView.frame.size.width / 100 * 40	, 20))
                btn_ShowDetail.backgroundColor = UIColor(hexString: "#dddddd")
                
                
                btn_ShowDetail.titleLabel?.font = UIFont(name: "HelveticaNeue-bold", size: 12)
                
                
                
                
                 btn_ShowDetail.setFAText(prefixText: "", icon: FAType.FADownload, postfixText:  "  Download", size: 12 , forState: .Normal)
                
                btn_ShowDetail.layer.cornerRadius = 5
                btn_ShowDetail.tag = i
                btn_ShowDetail.addTarget(self, action: #selector(ClaimItemDetails.DownloadFromList(_:)), forControlEvents: .TouchUpInside)
                btn_ShowDetail.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                AttachmentListViewInn.addSubview(btn_ShowDetail)
                
                
                HeightCal = HeightCal + 40
            }
            
            
            
            
            
            
            
            let btn_ApplyFilter : UIButton = UIButton(frame: CGRectMake(FilterBGLayer.frame.width / 100 * 22, FilterView.frame.size.height - 50 , 120 ,24))
            btn_ApplyFilter.setTitle("Dismiss", forState: .Normal)
            btn_ApplyFilter.layer.cornerRadius = 5
            btn_ApplyFilter.tag = sender.tag
            btn_ApplyFilter.backgroundColor = UIColor(hexString: "#2190ea")
            btn_ApplyFilter.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            btn_ApplyFilter.titleLabel!.font =  UIFont(name:"helvetica-Bold", size: 12)
            btn_ApplyFilter.addTarget(self, action:#selector(ClaimItemDetails.DismissFilter(_:)), forControlEvents: .TouchUpInside)
            FilterView.addSubview(btn_ApplyFilter)
            
            
            FilterBGLayer.addSubview(FilterView)
            self.view.addSubview(FilterBGLayer)
        }
        else
        {
            
             let FileName = String(ClaimJSON["CliamItemList"][sender.tag]["ClaimItemsAttachments"][0]["EncodeFileName"])
            self.DownLoadFile(FileName)
            
            
        }

    }
    
    func DismissFilter(sender: UIButton)
    {
        isFilterOpen = false
        FilterBGLayer.removeFromSuperview()
        
    }
    
    func DownloadFromList(sender:UIButton)
    {
        let FileName = String(self.MultiAttachment[sender.tag]["EncodeFileName"])
        self.DownLoadFile(FileName)
    }
    
    func DownLoadFile(FilePathURL:String)
    {
        
        if let tmpDomainURL = self.UserInfo.stringForKey("DomainURL")
        {
            objActivityIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, Viewframe.width, self.view.frame.size.height))
            objActivityIndicaor.activityIndicatorViewStyle = .WhiteLarge
            objActivityIndicaor.center = self.view.center
            objActivityIndicaor.transform = CGAffineTransformMakeScale(1.5, 1.5);
            objActivityIndicaor.hidesWhenStopped = true
            objActivityIndicaor.color = UIColor.grayColor()
            self.view.addSubview(objActivityIndicaor)
            objActivityIndicaor.startAnimating()
            
            let URLStr = "http://" + tmpDomainURL + "/WorkFlowAttachment/Client" + String(self.UserInfo.stringForKey("ClientID")!) + "/" + FilePathURL
            
            var image: UIImage?
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                image =  UIImage(data: NSData(contentsOfURL: NSURL(string:URLStr)!)!)
                dispatch_async(dispatch_get_main_queue()) {
                    
                    UIImageWriteToSavedPhotosAlbum(image! , self,nil, nil)
                     self.objActivityIndicaor.stopAnimating()
                    
                    let alert: UIAlertView = UIAlertView(title: "", message: "Download completed.", delegate: nil, cancelButtonTitle: nil);
                    alert.show()
                    
                    // Delay the dismissal by 5 seconds
                    let delay = 2.0 * Double(NSEC_PER_SEC)
                    let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
                    dispatch_after(time, dispatch_get_main_queue(), {
                        alert.dismissWithClickedButtonIndex(-1, animated: true)
                    })
                }
            }
        }

    }
    
    
    

}
