//
//  IrregularityInfo.swift
//  Talentoz
//
//  Created by forziamac on 10/02/16.
//  Copyright © 2016 Forzia Tech Private Ltd. All rights reserved.
//


import UIKit

class IrregularityListB : UITableView, UITableViewDataSource , UITableViewDelegate  {
   
    
    //# MARK: Public Variables
    internal var ActivityDate : String!
    internal var Mode : Int!//1->Tardiness,2->Undertime
    internal var MainView : UIView!
    
    //# MARK: Local Variables
    var objLoadingIndi : UIActivityIndicatorView!
    var objCallBack : AjaxCallBack!
    let defaults = NSUserDefaults.standardUserDefaults()
    var Irregularities : Talentoz.JSON!
    var ActivityListView = UITableView()
    
    //# MARK: Custom Methods
    func GetIrregularityList()
    {
        
        objCallBack = AjaxCallBack()
        objCallBack.MethodName = "GetTardinessandUndertimeDetails"
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pMode",value: String(self.Mode))
            objCallBack.ParameterList.append(objparam)
            
            
            objCallBack.post(GetIrregularityListOnComplete, OnError: GetIrregularityListOnError)
            
        }
        
    }
    
    func GetIrregularityListOnError(Response: NSError ){
        
    }
    
    func GetIrregularityListOnComplete (ResultString :NSString? ) {
        dispatch_async(dispatch_get_main_queue(),{
            
            
            self.Irregularities = JSON(data: self.objCallBack.GetJSONData(ResultString)!)
            
            
            let lbl_TitleHeader : UILabel = UILabel(frame: CGRectMake(0  , 0, self.MainView.frame.size.width ,30))
            lbl_TitleHeader.backgroundColor = UIColor.clearColor()
            lbl_TitleHeader.textAlignment = .Center
            lbl_TitleHeader.font = UIFont(name:"HelveticaNeue", size: 12.0)
            lbl_TitleHeader.textColor = UIColor(hexString: "#666666")
            if self.Mode == 1
            {
                lbl_TitleHeader.text = "Tardiness Report"
            }
            else
            {
                lbl_TitleHeader.text = "Undertime Report"
            }
            
            self.MainView.addSubview(lbl_TitleHeader)
        
            
            
            
            let bottomborderview : UIView = UIView(frame: CGRectMake(self.MainView.frame.size.width / 100 * 2  , 32, self.MainView.frame.size.width / 100 * 96 ,1))
            bottomborderview.layer.borderWidth = 1
            bottomborderview.layer.borderColor = UIColor(hexString: "#e5e5e5")?.CGColor
            self.MainView.addSubview(bottomborderview)
            
            
            
            let lbl_Date : UILabel = UILabel(frame: CGRectMake(self.MainView.frame.size.width / 100 * 5  , 35, self.MainView.frame.size.width / 100 * 45 ,20))
            lbl_Date.backgroundColor = UIColor(hexString:"#2190ea")
            lbl_Date.textAlignment = .Center
            lbl_Date.font = UIFont(name:"HelveticaNeue-bold", size: 10.0)
            lbl_Date.textColor = UIColor.whiteColor()
            lbl_Date.text = "Date"
            self.MainView.addSubview(lbl_Date)
            
            
            let lbl_Hours : UILabel = UILabel(frame: CGRectMake(self.MainView.frame.size.width / 100 * 50, 35, self.MainView.frame.size.width / 100 * 45 ,20))
            lbl_Hours.backgroundColor = UIColor(hexString:"#2190ea")
            lbl_Hours.textAlignment = .Center
            lbl_Hours.textColor = UIColor.whiteColor()
            lbl_Hours.font = UIFont(name:"HelveticaNeue-bold", size: 10.0)
            lbl_Hours.text = "Hours"
            self.MainView.addSubview(lbl_Hours)
            
            //Table view height calculate by records count
            var CellHeight : CGFloat!
            if self.Irregularities.count > 0
            {
                if self.Irregularities.count > 10
                {
                    CellHeight = 25 * 10
                }
                else
                {
                    CellHeight = 25 * CGFloat(self.Irregularities.count + 1)
                }
            }
            else
            {
                CellHeight = 25
            }
            
            
            
            
            self.ActivityListView = UITableView(frame: CGRectMake(self.MainView.frame.width / 100 * 5 , 55 , self.MainView.frame.width / 100 * 90 , CellHeight ))
            self.ActivityListView.showsVerticalScrollIndicator = true
            self.ActivityListView.delegate = self
            self.ActivityListView .dataSource = self
            self.ActivityListView.rowHeight = 25
            self.ActivityListView.separatorColor = UIColor.whiteColor()
            self.ActivityListView.backgroundColor = UIColor.clearColor()
            self.MainView.addSubview(self.ActivityListView)
            
            let lbl_Totaltxt : UILabel = UILabel(frame: CGRectMake(self.MainView.frame.size.width / 100 * 20  , self.ActivityListView.frame.height + 65.0, self.MainView.frame.size.width / 100 * 30 ,20))
            lbl_Totaltxt.backgroundColor = UIColor.clearColor()
            lbl_Totaltxt.textAlignment = .Right
            lbl_Totaltxt.font = UIFont(name:"HelveticaNeue-bold", size: 12.0)
            lbl_Totaltxt.textColor = UIColor(hexString: "#666666")

            lbl_Totaltxt.text = "Total Hours  : "
            self.MainView.addSubview(lbl_Totaltxt)
            
            
            let lbl_Totalval : UILabel = UILabel(frame: CGRectMake(self.MainView.frame.size.width / 100 * 52, self.ActivityListView.frame.height + 65.0, self.MainView.frame.size.width / 100 * 45 ,20))
            lbl_Totalval.backgroundColor = UIColor.clearColor()
            lbl_Totalval.textColor = UIColor(hexString: "#333333")
            lbl_Totalval.textAlignment = .Center
            lbl_Totalval.font = UIFont(name:"HelveticaNeue-Bold", size: 12.0)
            lbl_Totalval.text = self.MinuteToHoursString(self.CalculateSummaryHours())
            self.MainView.addSubview(lbl_Totalval)
      
            
            
        })
        
        
    }
    
    //Sum the total hours from source object
    func CalculateSummaryHours()-> Int
    {
        var TotalHour : Int!=0
        for var i = 0; i < self.Irregularities.count ; i++ {
            if self.Mode == 1
            {
                if self.Irregularities[i]["Tardiness"] != nil
                {
                    TotalHour = TotalHour +  self.Irregularities[i]["Tardiness"].int!
                }
            }
            else
            {
                if self.Irregularities[i]["Undertime"] != nil
                {
                    TotalHour = TotalHour +  self.Irregularities[i]["Undertime"].int!
                }
            }
        }
        
        return TotalHour
    }
    
    func MinuteToHoursString(MinVal:Int)-> String
    {
        
        var Hour : Int!
        var Minute : Int!
        
        var HourStr : String!
        var MinuteStr : String!
        
        Minute = MinVal % 60
        Hour = (MinVal - Minute) / 60
        
        if Hour < 10
        {
            HourStr = "0" + String(Hour)
        }
        else
        {
            HourStr = String(Hour)
        }
        
        if Minute < 10
        {
            MinuteStr = "0" + String(Minute)
        }
        else
        {
            MinuteStr = String(Minute)
        }
        
        return HourStr + ":" + MinuteStr
    }
    
    
   //# MARK: Table View Methods
    func numberOfSectionsInTableView(ActivityListView: UITableView) -> Int {
        return 1
    }
    
    func tableView(ActivityListView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.Irregularities.count
        
    }
    
    func tableView(ActivityListView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        
        // Alternate color
        if indexPath.row % 2 == 0
        {
            cell.backgroundColor = UIColor.whiteColor()
        }
        else
        {
            cell.backgroundColor = UIColor(hexString: "#f5f5f5")
        }
        
        let lbl_InTime : UILabel = UILabel(frame: CGRectMake(self.ActivityListView.frame.size.width / 100 * 5  , 5, self.ActivityListView.frame.size.width / 100 * 45 ,20))
        lbl_InTime.backgroundColor = UIColor.clearColor()
        lbl_InTime.textAlignment = .Center
        lbl_InTime.font = UIFont(name:"HelveticaNeue", size: 10.0)
        lbl_InTime.textColor = UIColor(hexString: "#666666")
    
        if self.Irregularities[indexPath.row]["ReportingDate"] != nil
        {
            var arrstr = String(self.Irregularities[indexPath.row]["ReportingDate"]).componentsSeparatedByString("T")
            arrstr = arrstr[0].componentsSeparatedByString("-")
            lbl_InTime.text = String(arrstr[2]) + "/" + String(arrstr[1]) + "/" + String(arrstr[0])
        }
        cell.addSubview(lbl_InTime)
        
        
        let lbl_Hours : UILabel = UILabel(frame: CGRectMake(self.ActivityListView.frame.size.width / 100 * 50, 5, self.ActivityListView.frame.size.width / 100 * 45 ,20))
        lbl_Hours.backgroundColor = UIColor.clearColor()
        lbl_Hours.textAlignment = .Center
        lbl_Hours.textColor = UIColor(hexString: "#666666")
        lbl_Hours.font = UIFont(name:"HelveticaNeue", size: 10.0)
      
        
        if self.Mode == 1
        {
            if self.Irregularities[indexPath.row]["Tardiness"] != nil
            {
                lbl_Hours.text = self.MinuteToHoursString(self.Irregularities[indexPath.row]["Tardiness"].int!)
            }
        }
        else
        {
            if self.Irregularities[indexPath.row]["Undertime"] != nil
            {
                lbl_Hours.text = self.MinuteToHoursString(self.Irregularities[indexPath.row]["Undertime"].int!)
            }
        }
        cell.addSubview(lbl_Hours)
        
        
        return cell
    }
    
    
    
    
}



 