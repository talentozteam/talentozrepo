
//
//  AddClaim.swift
//  Talentoz
//
//  Created by forziamac on 05/05/16.
//  Copyright © 2016 Forzia Tech Private Ltd. All rights reserved.
//

import UIKit

class AddClaim: BaseTabBarVC, UIImagePickerControllerDelegate, UINavigationControllerDelegate,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    //# MARK: Control Outlets
    
    @IBOutlet var btnClaimType: UIButton!
    @IBOutlet var btnCTDownSymbol: UIButton!
    @IBOutlet var CTSeparateLineVw: UIView!
    @IBOutlet var btnCurrency: UIButton!
    @IBOutlet var btnCurrDownSymbol: UIButton!
    @IBOutlet var CurrSeparateLineVw: UIView!
    @IBOutlet var txtAmount: UITextField!
    @IBOutlet var AmtSeparateLineVw: UIView!
    
    @IBOutlet var btnDate: UIButton!
    @IBOutlet var DateDownSymbol: UIButton!
    @IBOutlet var DateSeparateLineVw: UIView!
    
    @IBOutlet var btnProject: UIButton!
    @IBOutlet var ProDownSymbol: UIButton!
    @IBOutlet var ProSeparateLineVw: UIView!
    @IBOutlet var txtDescription: UITextField!
    @IBOutlet var DescSeparateLineVw: UIView!
    @IBOutlet var lblAttachment: UILabel!
    @IBOutlet var btnAddFile: UIButton!
    @IBOutlet var btnAdd: UIButton!
    @IBOutlet var imgOutlet: UIImageView!
    @IBOutlet var innerviews: UIView!
    @IBOutlet var UIScroll: UIScrollView!
    
    @IBOutlet var MainView: UIView!
     //# MARK: Local Variables
    var ClaimType = [String]()
    var Currency = [String]()
    var Project = [String]()
    let defaults = NSUserDefaults.standardUserDefaults()
    var objCallBack = AjaxCallBack()
    var objCallBack1 = AjaxCallBack()
    var objCallBack2 = AjaxCallBack()
    
    var ClaimTypeSource : Talentoz.JSON!
    var CurrencySource : Talentoz.JSON!
    var ProjectSource : Talentoz.JSON!
    
    let ClaimTypedropDown = DropDown()
    let CurrencydropDown = DropDown()
    let ProjectdropDown = DropDown()
    
    var objActivityIndicaor : UIActivityIndicatorView!
    
    var iClaimType : Int = -1
    var iCurrency : Int = -1
    var iProject : Int = -1
    var iDate : NSDate!
    
    var imagePicker: UIImagePickerController!
    var FilterView : UIView!
    var FilterBGLayer : UIView!
    var isFilterOpen = false
    var AttachFileActualName: String! = ""
    var AttachFileGenerateName: String!  = ""
    var IsAttachmentRequired = false
    
    var AttachmentCollections = [AttachmentCollection]()
    var AttachCollectionView : UICollectionView!
    var ImageCache : AFImageCache!
    
    
    //# MARK: Default Methods
    override func viewDidLoad() {
        
        txtAmount.delegate = self
        txtDescription.delegate = self
        
        super.UINavigationBarTitle = "Add Claim"
        super.viewDidLoad()
        
        if(Viewframe.width==320)
        {
            self.UIScroll.contentSize = CGSize(  width: Viewframe.width, height: 680)
        }
        else
        {
            self.UIScroll.frame = CGRectMake(0 , 0  ,Viewframe.width, 680)
        }
        
        DesignControls()
        BindClaimType()

        // Do any additional setup after loading the view.
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //# MARK: Controls Actions
    
    @IBAction func btnClaimType_Click(sender: AnyObject) {
        if ClaimTypedropDown.hidden {
            ClaimTypedropDown.show()
        } else {
            ClaimTypedropDown.hide()
        }
    }
    
    @IBAction func btnCTDownSymbol_Click(sender: AnyObject) {
        if ClaimTypedropDown.hidden {
            ClaimTypedropDown.show()
        } else {
            ClaimTypedropDown.hide()
        }
    }
    
    @IBAction func btnCurrency_Click(sender: AnyObject) {
        if CurrencydropDown.hidden {
            CurrencydropDown.show()
        } else {
            CurrencydropDown.hide()
        }
    }
    
    @IBAction func btnCurrDownSymbol_Click(sender: AnyObject) {
        if CurrencydropDown.hidden {
            CurrencydropDown.show()
        } else {
            CurrencydropDown.hide()
        }
    }
    
    
    @IBAction func btnProject_Click(sender: AnyObject) {
        if ProjectdropDown.hidden {
            ProjectdropDown.show()
        } else {
            ProjectdropDown.hide()
        }
    }
    
    @IBAction func btnProjectDownSymbol_Click(sender: AnyObject) {
        if ProjectdropDown.hidden {
            ProjectdropDown.show()
        } else {
            ProjectdropDown.hide()
        }
    }
    
    @IBAction func btnAdd_Click(sender: AnyObject) {
        self.ValidateTheInputs()
    }
    
    @IBAction func btnAddFile_Click(sender: AnyObject) {
        ShowAttachmentOption()
    }
    
    
   
    
     //# MARK: Custom Methods
    
    func DesignControls()
    {
        
        
        self.MainView.frame = CGRectMake(0, 0  ,self.UIScroll.frame.width, self.UIScroll.frame.height)
        
        
         self.innerviews.frame = CGRectMake(self.MainView.frame.width / 100 * 3 , 70  ,self.MainView.frame.width / 100 * 94, self.MainView.frame.height)
        
        self.btnClaimType.frame = CGRectMake(self.innerviews.frame.width / 100 * 5 , 15  ,self.innerviews.frame.width / 100 * 80, 20)
        
        self.btnClaimType.backgroundColor = UIColor.clearColor()
        self.btnClaimType.layer.cornerRadius=5
        self.btnClaimType.setTitleColor(UIColor.init(hexString: "#666666"), forState: UIControlState.Normal)
     self.btnCTDownSymbol.frame = CGRectMake(self.innerviews.frame.width / 100 * 85 , 15  ,self.innerviews.frame.width / 100 * 10, 20)
           self.btnCTDownSymbol.setFAIcon(FAType.FAAngleDown, iconSize: CGFloat(20),forState: .Normal)
        self.CTSeparateLineVw.frame = CGRectMake(self.innerviews.frame.width / 100 * 5 , 35  ,self.innerviews.frame.width / 100 * 87, 1)
        
        
        
        
        
        
        self.btnCurrency.frame = CGRectMake(self.innerviews.frame.width / 100 * 5 , 60  ,self.innerviews.frame.width / 100 * 80, 20)
        self.btnCurrency.backgroundColor = UIColor.clearColor()
        self.btnCurrency.layer.cornerRadius=5
        self.btnCurrency.setTitleColor(UIColor.init(hexString: "#666666"), forState: UIControlState.Normal)
        
        self.btnCurrDownSymbol.frame = CGRectMake(self.innerviews.frame.width / 100 * 85 , 60  ,self.innerviews.frame.width / 100 * 10, 20)
         self.btnCurrDownSymbol.setFAIcon(FAType.FAAngleDown, iconSize: CGFloat(20),forState: .Normal)
        
         self.CurrSeparateLineVw.frame = CGRectMake(self.innerviews.frame.width / 100 * 5 , 80  ,self.innerviews.frame.width / 100 * 87, 1)
        
        
        
        
        
        self.txtAmount.frame = CGRectMake(self.innerviews.frame.width / 100 * 5 , 105  ,self.innerviews.frame.width / 100 * 80, 20)
        txtAmount.layer.borderWidth = 0
        txtAmount.layer.cornerRadius = 0
        txtAmount.layer.borderColor = UIColor.clearColor().CGColor
        txtAmount.textColor = UIColor(hexString: "#666666")
        txtAmount.font = UIFont(name:"HelveticaNeue-bold", size: 12.0)
        self.AmtSeparateLineVw.frame = CGRectMake(self.innerviews.frame.width / 100 * 5 , 125  ,self.innerviews.frame.width / 100 * 87, 1)
        
        
        self.btnDate.frame = CGRectMake(self.innerviews.frame.width / 100 * 5 , 145  ,self.innerviews.frame.width / 100 * 80, 20)
        self.btnDate.backgroundColor = UIColor.clearColor()
        self.btnDate.layer.cornerRadius=5
        self.btnDate.setTitleColor(UIColor.init(hexString: "#666666"), forState: UIControlState.Normal)
        self.DateDownSymbol.frame = CGRectMake(self.innerviews.frame.width / 100 * 85 , 145  ,self.innerviews.frame.width / 100 * 10, 20)
        self.DateDownSymbol.setFAIcon(FAType.FAAngleDown, iconSize: CGFloat(20),forState: .Normal)
        self.DateSeparateLineVw.frame = CGRectMake(self.innerviews.frame.width / 100 * 5 , 165  ,self.innerviews.frame.width / 100 * 87, 1)
        self.btnDate.addTarget(self, action: #selector(AddClaim.ShowDatePicker(_:)), forControlEvents: .TouchUpInside)
        self.DateDownSymbol.addTarget(self, action: #selector(AddClaim.ShowDatePicker(_:)), forControlEvents: .TouchUpInside)
        
        
        
        
        self.btnProject.frame = CGRectMake(self.innerviews.frame.width / 100 * 5 , 185  ,self.innerviews.frame.width / 100 * 80, 20)
        self.btnProject.backgroundColor = UIColor.clearColor()
        self.btnProject.layer.cornerRadius=5
        self.btnProject.setTitleColor(UIColor.init(hexString: "#666666"), forState: UIControlState.Normal)
        self.ProDownSymbol.frame = CGRectMake(self.innerviews.frame.width / 100 * 85 , 185  ,self.innerviews.frame.width / 100 * 10, 20)
        
        self.ProDownSymbol.setFAIcon(FAType.FAAngleDown, iconSize: CGFloat(20),forState: .Normal)
        self.ProSeparateLineVw.frame = CGRectMake(self.innerviews.frame.width / 100 * 5 , 205  ,self.innerviews.frame.width / 100 * 87, 1)
        
        
        
        
         
        
        
        self.txtDescription.frame = CGRectMake(self.innerviews.frame.width / 100 * 5 , 225  ,self.innerviews.frame.width / 100 * 80, 20)
        
        txtDescription.layer.borderWidth = 0
        txtDescription.layer.cornerRadius = 0
        txtDescription.layer.borderColor = UIColor.clearColor().CGColor
        txtDescription.textColor = UIColor(hexString: "#666666")
        txtDescription.font = UIFont(name:"HelveticaNeue-bold", size: 12.0)
        self.DescSeparateLineVw.frame = CGRectMake(self.innerviews.frame.width / 100 * 5 , 245  ,self.innerviews.frame.width / 100 * 87, 1)
        
        
        
        
        self.lblAttachment.frame = CGRectMake(self.innerviews.frame.width / 100 * 5 , 265  ,self.innerviews.frame.width / 100 * 35, 20)
        
        lblAttachment.textColor = UIColor(hexString: "#666666")
        lblAttachment.font = UIFont(name:"HelveticaNeue-bold", size: 12.0)
        
        
        self.btnAddFile.frame = CGRectMake(self.innerviews.frame.width / 100 * 60 , 265  ,self.innerviews.frame.width / 100 * 35, 20)
        
        
        
        
        self.imgOutlet.frame = CGRectMake(self.innerviews.frame.width / 100 * 5 , 290  ,30, 30)
        
        self.btnAdd.frame = CGRectMake(self.innerviews.frame.width / 100 * 10 , 350  ,self.innerviews.frame.width / 100 * 80, 30)
        
        
        self.btnAdd.setTitle("Add", forState: .Normal)
        self.btnAdd.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        self.btnAdd.setFAText(prefixText: "", icon: FAType.FAPlus, postfixText:  "  Add Claim", size: 12 , forState: .Normal)
        
        
    }
    
    func CreateComboSourceArray(TypeOfCombo : Int, Jsonval: Talentoz.JSON)
    {
        for i in 0 ..< Jsonval.count  {
            
            if (TypeOfCombo == 0)
            {
                self.ClaimType.append(String(Jsonval[i]["ClaimTypeName"]))
            }
            else if (TypeOfCombo == 1)
            {
                self.Currency.append(String(Jsonval[i]["CurrencyName"]))
            }
            else
            {
                self.Project.append(String(Jsonval[i]["LookUpDescription"]))
            }
            
        }
    }
    
    
    func BindClaimType() //Get Claim Type Combo Source
    {
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            objActivityIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, Viewframe.width, self.view.frame.size.height))
            objActivityIndicaor.activityIndicatorViewStyle = .WhiteLarge
            objActivityIndicaor.center = self.view.center
            objActivityIndicaor.transform = CGAffineTransformMakeScale(1.5, 1.5);
            objActivityIndicaor.hidesWhenStopped = true
            objActivityIndicaor.color = UIColor.grayColor()
            self.view.addSubview(objActivityIndicaor)
            objActivityIndicaor.startAnimating()
            
            objCallBack = AjaxCallBack()
            
            objCallBack.MethodName = "GetEligibleClaimTypesforEmployee"
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objCallBack.post(BindClaimTypeOnComplete, OnError: BindClaimTypeOnError)
            
        }
    }
    
    func BindClaimTypeOnComplete (ResultString :NSString? ) {
        
        dispatch_async(dispatch_get_main_queue(),{
            self.objActivityIndicaor.stopAnimating()
            self.ClaimTypeSource = JSON(data: self.objCallBack.GetJSONData(ResultString)!)
            self.CreateComboSourceArray(0, Jsonval: self.ClaimTypeSource)
            
            self.ClaimTypedropDown.dataSource = self.ClaimType
            self.ClaimTypedropDown.selectionAction = { [unowned self] (index, item) in
                self.iClaimType = self.ClaimTypeSource[index]["ClaimTypeID"].int!
                self.btnClaimType.setTitle(item, forState: .Normal)
                
                self.IsAttachmentRequired = false
                
                if(self.ClaimTypeSource[index]["ConfigurationID"].int == 1)
                {
                    if(self.ClaimTypeSource[index]["ConfigurationValue"].string! == "True")
                    {
                          self.IsAttachmentRequired = true
                    }
                }
            }
            
            self.ClaimTypedropDown.anchorView = self.btnClaimType
            self.ClaimTypedropDown.bottomOffset = CGPoint(x: 0, y:self.btnClaimType.bounds.height)
            
            self.BindCurrency()
            
        })
    }
    
    func BindClaimTypeOnError(Response: NSError ){
        
    }
    
    func BindCurrency() // Get Currency Combo Source
    {
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            objCallBack1 = AjaxCallBack()
            objCallBack1.MethodName = "GetSourceForCurrencyCombo"
            let ResultJSON = objCallBack1.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack1.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack1.ParameterList.append(objparam)
            
            objCallBack1.post(BindCurrencyOnComplete, OnError: BindCurrencyOnError)
            
        }
    }
    
    func BindCurrencyOnComplete (ResultString :NSString? ) {
        
        dispatch_async(dispatch_get_main_queue(),{
            self.objActivityIndicaor.stopAnimating()
            self.CurrencySource = JSON(data: self.objCallBack1.GetJSONData(ResultString)!)
            self.CreateComboSourceArray(1, Jsonval: self.CurrencySource)
            
            
            self.CurrencydropDown.dataSource = self.Currency
            self.CurrencydropDown.selectionAction = { [unowned self] (index, item) in
                self.iCurrency = self.CurrencySource[index]["CurrencyID"].int!
                self.btnCurrency.setTitle(item, forState: .Normal)
            }
            
            self.CurrencydropDown.anchorView = self.btnCurrency
            self.CurrencydropDown.bottomOffset = CGPoint(x: 0, y:self.btnCurrency.bounds.height)
            
            self.BindProject()
        })
    }
    
    func BindCurrencyOnError(Response: NSError ){
        
    }
    
    func BindProject() // Get Project Combo Source
    {
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            objCallBack2 = AjaxCallBack()
            objCallBack2.MethodName = "GetProjectListForClaimRequest"
            let ResultJSON = objCallBack2.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack2.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack2.ParameterList.append(objparam)
            
            objCallBack2.post(BindProjectOnComplete, OnError: BindProjectOnError)
            
        }

    }
    
    func BindProjectOnComplete (ResultString :NSString? ) {
        
        dispatch_async(dispatch_get_main_queue(),{
            self.objActivityIndicaor.stopAnimating()
            self.ProjectSource = JSON(data: self.objCallBack2.GetJSONData(ResultString)!)
            self.CreateComboSourceArray(2,Jsonval: self.ProjectSource)
            
            self.ProjectdropDown.dataSource = self.Project
            self.ProjectdropDown.selectionAction = { [unowned self] (index, item) in
                self.iProject = self.ProjectSource[index]["LookUpID"].int!
                 self.btnProject.setTitle(item, forState: .Normal)
            }
            
            self.ProjectdropDown.anchorView = self.btnProject
            self.ProjectdropDown.bottomOffset = CGPoint(x: 0, y:self.btnProject.bounds.height)
            
        })
    }
    
    func BindProjectOnError(Response: NSError ){
        
    }
    
// File Attachment Option
   
    
    func ShowAttachmentOption()
    {
        
        
        FilterBGLayer = UIView(frame: CGRectMake(0, 0, Viewframe.width  ,  Viewframe.height))
        FilterBGLayer.backgroundColor = UIColor(colorLiteralRed: 0 , green: 0, blue: 0, alpha: 0.6)
        let GestureLeaveDetail1 = UITapGestureRecognizer(target: self, action: #selector(AddClaim.DismissFilter(_:)))
        FilterBGLayer.addGestureRecognizer(GestureLeaveDetail1)
        FilterView = UIView(frame: CGRectMake(FilterBGLayer.frame.width / 100 * 10 , FilterBGLayer.frame.height  / 100 * 25 , FilterBGLayer.frame.width  / 100 * 80, FilterBGLayer.frame.height  / 100 * 40))
        FilterView.backgroundColor = UIColor.whiteColor()
        FilterView.layer.cornerRadius = 8
        
        let btnCapture : UIButton = UIButton (frame: CGRectMake(FilterView.frame.width / 100 * 10 ,  70 , FilterView.frame.width / 100 * 80, 30))
        btnCapture.setTitle("Take a Photo", forState: .Normal)
        btnCapture.addTarget(self, action: #selector(AddClaim.OpenCamera(_:)), forControlEvents: .TouchUpInside)
        btnCapture.backgroundColor = UIColor(hexString: "#237ec9")
        btnCapture.layer.cornerRadius = 5
        btnCapture.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        btnCapture.setFAText(prefixText: "", icon: FAType.FACamera, postfixText:"  Take a Photo", size: 12, forState: .Normal)
        FilterView.addSubview(btnCapture)
        
        let btnBrowseFolder : UIButton = UIButton (frame: CGRectMake(FilterView.frame.width / 100 * 10 ,  115 , FilterView.frame.width / 100 * 80, 30))
        btnBrowseFolder.setTitle("Choose From Gallery", forState: .Normal)
        btnBrowseFolder.addTarget(self, action: #selector(AddClaim.BrowseFolder(_:)), forControlEvents: .TouchUpInside)
        btnBrowseFolder.backgroundColor = UIColor(hexString: "#237ec9")
        btnBrowseFolder.setFAText(prefixText: "", icon: FAType.FAFolder, postfixText:"  Choose From Gallery", size: 12, forState: .Normal)
        btnBrowseFolder.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        btnBrowseFolder.layer.cornerRadius = 5
        FilterView.addSubview(btnBrowseFolder)
        
        FilterBGLayer.addSubview(FilterView)
        self.view.addSubview(FilterBGLayer)
        
    }
    
    
    func BrowseFolder(sender:UIButton)
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
            
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            imagePicker.allowsEditing = false
            imagePicker.navigationBar.barTintColor  =  UIColor(colorLiteralRed: 64/255, green: 156/255, blue: 255/255, alpha: 1)
            imagePicker.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
            imagePicker.allowsEditing = true
            
            self.presentViewController(imagePicker, animated: true, completion: nil)
            
        }
    }
    
    //Close The Layer
    func DismissFilter(sender: UIButton)
    {
        isFilterOpen = false
        FilterBGLayer.removeFromSuperview()
        
    }
    
    func OpenCamera(sender:UIButton)
    {
        
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
            imagePicker.allowsEditing = false
            
            self.presentViewController(imagePicker, animated: true, completion: nil)
            
        }
        
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        imgOutlet.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        imgOutlet.hidden = true
       
        
        if picker.sourceType == UIImagePickerControllerSourceType.PhotoLibrary  {
             let imageURL = info[UIImagePickerControllerReferenceURL] as! NSURL
            self.AttachFileActualName = String(imageURL.path!)
        }
        else
        {
            self.AttachFileActualName = "Captured"
        }
        
        let iAttach = AttachmentCollection()
        iAttach.ActualFileName = self.AttachFileActualName
        //iAttach.GeneratedFileName = self.AttachFileGenerateName
        iAttach.ImageData =  self.imgOutlet.image
        self.AttachmentCollections.append(iAttach)
        
        self.BindAttachmentList()
        
        
        isFilterOpen = false
        FilterBGLayer.removeFromSuperview()
        
        Fileupload()
        
        
    }
    
    func Fileupload()
    {
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            objActivityIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, Viewframe.width, self.view.frame.size.height))
            objActivityIndicaor.activityIndicatorViewStyle = .WhiteLarge
            objActivityIndicaor.center = self.view.center
            objActivityIndicaor.transform = CGAffineTransformMakeScale(1.5, 1.5);
            objActivityIndicaor.hidesWhenStopped = true
            objActivityIndicaor.color = UIColor.grayColor()
            self.view.addSubview(objActivityIndicaor)
            objActivityIndicaor.startAnimating()
            
            objCallBack1 = AjaxCallBack()
            
            if let unwrappedImage = imgOutlet.image {
                
                let data = imgOutlet.image!.lowQualityJPEGNSData
                
                let ResultJSON = objCallBack1.DeserializeJSONString(UserInfo)
                objCallBack1.MethodName = "FileUploadForLeave"
                
                let base64String = data.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
                
                var objparam = Parameter(Name: "stream",value: String(base64String))
                objCallBack1.ParameterList.append(objparam)
                
                objparam = Parameter(Name: "ext",value: String(self.imageType(data)))
                objCallBack1.ParameterList.append(objparam)
                
                objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
                objCallBack1.ParameterList.append(objparam)
                
                
                objCallBack1.post(FileuploadOnComplete, OnError: FileuploadOnErrorOnError)
            }
            
            
        }
        
    }
    func FileuploadOnComplete(ResultString :NSString? ) {
        dispatch_async(dispatch_get_main_queue(),{
            
            self.objActivityIndicaor.stopAnimating()
            let Result : String = self.objCallBack1.GetJSONResultString(ResultString)!
            
            self.AttachFileGenerateName = String(Result)
            
            self.AttachmentCollections[self.AttachmentCollections.count-1].GeneratedFileName = self.AttachFileGenerateName
            
        })
        
        
    }
    
    func FileuploadOnErrorOnError(Response: NSError ){
        
        
    }
    
    
    func imageType(imgData : NSData) -> String
    {
        var c = [UInt8](count: 1, repeatedValue: 0)
        imgData.getBytes(&c, length: 1)
        
        let ext : String
        
        switch (c[0]) {
        case 0xFF:
            
            ext = "jpg"
            
        case 0x89:
            
            ext = "png"
        case 0x47:
            
            ext = "gif"
        case 0x49, 0x4D :
            ext = "tiff"
        default:
            ext = "" //unknown
        }
        
        return ext
    }
    
    func ShowDatePicker(sender:UIButton)
    {
        
        FilterBGLayer = UIView(frame: CGRectMake(0, 0, Viewframe.width  ,  Viewframe.height))
        FilterBGLayer.backgroundColor = UIColor(colorLiteralRed: 0 , green: 0, blue: 0, alpha: 0.6)
        let GestureLeaveDetail1 = UITapGestureRecognizer(target: self, action: #selector(AddClaim.DismissFilter(_:)))
        FilterBGLayer.addGestureRecognizer(GestureLeaveDetail1)
        FilterView = UIView(frame: CGRectMake(FilterBGLayer.frame.width / 100 * 10 , FilterBGLayer.frame.height  / 100 * 25 , FilterBGLayer.frame.width  / 100 * 80, FilterBGLayer.frame.height  / 100 * 60))
        FilterView.backgroundColor = UIColor.whiteColor()
        FilterView.layer.cornerRadius = 8
        let TimePick : UIDatePicker =  UIDatePicker(frame: CGRect(x: 10, y: 5, width: FilterView.frame.width - 10 ,height: FilterView.frame.width - 30))
        TimePick.datePickerMode = .Date
        let calendar: NSCalendar = NSCalendar(calendarIdentifier: NSGregorianCalendar)!
        // let calendar: NSCalendar = NSCalendar.currentCalendar()
        calendar.timeZone = NSTimeZone(name: "UTC")!
        var DateStr = ""
        if self.btnDate.titleLabel?.text == "Date"
        {
            let date = NSDate()
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components([.Year,.Month,.Day], fromDate: date)
            
            DateStr = String(components.day) + "/" + String(components.month) + "/" + String(components.year)
        }
        else
        {
            DateStr = (self.btnDate.titleLabel?.text)!
        }
        
        var array = DateStr.componentsSeparatedByString("/")
        let components: NSDateComponents = NSDateComponents()
        components.year = Int(array[2])!
        components.month = Int(array[1])!
        components.day = Int(array[0])!
        let defaultDate: NSDate = calendar.dateFromComponents(components)!
        TimePick.date = defaultDate
        
        TimePick.addTarget(self, action: #selector(AddClaim.handlerDate(_:)), forControlEvents: UIControlEvents.ValueChanged)
        
        
        
        let timeFormatter = NSDateFormatter()
        timeFormatter.dateFormat = "dd/MM/yyyy"
        
        
        
       
        self.iDate = defaultDate
        self.btnDate.setTitle(String(timeFormatter.stringFromDate(defaultDate)), forState: .Normal)
            
      
        
        
        let btn_ApplyFilter : UIButton = UIButton(frame: CGRectMake(FilterBGLayer.frame.width / 100 * 26, TimePick.frame.height + 25, 120 ,24))
        btn_ApplyFilter.setTitle("Go", forState: .Normal)
        btn_ApplyFilter.layer.cornerRadius = 5
        btn_ApplyFilter.tag = sender.tag
        btn_ApplyFilter.backgroundColor = UIColor(hexString: "#2190ea")
        btn_ApplyFilter.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        btn_ApplyFilter.titleLabel!.font =  UIFont(name:"helvetica-Bold", size: 12)
        btn_ApplyFilter.addTarget(self, action: #selector(AddClaim.DateChanged(_:)), forControlEvents: .TouchUpInside)
        FilterView.addSubview(btn_ApplyFilter)
        
        FilterView.addSubview(TimePick)
        FilterBGLayer.addSubview(FilterView)
        self.view.addSubview(FilterBGLayer)
        
    }
    
    func DateChanged(sender: UIButton)
    {
        
        
        isFilterOpen = false
        FilterBGLayer.removeFromSuperview()
        
    }
 
    
    func handlerDate(sender: UIDatePicker) {
        let timeFormatter = NSDateFormatter()
        timeFormatter.dateFormat = "dd/MM/yyyy"
        self.iDate = sender.date
       
        self.btnDate.setTitle(String(timeFormatter.stringFromDate(sender.date)), forState: .Normal)
        
        // do what you want to do with the string.
    }
    
    // Save Claim
    
    func ValidateTheInputs()
    {
        
        if (self.iClaimType == -1)
        {
            let alert = UIAlertController(title: "Required", message: "Please choose the claim type.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.presentViewController(alert, animated: true, completion: nil)}
            return
        }
        
        if (self.iCurrency == -1)
        {
            let alert = UIAlertController(title: "Required", message: "Please choose the currency.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.presentViewController(alert, animated: true, completion: nil)}
            return
        }
        
        if ( txtAmount.text == "")
        {
            let alert = UIAlertController(title: "Required", message: "Please enter the claim amount.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.presentViewController(alert, animated: true, completion: nil)}
            return
        }
        
        let num = Double(txtAmount.text!)
        if num == nil {
            let alert = UIAlertController(title: "Required", message: "Please enter the valid claim amount.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.presentViewController(alert, animated: true, completion: nil)}
            return

        }
        
        if self.btnDate.titleLabel?.text == "Date"
        {
                let alert = UIAlertController(title: "Required", message: "Please pick the claim date.", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    self.presentViewController(alert, animated: true, completion: nil)}
                return            
        }
        
        
        
        if ( self.IsAttachmentRequired == true)
        {
            if(self.AttachmentCollections.count == 0)
            {
                let alert = UIAlertController(title: "Required", message: "Attachment is mandatory.", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    self.presentViewController(alert, animated: true, completion: nil)}
                
                return
            }
            
        }
        
        self.SaveClaim()
        
     
    }
    
    
    func SaveClaim(){
        self.btnAdd.enabled = false
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            
            
            objActivityIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, Viewframe.width, self.view.frame.size.height))
            objActivityIndicaor.activityIndicatorViewStyle = .WhiteLarge
            objActivityIndicaor.center = self.view.center
            objActivityIndicaor.transform = CGAffineTransformMakeScale(1.5, 1.5);
            objActivityIndicaor.hidesWhenStopped = true
            objActivityIndicaor.color = UIColor.grayColor()
            self.view.addSubview(objActivityIndicaor)
            objActivityIndicaor.startAnimating()
            
            
            objCallBack = AjaxCallBack()
            
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            objCallBack.MethodName = "SaveClaimTypeRequestItems"
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pItemID",value: "-1")
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pClaimTypeID",value: String(self.iClaimType))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pCurrencyID",value: String(self.iCurrency))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pClaimDate",value: String(btnDate.titleLabel!.text!))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pAmount",value:  String(txtAmount.text!))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pDescription",value: String(txtDescription.text!))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pProjectID",value: String(self.iProject))
            objCallBack.ParameterList.append(objparam)
            
            let AttachStr = self.ClaimsAttachment()
            
            objparam = Parameter(Name: "pClaimItemsAttachment",value: AttachStr)
            objCallBack.ParameterList.append(objparam)           
          
            
            objCallBack.post(SaveClaimOnComplete, OnError: SaveClaimOnError)
            
        }
        
        
        
    }
    
    func ClaimsAttachment() -> String
    {
        var Attachment : String = ""
        
        for i in 0 ..< self.AttachmentCollections.count  {
            
            if (Attachment == "")
            {
                Attachment = self.AttachmentCollections[i].ActualFileName + "," + self.AttachmentCollections[i].GeneratedFileName
            }
            else
            {
                Attachment = Attachment + "|" + self.AttachmentCollections[i].ActualFileName + "," + self.AttachmentCollections[i].GeneratedFileName
            }
        }
        
        return Attachment
    }
    
    
    func SaveClaimOnComplete(ResultString :NSString? ) {
        dispatch_async(dispatch_get_main_queue(),{
            let ResultData =  JSON(data: self.objCallBack.GetJSONData(ResultString)!)
            self.objActivityIndicaor.stopAnimating()
            if ResultData["TypeOfError"] == 4
            {
                
                let alert = UIAlertController(title: "Success", message: "The claim has been added successfully.", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: self.CloseForm))
                
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    self.presentViewController(alert, animated: true, completion: nil)}
            }
            else
            {
                
                self.btnAdd.enabled = true
                
                let alert = UIAlertController(title: "Alert", message: String(ResultData["ErrorMessage"]), preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    self.presentViewController(alert, animated: true, completion: nil)}
            }
        })
        
        
    }
    
    func SaveClaimOnError(Response: NSError ){
        
        
    }
    
    func CloseForm(sender:UIAlertAction)
    {
        let Claimstoryboard = UIStoryboard(name: "ClaimsDashBoard", bundle: nil)
        let vc1 = Claimstoryboard.instantiateViewControllerWithIdentifier("ClaimsDashBoardVC")
        self.presentViewController(vc1, animated: true, completion: nil)
        
    }
    
    
    //Only Allow Double Values
    func textField(textField: UITextField,
                   shouldChangeCharactersInRange range: NSRange,
                                                 replacementString string: String) -> Bool {
        if textField == txtAmount {
            
            let inverseSet = NSCharacterSet(charactersInString:"0123456789.").invertedSet
            let components = string.componentsSeparatedByCharactersInSet(inverseSet)
            let filtered = components.joinWithSeparator("")
            return string == filtered
            
        } else if textField == txtDescription {
            let maxLength = 255
            let currentString: NSString = textField.text!
            let newString: NSString =
                currentString.stringByReplacingCharactersInRange(range, withString: string)
            return newString.length <= maxLength
        }
        
       return true
        
    }
    
    //-Text field move up when hidden by KEYBOARD//
    
    func textFieldDidBeginEditing(textField: UITextField) {
        animateViewMoving(true, moveValue: 100)
    }
    func textFieldDidEndEditing(textField: UITextField) {
        animateViewMoving(false, moveValue: 100)
    }
    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:NSTimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = CGRectOffset(self.view.frame, 0,  movement)
        UIView.commitAnimations()
    }
    

    // Attachment List View
    
    func BindAttachmentList()  {
        
        let dCollectionViewFlowLayout : UICollectionViewFlowLayout = UICollectionViewFlowLayout ()
        self.AttachCollectionView = UICollectionView (frame: self.view.frame, collectionViewLayout: dCollectionViewFlowLayout)
        self.AttachCollectionView.delegate = self
        self.AttachCollectionView.dataSource = self
        self.AttachCollectionView.backgroundColor = UIColor.blackColor()
        
        self.AttachCollectionView.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "cellIdentifier")
        self.UIScroll.addSubview(self.AttachCollectionView)
        
        self.AttachCollectionView.frame = CGRectMake(Viewframe.width / 100 * 10 ,350, Viewframe.width / 100 * 80,50)
        
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  1
    }
    
    func collectionView(collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: self.Viewframe.width, height: 50)
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        
        let cell : UICollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("cellIdentifier", forIndexPath: indexPath)
        
        cell.backgroundColor = UIColor.whiteColor()
        
        let scroll: UIScrollView = UIScrollView(frame: CGRectMake(Viewframe.width / 100 * 10, 0, Viewframe.width / 100 * 80 , AttachCollectionView.frame.size.height))
        scroll.delegate = self
        scroll.backgroundColor = UIColor.whiteColor()
        cell.addSubview(scroll)
        
        var xbase : CGFloat = 10
        
        for i in 0 ..< self.AttachmentCollections.count  {
            
            let ContainerView = UIView(frame: CGRectMake(xbase + 3, 3 , 44 , 44))
            ContainerView.layer.borderColor = UIColor(hexString: "#cccccc")?.CGColor
            ContainerView.layer.borderWidth = 1
            
           
            let imgView : UIImageView = UIImageView(frame: CGRectMake(3, 3, 38, 38))
            imgView.layer.cornerRadius = 0.5
            imgView.layer.borderWidth = 0
            imgView.image = self.AttachmentCollections[i].ImageData
            ContainerView.addSubview(imgView)
            
            
            let img_EmpPic:  UIButton = UIButton(frame: CGRectMake(31, 0, 13, 13))
            img_EmpPic.layer.borderWidth = 0.5
            img_EmpPic.layer.masksToBounds = false
            img_EmpPic.layer.borderColor = UIColor.lightGrayColor().CGColor
            img_EmpPic.backgroundColor = UIColor.whiteColor()
            img_EmpPic.layer.cornerRadius = img_EmpPic.frame.height/2
            img_EmpPic.clipsToBounds = true
            img_EmpPic.tag = i
            img_EmpPic.setTitleColor(UIColor.redColor(), forState: .Normal)
            img_EmpPic.setTitle("Remove", forState: .Normal)
            img_EmpPic.setFAIcon(FAType.FARemove, iconSize: 10, forState: .Normal)
            img_EmpPic.addTarget(self, action: #selector(AddClaim.RemoveImage(_:)), forControlEvents: .TouchUpInside)
            ContainerView.addSubview(img_EmpPic)
  
            
            scroll.addSubview(ContainerView)
            xbase += 44 + 10
            
            
        }
        
        
        scroll.contentSize = CGSizeMake(xbase, scroll.frame.size.height)
        
        
        return cell
        
    }
    
    func RemoveImage(sender:UIButton)
    {
        self.AttachmentCollections.removeAtIndex(sender.tag)
        self.AttachCollectionView.reloadData()
    }

 

}
