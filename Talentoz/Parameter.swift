//
//  File.swift
//  Talentoz
//
//  Created by forziamac on 09/12/15.
//  Copyright © 2015 Forzia Tech Private Ltd. All rights reserved.
//
class Parameter {
   private var PName :String = ""
   private var PValue :String = ""
    
    init(Name:String,value:String){
    PName = Name
    PValue=value
    
    }
    
    var ParameterName:String
        {
        set
        {
            PName = newValue
        }
        get {
            return PName
        }
    }
    
    var ParameterValue:String
        {
        set
        {
            PValue = newValue
        }
        get {
            return PValue
        }
    }
}
