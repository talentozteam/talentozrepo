//
//  Profile.swift
//  Talentoz
//
//  Created by forziamac on 21/12/15.
//  Copyright © 2015 Forzia Tech Private Ltd. All rights reserved.
//

import UIKit

class ProfileVC:   BaseTabBarVC ,UICollectionViewDataSource , UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    internal var UserID : Int = -1
    let objCallBack = AjaxCallBack()
      let objCallBack1 = AjaxCallBack()
      let objCallBack2 = AjaxCallBack()
    let objCallBack3 = AjaxCallBack()
    let defaults = NSUserDefaults.standardUserDefaults()
     var dCollectionView : UICollectionView!
    var TempJson: Talentoz.JSON!
    var ModeOfView: Int!=0
    var IsTeamApplicable: Bool!=false
    var MobileNumber : String = ""
    var EmailID : String = ""
    
    
    var objContactActivityIndicaor : UIActivityIndicatorView!
    var objSubMgrActivityIndicaor : UIActivityIndicatorView!
    var objLBActivityIndicaor : UIActivityIndicatorView!
    
    @IBOutlet var EmailIcon: UIButton!
    
    
    
    let vscrollView = UIScrollView(frame: UIScreen.mainScreen().bounds)
    
    override func viewDidLoad() {
        self.view.addSubview(vscrollView)
        self.vscrollView.contentSize = CGSize(width: self.view.frame.width, height: 1000)
        self.vscrollView.backgroundColor = UIColor.whiteColor()
        super.UINavigationBarTitle = "Profile"
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(hexString: "#FFFFF2")
        
    GetEmployeeProfile()
    GetManagerList()
 
        
        
        // Do any additional setup after loading the view.
    }
    
 

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func goBack() {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("HomeInitialView")
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    
    func BindProfileInfo(JSONStrin: Talentoz.JSON!){
        
        // Bind Profile Information
        let ProfileView=UIView(frame: CGRectMake(0,60, self.view.frame.width, 180))
      //  ProfileView.backgroundColor = UIColor(hexString: "#2870ea")
        
       ProfileView.backgroundColor =  UIColor(colorLiteralRed: 28/255, green: 138/255, blue: 255/255, alpha: 1)
       
   
        if let tmpDomainURL = self.defaults.stringForKey("DomainURL")
        {
            
            if  (JSONStrin[0]["EmployeePhoto"].type == Talentoz.Type.Null || JSONStrin[0]["EmployeePhoto"].string == ""  )
            {
                
                let img_EmpPic:  UIButton = UIButton(frame: CGRectMake((ProfileView.frame.width / 100 * 38), 7, 75, 75))
                img_EmpPic.layer.borderWidth = 2
                img_EmpPic.layer.masksToBounds = false
                img_EmpPic.layer.borderColor = UIColor.whiteColor().CGColor
               // img_EmpPic.backgroundColor = UIColor(hexString: String("#0CDBFF"))
                img_EmpPic.layer.cornerRadius = img_EmpPic.frame.height/2
                img_EmpPic.clipsToBounds = true
                img_EmpPic.titleLabel!.font = UIFont(name: "helvetica-bold", size: 25)
                img_EmpPic.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                img_EmpPic.setTitle(String(JSONStrin[0]["EmployeeName"])[0], forState: UIControlState.Normal)
               ProfileView.addSubview(img_EmpPic)
            }else{
                let imgView : UIImageView = UIImageView(frame: CGRectMake((ProfileView.frame.width / 100 * 38), 7, 75, 75))
                
                
                imgView.layer.borderWidth = 1
                imgView.layer.masksToBounds = false
                imgView.layer.borderColor = UIColor.whiteColor().CGColor
                imgView.layer.cornerRadius = imgView.frame.height/2
                imgView.clipsToBounds = true
                let employeePic = "http://" + tmpDomainURL + "/Uploadedfiles/Client" + String(self.defaults.stringForKey("ClientID")!) + "/Image/" + String( JSONStrin[0]["EmployeePhoto"])
                imgView.setImageWithUrl(NSURL(string: employeePic)!)
               ProfileView.addSubview(imgView)
            }
            
          
        }
     
        
        
     
        
        let lbl_Name : UILabel = UILabel(frame: CGRectMake(ProfileView.frame.width*0.03, 90, ProfileView.frame.width * 0.94, 15))
        
        lbl_Name.textAlignment = .Center
        lbl_Name.textColor = UIColor.whiteColor()
        lbl_Name.font = UIFont(name:"HelveticaNeue-Bold", size: 15.0)
        if (JSONStrin[0]["EmployeeName"] != nil)
        {
            lbl_Name.text = JSONStrin[0]["EmployeeName"].string
        }
        ProfileView.addSubview(lbl_Name)
        
        let lbl_PositionName : UILabel = UILabel(frame: CGRectMake(ProfileView.frame.width*0.03, 110, ProfileView.frame.width  * 0.94, 15))
        
        lbl_PositionName.textAlignment = .Center
         lbl_PositionName.textColor = UIColor.whiteColor()
        lbl_PositionName.font = lbl_PositionName.font.fontWithSize(13.0)
        if (JSONStrin[0]["PositionName"] != nil)
        {
        lbl_PositionName.text = JSONStrin[0]["PositionName"].string
        }
        ProfileView.addSubview(lbl_PositionName)
        
        let lbl_BusinessUit : UILabel = UILabel(frame: CGRectMake(0, 130, ProfileView.frame.width , 15))
        
        lbl_BusinessUit.textAlignment = .Center
         lbl_BusinessUit.textColor = UIColor.whiteColor()
        lbl_BusinessUit.font = lbl_BusinessUit.font.fontWithSize(13.0)
        if (JSONStrin[0]["BusinessUnitName"] != nil)
        {
        lbl_BusinessUit.text =  JSONStrin[0]["BusinessUnitName"].string
        }
        ProfileView.addSubview(lbl_BusinessUit)
        
        let lbl_Worklocation : UILabel = UILabel(frame: CGRectMake(0, 150, ProfileView.frame.width , 15))
        
        lbl_Worklocation.textAlignment = .Center
        lbl_Worklocation.textColor = UIColor.whiteColor()
        lbl_Worklocation.font = lbl_Worklocation.font.fontWithSize(13.0)
        if (JSONStrin[0]["WorkLocationName"] != nil)
        {
        lbl_Worklocation.text = JSONStrin[0]["WorkLocationName"].string
        }
        ProfileView.addSubview(lbl_Worklocation)
        
        self.vscrollView.addSubview(ProfileView)
        
        
        
        let ContctInfoView=UIView(frame: CGRectMake(0,230, self.view.frame.width, 150))
        ContctInfoView.backgroundColor = UIColor.whiteColor()
        
        
        
        
        
        let ContainerTitalView = UIView(frame: CGRectMake(0, 0, self.view.frame.width , 35))
        ContainerTitalView.backgroundColor = UIColor(hexString: "#f5f5f5")
        ContctInfoView.addSubview(ContainerTitalView)
        
        let ContctInfotitle : UILabel = UILabel(frame: CGRectMake(15, 0, self.view.frame.width , 30))
        
        ContctInfotitle.textAlignment = .Left
        ContctInfotitle.font = UIFont(name: "helvetica-bold", size: 14)
        ContctInfotitle.text = "Contact Information"
        ContainerTitalView.addSubview(ContctInfotitle)
        
        
        let lbl_Email_Title : UILabel = UILabel(frame: CGRectMake(15, 40, ContctInfoView.frame.width , 15))
         //lbl_Email_Title.textColor = UIColor(hexString: "#333333")
        lbl_Email_Title.textAlignment = .Left
        lbl_Email_Title.font = lbl_Email_Title.font.fontWithSize(14.0)
        lbl_Email_Title.text = "Email"
        ContctInfoView.addSubview(lbl_Email_Title)
   
        
        let btn_Email_Txt : UIButton = UIButton(frame: CGRectMake(15, 60, ContctInfoView.frame.width , 15))
        btn_Email_Txt.setTitleColor(UIColor(colorLiteralRed: 28/255, green: 138/255, blue: 255/255, alpha: 1), forState: .Normal)
      
     
       
        
        
        if let strEmail = JSONStrin[0]["Email"].string
        {
            
            btn_Email_Txt.setTitle(strEmail, forState: .Normal)
            EmailID = "mailto://" + strEmail
            
            btn_Email_Txt.addTarget(self, action: #selector(ProfileVC.ComposeMail(_:)), forControlEvents: .TouchUpInside)
            btn_Email_Txt.titleLabel!.font =  UIFont(name:"HelveticaNeue", size: 13.0)
            btn_Email_Txt.setFAText(prefixText: "", icon: FAType.FAEnvelope, postfixText: " " + strEmail, size: 13, forState: .Normal)
                }
        btn_Email_Txt.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        btn_Email_Txt.contentVerticalAlignment = UIControlContentVerticalAlignment.Center
    
        ContctInfoView.addSubview(btn_Email_Txt)
        
    
        
        let lbl_Mobile_Title : UILabel = UILabel(frame: CGRectMake(15, 90 ,ContctInfoView.frame.width , 15))
        //lbl_Mobile_Title.textColor = UIColor(hexString: "#333333")
        lbl_Mobile_Title.textAlignment = .Left
        lbl_Mobile_Title.font = lbl_Mobile_Title.font.fontWithSize(14.0)
        lbl_Mobile_Title.text = "Mobile"
        ContctInfoView.addSubview(lbl_Mobile_Title)
        
        let btn_Mobile_Txt : UIButton = UIButton(frame: CGRectMake(15, 110, ContctInfoView.frame.width , 15))
        btn_Mobile_Txt.setTitleColor(UIColor(colorLiteralRed: 28/255, green: 138/255, blue: 255/255, alpha: 1), forState: .Normal)
     
        //lbl_Mobile_Txt.tit= lbl_Mobile_Txt.font.fontWithSize(12.0)
        if let strMobilePhone = JSONStrin[0]["MobilePhone"].string
        {
              btn_Mobile_Txt.setTitle(strMobilePhone, forState: .Normal)
              MobileNumber = "tel://" + strMobilePhone
                
            
            btn_Mobile_Txt.addTarget(self, action: #selector(ProfileVC.MakeCall(_:)), forControlEvents: .TouchUpInside)
            btn_Mobile_Txt.titleLabel!.font =  UIFont(name:"HelveticaNeue", size: 13.0)
               btn_Mobile_Txt.setFAText(prefixText: "", icon: FAType.FAPhone, postfixText: " " + strMobilePhone, size: 13, forState: .Normal)
           
        
        }
        btn_Mobile_Txt.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        btn_Mobile_Txt.contentVerticalAlignment = UIControlContentVerticalAlignment.Center
       
        ContctInfoView.addSubview(btn_Mobile_Txt)
        
        let lbl_Office_Title : UILabel = UILabel(frame: CGRectMake(15, 140, ContctInfoView.frame.width , 15))
        lbl_Office_Title.textColor = UIColor.blackColor()
        lbl_Office_Title.textAlignment = .Left
        lbl_Office_Title.font = lbl_Office_Title.font.fontWithSize(14.0)
        lbl_Office_Title.text = "Office Phone"
        ContctInfoView.addSubview(lbl_Office_Title)
        
        let lbl_Office_Txt : UILabel = UILabel(frame: CGRectMake(15, 160, ContctInfoView.frame.width , 15))
        
        lbl_Office_Txt.textColor = UIColor.blackColor()
        lbl_Office_Txt.textAlignment = .Left
        lbl_Office_Txt.font = lbl_Office_Txt.font.fontWithSize(13.0)
        if let strMobilePhone = JSONStrin[0]["OfficePhone"].string
        {
        lbl_Office_Txt.text = JSONStrin[0]["OfficePhone"].string
        }
        ContctInfoView.addSubview(lbl_Office_Txt)
        
        self.vscrollView.addSubview(ContctInfoView)
        
        
        
    }
    
    func MakeCall(sender: UIButton)
    {
        
        UIApplication.sharedApplication().openURL(NSURL(string: MobileNumber)!)
    }
    
    func ComposeMail(sender: UIButton)
    {
        
        UIApplication.sharedApplication().openURL(NSURL(string: EmailID)!)
    }
 
    
    func GetEmployeeProfile()
    {
        objContactActivityIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
        objContactActivityIndicaor.activityIndicatorViewStyle = .WhiteLarge
        var objPosition =  self.view.center
        objPosition.y =  objPosition.y.advancedBy(-40.0)
        objContactActivityIndicaor.center = objPosition
        objContactActivityIndicaor.hidesWhenStopped = true
        objContactActivityIndicaor.color = UIColor.grayColor()
        self.view.addSubview(objContactActivityIndicaor)
        objContactActivityIndicaor.startAnimating()
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            objCallBack.MethodName = "GetEmployeeProfile"
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            if self.UserID == -1
            {
                objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))                
            }
            else
            {
                 objparam = Parameter(Name: "pUserID",value: String(self.UserID))
            }
            objCallBack.ParameterList.append(objparam)
            
            objCallBack.post(GetEmployeeProfileOnComplete, OnError: EmpProfileOnErrorOnError)
            
        }
        
    }
    func GetEmployeeProfileOnComplete (ResultString :NSString? ) {
        
        dispatch_async(dispatch_get_main_queue(),{
             self.objContactActivityIndicaor.stopAnimating()
            self.BindProfileInfo(JSON(data: self.objCallBack.GetJSONData(ResultString)!))
        })
        
    }
    
    
    //Response on error callback
    func EmpProfileOnErrorOnError(Response: NSError ){
        
    }
    
 
    
    func GetManagerList()
    {
        objSubMgrActivityIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
        objSubMgrActivityIndicaor.activityIndicatorViewStyle = .WhiteLarge
        var objPosition =  self.view.center
        objPosition.y =  objPosition.y.advancedBy(-40.0)
        objSubMgrActivityIndicaor.center = objPosition
        objSubMgrActivityIndicaor.hidesWhenStopped = true
        objSubMgrActivityIndicaor.color = UIColor.grayColor()
        self.view.addSubview(objSubMgrActivityIndicaor)
        objSubMgrActivityIndicaor.startAnimating()
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
             
            self.ModeOfView = 0
            objCallBack1.MethodName = "GetManagerList"
            let ResultJSON = objCallBack1.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack1.ParameterList.append(objparam)
            
            if self.UserID == -1
            {
                objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            }
            else
            {
                objparam = Parameter(Name: "pUserID",value: String(self.UserID))
            }
            objCallBack1.ParameterList.append(objparam)
            
            objCallBack1.post(GetManagerListOnComplete, OnError: EmpProfileOnErrorOnError)
            
        
            
        }
    }
    
    func GetManagerListOnComplete (ResultString :NSString?) {
        
            
            dispatch_async(dispatch_get_main_queue(),{
                
                  self.objSubMgrActivityIndicaor.stopAnimating()
                
                self.TempJson = JSON(data: self.objCallBack1.GetJSONData(ResultString)!)
                
                
                let ContainerTitalView = UIView(frame: CGRectMake(0, 430, self.view.frame.width , 35))
                ContainerTitalView.backgroundColor = UIColor(hexString: "#f5f5f5")
                self.vscrollView.addSubview(ContainerTitalView)

                
                let lbl_ReportingStructureView_Title : UILabel = UILabel(frame: CGRectMake(15, 7, self.view.frame.width, 16))
                
                lbl_ReportingStructureView_Title.textAlignment = .Left
                lbl_ReportingStructureView_Title.font  	= UIFont(name: "helvetica-bold", size: 14)
                lbl_ReportingStructureView_Title.text = "Reporting Structure"
                ContainerTitalView.addSubview(lbl_ReportingStructureView_Title)
                
                
                let lbl_MgrView_Title : UILabel = UILabel(frame: CGRectMake(15, 470, self.view.frame.width , 16))
                lbl_MgrView_Title.textColor = UIColor.blackColor()
                lbl_MgrView_Title.textAlignment = .Left
                lbl_MgrView_Title.font = UIFont(name: "helvetica-bold", size: 13)
                lbl_MgrView_Title.text = "Manager(s)"
                self.vscrollView.addSubview(lbl_MgrView_Title)
                
                let dCollectionViewFlowLayout : UICollectionViewFlowLayout = UICollectionViewFlowLayout ()
                self.dCollectionView = UICollectionView (frame: self.view.frame, collectionViewLayout: dCollectionViewFlowLayout)
                self.dCollectionView.delegate = self
                self.dCollectionView.dataSource = self
                self.dCollectionView.backgroundColor = UIColor.blackColor()
                self.dCollectionView.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "cellIdentifier")
                self.dCollectionView.frame = CGRectMake(0,500, self.view.frame.size.width,120)
                self.vscrollView.addSubview(self.dCollectionView)
                
                self.GetSubordinateList()
                
            })
        
    }
    
    func GetSubordinateListOnComplete (ResultString :NSString?) {
        
        
        dispatch_async(dispatch_get_main_queue(),{
            self.TempJson = JSON(data: self.objCallBack3.GetJSONData(ResultString)!)
            self.objSubMgrActivityIndicaor.stopAnimating()

            if self.TempJson.count>0
            {
                self.IsTeamApplicable = true
                
                let lblSubordinateTitle : UILabel = UILabel(frame: CGRectMake(15, 620, self.view.frame.width , 15))
                lblSubordinateTitle.backgroundColor = UIColor.whiteColor()
                lblSubordinateTitle.textAlignment = .Left
                lblSubordinateTitle.font = UIFont(name: "helvetica-bold", size: 13)
                lblSubordinateTitle.textColor = UIColor.blackColor()
                lblSubordinateTitle.text = "Subordinate(s)"
                self.vscrollView.addSubview(lblSubordinateTitle)
                
                
                
                let dCollectionViewFlowLayout : UICollectionViewFlowLayout = UICollectionViewFlowLayout ()
                self.dCollectionView = UICollectionView (frame: self.view.frame, collectionViewLayout: dCollectionViewFlowLayout)
                self.dCollectionView.delegate = self
                self.dCollectionView.dataSource = self
                self.dCollectionView.backgroundColor = UIColor.blackColor()
                self.dCollectionView.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "cellIdentifier")
                self.dCollectionView.frame = CGRectMake(0,650, self.view.frame.size.width,110)
                self.vscrollView.addSubview(self.dCollectionView)
            }
            
           
               self.BindLeaveBalance()
           
           
            
        })
        
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  1
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            return CGSize(width: self.view.frame.size.width, height: 110)
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        
        let cell : UICollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("cellIdentifier", forIndexPath: indexPath)
        
        cell.backgroundColor = UIColor.whiteColor()
        
        let scroll: UIScrollView = UIScrollView(frame: CGRectMake(0, 0, dCollectionView.frame.size.width, 150))
        scroll.delegate = self
        scroll.backgroundColor = UIColor.whiteColor()
        cell.addSubview(scroll)
        
        var xbase : CGFloat = 10
        
        
        
        if self.TempJson.count==1 && self.ModeOfView == 0
        {
            
            
            if  (self.TempJson[0]["EmployeePhoto"].type == Talentoz.Type.Null || self.TempJson[0]["EmployeePhoto"].string == ""  )
            {
                
                let img_EmpPic:  UIButton = UIButton(frame: CGRectMake(xbase + 27, 7, 60, 60))
                img_EmpPic.layer.borderWidth = 0.5
                img_EmpPic.layer.masksToBounds = false
                img_EmpPic.layer.borderColor = UIColor.lightGrayColor().CGColor
                img_EmpPic.backgroundColor = UIColor.SetRandomColor()
                img_EmpPic.layer.cornerRadius = img_EmpPic.frame.height/2
                img_EmpPic.clipsToBounds = true
                if self.TempJson[0]["subordinates"] != nil
                {
                    img_EmpPic.addTarget(self, action: #selector(ProfileVC.ShowEmpProfile(_:)), forControlEvents: .TouchUpInside)
                }
                img_EmpPic.titleLabel!.font = UIFont(name: "helvetica-bold", size: 25)
                img_EmpPic.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                img_EmpPic.setTitle(String(self.TempJson[0]["EmployeeName"])[0], forState: UIControlState.Normal)
                scroll.addSubview(img_EmpPic)
                
            }
            else
            {
                let imgView : UIImageView = UIImageView(frame: CGRectMake(xbase + 27, 7, 60, 60))
                imgView.layer.borderWidth = 1
                imgView.layer.masksToBounds = false
                imgView.layer.borderColor = UIColor.whiteColor().CGColor
                imgView.layer.cornerRadius = imgView.frame.height/2
                imgView.clipsToBounds = true
                scroll.addSubview(imgView)
                if let tmpDomainURL = self.defaults.stringForKey("DomainURL")
                {                 
                    
                    let employeePic = "http://" + tmpDomainURL + "/Uploadedfiles/Client" + String(self.defaults.stringForKey("ClientID")!) + "/Image/" + String(self.TempJson[0]["EmployeePhoto"])
                    imgView.setImageWithUrl(NSURL(string: employeePic)!)
                }
                
                if self.TempJson[0]["subordinates"] != nil
                {
                    imgView.tag = self.TempJson[0]["UserID"].int!
                     imgView.userInteractionEnabled = true
                    let GestureLeaveDetail1 = UITapGestureRecognizer(target: self, action: #selector(ProfileVC.ShowEmpProfileImage(_:)))
                    imgView.addGestureRecognizer(GestureLeaveDetail1)
                }
                
            }
            
        
            
            
            
            let lbl_Title : UILabel = UILabel(frame: CGRectMake(xbase , 70, 110, 15))
            lbl_Title.textColor = UIColor.blackColor()
            lbl_Title.textAlignment = .Center
            lbl_Title.font = lbl_Title.font.fontWithSize(12.0)
            scroll.addSubview(lbl_Title)
            lbl_Title.text = self.TempJson[0]["EmployeeName"].string
            
            let lbl_Title1 : UILabel = UILabel(frame: CGRectMake(xbase , 85, 110, 15))
            
            lbl_Title1.textAlignment = .Center
            lbl_Title1.font = lbl_Title1.font.fontWithSize(10.0)
            lbl_Title1.textColor = UIColor.blackColor()
            scroll.addSubview(lbl_Title1)
            
          
            
            lbl_Title1.text = self.TempJson[0]["PositionName"].string
            xbase += 100 + 10
          

        }
        else
        {
        
        for var i = 0; i < self.TempJson.count; i++ {
            
      
            
            if self.ModeOfView == 0
            {
               
                if  (self.TempJson[i]["EmployeePhoto"].type == Talentoz.Type.Null || self.TempJson[i]["EmployeePhoto"].string == ""  )
                {
                    
                    let img_EmpPic:  UIButton = UIButton(frame: CGRectMake(xbase + 27, 7, 60, 60))
                    img_EmpPic.layer.borderWidth = 0.5
                    img_EmpPic.layer.masksToBounds = false
                    img_EmpPic.layer.borderColor = UIColor.lightGrayColor().CGColor
                    img_EmpPic.backgroundColor = UIColor.SetRandomColor()
                    img_EmpPic.layer.cornerRadius = img_EmpPic.frame.height/2
                    img_EmpPic.clipsToBounds = true
                    if self.TempJson[i]["subordinates"] != nil
                    {
                        img_EmpPic.tag = self.TempJson[i]["UserID"].int!
                        img_EmpPic.addTarget(self, action: #selector(ProfileVC.ShowEmpProfile(_:)), forControlEvents: .TouchUpInside)
                    }
                    img_EmpPic.titleLabel!.font = UIFont(name: "helvetica-bold", size: 25)
                    img_EmpPic.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                    img_EmpPic.setTitle(String(self.TempJson[i]["EmployeeName"])[0], forState: UIControlState.Normal)
                    scroll.addSubview(img_EmpPic)
                    
                   
                    
                }
                else
                {
                    let imgView : UIImageView = UIImageView(frame: CGRectMake(xbase + 27, 7, 60, 60))
                    imgView.layer.borderWidth = 1
                    imgView.layer.masksToBounds = false
                    imgView.layer.borderColor = UIColor.whiteColor().CGColor
                    imgView.layer.cornerRadius = imgView.frame.height/2
                    imgView.clipsToBounds = true
                    scroll.addSubview(imgView)
                    if let tmpDomainURL = self.defaults.stringForKey("DomainURL")
                    {
                        
                        let employeePic = "http://" + tmpDomainURL + "/Uploadedfiles/Client" + String(self.defaults.stringForKey("ClientID")!) + "/Image/" + String(self.TempJson[i]["EmployeePhoto"])
                        imgView.setImageWithUrl(NSURL(string: employeePic)!)
                    }
                    
                    if self.TempJson[i]["subordinates"] != nil
                    {
                        imgView.tag = self.TempJson[i]["UserID"].int!
                      imgView.userInteractionEnabled = true
                        let GestureLeaveDetail1 = UITapGestureRecognizer(target: self, action: #selector(ProfileVC.ShowEmpProfileImage(_:)))
                        imgView.addGestureRecognizer(GestureLeaveDetail1)
                    }
                    
                }
                
                
                let lbl_Title : UILabel = UILabel(frame: CGRectMake(xbase , 70, 110, 15))
                lbl_Title.backgroundColor = UIColor.whiteColor()
                lbl_Title.textAlignment = .Center
                lbl_Title.font = lbl_Title.font.fontWithSize(12.0)
                lbl_Title.textColor = UIColor.blackColor()
                scroll.addSubview(lbl_Title)
                lbl_Title.text =  self.TempJson[i]["EmployeeName"].string
                
                let lbl_Title1 : UILabel = UILabel(frame: CGRectMake(xbase , 85, 110, 15))
                lbl_Title1.backgroundColor = UIColor.whiteColor()
                lbl_Title1.textAlignment = .Center
                lbl_Title1.font = lbl_Title1.font.fontWithSize(10.0)
                 lbl_Title1.textColor = UIColor.blackColor()
                scroll.addSubview(lbl_Title1)
                lbl_Title1.text =  self.TempJson[i]["PositionName"].string
                xbase += 100 + 10
              
              
                
            }
            else
            {
                
                let btnButton : UIButton = UIButton(frame: CGRectMake(xbase + 25 , 12, 50, 50))
                btnButton.backgroundColor = UIColor.clearColor()
                btnButton.layer.cornerRadius = btnButton.frame.height/2
                btnButton.layer.borderWidth = 4
                if  (self.TempJson[i]["LeaveColorCode"].type == Talentoz.Type.Null || self.TempJson[i]["LeaveColorCode"].string == ""  )
                {
                    
                btnButton.layer.borderColor = UIColor(colorLiteralRed: 61/255, green: 190/255, blue: 1, alpha: 1).CGColor
                  //  red: 61/255, green: 190/255, blue:255/255, alpha: 1
                }
                else
                {
                     btnButton.layer.borderColor = UIColor(hexString: String(self.TempJson[i]["LeaveColorCode"]))!.CGColor
                }
                //btnButton.titleLabel?.textColor = UIColor.redColor()
                btnButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
                
                
                btnButton.setTitle(String(self.TempJson[i]["LeaveBalance"]), forState: UIControlState.Normal)
                btnButton.titleLabel!.font = UIFont(name: "helvetica-bold", size: 15)
                scroll.addSubview(btnButton)
                // btnButton.text = String(self.SubJson[i]["EmployeeName"])
                
                let lbl_Title : UILabel = UILabel(frame: CGRectMake(xbase, 75, 100, 15))
                lbl_Title.backgroundColor = UIColor.whiteColor()
                lbl_Title.textAlignment = .Center
                lbl_Title.font = lbl_Title.font.fontWithSize(12.0)
                scroll.addSubview(lbl_Title)
                lbl_Title.text =  self.TempJson[i]["LeaveType"].string
                xbase += 100 + 10
            }

            
        }
        }
        
        scroll.contentSize = CGSizeMake(xbase, scroll.frame.size.height)
        
        
        return cell
        
    }
    
    func ShowEmpProfile(sender : UIButton)
    {
        let  ProfileVCon = ProfileVC()
        ProfileVCon.UserID = (sender.tag)
        
        self.presentViewController(ProfileVCon, animated: true, completion: nil)
    }
    
    func ShowEmpProfileImage(sender : UITapGestureRecognizer)
    {
        let  ProfileVCon = ProfileVC()
        ProfileVCon.UserID = (sender.view?.tag)!
        
        self.presentViewController(ProfileVCon, animated: true, completion: nil)
    }
    
    func GetSubordinateList()
    {
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            objSubMgrActivityIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            objSubMgrActivityIndicaor.activityIndicatorViewStyle = .WhiteLarge
            var objPosition =  self.view.center
            objPosition.y =  objPosition.y.advancedBy(-40.0)
            objSubMgrActivityIndicaor.center = objPosition
            objSubMgrActivityIndicaor.hidesWhenStopped = true
            objSubMgrActivityIndicaor.color = UIColor.grayColor()
            self.view.addSubview(objSubMgrActivityIndicaor)
            objSubMgrActivityIndicaor.startAnimating()
            
            self.ModeOfView = 0
            objCallBack3.MethodName = "GetFullSubList"
            let ResultJSON = objCallBack3.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack3.ParameterList.append(objparam)
            
            if self.UserID == -1
            {
                objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            }
            else
            {
                objparam = Parameter(Name: "pUserID",value: String(self.UserID))
            }
            objCallBack3.ParameterList.append(objparam)
            
            objCallBack3.post(GetSubordinateListOnComplete, OnError: EmpProfileOnErrorOnError)
            
        }
    }
    
    func BindLeaveBalance(){
        
        objLBActivityIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
        objLBActivityIndicaor.activityIndicatorViewStyle = .WhiteLarge
        var objPosition =  self.view.center
        objPosition.y =  objPosition.y.advancedBy(-40.0)
        objLBActivityIndicaor.center = objPosition
        objLBActivityIndicaor.hidesWhenStopped = true
        objLBActivityIndicaor.color = UIColor.grayColor()
        self.view.addSubview(objLBActivityIndicaor)
        objLBActivityIndicaor.startAnimating()
        
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
           
            objCallBack2.MethodName = "GetEligebleLeaves"
            let ResultJSON = objCallBack2.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack2.ParameterList.append(objparam)
            
            if self.UserID == -1
            {
                objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            }
            else
            {
                objparam = Parameter(Name: "pUserID",value: String(self.UserID))
            }
            objCallBack2.ParameterList.append(objparam)
            
            objCallBack2.post(setupCollectionView, OnError: EmpProfileOnErrorOnError)
            
        }
    }
    
    func setupCollectionView (ResultString :NSString? ) {
        
        dispatch_async(dispatch_get_main_queue(),{
             self.ModeOfView = 1
            self.TempJson = JSON(data: self.objCallBack2.GetJSONData(ResultString)!)
            
            self.objLBActivityIndicaor.stopAnimating()
            
            var Yaxis : CGFloat = 0
            
            if self.IsTeamApplicable == true
            {
                Yaxis = 150
            }
            
            let ContainerTitalView = UIView(frame: CGRectMake(0, Yaxis + 650, self.view.frame.width , 35))
            ContainerTitalView.backgroundColor = UIColor(hexString: "#f5f5f5")
            self.vscrollView.addSubview(ContainerTitalView)

            
            
            let lblLeaveBalanceTitle : UILabel = UILabel(frame: CGRectMake(15, 0, self.view.frame.width , 30))
            
            lblLeaveBalanceTitle.textAlignment = .Left
            lblLeaveBalanceTitle.font = UIFont(name: "helvetica-bold", size: 14)
            lblLeaveBalanceTitle.text = "Leave Balance"
            lblLeaveBalanceTitle.textColor = UIColor(hexString: "#333333")
            ContainerTitalView.addSubview(lblLeaveBalanceTitle)
            
            let dCollectionViewFlowLayout : UICollectionViewFlowLayout = UICollectionViewFlowLayout ()
            self.dCollectionView = UICollectionView (frame: self.view.frame, collectionViewLayout: dCollectionViewFlowLayout)
            self.dCollectionView.delegate = self
            self.dCollectionView.dataSource = self
            self.dCollectionView.scrollEnabled=false
            self.dCollectionView.backgroundColor = UIColor.blackColor()
            
            self.dCollectionView.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "cellIdentifier")
            self.vscrollView.addSubview(self.dCollectionView)
            
            self.dCollectionView.frame = CGRectMake(0,Yaxis + 700, self.view.frame.size.width,100)
            
        })
        
        
        
    }
 

 
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
