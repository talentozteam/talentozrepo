//
//  ClaimsFinalSubmit.swift
//  Talentoz
//
//  Created by forziamac on 20/05/16.
//  Copyright © 2016 Forzia Tech Private Ltd. All rights reserved.
//

import UIKit

class ClaimsFinalSubmit: BaseTabBarVC {
    
     //# MARK: Control Outlets
    @IBOutlet var txtPurpose: UITextField!
    @IBOutlet var vw_PurposeSeparate: UIView!
    @IBOutlet var btnFromDate: UIButton!
    @IBOutlet var btnFromDate_DownSymbol: UIButton!
    @IBOutlet var vw_FDSeparate: UIView!
    @IBOutlet var btnTodate: UIButton!
    @IBOutlet var btnTodate_DownSymbol: UIButton!
    @IBOutlet var vw_TDSeparate: UIView!
    @IBOutlet var vw_TotalClaim: UIView!
    @IBOutlet var lblTotalClaim: UILabel!
    @IBOutlet var lblCurrencySymbol: UILabel!
    @IBOutlet var lblTotalAmount: UILabel!
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var innerview: UIView!
    @IBOutlet var UIScroll: UIScrollView!
    
    
    //#MARK: Local Variables
    var FilterView : UIView!
    var FilterBGLayer : UIView!
    var isFilterOpen = false
    var iFromDate : NSDate!
    var iToDate : NSDate!
    var iItemsIDs: String = "-1"
    var objActivityIndicaor : UIActivityIndicatorView!
    var objCallBack = AjaxCallBack()
    let defaults = NSUserDefaults.standardUserDefaults()
    
    //#MARK: Public Variables
   internal var ClaimItemsArray = [ClaimDetailsItem]()
    
    
 //# MARK: Default Methods
    override func viewDidLoad() {
        
        super.UINavigationBarTitle = "Submit Claims"
        super.viewDidLoad()
        
        if(Viewframe.width==320)
        {
            self.UIScroll.contentSize = CGSize(  width: Viewframe.width, height: 680)
        }
        else
        {
            self.UIScroll.frame = CGRectMake(0, 0 ,Viewframe.width, 680)
        }
        
        DisplayDesign()
        TotalAmount()
        
        txtPurpose.delegate = self

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//# MARK: Button Actions
    
    @IBAction func btnSubmit_Click(sender: AnyObject) {
    }
    
    @IBAction func btnFromDate_Click(sender: AnyObject) {
        ShowDatePicker(sender as! UIButton)
    }
    @IBAction func btnFromDateDown_Click(sender: AnyObject) {
        ShowDatePicker(sender as! UIButton)
    }
    @IBAction func btnToDate_Click(sender: AnyObject) {
        ShowDatePicker(sender as! UIButton)
    }
    @IBAction func btnToDateDown_Click(sender: AnyObject) {
        ShowDatePicker(sender as! UIButton)
    }
    
    
    func textField(textField: UITextField,
                   shouldChangeCharactersInRange range: NSRange,
                                                 replacementString string: String) -> Bool {
        
            let maxLength = 100
            let currentString: NSString = textField.text!
            let newString: NSString =
                currentString.stringByReplacingCharactersInRange(range, withString: string)
            return newString.length <= maxLength
       
        
        
        
    }
    
//# MARK: Date Picker Actions
    
    func DisplayDesign()  {
        
        self.innerview.frame = CGRectMake(Viewframe.width / 100 * 2 , 75  ,Viewframe.width / 100 * 96, Viewframe.height)
        
        self.txtPurpose.frame = CGRectMake(Viewframe.width / 100 * 5 , 20  ,innerview.frame.size.width / 100 * 90, 22)
        
        txtPurpose.layer.borderWidth = 0
        txtPurpose.layer.cornerRadius = 0
        txtPurpose.layer.borderColor = UIColor.clearColor().CGColor
        txtPurpose.textColor = UIColor(hexString: "#666666")
        txtPurpose.font = UIFont(name:"HelveticaNeue-bold", size: 14.0)
        self.vw_PurposeSeparate.frame = CGRectMake(Viewframe.width / 100 * 5 , 44  ,innerview.frame.size.width / 100 * 90, 0.5)
        vw_PurposeSeparate.backgroundColor = UIColor(hexString: "#cccccc")
        
        self.btnFromDate.frame = CGRectMake(Viewframe.width / 100 * 5 , 75  ,innerview.frame.size.width / 100 * 90, 20)
        self.btnFromDate.backgroundColor = UIColor.clearColor()
        self.btnFromDate.layer.cornerRadius=5
        self.btnFromDate.tag = 1
        self.btnFromDate.setTitleColor(UIColor.init(hexString: "#666666"), forState: UIControlState.Normal)
        self.btnFromDate_DownSymbol.frame = CGRectMake(Viewframe.width / 100 * 85 , 75  ,innerview.frame.size.width / 100 * 10, 20)
        self.btnFromDate_DownSymbol.setFAIcon(FAType.FAAngleDown, iconSize: CGFloat(20),forState: .Normal)
          self.btnFromDate_DownSymbol.tag = 1
        self.vw_FDSeparate.frame = CGRectMake(Viewframe.width / 100 * 5 , 95  ,innerview.frame.size.width / 100 * 90, 1)
     
        
        
        self.btnTodate.frame = CGRectMake(Viewframe.width / 100 * 5 , 125  ,Viewframe.width / 100 * 90, 20)
        self.btnTodate.backgroundColor = UIColor.clearColor()
        self.btnTodate.layer.cornerRadius=5
        self.btnTodate.tag = 2
        self.btnTodate.setTitleColor(UIColor.init(hexString: "#666666"), forState: UIControlState.Normal)
        self.btnTodate_DownSymbol.frame = CGRectMake(Viewframe.width / 100 * 85 , 125  ,innerview.frame.size.width / 100 * 10, 20)
        self.btnTodate_DownSymbol.setFAIcon(FAType.FAAngleDown, iconSize: CGFloat(20),forState: .Normal)
          self.btnTodate_DownSymbol.tag = 2
        self.vw_TDSeparate.frame = CGRectMake(Viewframe.width / 100 * 5 , 145  ,innerview.frame.size.width / 100 * 90, 1)
        
        
        
        
        
        self.lblTotalClaim.frame = CGRectMake(50, 10, Viewframe.width ,35)
        self.lblTotalClaim.textAlignment = .Left
        self.lblTotalClaim.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
        self.lblTotalClaim.textColor = UIColor(hexString: "#666666")
        
        
        
        self.lblCurrencySymbol.frame = CGRectMake(80, 40, Viewframe.width / 100 * 15 ,35)
        self.lblCurrencySymbol.textAlignment = .Left
        self.lblCurrencySymbol.font = UIFont(name:"HelveticaNeue-Bold", size: 16.0)
        self.lblCurrencySymbol.textColor = UIColor(hexString: "#666666")
        
        
        
        self.lblTotalAmount.frame = CGRectMake(125, 40, Viewframe.width ,35)
        self.lblTotalAmount.textAlignment = .Left
        self.lblTotalAmount.font = UIFont(name:"HelveticaNeue-Bold", size: 24.0)
        self.lblTotalAmount.textColor = UIColor(hexString: "#666666")
        
        
        
        
        self.vw_TotalClaim.frame = CGRectMake(Viewframe.width / 100 * 5 , 200  ,innerview.frame.size.width / 100 * 90, 100)
        
    
        
        
        self.btnSubmit.frame = CGRectMake(0, 390  ,innerview.frame.size.width, 30)
        self.btnSubmit.setTitle("Submit", forState: .Normal)
        self.btnSubmit.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        self.btnSubmit.setFAText(prefixText: "", icon: FAType.FACheck, postfixText:  "  Submit", size: 14 , forState: .Normal)
        self.btnSubmit.addTarget(self, action: #selector(ClaimsFinalSubmit.ValidateAndSaveTheInputs(_:)), forControlEvents: .TouchUpInside)
        
        
        
        
    }
    
    func ShowDatePicker(sender:UIButton)
    {
        
        FilterBGLayer = UIView(frame: CGRectMake(0, 0, Viewframe.width  ,  Viewframe.height))
        FilterBGLayer.backgroundColor = UIColor(colorLiteralRed: 0 , green: 0, blue: 0, alpha: 0.6)
        let GestureLeaveDetail1 = UITapGestureRecognizer(target: self, action: #selector(ClaimsFinalSubmit.DateChanged(_:)))
        FilterBGLayer.addGestureRecognizer(GestureLeaveDetail1)
        FilterView = UIView(frame: CGRectMake(FilterBGLayer.frame.width / 100 * 10 , FilterBGLayer.frame.height  / 100 * 20 , FilterBGLayer.frame.width  / 100 * 80, FilterBGLayer.frame.height  / 100 * 50))
        FilterView.backgroundColor = UIColor.whiteColor()
        FilterView.layer.cornerRadius = 8
        let TimePick : UIDatePicker =  UIDatePicker(frame: CGRect(x: 10, y: 5, width: FilterView.frame.width - 10 ,height: FilterView.frame.width - 30))
        TimePick.datePickerMode = .Date
        let calendar: NSCalendar = NSCalendar(calendarIdentifier: NSGregorianCalendar)!
        // let calendar: NSCalendar = NSCalendar.currentCalendar()
        calendar.timeZone = NSTimeZone(name: "UTC")!
        var DateStr = ""
        
        if (sender.tag == 1 && self.iFromDate == nil) || (sender.tag == 2 && self.iToDate == nil)
        {
            let date = NSDate()
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components([.Year,.Month,.Day], fromDate: date)
            
            DateStr = String(components.day) + "/" + String(components.month) + "/" + String(components.year)
        }
        else
        {
            if sender.tag == 1
            {
                DateStr = (self.btnFromDate.titleLabel?.text)!
            }
            else
            {
                DateStr = (self.btnTodate.titleLabel?.text)!
                
            }
            
        }
        
        var array = DateStr.componentsSeparatedByString("/")
        let components: NSDateComponents = NSDateComponents()
        components.year = Int(array[2])!
        components.month = Int(array[1])!
        components.day = Int(array[0])!
        let defaultDate: NSDate = calendar.dateFromComponents(components)!
        TimePick.date = defaultDate
        
        TimePick.addTarget(self, action: #selector(ClaimsFinalSubmit.handlerDate(_:)), forControlEvents: UIControlEvents.ValueChanged)
        
        
        
        let timeFormatter = NSDateFormatter()
        timeFormatter.dateFormat = "dd/MM/yyyy"
        
        
        if sender.tag == 1
        {
            self.iFromDate = defaultDate
        }
        else
        {
            self.iToDate = defaultDate
        }
        
        if (sender.tag == 1 )
        {
            self.btnFromDate.setTitle(String(timeFormatter.stringFromDate(defaultDate)), forState: .Normal)
        }
        else
        {
            self.btnTodate.setTitle(String(timeFormatter.stringFromDate(defaultDate)), forState: .Normal)
        }
        
       
        
        
        
        
        let btn_ApplyFilter : UIButton = UIButton(frame: CGRectMake(FilterBGLayer.frame.width / 100 * 22, TimePick.frame.height + 26, 120 ,24))
        btn_ApplyFilter.setTitle("Go", forState: .Normal)
        btn_ApplyFilter.layer.cornerRadius = 5
        btn_ApplyFilter.tag = sender.tag
        btn_ApplyFilter.backgroundColor = UIColor(hexString: "#71C974")
        btn_ApplyFilter.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        btn_ApplyFilter.titleLabel!.font =  UIFont(name:"helvetica-Bold", size: 14)
        btn_ApplyFilter.addTarget(self, action: #selector(ClaimsFinalSubmit.DateChanged(_:)), forControlEvents: .TouchUpInside)
        FilterView.addSubview(btn_ApplyFilter)
        
        FilterView.addSubview(TimePick)
        FilterBGLayer.addSubview(FilterView)
        self.view.addSubview(FilterBGLayer)
        
    }
    
    func DateChanged(sender: UIButton)
    {
        
        
        isFilterOpen = false
        FilterBGLayer.removeFromSuperview()
        
    }
    
  
    
    
    func handlerDate(sender: UIDatePicker) {
        let timeFormatter = NSDateFormatter()
        timeFormatter.dateFormat = "dd/MM/yyyy"
        
        if sender.tag == 1
        {
            self.iFromDate = sender.date
            self.btnFromDate.setTitle(String(timeFormatter.stringFromDate(sender.date)), forState: .Normal)
        }
        else
        {
            self.iToDate = sender.date
            self.btnTodate.setTitle(String(timeFormatter.stringFromDate(sender.date)), forState: .Normal)
        }
        
        
        
        
        // do what you want to do with the string.
    }
    
    func TotalAmount(){
        
        var TotalAmount: Double = 0.0
        
        for i in 0 ..< self.ClaimItemsArray.count  {
            
            if(self.ClaimItemsArray[i].Checked == true)
            {
                TotalAmount = TotalAmount + Double(self.ClaimItemsArray[i].Amount)!
                if (self.iItemsIDs == "-1")
                {
                    self.iItemsIDs = String(self.ClaimItemsArray[i].ItemID)
                }
                else
                {
                     self.iItemsIDs = self.iItemsIDs + "," + String(self.ClaimItemsArray[i].ItemID)
                }
            }
        }
        
        self.lblTotalAmount.text = ConvertValueWithDecimal(String(TotalAmount), NoofDecimal: 2)
        self.lblCurrencySymbol.text = self.ClaimItemsArray[0].CurrencyName
        
    }
    
    // Validation
    
    func ValidateAndSaveTheInputs(sender:UIButton)
    {
        
        if (self.txtPurpose.text == "")
        {
            let alert = UIAlertController(title: "Required", message: "Please enter the purpose.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.presentViewController(alert, animated: true, completion: nil)}
            return
        }
        
        if (self.iFromDate == nil )
        {
            let alert = UIAlertController(title: "Required", message: "Please choose the from date.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.presentViewController(alert, animated: true, completion: nil)}
            return
        }
        
        if (self.iToDate  == nil )
        {
            let alert = UIAlertController(title: "Required", message: "Please choose the to date.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.presentViewController(alert, animated: true, completion: nil)}
            return
        }
        
        if self.iFromDate  != nil && self.iToDate != nil
        {
            if self.iFromDate!.compare(self.iToDate!) == NSComparisonResult.OrderedDescending
            {
                //Show the alert for invalid users
                let alert = UIAlertController(title: "Validation", message: "From date can't be greater than to date.", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                
                return
                
            }
        }
        
        self.btnSubmit.enabled = false
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            
            
            objActivityIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height))
            objActivityIndicaor.activityIndicatorViewStyle = .WhiteLarge
            objActivityIndicaor.center = self.view.center
            objActivityIndicaor.transform = CGAffineTransformMakeScale(1.5, 1.5);
            objActivityIndicaor.hidesWhenStopped = true
            objActivityIndicaor.color = UIColor.grayColor()
            self.view.addSubview(objActivityIndicaor)
            objActivityIndicaor.startAnimating()
            
            
            objCallBack = AjaxCallBack()
            
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            objCallBack.MethodName = "SubmitClaimRequest"
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pCurrencyID",value: String(self.ClaimItemsArray[0].CurrencyID))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pPurpose",value: String(self.txtPurpose.text!))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pFromDate",value: String(btnFromDate.titleLabel!.text!))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pToDate",value: String(btnTodate.titleLabel!.text!))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pItemIDs",value: self.iItemsIDs)
            objCallBack.ParameterList.append(objparam)
            
            objCallBack.post(ValidateAndSaveTheInputsOnComplete, OnError: ValidateAndSaveTheInputsOnError)
            
        }
        
        
        
    }
    
    
    func ValidateAndSaveTheInputsOnComplete(ResultString :NSString? ) {
        dispatch_async(dispatch_get_main_queue(),{
            let ResultData =  JSON(data: self.objCallBack.GetJSONData(ResultString)!)
            self.objActivityIndicaor.stopAnimating()
            if ResultData["TypeOfError"] == 4
            {
                
                let alert = UIAlertController(title: "Success", message: "The claim has been submitted successfully.", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: self.CloseForm))
                
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    self.presentViewController(alert, animated: true, completion: nil)}
            }
            else
            {
                
                self.btnSubmit.enabled = true
                
                let alert = UIAlertController(title: "Alert", message: String(ResultData["ErrorMessage"]), preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    self.presentViewController(alert, animated: true, completion: nil)}
            }
        })
        
        
    }
    
    func ValidateAndSaveTheInputsOnError(Response: NSError ){
        
        
    }
    func CloseForm(sender:UIAlertAction)
    {
        let Claimstoryboard = UIStoryboard(name: "ClaimsDashBoard", bundle: nil)
        let vc1 = Claimstoryboard.instantiateViewControllerWithIdentifier("ClaimsDashBoardVC")
        self.presentViewController(vc1, animated: true, completion: nil)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
