//
//  ParseResponse.swift
//  Talentoz
//
//  Created by forziamac on 10/12/15.
//  Copyright © 2015 Forzia Tech Private Ltd. All rights reserved.
//

import Foundation
class ParseResponse:NSObject
{
    var elementValue: String?
    var success = false
    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [NSObject : AnyObject?]) {
        if elementName == "success" {
            elementValue = String()
        }
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String?) {
        
    }
    
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "success" {
            if elementValue == "true" {
                success = true
            }
            elementValue = nil
        }
    }
    
    func parser(parser: NSXMLParser, parseErrorOccurred parseError: NSError) {
       
    }
}
