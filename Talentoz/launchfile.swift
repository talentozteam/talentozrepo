//
//  launchfile.swift
//  Talentoz
//
//  Created by forziamac on 18/01/16.
//  Copyright © 2016 Forzia Tech Private Ltd. All rights reserved.
//

import Foundation
import UIKit
class LaunchVC: UIViewController   {
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
}