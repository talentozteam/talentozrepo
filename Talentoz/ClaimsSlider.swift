//
//  ClaimsSlider.swift
//  Talentoz
//
//  Created by forziamac on 20/04/16.
//  Copyright © 2016 Forzia Tech Private Ltd. All rights reserved.
//


import UIKit

class ClaimsSliderB  : UIViewController
{
    //# MARK: Public Variables
    internal var ClaimDetail : Talentoz.JSON!
    internal var IndexNo: Int! = -1
    
    //# MARK: Control Outlets
    
    @IBOutlet var lblCurrency: UILabel!
    @IBOutlet var lblAmount: UILabel!
    @IBOutlet var ClaimTitle: UILabel!
    @IBOutlet var btnClaim: UIButton!
    
    //# MARK: Local Variables
    var CurrencySymbolWidth : CGFloat = 0.0
    var CurrencyWidth : CGFloat = 0.0
    var Gap : CGFloat = 8
    
    
    //# MARK: Default Methods
     override func viewDidLoad() {        
        self.LoadSlider()
        
        self.ClaimTitle.frame = CGRectMake(self.view.frame.size.width / 100 * 10, 10  , self.view.frame.size.width / 100 * 80, 30)
        
        if ( self.IndexNo == -1)
        {
            self.lblCurrency.frame = CGRectMake(self.view.frame.size.width / 100 * 30, 60  , 50	, 30)
            self.lblAmount.frame = CGRectMake(0, 60  , self.view.frame.size.width, 30)
            self.lblAmount.textAlignment = .Center
        }
        else
        {
            self.lblCurrency.frame = CGRectMake(0 , 60  , self.CurrencySymbolWidth	, 30)
            self.lblCurrency.textAlignment = .Right
            self.lblAmount.frame = CGRectMake(self.CurrencySymbolWidth + Gap, 60  , self.CurrencyWidth, 30)
            self.lblAmount.textAlignment = .Left

            
        }
        self.btnClaim.frame = CGRectMake(self.view.frame.size.width / 100 * 30, 110  , self.view.frame.size.width / 100 * 40, 30)
        
    }
    
     //# MARK: Control Action
    
    @IBAction func btnViewClaims_Click(sender: AnyObject) {
        
        let ClaimListObj = ClaimsRequestListB()
        ClaimListObj.ModeofShow = 0
        self.presentViewController(ClaimListObj, animated: true, completion: nil)
        
    }
    
    //# MARK: Custom Methods
    
    func LoadSlider()
    {
        if ( self.IndexNo == -1)
        {
            self.lblCurrency.text = ""
            self.lblAmount.text = "0.00"
            
            
            
        }
        else
        {
            self.lblCurrency.text = self.ClaimDetail["CurrencyName"].string
           
            self.lblAmount.text = ConvertValueWithDecimal(String(self.ClaimDetail["TotalAmount"]),NoofDecimal: 2)
            
            self.CalculateWidth()
            
        }
    }
    
    func CalculateWidth()  {
        let charcount = self.lblAmount.text?.characters.count
        
        if charcount <= 4
        {
            
            self.CurrencySymbolWidth = (self.view.frame.size.width - self.Gap) / 100 * 39.5
            self.CurrencyWidth = (self.view.frame.size.width - self.Gap) / 100 * 60.5
        }
        else if charcount == 5
        {
            self.CurrencySymbolWidth = (self.view.frame.size.width - self.Gap) / 100 * 38.5
            self.CurrencyWidth = (self.view.frame.size.width - self.Gap) / 100 * 61.5
        }
        else if charcount == 6
        {
            self.CurrencySymbolWidth = (self.view.frame.size.width - self.Gap) / 100 * 37.5
            self.CurrencyWidth = (self.view.frame.size.width - self.Gap) / 100 * 62.5
        }
        else if charcount == 7
        {
            self.CurrencySymbolWidth = (self.view.frame.size.width - self.Gap) / 100 * 36.5
            self.CurrencyWidth = (self.view.frame.size.width - self.Gap) / 100 * 63.5
        }
        else if charcount == 8
        {
            self.CurrencySymbolWidth = (self.view.frame.size.width - self.Gap) / 100 * 35.5
            self.CurrencyWidth = (self.view.frame.size.width - self.Gap) / 100 * 64.5
        }
        else if charcount == 9
        {
            self.CurrencySymbolWidth = (self.view.frame.size.width - self.Gap) / 100 * 30
            self.CurrencyWidth = (self.view.frame.size.width - self.Gap) / 100 * 70
        }
        else if charcount == 10
        {
            self.CurrencySymbolWidth = (self.view.frame.size.width - self.Gap) / 100 * 28
            self.CurrencyWidth = (self.view.frame.size.width - self.Gap) / 100 * 72
        }
        else if charcount == 11
        {
            self.CurrencySymbolWidth = (self.view.frame.size.width - self.Gap) / 100 * 26
            self.CurrencyWidth = (self.view.frame.size.width - self.Gap) / 100 * 74
        }
        else
        {
            self.CurrencySymbolWidth = (self.view.frame.size.width - self.Gap) / 100 * 24
            self.CurrencyWidth = (self.view.frame.size.width - self.Gap) / 100 * 76
        }
        
        
    }
    
    
}

