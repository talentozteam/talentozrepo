//
//  LeaveDetail.swift
//  Talentoz
//
//  Created by forziamac on 28/12/15.
//  Copyright © 2015 Forzia Tech Private Ltd. All rights reserved.
//

import UIKit

class LeaveDetail: BaseTabBarVC {
    
    internal var LeaveDetails = LeaveRequest()
    internal var UserID : Int!
    internal var Mode : Int!
    internal var RowIndex: Int!
    
    private var CurrentActionCode : String = "0"
    
    @IBOutlet var lblEmpName: UILabel!
    @IBOutlet var lblPositionName: UILabel!
    @IBOutlet var lblWorkLocation: UILabel!
    @IBOutlet var lblFromDateTitle: UILabel!
    @IBOutlet var lblToDateTitle: UILabel!
    @IBOutlet var lblLeaveDaysTitle: UILabel!
    @IBOutlet var lblFromDate: UILabel!
    @IBOutlet var lblTodate: UILabel!
    @IBOutlet var lblLeaveDay: UILabel!
    @IBOutlet var lblLeaveTypeTitle: UILabel!
    @IBOutlet var lblLeaveType: UILabel!
    @IBOutlet var lblReasonTitle: UILabel!
    @IBOutlet var lblReason: UILabel!
    @IBOutlet var btnApprovaloutlet: UIButton!
    @IBOutlet var btnRejectOutlet: UIButton!
    @IBOutlet var lblApprovedStatus: UILabel!
    @IBOutlet var img_EmpPic: UIImageView!
    var objLoadingIndicaor : UIActivityIndicatorView!
    
    var OnCompletion: ((RowID:Int , ActionCode:String) -> Void )!
    
    let objCallBack = AjaxCallBack()
    let defaults = NSUserDefaults.standardUserDefaults()
    

    //Handles leave approval event
    @IBAction func ApproveTouch(sender: UIButton) {
        
        objLoadingIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, self.view.frame.width, self.view.frame.height))
        
        objLoadingIndicaor.activityIndicatorViewStyle = .WhiteLarge
        let objPosition =  self.view.center
        objLoadingIndicaor.center = objPosition
        objLoadingIndicaor.hidesWhenStopped = true
        objLoadingIndicaor.color = UIColor.grayColor()
        self.view.addSubview(objLoadingIndicaor)
        objLoadingIndicaor.startAnimating()
            
        self.ProcessLeaveRequest("2")
        
        
    }
    
    //Handles leave reject event
    @IBAction func RejectTouch(sender: UIButton) {
        objLoadingIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, self.view.frame.width, self.view.frame.height))
        
        objLoadingIndicaor.activityIndicatorViewStyle = .WhiteLarge
        let objPosition =  self.view.center
        objLoadingIndicaor.center = objPosition
        objLoadingIndicaor.hidesWhenStopped = true
        objLoadingIndicaor.color = UIColor.grayColor()
        self.view.addSubview(objLoadingIndicaor)
        objLoadingIndicaor.startAnimating()
        
        self.ProcessLeaveRequest("3")
 
    }
    func ProcessLeaveRequest(ActionCode: String)
    {
        self.CurrentActionCode = ActionCode
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            
            objCallBack.MethodName = "ApproveLeave"
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pRequestID",value: String(self.LeaveDetails.RequestID))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pAction",value: ActionCode)
            objCallBack.ParameterList.append(objparam)
            
            objCallBack.post(ApproveOnSuccess, OnError: ApproveOnError)
            
        }
        
    }
    
    //Response on error callback
    func ApproveOnError(Response: NSError ){
        
        
    }
    
    //Respone on success callback
    func ApproveOnSuccess(ResultString :NSString? ) {
  
            dispatch_async(dispatch_get_main_queue(),{
                
                
                var MessageText = ""
                
                if self.CurrentActionCode == "2"
                {
                    self.LeaveDetails.StatusText = "Approved"
                    self.LeaveDetails.Access = 0
                    MessageText = "approved"
                    
                }else{
                    
                    self.LeaveDetails.StatusText = "Rejected"
                    self.LeaveDetails.Access = 0
                    MessageText = "rejected"
                }
                
                self.objLoadingIndicaor.stopAnimating()
        self.btnApprovaloutlet.removeFromSuperview()
        self.btnRejectOutlet.removeFromSuperview()
             
                let alert = UIAlertController(title: "Success", message: "The leave has been " + MessageText + " successfully.", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    self.presentViewController(alert, animated: true, completion: nil)
                }
        })
       
        
      

   
    }
    
    override func goBack() {
        if self.Mode != -1
        {
        self.OnCompletion(RowID: self.RowIndex,ActionCode: self.CurrentActionCode )
        }
        dispatch_async(dispatch_get_main_queue(),{
            self.dismissViewControllerAnimated(true, completion: nil)
        })
    }
    

    override func viewDidLoad() {
        
        for subview in   self.view.subviews
        {
            subview.hidden = true
            
        }
        
        
        
        super.viewDidLoad()
        
        
        self.view.backgroundColor = UIColor.whiteColor()
      super.UINavigationBarTitle = "Leave Details"
      
        BindLeaveDetails()
        
     
        

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {
        
        
        super.viewDidAppear(animated)
         AlignLeaveDetails()
        for subview in   self.view.subviews
        {
            subview.hidden = false
            
        }
        
        if self.Mode == 0
        {
            self.btnApprovaloutlet.hidden = true
            self.btnRejectOutlet.hidden = true
            
            CreateDynamicLabel()
        }
        else
        {
            ModifyButtons()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 
    func AlignLeaveDetails()
    {
        
        dispatch_async(dispatch_get_main_queue(),{
            
            let FontSize = CGFloat(10)
            
            self.img_EmpPic.frame = CGRectMake(self.view.frame.size.width / 100 * 5, 65  ,50, 50)
            
            
            self.lblPositionName.font = self.lblEmpName.font.fontWithSize(FontSize)
            self.lblWorkLocation.font = self.lblEmpName.font.fontWithSize(FontSize)
            self.lblFromDateTitle.font = self.lblEmpName.font.fontWithSize(FontSize)
            self.lblToDateTitle.font = self.lblEmpName.font.fontWithSize(FontSize)
            self.lblLeaveDaysTitle.font = self.lblEmpName.font.fontWithSize(FontSize)
            
            self.lblFromDate.font = self.lblEmpName.font.fontWithSize(FontSize)
            self.lblTodate.font = self.lblEmpName.font.fontWithSize(FontSize)
            self.lblLeaveDay.font = self.lblEmpName.font.fontWithSize(FontSize)
            
            self.lblLeaveTypeTitle.font = self.lblEmpName.font.fontWithSize(FontSize)
            self.lblLeaveType.font = self.lblEmpName.font.fontWithSize(FontSize)
            
            self.lblReasonTitle.font = self.lblEmpName.font.fontWithSize(FontSize)
            self.lblReason.font = self.lblEmpName.font.fontWithSize(FontSize)
            
            self.lblEmpName.frame = CGRectMake((self.view.frame.size.width / 100 * 25 ) , 65 ,self.view.frame.size.width / 100 * 55, 18)
            self.lblEmpName.textColor = UIColor(hexString: "#333333")
            self.lblEmpName.font =  UIFont(name:"HelveticaNeue-Bold", size: 12.0)
            
            self.lblPositionName.frame = CGRectMake((self.view.frame.size.width / 100 * 25 ) , 83 ,self.view.frame.size.width / 100 * 75, 18)
            self.lblPositionName.textColor = UIColor(hexString: "#666666")
            self.lblPositionName.font =  UIFont(name:"HelveticaNeue", size: 12.0)
            
            
            self.lblWorkLocation.frame = CGRectMake((self.view.frame.size.width / 100 * 25 ) , 101 ,self.view.frame.size.width / 100 * 75, 18)
            self.lblWorkLocation.textColor = UIColor(hexString: "#666666")
            self.lblWorkLocation.font =  UIFont(name:"HelveticaNeue", size: 12.0)
            
            
            
            self.lblFromDateTitle.frame = CGRectMake((self.view.frame.size.width / 100 * 5 ) , self.view.frame.size.height / 100 * 26 ,self.view.frame.size.width / 100 * 30, 18)
            self.lblFromDateTitle.textColor = UIColor(hexString: "#333333")
            self.lblFromDateTitle.font =  UIFont(name:"HelveticaNeue-Bold", size: 12.0)
            
            
            self.lblToDateTitle.frame = CGRectMake((self.view.frame.size.width / 100 * 35 ) , self.view.frame.size.height / 100 * 26 ,self.view.frame.size.width / 100 * 30, 18)
            self.lblToDateTitle.textColor = UIColor(hexString: "#333333")
            self.lblToDateTitle.font =  UIFont(name:"HelveticaNeue-Bold", size: 12.0)
            
            
            self.lblLeaveDaysTitle.frame = CGRectMake((self.view.frame.size.width / 100 * 70 ) , self.view.frame.size.height / 100 * 26 ,self.view.frame.size.width / 100 * 30, 18)
            self.lblLeaveDaysTitle.textColor = UIColor(hexString: "#333333")
            self.lblLeaveDaysTitle.font =  UIFont(name:"HelveticaNeue-Bold", size: 12.0)
            
            
            
            self.lblFromDate.frame = CGRectMake((self.view.frame.size.width / 100 * 5 ) , self.view.frame.size.height / 100 * 30 ,self.view.frame.size.width / 100 * 30, 18)
            
            self.lblFromDate.textColor = UIColor(hexString: "#666666")
            self.lblFromDate.font =  UIFont(name:"HelveticaNeue", size: 12.0)
            
            self.lblTodate.frame = CGRectMake((self.view.frame.size.width / 100 * 35 ) , self.view.frame.size.height / 100 * 30 ,self.view.frame.size.width / 100 * 30, 18)
            self.lblTodate.textColor = UIColor(hexString: "#666666")
            self.lblTodate.font =  UIFont(name:"HelveticaNeue", size: 12.0)
            
            
            self.lblLeaveDay.frame = CGRectMake((self.view.frame.size.width / 100 * 70 ) , self.view.frame.size.height / 100 * 30 ,self.view.frame.size.width / 100 * 30, 18)
            self.lblLeaveDay.textColor = UIColor(hexString: "#666666")
            self.lblLeaveDay.font =  UIFont(name:"HelveticaNeue", size: 12.0)
            
            
            self.lblLeaveTypeTitle.frame = CGRectMake((self.view.frame.size.width / 100 * 5 ) , self.view.frame.size.height / 100 * 41 ,self.view.frame.size.width / 100 * 90, 18)
            self.lblLeaveTypeTitle.textColor = UIColor(hexString: "#333333")
            self.lblLeaveTypeTitle.font =  UIFont(name:"HelveticaNeue-Bold", size: 12.0)
            
            
            self.lblLeaveType.frame = CGRectMake((self.view.frame.size.width / 100 * 5 ) , self.view.frame.size.height / 100 * 45 ,self.view.frame.size.width / 100 * 90, 18)
            self.lblLeaveType.textColor = UIColor(hexString: "#666666")
            self.lblLeaveType.font =  UIFont(name:"HelveticaNeue", size: 12.0)
            
            
            
            self.lblReasonTitle.frame = CGRectMake((self.view.frame.size.width / 100 * 5 ) , self.view.frame.size.height / 100 * 54 ,self.view.frame.size.width / 100 * 90, 18)
            self.lblReasonTitle.textColor = UIColor(hexString: "#333333")
            self.lblReasonTitle.font =  UIFont(name:"HelveticaNeue-Bold", size: 12.0)
            
            
            
            
            self.lblReason.frame = CGRectMake((self.view.frame.size.width / 100 * 5 ) , self.view.frame.size.height / 100 * 58 ,self.view.frame.size.width / 100 * 90, 50)
            self.lblReason.textColor = UIColor(hexString: "#666666")
            self.lblReason.font =  UIFont(name:"HelveticaNeue", size: 12.0)
            self.lblReason.lineBreakMode = .ByWordWrapping
            self.lblReason.numberOfLines = 0
            
            
            self.btnApprovaloutlet.setFAText(prefixText: "", icon: FAType.FACheck, postfixText: "   Approve", size: CGFloat(14), forState: .Normal)
            self.btnRejectOutlet.setFAText(prefixText: "", icon: FAType.FAClose, postfixText: "   Reject", size: CGFloat(14), forState: .Normal)
            
            self.btnApprovaloutlet.frame = CGRectMake((self.view.frame.size.width / 100 * 5 ) , self.view.frame.size.height / 100 * 70 ,self.view.frame.size.width / 100 * 40, 25)
            self.btnRejectOutlet.frame = CGRectMake((self.view.frame.size.width / 100 * 55 ) , self.view.frame.size.height / 100 * 70 ,self.view.frame.size.width / 100 * 40, 25)
            
            self.btnApprovaloutlet.backgroundColor = UIColor(hexString: "#48C9B0")
            self.btnRejectOutlet.backgroundColor = UIColor(hexString: "#F46868")
            
            self.btnApprovaloutlet.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            self.btnRejectOutlet.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            
        })
    
    }
    
    func BindLeaveDetails()
    {
        
        if let tmpDomainURL = self.defaults.stringForKey("DomainURL")
        {
            
            if  (self.LeaveDetails.EmployeePhoto == nil )
            {
                
                let img_EmpPic:  UIButton = UIButton(frame: CGRectMake(self.view.frame.size.width / 100 * 5, 70  ,50, 50))
                img_EmpPic.layer.masksToBounds = false
                //img_EmpPic.layer.borderWidth = 1
                
                img_EmpPic.backgroundColor = UIColor(hexString: "#f5f5f5")
                img_EmpPic.clipsToBounds = true
                img_EmpPic.layer.borderWidth = 1
                img_EmpPic.layer.borderColor = UIColor(hexString: "#cccccc")?.CGColor
                // img_EmpPic.titleLabel!.font = UIFont(name: "helvetica-bold", size: 25)
                 
                img_EmpPic.setTitleColor(UIColor(hexString: "#666666"), forState: UIControlState.Normal)
                img_EmpPic.setFAIcon(FAType.FAUser, iconSize: 35, forState: .Normal)
                self.view.addSubview(img_EmpPic)
                
            }
            else
            {
                let employeePic = "http://" + tmpDomainURL + "/Uploadedfiles/Client" + String(self.defaults.stringForKey("ClientID")!) + "/Image/" + String(self.LeaveDetails.EmployeePhoto)
                img_EmpPic.setImageWithUrl(NSURL(string: employeePic)!)
            }
            
            
       
        }
        
        lblEmpName.text = String(self.LeaveDetails.EmployeeName)
        lblPositionName.text = String(self.LeaveDetails.PositionName)
        lblWorkLocation.text = String(self.LeaveDetails.WorkLocationName)
        lblFromDate.text = String(self.LeaveDetails.StartDateString)
        lblTodate.text = String(self.LeaveDetails.EndDateString)
        lblLeaveDay.text = String(self.LeaveDetails.NOOfDays)
        lblLeaveType.text = String(self.LeaveDetails.RequestDescription)
        lblReason.text = String(self.LeaveDetails.Reason)
        lblApprovedStatus.text = String(self.LeaveDetails.StatusText)
        lblApprovedStatus.hidden = true
        
    }
    
    func CreateDynamicLabel()
    {
        let lbl_LeaveStatus : UILabel = UILabel(frame: CGRectMake((self.view.frame.size.width / 100 * 55 ) , 65 ,self.view.frame.size.width / 100 * 43, 18))
        // lbl_PositionName.backgroundColor = UIColor.whiteColor()
        lbl_LeaveStatus.textAlignment = .Right
        if (self.LeaveDetails.BaseStatus == 1)
        {
            lbl_LeaveStatus.textColor = UIColor(hexString: "#e67f22")
        } else if (self.LeaveDetails.BaseStatus == 2) {
            
            lbl_LeaveStatus.textColor = UIColor(hexString: "#399a10")
        } else if (self.LeaveDetails.BaseStatus == 3) {
            lbl_LeaveStatus.textColor = UIColor(hexString: "#95a5a5")
        } else {
            lbl_LeaveStatus.textColor = UIColor(hexString: "#ff8877")
        }
        
        lbl_LeaveStatus.font =  UIFont(name:"HelveticaNeue-Bold", size: 12.0)
        lbl_LeaveStatus.text = String(self.LeaveDetails.StatusText)
       lbl_LeaveStatus.textAlignment = .Right
        
        self.view.addSubview(lbl_LeaveStatus)
    }
    
    func ModifyButtons ()
    {
        
        if (self.LeaveDetails.ISHRApply == 0 ) {
            if self.LeaveDetails.Access  == 1 {
                if self.LeaveDetails.Type  == 2 {
                    //self.CreateDynamicButton(cell, ButtonMode: 0, Xpos: 70, row: row)
                    //self.CreateDynamicButton(cell, ButtonMode: 1, Xpos: 85, row: row)
                    //reject.setVisibility(View.VISIBLE);
                    // approve.setVisibility(View.VISIBLE);
                    // txtStatusText.setVisibility(View.INVISIBLE);
                } else if self.LeaveDetails.Type  == 3 && self.LeaveDetails.ISLastStep  == 1 {
                    
                    //self.CreateDynamicButton(cell, ButtonMode: 0, Xpos: 50, row: row)
                    
                    //reject.setVisibility(View.INVISIBLE);
                    // approve.setVisibility(View.VISIBLE);
                    // txtStatusText.setVisibility(View.INVISIBLE);
                    
                    self.btnApprovaloutlet.hidden = true
                    self.btnRejectOutlet.hidden = true
                    self.CreateDynamicLabel()
                    
                }
                else if (self.LeaveDetails.ISLastStep  != 1) {
                    
                    //self.CreateDynamicButton(cell, ButtonMode: 0, Xpos: 70, row: row)
                    
                    // reject.setVisibility(View.INVISIBLE);
                    //approve.setVisibility(View.VISIBLE);
                    // txtStatusText.setVisibility(View.INVISIBLE);
                }
                
            } else if (self.LeaveDetails.Access == 0) {
                if ((self.LeaveDetails.RequestedFor == self.LeaveDetails.LastActionBy) && ( String(self.LeaveDetails.RequestedFor) == String(self.UserID)))  {
                    if self.Mode != 3 && self.LeaveDetails.StatusText != "Closed"  {
                        
                         self.CreateDynamicLabel()
                        // reject.setVisibility(View.INVISIBLE);
                        // approve.setVisibility(View.INVISIBLE);
                        
                    } else if self.Mode == 1 && self.LeaveDetails.StatusText == "Closed" && self.LeaveDetails.ParentID == "null" {
                         self.CreateDynamicLabel()
                        // reject.setVisibility(View.INVISIBLE);
                        // approve.setVisibility(View.INVISIBLE);
                    }
                } else if String(self.UserID) == String(self.LeaveDetails.RequestedBY) {
                    if self.Mode != 3 && self.LeaveDetails.StatusText == "Submitted" {
                        
                        self.btnApprovaloutlet.hidden = true
                        self.btnRejectOutlet.hidden = true
                         self.CreateDynamicLabel()
                        // reject.setVisibility(View.INVISIBLE);
                        // approve.setVisibility(View.INVISIBLE);
                    } else {
                        if self.LeaveDetails.StatusText == "Closed" {
                            self.btnApprovaloutlet.hidden = true
                            self.btnRejectOutlet.hidden = true
                             self.CreateDynamicLabel()
                            // reject.setVisibility(View.INVISIBLE);
                            // approve.setVisibility(View.INVISIBLE);
                            // }
                        } else if self.LeaveDetails.ISLastStep == 1 && self.Mode == 2 {
                            self.btnApprovaloutlet.hidden = true
                            self.btnRejectOutlet.hidden = true
                             self.CreateDynamicLabel()
                            // reject.setVisibility(View.INVISIBLE);
                            // approve.setVisibility(View.INVISIBLE);
                        }
                        
                    }
                }
                else if (self.LeaveDetails.ProcessType == 1 && String(self.LeaveDetails.RequestedBY) == String(self.UserID) && self.LeaveDetails.ISChildCreated != 1) {
                    if (self.Mode != 3) {
                        if (self.LeaveDetails.ParentID == "null") {
                            self.btnApprovaloutlet.hidden = true
                            self.btnRejectOutlet.hidden = true
                             self.CreateDynamicLabel()
                            // reject.setVisibility(View.INVISIBLE);
                            // approve.setVisibility(View.INVISIBLE);
                        }
                    }
                }else{
                    self.btnApprovaloutlet.hidden = true
                    self.btnRejectOutlet.hidden = true
                     self.CreateDynamicLabel()
                    // reject.setVisibility(View.INVISIBLE);
                    // approve.setVisibility(View.INVISIBLE);
                }
            }
        }
        else if ((self.LeaveDetails.ISHRApply == 1)) {
            if (self.LeaveDetails.LastActionBy == self.LeaveDetails.RequestedBY) {
                self.btnApprovaloutlet.hidden = true
                self.btnRejectOutlet.hidden = true
                 self.CreateDynamicLabel()
                // reject.setVisibility(View.INVISIBLE);
                //  approve.setVisibility(View.INVISIBLE);
            }
        }
        
        
        
    }

}
