//
//  BaseTabBarVC.swift
//  Talentoz
//
//  Created by forziamac on 12/12/15.
//  Copyright © 2015 Forzia Tech Private Ltd. All rights reserved.
//

import UIKit

class BaseTabBarVC: UIViewController , UITextFieldDelegate  {
    
   
    //# MARK: Local Variables
    var toolbar : UIToolbar!
    var isOpen = false
    let Viewframe = UIScreen.mainScreen().bounds
    //# MARK: Public Variables
    internal var UINavigationBarTitle: String!
    internal var NavigationBar : UINavigationBar!
    internal var QuickActionMenu : UIView!
    internal var QuickActionBGLayer : UIView!
    internal var QuickActionList : UIView!
    //# MARK: Default Methods
    override func viewDidLoad() {
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(animated: Bool) {
        
        dispatch_async(dispatch_get_main_queue(),{
            self.LoadTopNavigation(true, iBackId: -1 )
            self.toolbar = UIToolbar()
            
            self.toolbar.items = [
                
                UIBarButtonItem(image : UIImage(named: "Home-60.png"), style: UIBarButtonItemStyle.Plain, target: self, action: "HomeAction:"),
                UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil),
                UIBarButtonItem(image : UIImage(named: "Circled User Male-60.png"), style: UIBarButtonItemStyle.Plain, target: self, action: "ProfileAction:"),
                UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil),
                UIBarButtonItem(image : UIImage(named: "Beach-60.png"), style: UIBarButtonItemStyle.Plain, target: self, action: "HolidayAction:"),
                UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil),
                UIBarButtonItem(image : UIImage(named: "Quickaction.png"), style: UIBarButtonItemStyle.Plain, target: self, action: "QuickAction:")
                
            ]
            self.view.addSubview(self.toolbar)
            self.toolbar.frame = CGRectMake(0, self.view.bounds.height-44, self.view.bounds.width, 44)
        })
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    //# MARK: Toolbar Control Action
    
    func HomeAction(sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("HomeInitialView")
        self.presentViewController(vc, animated: true, completion: nil)
        
    }
    
    func ProfileAction(sender: UIBarButtonItem) {
        
        if self.isKindOfClass(ProfileVC)
        {
            return
        }
        let  ProfileVCon = ProfileVC()
        self.presentViewController(ProfileVCon, animated: true, completion: nil)
    }
    
    func HolidayAction(sender: UIBarButtonItem) {
        
        if self.isKindOfClass(Holidays)
        {
            return
        }
        let  Holiday = Holidays()
        self.presentViewController(Holiday, animated: true, completion: nil)
    }
    
    func QuickAction(sender: UIBarButtonItem) {
        
        if self.isKindOfClass(ApplyLeave)
        {
            BindQuickActionMenu(1)
        }
        else if self.isKindOfClass(RaiseAdjustment)
        {
            BindQuickActionMenu(2)
        }
        else if self.isKindOfClass(AddClaim)
        {
            BindQuickActionMenu(3)
        }
        else
        {
            BindQuickActionMenu(0)
        }
        
        
    }
    
     //# MARK: QUICK Action From ToolBar
    
    func BindQuickActionMenu(ModeView: Int)
    {
        // 0- All, 1-Apply Leave , 2-Raise Adjustment
        if isOpen
        {
            isOpen = false
            QuickActionBGLayer.removeFromSuperview()
        }
        else{
            
            isOpen = true
            QuickActionBGLayer = UIView(frame: CGRectMake(0, 0, Viewframe.width  ,  Viewframe.height))
            QuickActionBGLayer.backgroundColor = UIColor(colorLiteralRed: 0 , green: 0, blue: 0, alpha: 0.6)
            let GestureLeaveDetail1 = UITapGestureRecognizer(target: self, action: "DismissQuickActionMenu:")
            QuickActionBGLayer.addGestureRecognizer(GestureLeaveDetail1)
            QuickActionMenu = UIView(frame: CGRectMake(10, QuickActionBGLayer.frame.height - 55 , QuickActionBGLayer.frame.width - 20  , 50))
            QuickActionMenu.backgroundColor = UIColor(colorLiteralRed: 0.95, green: 0.95, blue: 0.95, alpha: 1)
            QuickActionMenu.layer.cornerRadius = 8
            let UICancelButton = UIButton(frame: CGRectMake(0, 0, QuickActionMenu.frame.width  ,  50 ))
            UICancelButton.setTitle("Dismiss", forState: .Normal)
            UICancelButton.setTitleColor(UIColor.redColor(), forState: .Normal)
            
            UICancelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Center
            UICancelButton.contentVerticalAlignment = UIControlContentVerticalAlignment.Center
            UICancelButton.addTarget(self, action: "UnBindQuickActionMenu:", forControlEvents: .TouchUpInside)
            QuickActionMenu.addSubview(UICancelButton)
            QuickActionBGLayer.addSubview(QuickActionMenu)
            
            var Hightlevel : CGFloat = 51
            
            if ModeView == 0
            {
                Hightlevel = 0.0
            }
            
            QuickActionList = UIView(frame: CGRectMake(10, QuickActionBGLayer.frame.height - (225 - Hightlevel) , QuickActionBGLayer.frame.width - 20  , 152 - Hightlevel))
            QuickActionList.backgroundColor = UIColor(colorLiteralRed: 0.95, green: 0.95, blue: 0.95, alpha: 1)
            QuickActionList.layer.cornerRadius = 7
            QuickActionBGLayer.addSubview(QuickActionList)
            
            if ModeView == 0
            {
                let UIApplyButton = UIButton(frame: CGRectMake(5, 0, QuickActionList.frame.width - 10  ,  50 ))
                UIApplyButton.setTitle("Apply Leave", forState: .Normal)
                
                UIApplyButton.setTitleColor(UIColor(hexString: "#0793F7"), forState: .Normal)
                UIApplyButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Center
                UIApplyButton.contentVerticalAlignment = UIControlContentVerticalAlignment.Center
                UIApplyButton.addTarget(self, action: "ApplyLeaveInvoke:", forControlEvents: .TouchUpInside)
                QuickActionList.addSubview(UIApplyButton)
                
                let sepview = UIView(frame: CGRectMake(0, 50,  QuickActionList.frame.width  , 1))
                sepview.backgroundColor = UIColor.lightGrayColor()
                QuickActionList.addSubview(sepview)
                
                let UIApplyAdjustment = UIButton(frame: CGRectMake(5, 51 , QuickActionList.frame.width - 10  ,  50 ))
                UIApplyAdjustment.setTitle("Raise Adjustment", forState: .Normal)
                
                UIApplyAdjustment.setTitleColor(UIColor(hexString: "#0793F7"), forState: .Normal)
                UIApplyAdjustment.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Center
                UIApplyAdjustment.contentVerticalAlignment = UIControlContentVerticalAlignment.Center
                UIApplyAdjustment.addTarget(self, action: "RaiseNewAdjustment:", forControlEvents: .TouchUpInside)
                QuickActionList.addSubview(UIApplyAdjustment)
                
                let sepview1 = UIView(frame: CGRectMake(0, 105,  QuickActionList.frame.width  , 1))
                sepview1.backgroundColor = UIColor.lightGrayColor()
                QuickActionList.addSubview(sepview1)
                
                let UIApplyAddClaim = UIButton(frame: CGRectMake(5, 105 , QuickActionList.frame.width - 10  ,  50 ))
                UIApplyAddClaim.setTitle("Add Claim", forState: .Normal)
                
                UIApplyAddClaim.setTitleColor(UIColor(hexString: "#0793F7"), forState: .Normal)
                UIApplyAddClaim.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Center
                UIApplyAddClaim.contentVerticalAlignment = UIControlContentVerticalAlignment.Center
                UIApplyAddClaim.addTarget(self, action: #selector(BaseTabBarVC.AddAClaim(_:)), forControlEvents: .TouchUpInside)
                QuickActionList.addSubview(UIApplyAddClaim)
                
            }
            else if ModeView == 1
            {
                let UIApplyAdjustment = UIButton(frame: CGRectMake(5, 0, QuickActionList.frame.width - 10  ,  50 ))
                UIApplyAdjustment.setTitle("Raise Adjustment", forState: .Normal)
                
                UIApplyAdjustment.setTitleColor(UIColor(hexString: "#0793F7"), forState: .Normal)
                UIApplyAdjustment.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Center
                UIApplyAdjustment.contentVerticalAlignment = UIControlContentVerticalAlignment.Center
                UIApplyAdjustment.addTarget(self, action: "RaiseNewAdjustment:", forControlEvents: .TouchUpInside)
                QuickActionList.addSubview(UIApplyAdjustment)
                
                
                let sepview1 = UIView(frame: CGRectMake(0, 50,  QuickActionList.frame.width  , 1))
                sepview1.backgroundColor = UIColor.lightGrayColor()
                QuickActionList.addSubview(sepview1)
                
                let UIApplyAddClaim = UIButton(frame: CGRectMake(5, 51 , QuickActionList.frame.width - 10  ,  50 ))
                UIApplyAddClaim.setTitle("Add Claim", forState: .Normal)
                
                UIApplyAddClaim.setTitleColor(UIColor(hexString: "#0793F7"), forState: .Normal)
                UIApplyAddClaim.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Center
                UIApplyAddClaim.contentVerticalAlignment = UIControlContentVerticalAlignment.Center
                UIApplyAddClaim.addTarget(self, action: #selector(BaseTabBarVC.AddAClaim(_:)), forControlEvents: .TouchUpInside)
                QuickActionList.addSubview(UIApplyAddClaim)
            }
            else if ModeView == 2
            {
                let UIApplyButton = UIButton(frame: CGRectMake(5, 0, QuickActionList.frame.width - 10  ,  50))
                UIApplyButton.setTitle("Apply Leave", forState: .Normal)
                
                UIApplyButton.setTitleColor(UIColor(hexString: "#0793F7"), forState: .Normal)
                UIApplyButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Center
                UIApplyButton.contentVerticalAlignment = UIControlContentVerticalAlignment.Center
                UIApplyButton.addTarget(self, action: "ApplyLeaveInvoke:", forControlEvents: .TouchUpInside)
                QuickActionList.addSubview(UIApplyButton)
                
                let sepview1 = UIView(frame: CGRectMake(0, 50,  QuickActionList.frame.width  , 1))
                sepview1.backgroundColor = UIColor.lightGrayColor()
                QuickActionList.addSubview(sepview1)
                
                let UIApplyAddClaim = UIButton(frame: CGRectMake(5, 51 , QuickActionList.frame.width - 10  ,  50 ))
                UIApplyAddClaim.setTitle("Add Claim", forState: .Normal)
                
                UIApplyAddClaim.setTitleColor(UIColor(hexString: "#0793F7"), forState: .Normal)
                UIApplyAddClaim.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Center
                UIApplyAddClaim.contentVerticalAlignment = UIControlContentVerticalAlignment.Center
                UIApplyAddClaim.addTarget(self, action: #selector(BaseTabBarVC.AddAClaim(_:)), forControlEvents: .TouchUpInside)
                QuickActionList.addSubview(UIApplyAddClaim)
            }
            else if ModeView == 3
            {
                let UIApplyButton = UIButton(frame: CGRectMake(5, 0, QuickActionList.frame.width - 10  ,  50 ))
                UIApplyButton.setTitle("Apply Leave", forState: .Normal)
                
                UIApplyButton.setTitleColor(UIColor(hexString: "#0793F7"), forState: .Normal)
                UIApplyButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Center
                UIApplyButton.contentVerticalAlignment = UIControlContentVerticalAlignment.Center
                UIApplyButton.addTarget(self, action: "ApplyLeaveInvoke:", forControlEvents: .TouchUpInside)
                QuickActionList.addSubview(UIApplyButton)
                
                let sepview = UIView(frame: CGRectMake(0, 50,  QuickActionList.frame.width  , 1))
                sepview.backgroundColor = UIColor.lightGrayColor()
                QuickActionList.addSubview(sepview)
                
                let UIApplyAdjustment = UIButton(frame: CGRectMake(5, 51 , QuickActionList.frame.width - 10  ,  50 ))
                UIApplyAdjustment.setTitle("Raise Adjustment", forState: .Normal)
                
                UIApplyAdjustment.setTitleColor(UIColor(hexString: "#0793F7"), forState: .Normal)
                UIApplyAdjustment.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Center
                UIApplyAdjustment.contentVerticalAlignment = UIControlContentVerticalAlignment.Center
                UIApplyAdjustment.addTarget(self, action: "RaiseNewAdjustment:", forControlEvents: .TouchUpInside)
                QuickActionList.addSubview(UIApplyAdjustment)
            }
            
        
            self.view.addSubview(QuickActionBGLayer)
            
          
        }
        
    }
    
    func AddAClaim(sender: UIButton)
    {
        let objClaim = AddClaim()
        self.presentViewController(objClaim, animated: true, completion: nil)
    }
    
    // Apply new leave request from tool bar quick actions
    func ApplyLeaveInvoke(sender: UIButton)
    {
        isOpen = false
        QuickActionBGLayer.removeFromSuperview()
        let  ObjApplyLeave = ApplyLeave()
        self.presentViewController(ObjApplyLeave, animated: true, completion: nil)
        
    }
    
    // Raise new time adjustment request from tool bar quick actions
    func RaiseNewAdjustment(sender: UIButton)
    {
        isOpen = false
        QuickActionBGLayer.removeFromSuperview()
        
        let  ObjApplyAdjustment = RaiseAdjustment()
         ObjApplyAdjustment.AdjustmentDate = "Select Date"
        self.presentViewController(ObjApplyAdjustment, animated: true, completion: nil)
        
    }
    
    // Close Quick Action Modal window when click the dismiss button
    func UnBindQuickActionMenu(sender: UIButton)
    {
        isOpen = false
        QuickActionBGLayer.removeFromSuperview()
        
    }
    
    // Close Quick Action Modal window when click around area
    func DismissQuickActionMenu(sender: UITapGestureRecognizer)
    {
        isOpen = false
        QuickActionBGLayer.removeFromSuperview()
        
    }

    
     func setFrame(frame: CGRect) {
        super.view.frame = CGRectMake(0.0, 0.0, 320.0, 50.0)
    }
    
   //# MARK: Navigation Bar Feature
    
    func LoadTopNavigation(isBack : Bool , iBackId : Int )
    {
        NavigationBar = UINavigationBar(frame: CGRectMake(0, 0, self.view.frame.width  , 60))
        NavigationBar.barTintColor  =  UIColor(colorLiteralRed: 64/255, green: 156/255, blue: 255/255, alpha: 1)
        
        let ctrl: UIView = UIView(frame: CGRectMake(0, 0, self.view.frame.width  , 60))
        ctrl.backgroundColor = UIColor(colorLiteralRed: 28/255, green: 138/255, blue: 255/255, alpha: 1)

        ctrl.autoresizingMask = .FlexibleRightMargin
        NavigationBar.addSubview(ctrl)
        
        let navigationBarApp = UINavigationBar.appearance()
        navigationBarApp.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        // Create a navigation item with a title
        let navigationItem = UINavigationItem()
      
        navigationItem.title = self.UINavigationBarTitle
        
        // Create left and right button for navigation item
        if (isBack==true)
        {
            let leftButton =  UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Plain, target: self, action:"goBack")
            leftButton.tintColor = UIColor.whiteColor()
            leftButton.setFAIcon(FAType.FAAngleLeft, iconSize: CGFloat(25))
            navigationItem.leftBarButtonItem = leftButton
            
        }
        
        // Assign the navigation item to the navigation bar
        NavigationBar.items = [navigationItem]
        
       
        
        // Make the navigation bar a subview of the current view controller
        self.view.addSubview(NavigationBar)
        
    }
    
    func goBack(){
        dismissViewControllerAnimated(true, completion: nil)
    }
    

    //# MARK: Custom Action
    
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

    func textFieldShouldReturn(textField: UITextField!) -> Bool // called when 'return' key pressed. return NO to ignore.
    {
        return true;
    }

}




