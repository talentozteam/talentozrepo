//
//  LeaveRequest.swift
//  Talentoz
//
//  Created by forziamac on 29/12/15.
//  Copyright © 2015 Forzia Tech Private Ltd. All rights reserved.
//

import Foundation
import UIKit

class LeaveRequest{
    internal var EmployeePhoto,EmployeeName,PositionName,RequestDescription,StartDateString,EndDateString,StatusText,ParentID,WorkLocationName,Reason: String!
    internal var  RequestID,Access, Type, ISLastStep ,ProcessType, ISChildCreated, BaseStatus, ISHRApply ,RequestedFor
    ,LastActionBy,RequestedBY: Int!
    
    internal var NOOfDays : Double!
    
}


class AttendanceAdjustmentRequest{
    internal var EmployeePhoto,EmployeeName,PositionName,DateLineDesc,WorkLocationName,TotalHours,DateLine,Remarks,StatusDesc: String!
    internal var  UserID,RequestID,Status,Order: Int!
    
}

class ActivitiesList{
    internal var TotalTime,OutTime,RwNo,InTime,ShirtOutTime,ShirtInTime,Sum_of_TotalTime : String!
    internal var IsInTime,RowID,IsOutTime : Int!
    internal var IsNewlyCreated : Bool!
}

class AdjustmentListCollection:Serializable {
    internal var DTR_Adjustment : Array<AdjustmentList> = []
   
}


class ClaimRequest{
    internal var EmployeePhoto,EmployeeName,PositionName,WorkLocationName,BusinessUnitName,CurrencyShortName,CurrencySymbol,ProgramTitle,RequestedOnString,StartDateString,EndDateString,StatusText,TotalAmount : String!
    internal var  UserID,RequestID,Status,Access,Type,BaseStatus,IsLastStep,RequestedFor,LastActionBy,RequestedBy,ProcessType: Int!
    
}

class AdjustmentList:Serializable{
    
    internal var ClientID,UserID,WebType,IsAdjusted : Int!
    internal var DtrDatetime,DateLine : String!
}

class ClaimDetailsItem {
    internal var ClientID,UserID,ClaimTypeID,ItemID,CurrencyID,ProjectID : Int!
    internal var Amount,ClaimDateString,ClaimItemsAttachment,ClaimTypeName,CurrencyName,Description,ProjectName : String!
    internal var Checked : Bool!
}

class AttachmentCollection : UIViewController {
     
    internal var ActualFileName,GeneratedFileName,FileExtensio : String!
    internal var ImageData : UIImage!
}