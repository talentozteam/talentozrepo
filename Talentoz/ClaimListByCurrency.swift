//
//  ClaimListByCurrency.swift
//  Talentoz
//
//  Created by forziamac on 12/05/16.
//  Copyright © 2016 Forzia Tech Private Ltd. All rights reserved.
//

import UIKit

class ClaimListByCurrency: BaseTabBarVC, UITableViewDataSource ,UITableViewDelegate{
    //# MARK: Local Variables
    var objCallBack = AjaxCallBack()
    let defaults = NSUserDefaults.standardUserDefaults()
    var SummaryListRef:UITableView!
    var SummaryJSON : Talentoz.JSON!
    var objActivityIndicaor : UIActivityIndicatorView!
    
     //# MARK: Default Methods
    override func viewDidLoad() {
        super.UINavigationBarTitle = "Select Claim by currency"
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.GetSummaryData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     //# MARK: Custom Methods
    
    func GetSummaryData()
    {
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            objActivityIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, self.view.frame.width, self.view.frame.height))
            objActivityIndicaor.activityIndicatorViewStyle = .WhiteLarge
            objActivityIndicaor.center = self.view.center
            objActivityIndicaor.transform = CGAffineTransformMakeScale(1.5, 1.5);
            objActivityIndicaor.hidesWhenStopped = true
            objActivityIndicaor.color = UIColor.grayColor()
            self.view.addSubview(objActivityIndicaor)
            objActivityIndicaor.startAnimating()
            
            objCallBack = AjaxCallBack()
            
            objCallBack.MethodName = "GetAllClaimItem"
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objCallBack.post(GetSummaryDataOnComplete, OnError: GetSummaryOnError)
            
        }
    }
    
    func GetSummaryDataOnComplete (ResultString :NSString? ) {
        
        dispatch_async(dispatch_get_main_queue(),{
            self.objActivityIndicaor.stopAnimating()
            self.SummaryJSON = JSON(data: self.objCallBack.GetJSONData(ResultString)!)
            self.BindCurrencySummary()
        })
    }
    
    func GetSummaryOnError(Response: NSError ){
        
    }
    
    func BindCurrencySummary()
    {
        let SummaryList: UITableView = UITableView(frame: CGRectMake(0,65, Viewframe.width, Viewframe.height - 115 ))
        SummaryList.showsVerticalScrollIndicator = true
        SummaryList.delegate = self
        SummaryList.dataSource = self
        SummaryList.rowHeight = 70
        SummaryList.backgroundColor = UIColor.clearColor()
        SummaryList.separatorColor = UIColor.clearColor()
      //  let blurEffect = UIBlurEffect(style: .Light)
       // let blurEffectView = UIVisualEffectView(effect: blurEffect)
        //SummaryList.backgroundView = blurEffectView
        //if you want translucent vibrant table view separator lines
        //SummaryList.separatorEffect = UIVibrancyEffect(forBlurEffect: blurEffect)
        self.view.addSubview(SummaryList )
        self.SummaryListRef = SummaryList
    }
    
    func numberOfSectionsInTableView(SummaryListRef: UITableView) -> Int {
        return 1
    }
    
    func tableView(SummaryListRef: UITableView, numberOfRowsInSection section: Int) -> Int {
     return self.SummaryJSON.count    
        
    }
    
    func tableView(SummaryListRef: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.backgroundColor = UIColor.clearColor()
        
        let ContentVw: UIView = UIView(frame: CGRectMake(Viewframe.width / 100 * 2, 10, Viewframe.width / 100 * 96, 65		 ))
        cell.addSubview(ContentVw)
        ContentVw.layer.borderWidth = 0.5
        ContentVw.layer.borderColor = UIColor(hexString: "#eeeeee")!.CGColor
        ContentVw.backgroundColor = UIColor(hexString: "#f7f7f7")
        
        
         let vw_EmpPic:  UIView = UIView(frame: CGRectMake(10, 5, 50, 50))
         vw_EmpPic.layer.cornerRadius = vw_EmpPic.frame.height/2
        vw_EmpPic.layer.borderWidth = 0.5
        vw_EmpPic.layer.masksToBounds = false
        vw_EmpPic.layer.borderColor = UIColor.lightGrayColor().CGColor
        vw_EmpPic.backgroundColor = UIColor.SetRandomColor()
        vw_EmpPic.clipsToBounds = true
         ContentVw.addSubview(vw_EmpPic)
        
        let lbl_Claims : UILabel = UILabel(frame: CGRectMake(5, 30, 60 ,20))
        lbl_Claims.textAlignment = .Center
        lbl_Claims.font = UIFont(name:"HelveticaNeue-Bold", size: 10.0)
        lbl_Claims.textColor = UIColor.whiteColor()
        lbl_Claims.text = "Claims"
        ContentVw.addSubview(lbl_Claims)
        
        
        let img_EmpPic:  UIButton = UIButton(frame: CGRectMake(0, 8, 50, 20))
        
        img_EmpPic.layer.masksToBounds = false
        img_EmpPic.layer.borderColor = UIColor.clearColor().CGColor
        img_EmpPic.backgroundColor = UIColor.clearColor()
       
        img_EmpPic.titleLabel!.font = UIFont(name: "HelveticaNeue-bold", size: 16)
        img_EmpPic.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        img_EmpPic.setTitle(String(self.SummaryJSON[indexPath.row]["CliamItemList"].count), forState: UIControlState.Normal)
        vw_EmpPic.addSubview(img_EmpPic)
        
        let lbl_CurrencySymbol : UILabel = UILabel(frame: CGRectMake(120, 15, (Viewframe.width - img_EmpPic.frame.size.width) / 100 * 15 ,35))
        lbl_CurrencySymbol.textAlignment = .Left
        lbl_CurrencySymbol.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
        lbl_CurrencySymbol.textColor = UIColor(hexString: String("#666666"))
        lbl_CurrencySymbol.text = self.SummaryJSON[indexPath.row]["CurrencyName"].string
        ContentVw.addSubview(lbl_CurrencySymbol)
        
        let lbl_ClaimedAmount : UILabel = UILabel(frame: CGRectMake(160, 15, (Viewframe.width - img_EmpPic.frame.size.width) / 100 * 30 ,35))
        lbl_ClaimedAmount.textAlignment = .Left
        lbl_ClaimedAmount.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
        lbl_ClaimedAmount.textColor = UIColor(hexString: String("#666666"))
        lbl_ClaimedAmount.text = ConvertValueWithDecimal( String(self.SummaryJSON[indexPath.row]["TotalAmount"]),NoofDecimal: 2)
        ContentVw.addSubview(lbl_ClaimedAmount)
        
        
        let lbl_ArrowSymbol : UILabel = UILabel(frame: CGRectMake(Viewframe.width / 100 * 62, 15, (Viewframe.width - img_EmpPic.frame.size.width) / 100 * 30 ,35))
        lbl_ArrowSymbol.textAlignment = .Right
        lbl_ArrowSymbol.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
        lbl_ArrowSymbol.textColor = UIColor(hexString: String("#666666"))
        lbl_ArrowSymbol.setFAIcon(FAType.FAArrowRight, iconSize: 15)
        ContentVw.addSubview(lbl_ArrowSymbol)
        
        cell.tag = indexPath.row
        
        let GestureClaimDetail = UITapGestureRecognizer(target: self, action: #selector(ClaimListByCurrency.ShowClaimDetail(_:)))
        ContentVw.addGestureRecognizer(GestureClaimDetail)
        
        
        return cell
    }
    
    func ShowClaimDetail(sender: UITapGestureRecognizer)
    {
        let tapLocation = sender.locationInView(self.SummaryListRef)
        let indexPath = self.SummaryListRef.indexPathForRowAtPoint(tapLocation)
        
        let  objClaimDetail1 = ClaimItemDetails()
        objClaimDetail1.ClaimJSON = self.SummaryJSON[(indexPath?.row)!]
        self.presentViewController(objClaimDetail1, animated: true, completion: nil)
        
    }
    
  
 



}
