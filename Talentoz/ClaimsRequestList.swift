//
//  ClaimsRequestList.swift
//  Talentoz
//
//  Created by forziamac on 20/04/16.
//  Copyright © 2016 Forzia Tech Private Ltd. All rights reserved.
//

import UIKit

class ClaimsRequestListB: BaseTabBarVC, UITableViewDataSource ,UITableViewDelegate {
    
    //# MARK: Local Variables
    var customSC: UISegmentedControl?
    var SelfClaimListData : Talentoz.JSON!
    var TeamClaimListData : Talentoz.JSON!
    var CurrentClaimListData = [ClaimRequest]()
    var arrSelfClaimList = [ClaimRequest]()
    var arrTeamClaimList = [ClaimRequest]()
    var UserID : Int!
    var objCallBack = AjaxCallBack()
    let defaults = NSUserDefaults.standardUserDefaults()
    var CurrentCell =  UITableViewCell()
    let objRole = RoleManager()
    var CurrentRowIndex : Int!
    var CurrentActionType: Int!
    var ClaimListView = UITableView()
    var objLoadingIndicaor : UIActivityIndicatorView!
    var swipeRight : UISwipeGestureRecognizer!
    var swipeLeft : UISwipeGestureRecognizer!
    var CurrentPageCount : Int!
    var VisibleShowMore : Bool = false
    var ClaimViewRef : UIView!
    //# MARK: Public Variables
    internal var ModeofShow: Int! = 0
    var LoadMoreClicked : Int = 0
    //# MARK: Default Methods
    override func viewDidLoad() {
        
        self.view.backgroundColor = UIColor.whiteColor()
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            self.UserID = Int(ResultJSON!["UserID"]as! NSNumber)
        }
        
        super.UINavigationBarTitle = "Claims List"
        super.viewDidLoad()
        BindContextTab()
        
        if objRole.ISManager{
            self.AddGesture()
        }
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //# MARK: Tab Controls
    
    func BindContextTab()
    {
        var items = ["My Claims"]
        if objRole.ISManager{
            items = ["My Claims", "Team Claims"]
        }
        
        
        let ClaimView=UIView(frame: CGRectMake(0, 60,  Viewframe.width , 48))
        ClaimView.backgroundColor = UIColor(colorLiteralRed: 28/255, green: 138/255, blue: 255/255, alpha: 1)
        
        
        customSC = UISegmentedControl(items: items)
        customSC!.selectedSegmentIndex = 0
        customSC!.frame = CGRectMake(Viewframe.width / 100 * 2, 5,Viewframe.width / 100 * 96,40)
        customSC!.layer.cornerRadius = 0.5
        customSC!.backgroundColor = UIColor(colorLiteralRed: 28/255, green: 138/255, blue: 255/255, alpha: 1)
        customSC!.tintColor = UIColor.whiteColor()
        customSC!.layer.borderColor = UIColor.whiteColor().CGColor
        customSC!.layer.borderWidth = 0
        customSC!.addTarget(self, action: #selector(ClaimsRequestListB.changeColor(_:)), forControlEvents: .ValueChanged)
        
        ClaimView.addSubview(customSC!)
        self.view.addSubview(ClaimView)
        
        ClaimViewRef = ClaimView
        
        customSC!.selectedSegmentIndex = self.ModeofShow
        BindClaimList()
        
    }
    
    func LoadTeamClaimList(gestureReconizer: UISwipeGestureRecognizer) {
        customSC!.selectedSegmentIndex = 1
        BindClaimList()
    }
    
    func LoadSelfClaimList(gestureReconizer: UISwipeGestureRecognizer) {
        customSC!.selectedSegmentIndex = 0
        BindClaimList()
    }
    
    
    func changeColor(sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            BindClaimList()
            break;
        case 1:
            BindClaimList()
            break;
        default:
            break;
        }
    }
    
    
    
    func BindClaimList()
    {
        if self.customSC!.selectedSegmentIndex == 0
        {
            if self.SelfClaimListData != nil
            {
                ConstructClaimList()
            }else
            {
                GetClaimList()
            }
            
        }
        else
        {
            if self.TeamClaimListData != nil
            {
                ConstructClaimList()
            }else
            {
                GetClaimList()
            }
            
        }
        
    }
    
    //# MARK: Callback Methods
    
    func GetClaimList()
    {
        objLoadingIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, Viewframe.width, Viewframe.height))
        
        objLoadingIndicaor.activityIndicatorViewStyle = .WhiteLarge
        let objPosition =  self.view.center
        objLoadingIndicaor.center = objPosition
        objLoadingIndicaor.hidesWhenStopped = true
        objLoadingIndicaor.color = UIColor.grayColor()
        self.view.addSubview(objLoadingIndicaor)
        objLoadingIndicaor.startAnimating()
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            
            objCallBack = AjaxCallBack()
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objCallBack.MethodName = "GetClaimRequestList"
            
            if customSC!.selectedSegmentIndex == 0
            {
                objparam = Parameter(Name: "pContextMode",value: "1")
                objCallBack.ParameterList.append(objparam)
                
                objparam = Parameter(Name: "pPageIndex",value: "0")
                objCallBack.ParameterList.append(objparam)
                
                objparam = Parameter(Name: "pPageSize",value: "100000")
                objCallBack.ParameterList.append(objparam)
            }
            else
            {
                objparam = Parameter(Name: "pContextMode",value: "2")
                objCallBack.ParameterList.append(objparam)
                
                if self.CurrentPageCount == nil
                {
                    self.CurrentPageCount = 0
                }
                objparam = Parameter(Name: "pPageIndex",value: String(self.CurrentPageCount))
                objCallBack.ParameterList.append(objparam)
                
                objparam = Parameter(Name: "pPageSize",value: "10")
                objCallBack.ParameterList.append(objparam)
            }
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            
            
            objCallBack.post(ClaimListOnComplete, OnError: ClaimListOnErrorOnError)
            
        }
        
    }
    
    //Response on output callback
    func ClaimListOnComplete (ResultString :NSString? ) {
        if self.customSC!.selectedSegmentIndex == 0
        {
            if self.objCallBack.GetJSONData(ResultString) != nil
            {
                self.SelfClaimListData =   JSON(data: self.objCallBack.GetJSONData(ResultString)!)
                self.arrSelfClaimList = self.GetClaimRequestArray(self.SelfClaimListData , IsManager: 0 )
            }
           
        }
        else
        {
            if self.objCallBack.GetJSONData(ResultString) != nil
            {
                self.TeamClaimListData =   JSON(data: self.objCallBack.GetJSONData(ResultString)!)
                self.arrTeamClaimList = self.GetClaimRequestArray(self.TeamClaimListData , IsManager: 1 )
            }
            
        }
        ConstructClaimList()
    }
    
    //Response on error callback
    func ClaimListOnErrorOnError(Response: NSError ){
        
    }
    
    // Convert JSON Array Object To Custom object collection
    func GetClaimRequestArray(ClaimJSON: Talentoz.JSON!, IsManager: Int) -> [ClaimRequest]
    {
        
        var arrTempClaimRequest = [ClaimRequest]()
        
        for i in 0 ..< ClaimJSON.count  {
            
            let iClaim = ClaimRequest()
            
            iClaim.RequestID = ClaimJSON[i]["RequestID"].int
            iClaim.EmployeePhoto  = ClaimJSON[i]["EmployeePhoto"].string
            iClaim.EmployeeName  = ClaimJSON[i]["EmployeeName"].string
            iClaim.PositionName  = ClaimJSON[i]["PositionName"].string
            iClaim.StartDateString  = ClaimJSON[i]["StartDateString"].string
            iClaim.EndDateString  = ClaimJSON[i]["EndDateString"].string
            iClaim.StatusText  = ClaimJSON[i]["StatusText"].string
            iClaim.WorkLocationName  = ClaimJSON[i]["WorkLocationName"].string
            
            iClaim.BusinessUnitName = ClaimJSON[i]["BusinessUnitName"].string
            iClaim.CurrencyShortName = ClaimJSON[i]["CurrencyShortName"].string
            iClaim.CurrencySymbol = ClaimJSON[i]["CurrencySymbol"].string
            iClaim.ProgramTitle = ClaimJSON[i]["ProgramTitle"].string
            iClaim.RequestedOnString = ClaimJSON[i]["RequestedOnString"].string
            iClaim.TotalAmount = String(ClaimJSON[i]["TotalAmount"])
            
            iClaim.UserID = ClaimJSON[i]["UserID"].int
            iClaim.Status = ClaimJSON[i]["Status"].int
            iClaim.Access = ClaimJSON[i]["Access"].int
            iClaim.Type = ClaimJSON[i]["Type"].int
            iClaim.BaseStatus = ClaimJSON[i]["BaseStatus"].int
            iClaim.IsLastStep = ClaimJSON[i]["IsLastStep"].int
            
            iClaim.RequestedFor = ClaimJSON[i]["RequestedFor"].int
            iClaim.LastActionBy = ClaimJSON[i]["LastActionBy"].int
            iClaim.RequestedBy = ClaimJSON[i]["RequestedBy"].int
            iClaim.ProcessType = ClaimJSON[i]["ProcessType"].int
            
            
            
            
   
            if IsManager == 0
            {
                arrTempClaimRequest.append(iClaim)
            }
            else
            {
                self.arrTeamClaimList.append(iClaim)
                self.CurrentClaimListData.append(iClaim)
            }
            
        }
        
        if IsManager == 1
        {
            if ClaimJSON.count == 10
            {
                VisibleShowMore = true
            }
            else
            {
                VisibleShowMore = false
            }
            
            self.CurrentPageCount = self.CurrentPageCount + 1
            arrTempClaimRequest = self.arrTeamClaimList
        }
        
        
        return arrTempClaimRequest
    }
    
    //Bind Claim list table
    func ConstructClaimList()
    {
        
        dispatch_async(dispatch_get_main_queue(),{
            
            self.objLoadingIndicaor.stopAnimating()
            
            if self.customSC!.selectedSegmentIndex == 0
            {
                self.CurrentClaimListData = self.arrSelfClaimList
            }
            else
            {
                self.CurrentClaimListData =  self.arrTeamClaimList
            }
            
            self.ClaimListView.removeFromSuperview()
            
            if ( self.CurrentClaimListData.count>0)
            {
           
            self.ClaimListView = UITableView(frame: CGRectMake(0, 108, self.view.frame.width, self.view.frame.height  - 163 ))
            self.ClaimListView.showsVerticalScrollIndicator = true
            self.ClaimListView.delegate = self
            self.ClaimListView .dataSource = self
            if self.customSC!.selectedSegmentIndex == 0 //self
            {
                
            self.ClaimListView.rowHeight = 120
          self.ClaimListView.separatorColor = UIColor.clearColor()
                
            }
            else // team
            {
                self.ClaimListView.rowHeight = 160
                self.ClaimListView.separatorColor = UIColor.clearColor()
                
            }
            self.ClaimListView.backgroundColor = UIColor.clearColor()
            let blurEffect = UIBlurEffect(style: .Light )
           let blurEffectView = UIVisualEffectView(effect: blurEffect)
           self.ClaimListView.backgroundView = blurEffectView
            
            
            
            //if you want translucent vibrant table view separator lines
            self.ClaimListView.separatorColor = UIColor.clearColor()
            //self.ClaimListView.separatorEffect = UIVibrancyEffect(forBlurEffect: blurEffect)
            self.view.addSubview(self.ClaimListView )
            }
            else
            {
                let lbl_NotFound : UILabel = UILabel(frame: CGRectMake(0, 108, self.view.frame.width, 25 ))
                lbl_NotFound.textAlignment = .Center
                lbl_NotFound.font = UIFont(name:"HelveticaNeue", size: 12.0)
                lbl_NotFound.textColor = UIColor(hexString: String("#ff8877"))
                lbl_NotFound.text = "No claim(s) to show."
                self.view.addSubview(lbl_NotFound)
            }
            
            
            if self.customSC!.selectedSegmentIndex == 1
            {
                if(self.LoadMoreClicked==1)
                {
                    self.scrollToLastRow()
                }
            }
            
        })
        
        
    }
    
    
    //# MARK: Table List View Methods
    
    func numberOfSectionsInTableView(ClaimListView: UITableView) -> Int {
        return 1
    }
    
    func tableView(ClaimListView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.customSC!.selectedSegmentIndex == 1
        {
            if self.VisibleShowMore == true
            {
                return self.CurrentClaimListData.count + 1
            }
            else
            {
                return self.CurrentClaimListData.count
            }
            
        }
        else
        {
            return self.CurrentClaimListData.count
        }
        
        
        
    }
    
    func tableView(ClaimListView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
       
        cell.backgroundColor = UIColor.clearColor()
        
        var IsShowMoreSection = false
        
        if self.customSC!.selectedSegmentIndex == 1
        {
            if self.VisibleShowMore == true
            {
                if self.CurrentClaimListData.count   == indexPath.row
                {
                    IsShowMoreSection = true
                }
            }
        }
        
        
        
        var HeightCalculated : CGFloat = 0
        
        if IsShowMoreSection == false
        {
            
            if self.customSC!.selectedSegmentIndex != 0 // Team
            {
                let ContentVw: UIView = UIView(frame: CGRectMake(Viewframe.width / 100 * 2, 10, Viewframe.width / 100 * 96, 155))
                cell.addSubview(ContentVw)
                
                ContentVw.layer.cornerRadius=5
                ContentVw.backgroundColor=UIColor(hexString: String("#f5f5f5"))
                
                
                
                
                
                if let tmpDomainURL = self.defaults.stringForKey("DomainURL")
                {
                    if String(self.CurrentClaimListData[indexPath.row].EmployeePhoto) == "nil" ||  String(self.CurrentClaimListData[indexPath.row].EmployeePhoto) == ""
                    {
                        let img_EmpPic:  UIButton = UIButton(frame: CGRectMake(10, 25, 50, 50))
                        img_EmpPic.layer.borderWidth = 0.5
                        img_EmpPic.layer.masksToBounds = false
                        img_EmpPic.layer.borderColor = UIColor.lightGrayColor().CGColor
                        img_EmpPic.backgroundColor = UIColor(hexString: String("#0CDBFF"))
                        img_EmpPic.layer.cornerRadius = img_EmpPic.frame.height/2
                        img_EmpPic.clipsToBounds = true
                        img_EmpPic.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                        img_EmpPic.setTitle(String(self.CurrentClaimListData[indexPath.row].EmployeeName)[0], forState: UIControlState.Normal)
                        ContentVw.addSubview(img_EmpPic)
                    }
                    else{
                        let img_EmpPic:  UIImageView = UIImageView(frame: CGRectMake(10, 25, 50, 50))
                        img_EmpPic.layer.borderWidth = 0.5
                        img_EmpPic.layer.masksToBounds = false
                        img_EmpPic.layer.borderColor = UIColor.lightGrayColor().CGColor
                        img_EmpPic.backgroundColor = UIColor(hexString: String("#0CDBFF"))
                        img_EmpPic.layer.cornerRadius = img_EmpPic.frame.height/2
                        img_EmpPic.clipsToBounds = true
                        
                        ContentVw.addSubview(img_EmpPic)
                        
                        if self.CurrentClaimListData[indexPath.row].EmployeePhoto != nil
                        {
                            let employeePic = "http://" + tmpDomainURL + "/Uploadedfiles/Client" + String(self.defaults.stringForKey("ClientID")!) + "/Image/" + String(self.CurrentClaimListData[indexPath.row].EmployeePhoto!)
                            img_EmpPic.setImageWithUrl(NSURL(string: employeePic)!)
                        }
                    }
                    
                    
                    
                }
                
                let lbl_EmployeeName : UILabel = UILabel(frame: CGRectMake(70, 10, ContentVw.frame.width / 100 * 75 ,18))
                lbl_EmployeeName.textAlignment = .Left
                lbl_EmployeeName.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
                lbl_EmployeeName.textColor = UIColor(hexString: String("#666666"))
                lbl_EmployeeName.text = String(self.CurrentClaimListData[indexPath.row].EmployeeName)
                ContentVw.addSubview(lbl_EmployeeName)
                
                
                
                let lbl_PositionName : UILabel = UILabel(frame: CGRectMake(70, 30, ContentVw.frame.width / 100 * 50 ,18))
                lbl_PositionName.textAlignment = .Left
                lbl_PositionName.textColor = UIColor(hexString: String("#666666"))
                lbl_PositionName.font = UIFont(name:"HelveticaNeue", size: 14.0)
                lbl_PositionName.text = String(self.CurrentClaimListData[indexPath.row].PositionName)
                ContentVw.addSubview(lbl_PositionName)
                
                
                let lbl_ClaimDescription : UILabel = UILabel(frame: CGRectMake(70 , 50, ContentVw.frame.width / 100 * 50,18))
                lbl_ClaimDescription.textAlignment = .Left
                lbl_ClaimDescription.textColor = UIColor(hexString: String("#666666"))
                lbl_ClaimDescription.font = UIFont(name:"HelveticaNeue", size: 14.0)
                lbl_ClaimDescription.text = self.CurrentClaimListData[indexPath.row].ProgramTitle
                ContentVw.addSubview(lbl_ClaimDescription)
                
                
                
                let lbl_ClaimPeriod : UILabel = UILabel(frame: CGRectMake(70 , 72 , ContentVw.frame.width / 100 * 70,18))
                lbl_ClaimPeriod.textAlignment = .Left
                lbl_ClaimPeriod.textColor = UIColor(hexString: String("#666666"))
                lbl_ClaimPeriod.font = UIFont(name:"HelveticaNeue", size: 14.0)
                lbl_ClaimPeriod.setFAText(prefixText: "", icon: FAType.FACalendar, postfixText: "  " + String(self.CurrentClaimListData[indexPath.row].StartDateString) + " - " + String(self.CurrentClaimListData[indexPath.row].EndDateString) , size: 14.0)
                ContentVw.addSubview(lbl_ClaimPeriod)
                
                
                let vwline : UIView = UIView(frame: CGRectMake(10 ,  120 , Viewframe.width / 100 * 88,1))
                vwline.layer.borderColor = UIColor(hexString: "#cccccc")?.CGColor
                vwline.layer.borderWidth = 1
                ContentVw.addSubview(vwline)
                
                let lbl_ClaimSubmitted : UILabel = UILabel(frame: CGRectMake(10 ,  128 , Viewframe.width / 100 * 65,18))
                lbl_ClaimSubmitted.textAlignment = .Left
                lbl_ClaimSubmitted.textColor = UIColor(hexString: String("#666666"))
                
                lbl_ClaimSubmitted.font = UIFont(name:"HelveticaNeue", size: 14.0)
                
                lbl_ClaimSubmitted.setFAText(prefixText: "Submitted on :  "  , icon: FAType.FACalendar, postfixText: "  " + String(self.CurrentClaimListData[indexPath.row].RequestedOnString)  , size: 12.0)
                ContentVw.addSubview(lbl_ClaimSubmitted)
                
                
                
                let lbl_ClaimCurrencySymbol : UILabel = UILabel(frame: CGRectMake( Viewframe.width / 100 * 59 ,  128, self.view.frame.size.width / 100 * 13,20))
                lbl_ClaimCurrencySymbol.textAlignment = .Right
                lbl_ClaimCurrencySymbol.textColor = UIColor(hexString: String("#2190ea"))
                lbl_ClaimCurrencySymbol.font = UIFont(name:"HelveticaNeue", size: 14.0)
                lbl_ClaimCurrencySymbol.text = self.CurrentClaimListData[indexPath.row].CurrencyShortName
                ContentVw.addSubview(lbl_ClaimCurrencySymbol)
                
                let lbl_ClaimAmount : UILabel = UILabel(frame: CGRectMake(Viewframe.width / 100 * 75	 , 127, self.view.frame.size.width / 100 * 30,20))
                lbl_ClaimAmount.textAlignment = .Left
                lbl_ClaimAmount.textColor = UIColor(hexString: String("#2190ea"))
                lbl_ClaimAmount.font = UIFont(name:"HelveticaNeue-Bold", size: 18.0)
                lbl_ClaimAmount.text = ConvertValueWithDecimal( String(self.CurrentClaimListData[indexPath.row].TotalAmount),NoofDecimal: 2)
                ContentVw.addSubview(lbl_ClaimAmount)
                
                
               
                
            }
            else
            {
                
                
                // Self
                
                let ContentVw: UIView = UIView(frame: CGRectMake(Viewframe.width / 100 * 2, 10, Viewframe.width / 100 * 96, 115		 ))
                cell.addSubview(ContentVw)
                
                ContentVw.layer.cornerRadius=5
                ContentVw.backgroundColor=UIColor(hexString: String("#f5f5f5"))
                  
            
            let lbl_ClaimDescription : UILabel = UILabel(frame: CGRectMake(10 , 5, Viewframe.width / 100 * 85,20))
            lbl_ClaimDescription.textAlignment = .Left
            lbl_ClaimDescription.textColor = UIColor(hexString: String("#666666"))
            lbl_ClaimDescription.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
            lbl_ClaimDescription.text = self.CurrentClaimListData[indexPath.row].ProgramTitle
            ContentVw.addSubview(lbl_ClaimDescription)
            
        
            
            let lbl_ClaimPeriod : UILabel = UILabel(frame: CGRectMake(10 , 30 , Viewframe.width / 100 * 85,30))
            lbl_ClaimPeriod.textAlignment = .Left
            lbl_ClaimPeriod.textColor = UIColor(hexString: String("#666666"))
            lbl_ClaimPeriod.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
            lbl_ClaimPeriod.setFAText(prefixText: "", icon: FAType.FACalendar, postfixText: "  " + String(self.CurrentClaimListData[indexPath.row].StartDateString) + " - " + String(self.CurrentClaimListData[indexPath.row].EndDateString) , size: 14.0)
            ContentVw.addSubview(lbl_ClaimPeriod)
            
            
                let vwline3 : UIView = UIView(frame: CGRectMake(10 ,  80 , Viewframe.width / 100 * 88,1))
                vwline3.layer.borderColor = UIColor(hexString: "#cccccc")?.CGColor
                vwline3.layer.borderWidth = 1
                ContentVw.addSubview(vwline3)
            
            
            
            let lbl_ClaimSubmitted : UILabel = UILabel(frame: CGRectMake(10 ,  88 , ContentVw.frame.width / 100 * 63,18))
            lbl_ClaimSubmitted.textAlignment = .Left
            lbl_ClaimSubmitted.textColor = UIColor(hexString: String("#666666"))
            
            lbl_ClaimSubmitted.font = UIFont(name:"HelveticaNeue", size: 12.0)
            
            lbl_ClaimSubmitted.setFAText(prefixText: "Submitted on :  "  , icon: FAType.FACalendar, postfixText: "  " + String(self.CurrentClaimListData[indexPath.row].RequestedOnString)  , size: 12.0)
            ContentVw.addSubview(lbl_ClaimSubmitted)
            
            
            
            let lbl_ClaimCurrencySymbol : UILabel = UILabel(frame: CGRectMake( ContentVw.frame.width / 100 * 59 ,  89, ContentVw.frame.width / 100 * 13,18))
            lbl_ClaimCurrencySymbol.textAlignment = .Right
            lbl_ClaimCurrencySymbol.textColor = UIColor(hexString: String("#2190ea"))
            lbl_ClaimCurrencySymbol.font = UIFont(name:"HelveticaNeue", size: 14.0)
            lbl_ClaimCurrencySymbol.text = self.CurrentClaimListData[indexPath.row].CurrencyShortName
            ContentVw.addSubview(lbl_ClaimCurrencySymbol)
            
            let lbl_ClaimAmount : UILabel = UILabel(frame: CGRectMake(ContentVw.frame.width / 100 * 75 , 88, ContentVw.frame.width / 100 * 30,20))
            lbl_ClaimAmount.textAlignment = .Left
            lbl_ClaimAmount.textColor = UIColor(hexString: String("#2190ea"))
            lbl_ClaimAmount.font = UIFont(name:"HelveticaNeue-Bold", size: 18.0)
            lbl_ClaimAmount.text = ConvertValueWithDecimal ( String(self.CurrentClaimListData[indexPath.row].TotalAmount),NoofDecimal: 2)
                
            ContentVw.addSubview(lbl_ClaimAmount)
                
                
                
            }
            
            if self.customSC!.selectedSegmentIndex == 1
            {
                self.ModifyButtons(cell,row: indexPath.row )
            }else
            {
                self.CreateDynamicLabel(cell, row: indexPath.row )
            }
            
            
            
            
            let GestureClaimDetail = UITapGestureRecognizer(target: self, action: #selector(ClaimsRequestListB.ShowClaimDetail(_:)))
            cell.addGestureRecognizer(GestureClaimDetail)
        }
        else
        {
            let lbl_PositionName : UILabel = UILabel(frame: CGRectMake(70, 28, Viewframe.width / 100 * 45 ,22))
            lbl_PositionName.textAlignment = .Left
            lbl_PositionName.textColor = UIColor.whiteColor()
            lbl_PositionName.font = UIFont(name:"HelveticaNeue-bold", size: 14.0)
            lbl_PositionName.text = "Load More. . ."
            lbl_PositionName.textAlignment = .Center
            lbl_PositionName.backgroundColor = UIColor(hexString: "#2190ea")
            lbl_PositionName.layer.cornerRadius = 5
            cell.addSubview(lbl_PositionName)
            let GestureClaimDetail = UITapGestureRecognizer(target: self, action: "LoadMore:")
            cell.addGestureRecognizer(GestureClaimDetail)
            
            
        }
        
        
        
        return cell
    }
    
    
    
    
    func ShowClaimDetail(sender: UITapGestureRecognizer)
    {
        let tapLocation = sender.locationInView(self.ClaimListView)
        let indexPath = self.ClaimListView.indexPathForRowAtPoint(tapLocation)
        
       let  objClaimDetail1 = ClaimsDetails()
        objClaimDetail1.ClaimDetailsObj = self.CurrentClaimListData[(indexPath?.row)!]
        objClaimDetail1.Mode = self.customSC!.selectedSegmentIndex
      objClaimDetail1.OnCompletion = HandleDismissEvent
      objClaimDetail1.RowIndex = indexPath?.row
       self.presentViewController(objClaimDetail1, animated: true, completion: nil)
        
    }
    
    func LoadMore(sender: UITapGestureRecognizer)
    {
        
        GetClaimList()
        self.LoadMoreClicked = 1
        //ClaimListView.reloadData()
        
        
        
        
        
        
    }
    
    func scrollToLastRow() {
        let indexPath = NSIndexPath(forRow: self.CurrentClaimListData.count - 1, inSection: 0)
        self.ClaimListView.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Bottom, animated: true)
    }
    
    //# MARK: Custom Methods
    
    //Handles dismiss event of modal view
    func HandleDismissEvent(RowID:Int , ActionCode:String) -> Void
    {
        if ActionCode == "1" // After approve
        {
            
            self.CurrentCell  =  self.ClaimListView.cellForRowAtIndexPath( NSIndexPath(forRow: RowID, inSection: 0))!
            
            self.arrTeamClaimList[RowID].StatusText = "Approved"
            self.arrTeamClaimList[RowID].Access = 0
            
            self.CurrentClaimListData[RowID].StatusText = "Approved"
            self.CurrentClaimListData[RowID].Access = 0
            
            self.CreateDynamicLabel( self.CurrentCell,row: RowID,StatusText: "Approved")
            
            dispatch_async(dispatch_get_main_queue(),{
                for subview in   self.CurrentCell.subviews
                {
                    if subview.tag  == RowID + 1
                    {
                        
                        subview.removeFromSuperview()
                    }
                    
                }})
            
        }else if ActionCode == "2" // After Reject
        {
            self.CurrentCell  =  self.ClaimListView.cellForRowAtIndexPath( NSIndexPath(forRow: RowID, inSection: 0))!
            
            self.arrTeamClaimList[RowID].StatusText = "Rejected"
            self.arrTeamClaimList[RowID].Access = 0
            
            self.CurrentClaimListData[RowID].StatusText = "Rejected"
            self.CurrentClaimListData[RowID].Access = 0
            
            self.CreateDynamicLabel( self.CurrentCell,row: RowID,StatusText: "Rejected")
            dispatch_async(dispatch_get_main_queue(),{
                for subview in   self.CurrentCell.subviews
                {
                    if subview.tag   == RowID + 1
                    {
                        
                        subview.removeFromSuperview()
                    }
                    
                }
            })
            
        }
        
    }
    
    
    //Check privilleges of show Approve Reject Button in the Claim list
    func ModifyButtons (cell:UITableViewCell , row:Int)
    {    
            if self.CurrentClaimListData[row].Access == 1 {
                if self.CurrentClaimListData[row].Type == 2 {
                    self.CreateDynamicButton(cell, ButtonMode: 0, Xpos: 72, row: row)
                    self.CreateDynamicButton(cell, ButtonMode: 1, Xpos: 85, row: row)
                   // self.CreateDynamicLabel(cell, row: row )
                    
                } else if self.CurrentClaimListData[row].Type == 3 && self.CurrentClaimListData[row].IsLastStep == 1 {
                    self.CreateDynamicLabel(cell, row: row )
                }
                else if (self.CurrentClaimListData[row].IsLastStep != 1) {
                    self.CreateDynamicButton(cell, ButtonMode: 0, Xpos: 70, row: row)
                     // self.CreateDynamicLabel(cell, row: row )
                }
                
            } else if (self.CurrentClaimListData[row].Access == 0) {
                if ((self.CurrentClaimListData[row].RequestedFor == self.CurrentClaimListData[row].LastActionBy)
                    && ( String(self.CurrentClaimListData[row].RequestedFor) == String(self.UserID)))  {
                    
                    if self.customSC!.selectedSegmentIndex != 3 && self.CurrentClaimListData[row].StatusText != "Closed"  {
                        self.CreateDynamicLabel(cell, row: row )
                        
                    } else if self.customSC!.selectedSegmentIndex == 1 && self.CurrentClaimListData[row].StatusText == "Closed" {
                        self.CreateDynamicLabel(cell, row: row )
                    }
                    
                } else if String(self.UserID) == String(self.CurrentClaimListData[row].RequestedBy) {
                    
                    if self.customSC!.selectedSegmentIndex != 3 && self.CurrentClaimListData[row].StatusText == "Submitted" {
                        self.CreateDynamicLabel(cell, row: row )
                    } else {
                        if self.CurrentClaimListData[row].StatusText == "Closed" {
                            self.CreateDynamicLabel(cell, row: row )
                        } else if self.CurrentClaimListData[row].IsLastStep == 1 && self.customSC!.selectedSegmentIndex == 2 {
                            self.CreateDynamicLabel(cell, row: row )
                        }
                        
                    }
                    
                }
                else if (self.CurrentClaimListData[row].ProcessType == 1 && String(self.CurrentClaimListData[row].RequestedBy) == String(self.UserID) ) {
                    if (self.customSC!.selectedSegmentIndex != 3) {
                            self.CreateDynamicLabel(cell, row: row )
                       
                    }
                }else{
                    self.CreateDynamicLabel(cell, row: row )
                }
            }
        
        
    }
    
    
    // Bind the status label with corresponding color
    func CreateDynamicLabel(cell:UITableViewCell , row:Int , StatusText: String = "" )
    {
        
        let lbl_ClaimStatus : UILabel = UILabel()
        
           if self.customSC!.selectedSegmentIndex != 0 // Team
           {
                lbl_ClaimStatus.frame = CGRectMake(75, 102, self.view.frame.size.width / 100 * 63 ,18)
            lbl_ClaimStatus.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
           }
           else
           { // Self
            
            lbl_ClaimStatus.frame = CGRectMake(15, 70, self.view.frame.size.width / 100 * 80 ,18)
            lbl_ClaimStatus.font = UIFont(name:"HelveticaNeue-Bold", size: 14.0)
            
            }
        
        lbl_ClaimStatus.textAlignment = .Left
        if (self.CurrentClaimListData[row].Status == 1)
        {
            lbl_ClaimStatus.textColor = UIColor(hexString: "#ffbb5c")
        } else if (self.CurrentClaimListData[row].Status == 2) {
            
            lbl_ClaimStatus.textColor = UIColor(hexString: "#48c9b0")
        } else if (self.CurrentClaimListData[row].Status == 3) {
            lbl_ClaimStatus.textColor = UIColor(hexString: "#ef6565")
        } else {
            lbl_ClaimStatus.textColor = UIColor(hexString: "#ff8877")
        }
        
        lbl_ClaimStatus.font = UIFont(name:"HelveticaNeue", size: 14.0)
        if StatusText == ""
        {
            lbl_ClaimStatus.text = String(self.CurrentClaimListData[row].StatusText)
        }
        else if StatusText == "Approved"
        {
            
            lbl_ClaimStatus.text = StatusText
             lbl_ClaimStatus.textColor = UIColor(hexString: "#e67f22")
        }
        else if StatusText == "Rejected"
        {
            
            lbl_ClaimStatus.text = StatusText
             lbl_ClaimStatus.textColor = UIColor(hexString: "#ff8877")
        }
        
        cell.addSubview(lbl_ClaimStatus)
    }
    
    
    // Bind Approve/Reject Button in the list
    func CreateDynamicButton(cell:UITableViewCell , ButtonMode:Int , Xpos: CGFloat, row: Int)
    {
        
        if ButtonMode == 0
        {
            let btnApprove : UIButton = UIButton(frame: CGRectMake(self.view.frame.size.width / 100 * Xpos , 40, 30,30))
            btnApprove.backgroundColor = UIColor(hexString: String("#48C9B0"))
            btnApprove.setFAIcon(FAType.FACheck, iconSize: 20, forState:  .Normal)
            btnApprove.addTarget(self, action: "ApproveClaim:", forControlEvents: .TouchUpInside)
            btnApprove.tag = row + 1
            btnApprove.layer.borderWidth = 0
            btnApprove.layer.cornerRadius =  btnApprove.frame.height/2
            btnApprove.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            
            cell.addSubview(btnApprove)
        }
        else
        {
            let btnReject : UIButton = UIButton(frame: CGRectMake(self.view.frame.size.width / 100 * Xpos , 40, 30,30))
            btnReject.backgroundColor = UIColor(hexString: String("#F46868"))
            btnReject.setFAIcon(FAType.FAClose, iconSize: 20, forState:  .Normal)
            btnReject.addTarget(self, action: "RejectClaim:", forControlEvents: .TouchUpInside)
            btnReject.tag = row + 1
            btnReject.layer.borderWidth = 0
            btnReject.layer.cornerRadius =  btnReject.frame.height/2
            btnReject.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            
            cell.addSubview(btnReject)
        }
        
        
    }
    
    
    func ApproveClaim(Sender: UIButton!)
    {
        self.CurrentRowIndex = Sender.tag
        self.CurrentActionType = 2
        self.CurrentCell  =  self.ClaimListView.cellForRowAtIndexPath( NSIndexPath(forRow: Sender.tag - 1, inSection: 0))!
        self.ProcessClaimRequest("2", RowIndex: Sender.tag - 1)
        
    }
    
    func RejectClaim(Sender: UIButton!)
    {
        self.CurrentActionType = 3
        self.CurrentRowIndex = Sender.tag
        self.CurrentCell  =  self.ClaimListView.cellForRowAtIndexPath( NSIndexPath(forRow: Sender.tag - 1, inSection: 0))!
        self.ProcessClaimRequest("3", RowIndex: Sender.tag - 1)
    }
    
    func AddGesture()
    {
        swipeRight = UISwipeGestureRecognizer(target: self, action: "LoadTeamClaimList:")
        swipeRight.direction = UISwipeGestureRecognizerDirection.Right
        self.view.addGestureRecognizer(swipeRight)
        
        swipeLeft = UISwipeGestureRecognizer(target: self, action: "LoadSelfClaimList:")
        swipeRight.direction = UISwipeGestureRecognizerDirection.Left
        self.view.addGestureRecognizer(swipeLeft)
    }
    
    func RemoveGesture()
    {
        self.view.removeGestureRecognizer(swipeLeft)
        self.view.removeGestureRecognizer(swipeRight)
    }
    
    // Approve or Reject the Claim by callback
    func ProcessClaimRequest(ActionCode: String, RowIndex: Int )
    {
        self.RemoveGesture()
        
        objLoadingIndicaor = UIActivityIndicatorView(frame: CGRectMake(0,0, Viewframe.width, Viewframe.height))
        
        objLoadingIndicaor.activityIndicatorViewStyle = .WhiteLarge
        let objPosition =  self.view.center
        objLoadingIndicaor.center = objPosition
        objLoadingIndicaor.hidesWhenStopped = true
        objLoadingIndicaor.color = UIColor.grayColor()
        self.view.addSubview(objLoadingIndicaor)
        objLoadingIndicaor.startAnimating()
        
        if let UserInfo = defaults.stringForKey("UserInfo")
        {
            let ResultJSON = objCallBack.DeserializeJSONString(UserInfo)
            
            objCallBack.MethodName = "ProcessAttendanceNote"
            
            var objparam = Parameter(Name: "pClientID",value: String(ResultJSON!["ClientID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            
            objparam = Parameter(Name: "pUserID",value: String(ResultJSON!["UserID"]as! NSNumber))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pRequestID",value: String(self.CurrentClaimListData[RowIndex].RequestID))
            objCallBack.ParameterList.append(objparam)
            
            objparam = Parameter(Name: "pAction",value: ActionCode)
            objCallBack.ParameterList.append(objparam)
            
            objCallBack.post(ApproveOnSuccess, OnError: ApproveOnError)
            
        }
        
    }
    
    //Response on error callback
    func ApproveOnError(Response: NSError ){
        
    }
    
    func ApproveOnSuccess (ResultString :NSString? ) {
        
        if   self.CurrentActionType == 2
        {
            dispatch_async(dispatch_get_main_queue(),{
                self.arrTeamClaimList[self.CurrentRowIndex - 1].StatusText = "Approved"
                self.arrTeamClaimList[self.CurrentRowIndex - 1].Access = 0
                
                self.CurrentClaimListData[self.CurrentRowIndex - 1].StatusText = "Approved"
                self.CurrentClaimListData[self.CurrentRowIndex - 1].Access = 0
                
                self.CreateDynamicLabel( self.CurrentCell,row: self.CurrentRowIndex - 1,StatusText: "Approved")
                dispatch_async(dispatch_get_main_queue(),{
                    self.objLoadingIndicaor.stopAnimating()
                    self.AddGesture()
                    DoneHUD.showInView(self.view, message: "Done")
                    for subview in   self.CurrentCell.subviews
                    {
                        if subview.tag  == self.CurrentRowIndex
                        {
                            subview.removeFromSuperview()
                        }
                        
                    }
                })
            })
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(),{
                self.arrTeamClaimList[self.CurrentRowIndex - 1].StatusText = "Rejected"
                self.arrTeamClaimList[self.CurrentRowIndex - 1].Access = 0
                
                self.CurrentClaimListData[self.CurrentRowIndex - 1].StatusText = "Rejected"
                self.CurrentClaimListData[self.CurrentRowIndex - 1].Access = 0
                
                self.CreateDynamicLabel( self.CurrentCell,row: self.CurrentRowIndex - 1,StatusText: "Rejected")
                
                dispatch_async(dispatch_get_main_queue(),{
                    self.objLoadingIndicaor.stopAnimating()
                    self.AddGesture()
                    DoneHUD.showInView(self.view, message: "Done")
                    for subview in   self.CurrentCell.subviews
                    {               
                        if subview.tag  == self.CurrentRowIndex
                        {
                            subview.removeFromSuperview()
                        }
                        
                    }
                    
                })
            })
        }
        
    }
    
    
}



